-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 28, 2018 at 05:53 PM
-- Server version: 10.1.35-MariaDB-1
-- PHP Version: 7.2.9-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sinven`
--

-- --------------------------------------------------------

--
-- Table structure for table `almacenes`
--

CREATE TABLE `almacenes` (
  `idalmacen` int(11) NOT NULL,
  `desc_almacen` varchar(30) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id_area` int(11) NOT NULL,
  `desc_area` varchar(40) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `atc_medica`
--

CREATE TABLE `atc_medica` (
  `id_atc` int(11) NOT NULL,
  `cod_atc` varchar(2) NOT NULL,
  `desc_atc` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bancos`
--

CREATE TABLE `bancos` (
  `idbancos` int(11) NOT NULL,
  `sigla_banco` varchar(10) NOT NULL,
  `nombre_banco` varchar(45) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `boletas`
--

CREATE TABLE `boletas` (
  `idboleta` int(11) NOT NULL,
  `idticket` varchar(11) DEFAULT NULL,
  `idempleado` int(11) NOT NULL,
  `idclientes` int(11) NOT NULL,
  `fecventa` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_tipcomprobante` int(11) NOT NULL,
  `factura_electronica` enum('0','1') NOT NULL DEFAULT '0',
  `idordencompra` int(11) DEFAULT NULL,
  `pagado` enum('0','1') NOT NULL DEFAULT '1',
  `total` double(13,2) NOT NULL,
  `subtotal` double(13,2) NOT NULL,
  `importe_impuesto` double(13,2) NOT NULL DEFAULT '0.00',
  `mto_impuesto` double(13,2) DEFAULT NULL,
  `num_ticket` varchar(15) DEFAULT NULL,
  `cod_cdr` varchar(4) DEFAULT NULL,
  `message_cdr` int(11) DEFAULT NULL,
  `id_baja` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cargo`
--

CREATE TABLE `cargo` (
  `id_cargo` int(11) NOT NULL,
  `desc_cargo` varchar(80) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `idclientes` int(11) NOT NULL,
  `idtip_documento` int(11) NOT NULL,
  `num_documento` varchar(15) NOT NULL,
  `apellidos_nombres` varchar(60) NOT NULL,
  `direccion` text,
  `tlf_hab` varchar(15) DEFAULT NULL,
  `tlf_movil` varchar(15) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `compras`
--

CREATE TABLE `compras` (
  `idcompra` int(11) NOT NULL,
  `idord_compra` int(11) DEFAULT NULL,
  `numfactura` varchar(15) NOT NULL,
  `idproveedor` int(11) NOT NULL,
  `fecha_compra` date NOT NULL,
  `idcondicion_pago` int(11) NOT NULL,
  `dias_pago` int(11) NOT NULL DEFAULT '0',
  `fecha_pagar` date DEFAULT NULL,
  `porc_descuento` float(5,2) DEFAULT NULL,
  `subtotal` float(13,2) NOT NULL,
  `mto_impuesto` float(13,2) NOT NULL,
  `total` float(13,2) NOT NULL,
  `pagado` enum('0','1') NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comprobantes_tip_documentos`
--

CREATE TABLE `comprobantes_tip_documentos` (
  `id_tipcomprobante` int(11) NOT NULL,
  `idtip_documento` int(11) NOT NULL,
  `status` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `conceptos_adicional_comprobantes`
--

CREATE TABLE `conceptos_adicional_comprobantes` (
  `id_conceptadicion` int(11) NOT NULL,
  `cod_conceptadicion` varchar(4) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_conceptadicion` varchar(90) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `conceptos_tributarios`
--

CREATE TABLE `conceptos_tributarios` (
  `id_conceptribut` int(11) NOT NULL,
  `cod_conceptribut` varchar(4) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_conceptribut` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `concepto_ventas_perdidas`
--

CREATE TABLE `concepto_ventas_perdidas` (
  `id_cvperdida` int(11) NOT NULL,
  `desc_cvperdida` varchar(30) NOT NULL,
  `det_cvperdida` text NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `condicion_pago`
--

CREATE TABLE `condicion_pago` (
  `idcondicion_pago` int(11) NOT NULL,
  `desc_condicion` varchar(45) NOT NULL,
  `predeterminado` enum('0','1') NOT NULL DEFAULT '0',
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `configuracion`
--

CREATE TABLE `configuracion` (
  `porc_impuesto` decimal(5,2) NOT NULL,
  `nombre_impuesto` varchar(8) NOT NULL,
  `iniciar_factura` int(11) DEFAULT NULL,
  `iniciar_boleta` int(11) DEFAULT NULL,
  `nautorizacionimpresa` varchar(12) NOT NULL,
  `imprimir_venta` enum('0','1') NOT NULL DEFAULT '0',
  `name_print` text,
  `id_moneda` int(11) NOT NULL,
  `autoidproducto` enum('0','1') NOT NULL DEFAULT '1',
  `inicia_codigo` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cotizacion`
--

CREATE TABLE `cotizacion` (
  `id_cotizacion` int(11) NOT NULL,
  `fec_cotizacion` date NOT NULL,
  `idclientes` int(11) NOT NULL,
  `neto_cotizacion` double(13,2) NOT NULL,
  `impuesto_cotizacion` double(13,2) NOT NULL,
  `total_cotizacion` double(13,2) NOT NULL,
  `id_tipo_pago` int(11) NOT NULL,
  `id_vendedor` int(11) NOT NULL COMMENT 'id usuario vendedor'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cuentasbancarias`
--

CREATE TABLE `cuentasbancarias` (
  `idcuentasbancarias` int(11) NOT NULL,
  `idbancos` int(11) NOT NULL,
  `numctabancaria` varchar(20) NOT NULL,
  `idtipocta` int(11) NOT NULL,
  `idmoneda` int(11) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `departamentos_pais`
--

CREATE TABLE `departamentos_pais` (
  `id_depto` int(11) NOT NULL,
  `cod_depto` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_depto` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus_depto` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `detalles_orden_pedidos`
--

CREATE TABLE `detalles_orden_pedidos` (
  `iddetalles_opedido` int(11) NOT NULL,
  `idordenpedido` int(11) NOT NULL,
  `idproductos` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `mto_ult_compra` decimal(13,2) NOT NULL DEFAULT '0.00',
  `porc_impuesto` decimal(5,2) NOT NULL DEFAULT '0.00',
  `exento_impuesto` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detalle_compras`
--

CREATE TABLE `detalle_compras` (
  `idcompra` int(11) NOT NULL,
  `idproducto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `mto_impuesto` float(5,2) NOT NULL DEFAULT '0.00',
  `excento_impuesto` enum('0','1') NOT NULL DEFAULT '0',
  `precio_unidad` float(13,2) NOT NULL DEFAULT '0.00',
  `total_importe` float(13,2) NOT NULL DEFAULT '0.00',
  `precio_neto` float(13,2) NOT NULL DEFAULT '0.00',
  `tot_impuesto` float(13,2) NOT NULL DEFAULT '0.00',
  `lote` char(10) NOT NULL,
  `fecha_vencimiento` char(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detalle_cotizacion`
--

CREATE TABLE `detalle_cotizacion` (
  `id_detalle_ctz` int(11) NOT NULL,
  `id_cotizacion` int(11) NOT NULL,
  `idproductos` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` double(13,2) NOT NULL,
  `precio_neto` double(13,2) NOT NULL,
  `mto_impuesto` double(13,2) NOT NULL,
  `exonerado_impuesto` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `detalle_inventario`
--

CREATE TABLE `detalle_inventario` (
  `id_lotes` int(11) NOT NULL,
  `idproductos` int(11) NOT NULL,
  `serial_1` int(11) DEFAULT NULL,
  `num_serial1` varchar(15) DEFAULT NULL,
  `serial_2` int(11) DEFAULT NULL,
  `num_serial2` varchar(15) DEFAULT NULL,
  `serial_3` int(11) DEFAULT NULL,
  `num_serial3` varchar(15) DEFAULT NULL,
  `lote` varchar(20) NOT NULL,
  `format_date` varchar(10) DEFAULT NULL,
  `date_valid` date DEFAULT NULL,
  `fecha_vencimiento` varchar(10) DEFAULT NULL,
  `idalmacen` int(11) DEFAULT NULL,
  `idubicacion` int(11) DEFAULT NULL,
  `pasillo` varchar(5) DEFAULT NULL,
  `stand` varchar(5) DEFAULT NULL,
  `fila` varchar(5) DEFAULT NULL,
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `fec_ingreso` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `detalle_inventario`
--
DELIMITER $$
CREATE TRIGGER `addstockinicial` BEFORE INSERT ON `detalle_inventario` FOR EACH ROW BEGIN
	UPDATE inventario SET stock = stock + NEW.cantidad WHERE idproductos = NEW.idproductos;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `detalle_rol`
--

CREATE TABLE `detalle_rol` (
  `idrol_sistema` int(11) NOT NULL,
  `idwin_state` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `det_boleta`
--

CREATE TABLE `det_boleta` (
  `idboleta` int(11) NOT NULL,
  `idproductos` int(11) NOT NULL,
  `num_serie` varchar(15) DEFAULT NULL,
  `lote` varchar(10) DEFAULT NULL,
  `id_promocion` int(11) DEFAULT NULL,
  `cantidad` int(11) NOT NULL,
  `costo_unidad` double(13,2) NOT NULL,
  `importe_impuesto` float(13,2) NOT NULL DEFAULT '0.00',
  `exento_impuesto` enum('1','0') NOT NULL DEFAULT '0',
  `mto_impuesto` double(13,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `det_ordencompras`
--

CREATE TABLE `det_ordencompras` (
  `idord_compra` int(11) NOT NULL,
  `idproductos` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `mto_unidad` double(13,2) NOT NULL DEFAULT '0.00',
  `exento_impuesto` enum('0','1') NOT NULL DEFAULT '0',
  `mto_impuesto` double(13,2) NOT NULL DEFAULT '0.00',
  `mto_total` double(13,2) NOT NULL DEFAULT '0.00',
  `entregado` enum('0','1') NOT NULL DEFAULT '0',
  `fec_entregado` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `det_pagoboleta`
--

CREATE TABLE `det_pagoboleta` (
  `idboleta` int(11) NOT NULL,
  `id_tipo_pago` int(11) NOT NULL,
  `mto_pago` double(13,2) NOT NULL DEFAULT '0.00',
  `idbanco` int(11) DEFAULT NULL,
  `num_tarjeta` varchar(18) DEFAULT NULL,
  `num_referencia` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `distritos`
--

CREATE TABLE `distritos` (
  `id_distrito` int(11) NOT NULL,
  `id_provincia` int(11) NOT NULL,
  `cod_distrito` varchar(6) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_distrito` varchar(55) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus_distrito` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `documento_relacionado_guiaremision`
--

CREATE TABLE `documento_relacionado_guiaremision` (
  `id_docrelaguiaremi` int(11) NOT NULL,
  `cod_docrelaguiaremi` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_docrelaguiaremi` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `detallar_concepto` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `empleado`
--

CREATE TABLE `empleado` (
  `idempleado` int(11) NOT NULL,
  `idtip_documento` int(11) NOT NULL,
  `num_documento` varchar(15) NOT NULL,
  `apellido_paterno` varchar(45) NOT NULL,
  `apellido_materno` varchar(45) DEFAULT NULL,
  `nombres` varchar(45) NOT NULL,
  `sexo` enum('1','0') NOT NULL DEFAULT '0',
  `fecnacimiento` date NOT NULL,
  `edocivil` enum('C','S','D','V','O') NOT NULL DEFAULT 'S',
  `direccion_hab` text,
  `tlf_movil` varchar(15) DEFAULT NULL,
  `tlf_habitacion` varchar(15) DEFAULT NULL,
  `email` varchar(65) NOT NULL,
  `id_profesion` int(11) DEFAULT NULL,
  `id_cargo` int(11) DEFAULT NULL,
  `fec_ingreso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fec_inactivo` date DEFAULT NULL,
  `visible` enum('1','0') NOT NULL DEFAULT '1',
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `empleado_tipo_documentos`
--

CREATE TABLE `empleado_tipo_documentos` (
  `id_empleado` int(11) NOT NULL,
  `idtip_documento` int(11) NOT NULL,
  `idwin_state` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `empresa`
--

CREATE TABLE `empresa` (
  `idtip_documento` int(11) NOT NULL,
  `num_fiscal` varchar(12) NOT NULL,
  `nombre_empresa` varchar(60) NOT NULL,
  `nombre_comercial` varchar(75) DEFAULT NULL,
  `direccion` text NOT NULL,
  `direccion_local` text,
  `tlf_fiscal` varchar(15) DEFAULT NULL,
  `email` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `estados_pais`
--

CREATE TABLE `estados_pais` (
  `idestados_pais` int(11) NOT NULL,
  `idpais` int(11) NOT NULL,
  `desc_estados` varchar(45) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `estatus_itemcomprobante`
--

CREATE TABLE `estatus_itemcomprobante` (
  `id_statusitem` int(11) NOT NULL,
  `cod_statusitem` varchar(1) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_statusitem` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `facturas_electronicas`
--

CREATE TABLE `facturas_electronicas` (
  `idfacturaelectronica` int(11) NOT NULL,
  `anulado` enum('0','1') NOT NULL DEFAULT '0',
  `fec_anulado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forma_farmaceutica`
--

CREATE TABLE `forma_farmaceutica` (
  `id_formfarmaceutica` int(11) NOT NULL,
  `desc_formfarmaceutica` text COLLATE utf8_spanish2_ci NOT NULL,
  `status_formfarmace` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `generico_medicamentos`
--

CREATE TABLE `generico_medicamentos` (
  `id_genmedic` int(11) NOT NULL,
  `cod_genmedic` varchar(8) DEFAULT NULL,
  `desc_genmedic` varchar(90) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `grupos`
--

CREATE TABLE `grupos` (
  `idgrupo` int(11) NOT NULL,
  `cod_grupo` varchar(8) NOT NULL,
  `desc_grupo` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `grupo_medica`
--

CREATE TABLE `grupo_medica` (
  `id_gmedica` int(11) NOT NULL,
  `cod_gmedica` varchar(4) DEFAULT NULL,
  `desc_gmedica` varchar(90) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventario`
--

CREATE TABLE `inventario` (
  `idproductos` int(11) NOT NULL,
  `stock` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kardex`
--

CREATE TABLE `kardex` (
  `idkardex` int(11) NOT NULL,
  `idproductos` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fec_ingreso` date NOT NULL,
  `id_mov_ingreso` int(11) NOT NULL,
  `id_user_ing` int(11) DEFAULT NULL,
  `fec_egreso` date DEFAULT NULL,
  `id_mov_salida` int(11) DEFAULT NULL,
  `num_doc_salida` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `id_user_sal` int(11) DEFAULT NULL,
  `fec_ing_kardex` datetime DEFAULT CURRENT_TIMESTAMP,
  `fec_movimiento_kardex` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `laboratorios`
--

CREATE TABLE `laboratorios` (
  `idlaboratorio` int(11) NOT NULL,
  `desc_laboratorio` varchar(60) NOT NULL,
  `reg_sanitario` varchar(18) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `linea`
--

CREATE TABLE `linea` (
  `idlinea` int(11) NOT NULL,
  `desc_linea` varchar(60) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `medicamentos`
--

CREATE TABLE `medicamentos` (
  `id_pro` char(4) DEFAULT NULL,
  `cod_atc` char(1) DEFAULT NULL,
  `n2_pro` char(3) DEFAULT NULL,
  `cod_grupo` char(4) DEFAULT NULL,
  `n4_pro` char(6) DEFAULT NULL,
  `cod_gener` char(8) DEFAULT NULL,
  `cod_pro` char(8) DEFAULT NULL,
  `gen_pro` char(150) DEFAULT NULL,
  `nom_pro` char(200) DEFAULT NULL,
  `pres_pro` char(150) DEFAULT NULL,
  `id_laborat` char(3) DEFAULT NULL,
  `mono_pro` text,
  `logo_pro` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `modalidad_traslado`
--

CREATE TABLE `modalidad_traslado` (
  `id_modtraslado` int(11) NOT NULL,
  `cod_modtraslado` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_modtraslado` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `moneda`
--

CREATE TABLE `moneda` (
  `idmoneda` int(11) NOT NULL,
  `code_moneda` varchar(3) NOT NULL,
  `code_intern_moneda` varchar(3) NOT NULL,
  `simb_moneda` varchar(6) NOT NULL,
  `nom_moneda` varchar(18) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `monedas_adicionales`
--

CREATE TABLE `monedas_adicionales` (
  `id_moneda` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `motivos_traslados`
--

CREATE TABLE `motivos_traslados` (
  `id_motivtraslad` int(11) NOT NULL,
  `cod_id_motivtraslad` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_id_motivtraslad` varchar(80) COLLATE utf8_spanish2_ci NOT NULL,
  `detallar_concepto` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ordenes_compras`
--

CREATE TABLE `ordenes_compras` (
  `idord_compra` int(11) NOT NULL,
  `idproveedor` int(11) NOT NULL,
  `fecha_entrega` date NOT NULL,
  `id_tipo_pago` int(11) NOT NULL,
  `idempleado` int(11) NOT NULL,
  `fec_registro` datetime DEFAULT CURRENT_TIMESTAMP,
  `entregado` enum('0','1') NOT NULL DEFAULT '0',
  `observacion` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orden_pedidos`
--

CREATE TABLE `orden_pedidos` (
  `idordenpedido` int(11) NOT NULL,
  `idproveedor` int(11) NOT NULL,
  `fecha_orden` date NOT NULL,
  `observacion` text,
  `idusuario` int(11) NOT NULL,
  `fec_registro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_aprobada` date DEFAULT NULL,
  `fecha_anulada` date DEFAULT NULL,
  `status` enum('A','C','N','E') NOT NULL DEFAULT 'A' COMMENT 'A=>Aprobado\nC=>Cancelado\nN=>Anulado\nE=>Entregado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pagos_compras`
--

CREATE TABLE `pagos_compras` (
  `idcompra` int(11) NOT NULL,
  `fecha_pago` date NOT NULL,
  `mto_pago` double(13,2) NOT NULL,
  `id_tipo_pago` int(11) DEFAULT NULL,
  `ref_pago` varchar(15) DEFAULT NULL,
  `monto_pagado` double(13,2) DEFAULT NULL,
  `fecha_pagado` date DEFAULT NULL,
  `fecha_reprogramada` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paises`
--

CREATE TABLE `paises` (
  `idpais` int(11) NOT NULL,
  `desc_pais` varchar(70) NOT NULL,
  `cod_pais` varchar(2) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `id_moneda` int(11) NOT NULL,
  `predeterminado` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `privilegios_rol`
--

CREATE TABLE `privilegios_rol` (
  `idpriv_rol` int(11) NOT NULL,
  `idrol_sistema` int(11) NOT NULL,
  `idwin_sistemas` int(11) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `idproductos` int(11) NOT NULL,
  `cod_producto` varchar(10) DEFAULT NULL,
  `nom_producto` varchar(60) NOT NULL,
  `is_principio_activo` enum('0','1') NOT NULL DEFAULT '0',
  `principio_activo` text NOT NULL,
  `concent_princ_activo` text NOT NULL,
  `id_formfarmaceutica` int(11) DEFAULT NULL,
  `reg_sanitario` varchar(15) DEFAULT NULL,
  `idlaboratorio` int(11) NOT NULL,
  `idgrupo` int(11) DEFAULT NULL,
  `idsubgrupo` int(11) DEFAULT NULL,
  `idlinea` int(11) DEFAULT NULL,
  `descripcion` text,
  `mto_compra` decimal(13,2) NOT NULL,
  `fec_ult_compra` date DEFAULT NULL,
  `exento_impuesto` enum('0','1') NOT NULL DEFAULT '0',
  `mto_impuesto` decimal(5,2) NOT NULL DEFAULT '0.00',
  `metod_venta` enum('1','2') NOT NULL DEFAULT '1',
  `mto_minimo` decimal(13,2) DEFAULT NULL,
  `mto_sugerido` decimal(13,2) DEFAULT NULL,
  `porc_ganancia` decimal(5,2) DEFAULT NULL,
  `facturarsinstock` enum('0','1') NOT NULL DEFAULT '0',
  `avisostockminimo` enum('0','1') NOT NULL,
  `stockminimo` int(11) NOT NULL DEFAULT '0',
  `medida` varchar(10) DEFAULT NULL,
  `peso` varchar(10) DEFAULT NULL,
  `idumedida` int(11) NOT NULL DEFAULT '13',
  `categoria` enum('1','2','3','4') NOT NULL DEFAULT '1',
  `usa_serial` enum('1','0') NOT NULL DEFAULT '1',
  `serial_1` int(11) DEFAULT NULL,
  `serial_2` int(11) DEFAULT NULL,
  `serial_3` int(11) DEFAULT NULL,
  `etiqueta_label` enum('0','1') NOT NULL DEFAULT '0',
  `etiqueta_precio` enum('0','1') NOT NULL DEFAULT '0',
  `etiqueta_codigo` enum('0','1') NOT NULL DEFAULT '0',
  `imagen` blob,
  `ocultar_venta` enum('0','1') NOT NULL DEFAULT '0',
  `fec_ingreso` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profesion`
--

CREATE TABLE `profesion` (
  `id_profesion` int(11) NOT NULL,
  `desc_profesion` varchar(80) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `proveedores`
--

CREATE TABLE `proveedores` (
  `idproveedor` int(11) NOT NULL,
  `idtip_documento` int(11) NOT NULL,
  `num_documento` varchar(15) NOT NULL,
  `razon_social` varchar(60) NOT NULL,
  `direccion` text NOT NULL,
  `idestados_pais` int(11) NOT NULL,
  `id_provincia` int(11) NOT NULL,
  `id_distrito` int(11) NOT NULL,
  `repr_legal` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `tlf_hab` varchar(15) DEFAULT NULL,
  `tlf_movil` varchar(15) DEFAULT NULL,
  `fec_ingreso` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `provincias`
--

CREATE TABLE `provincias` (
  `id_provincia` int(11) NOT NULL,
  `id_depto` int(11) NOT NULL,
  `cod_provincia` varchar(4) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `desc_provincia` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus_provincia` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `regimen_percepcion`
--

CREATE TABLE `regimen_percepcion` (
  `id_regpercepcion` int(11) NOT NULL,
  `cod_regpercepcion` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_regpercepcion` varchar(90) COLLATE utf8_spanish2_ci NOT NULL,
  `mto_tasa` double(6,2) NOT NULL DEFAULT '0.00',
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `regimen_retencion`
--

CREATE TABLE `regimen_retencion` (
  `id_regretencion` int(11) NOT NULL,
  `cod_regretencion` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `monto_regretencion` double(6,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rol_sistema`
--

CREATE TABLE `rol_sistema` (
  `idrol_sistema` int(11) NOT NULL,
  `nom_rol` varchar(30) NOT NULL,
  `is_editable` enum('0','1') NOT NULL DEFAULT '0',
  `state_default` int(11) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `hidden` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subgrupos`
--

CREATE TABLE `subgrupos` (
  `idsubgrupo` int(11) NOT NULL,
  `idgrupo` int(11) NOT NULL,
  `desc_subgrupo` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tarifa_serv_publico`
--

CREATE TABLE `tarifa_serv_publico` (
  `id_tarifaservpub` int(11) NOT NULL,
  `cod_tarifaservpub` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `cod_tarifa` varchar(4) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_servicio` enum('LUZ','AGUA') COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipoctasbancarias`
--

CREATE TABLE `tipoctasbancarias` (
  `idtipocta` int(11) NOT NULL,
  `desctipocta` varchar(45) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tipo_afectacionimpuesto`
--

CREATE TABLE `tipo_afectacionimpuesto` (
  `id_afecimpuesto` int(11) NOT NULL,
  `cod_afecimpuesto` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_afecimpuesto` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `is_default` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipo_comprobante`
--

CREATE TABLE `tipo_comprobante` (
  `id_tipcomprobante` int(11) NOT NULL,
  `cod_tipcomprobante` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_comprobante` text COLLATE utf8_spanish2_ci NOT NULL,
  `status_comprobante` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipo_documentotributario`
--

CREATE TABLE `tipo_documentotributario` (
  `id_tipdoctrib` int(11) NOT NULL,
  `cod_tipdoctrib` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_tipdoctrib` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `detallar_concepto` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipo_factura`
--

CREATE TABLE `tipo_factura` (
  `id_tipofactura` int(11) NOT NULL,
  `cod_tipofactura` varchar(4) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_tipofactura` varchar(80) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipo_isc`
--

CREATE TABLE `tipo_isc` (
  `id_tipoisc` int(11) NOT NULL,
  `cod_tipoisc` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_tipoisc` varchar(80) COLLATE utf8_spanish2_ci NOT NULL,
  `status_tipoisc` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipo_notacredito`
--

CREATE TABLE `tipo_notacredito` (
  `id_tiponotacred` int(11) NOT NULL,
  `cod_tiponotacred` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_tiponotacred` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `detallar_concepto` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipo_notadebito`
--

CREATE TABLE `tipo_notadebito` (
  `id_tiponotadeb` int(11) NOT NULL,
  `cod_tiponotadeb` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_tiponotadeb` varchar(55) COLLATE utf8_spanish2_ci NOT NULL,
  `detallar_concepto` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipo_operacion`
--

CREATE TABLE `tipo_operacion` (
  `id_tipoperacion` int(11) NOT NULL,
  `cod_tipoperacion` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_tipoperacion` varchar(75) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipo_pago`
--

CREATE TABLE `tipo_pago` (
  `id_tipo_pago` int(11) NOT NULL,
  `desc_tipopago` varchar(25) NOT NULL,
  `especif_banco` enum('0','1') NOT NULL DEFAULT '0',
  `especif_serial` enum('0','1') NOT NULL DEFAULT '0',
  `especif_request` enum('0','1') NOT NULL DEFAULT '0',
  `isdefault` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tipo_precioventa`
--

CREATE TABLE `tipo_precioventa` (
  `id_tipprecioventa` int(11) NOT NULL,
  `cod_tipprecioventa` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_tipprecioventa` varchar(80) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipo_series`
--

CREATE TABLE `tipo_series` (
  `id_serie` int(11) NOT NULL,
  `abrev_serie` varchar(8) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_serie` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus_serie` enum('0','1') COLLATE utf8_spanish2_ci DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipo_tributo`
--

CREATE TABLE `tipo_tributo` (
  `id_tipotributo` int(11) NOT NULL,
  `cod_tipotributo` varchar(4) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_tipotributo` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `codename_tipotributo` varchar(3) COLLATE utf8_spanish2_ci NOT NULL,
  `codecat_tipotributo` varchar(1) COLLATE utf8_spanish2_ci NOT NULL,
  `status_tipotributo` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tipo_valorventa`
--

CREATE TABLE `tipo_valorventa` (
  `id_tipovalorventa` int(11) NOT NULL,
  `cod_tipovalorventa` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_tipovalorventa` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tip_documento`
--

CREATE TABLE `tip_documento` (
  `idtip_documento` int(11) NOT NULL,
  `abrev_tipdocumento` varchar(8) NOT NULL,
  `desc_documento` varchar(45) NOT NULL,
  `code_institucion` varchar(1) NOT NULL,
  `predeterminado` enum('1','0') NOT NULL DEFAULT '0',
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ubicacion`
--

CREATE TABLE `ubicacion` (
  `idubicacion` int(11) NOT NULL,
  `desc_ubicacion` varchar(20) NOT NULL,
  `disp_ventas` enum('0','1') NOT NULL DEFAULT '0',
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unidadmedidad`
--

CREATE TABLE `unidadmedidad` (
  `idumedida` int(11) NOT NULL,
  `abrev_umedida` varchar(8) NOT NULL,
  `desc_umedida` varchar(55) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuario` int(11) NOT NULL,
  `idempleado` int(11) NOT NULL,
  `idrol_sistema` int(11) NOT NULL,
  `fec_ingreso` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fec_inactivo` date DEFAULT NULL,
  `login` varchar(10) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `visible` enum('0','1') NOT NULL DEFAULT '1',
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ventas_perdidas`
--

CREATE TABLE `ventas_perdidas` (
  `id_vperdida` int(11) NOT NULL,
  `id_pro` int(11) NOT NULL,
  `id_cvperdida` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ventas_temporal`
--

CREATE TABLE `ventas_temporal` (
  `idproductos` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `idusuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `win_sistemas`
--

CREATE TABLE `win_sistemas` (
  `idwin_state` int(11) NOT NULL,
  `name_state` varchar(60) NOT NULL,
  `state_parent` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `almacenes`
--
ALTER TABLE `almacenes`
  ADD PRIMARY KEY (`idalmacen`),
  ADD UNIQUE KEY `desc_almacen_UNIQUE` (`desc_almacen`);

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id_area`),
  ADD UNIQUE KEY `desc_area` (`desc_area`);

--
-- Indexes for table `atc_medica`
--
ALTER TABLE `atc_medica`
  ADD PRIMARY KEY (`id_atc`),
  ADD UNIQUE KEY `cod_atc` (`cod_atc`),
  ADD UNIQUE KEY `desc_atc` (`desc_atc`);

--
-- Indexes for table `bancos`
--
ALTER TABLE `bancos`
  ADD PRIMARY KEY (`idbancos`),
  ADD UNIQUE KEY `idbancos_UNIQUE` (`idbancos`),
  ADD UNIQUE KEY `sigla_banco_UNIQUE` (`sigla_banco`);

--
-- Indexes for table `boletas`
--
ALTER TABLE `boletas`
  ADD PRIMARY KEY (`idboleta`),
  ADD UNIQUE KEY `idboletas_UNIQUE` (`idboleta`),
  ADD KEY `num_ticket` (`idticket`,`id_tipcomprobante`,`factura_electronica`);

--
-- Indexes for table `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id_cargo`),
  ADD UNIQUE KEY `id_cargo_UNIQUE` (`id_cargo`),
  ADD UNIQUE KEY `desc_cargo_UNIQUE` (`desc_cargo`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`idclientes`),
  ADD UNIQUE KEY `idclientes_UNIQUE` (`idclientes`),
  ADD UNIQUE KEY `doc_identidad` (`idtip_documento`,`num_documento`);

--
-- Indexes for table `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`idcompra`),
  ADD UNIQUE KEY `idcompra_UNIQUE` (`idcompra`),
  ADD UNIQUE KEY `fact_prov` (`numfactura`,`idproveedor`);

--
-- Indexes for table `conceptos_adicional_comprobantes`
--
ALTER TABLE `conceptos_adicional_comprobantes`
  ADD PRIMARY KEY (`id_conceptadicion`),
  ADD UNIQUE KEY `cod_conceptadicion` (`cod_conceptadicion`),
  ADD KEY `desc_conceptadicion` (`desc_conceptadicion`);

--
-- Indexes for table `conceptos_tributarios`
--
ALTER TABLE `conceptos_tributarios`
  ADD PRIMARY KEY (`id_conceptribut`),
  ADD UNIQUE KEY `cod_conceptribut` (`cod_conceptribut`),
  ADD KEY `desc_conceptribut` (`desc_conceptribut`);

--
-- Indexes for table `concepto_ventas_perdidas`
--
ALTER TABLE `concepto_ventas_perdidas`
  ADD PRIMARY KEY (`id_cvperdida`);

--
-- Indexes for table `condicion_pago`
--
ALTER TABLE `condicion_pago`
  ADD PRIMARY KEY (`idcondicion_pago`),
  ADD UNIQUE KEY `idcondicion_pago_UNIQUE` (`idcondicion_pago`),
  ADD UNIQUE KEY `desc_condicion_UNIQUE` (`desc_condicion`);

--
-- Indexes for table `cotizacion`
--
ALTER TABLE `cotizacion`
  ADD PRIMARY KEY (`id_cotizacion`);

--
-- Indexes for table `cuentasbancarias`
--
ALTER TABLE `cuentasbancarias`
  ADD PRIMARY KEY (`idcuentasbancarias`),
  ADD UNIQUE KEY `idcuentasbancarias_UNIQUE` (`idcuentasbancarias`),
  ADD UNIQUE KEY `numctabancaria_UNIQUE` (`numctabancaria`),
  ADD KEY `idbancos_fk_idx` (`idbancos`),
  ADD KEY `idtipocta_fk_idx` (`idtipocta`);

--
-- Indexes for table `departamentos_pais`
--
ALTER TABLE `departamentos_pais`
  ADD PRIMARY KEY (`id_depto`);

--
-- Indexes for table `detalles_orden_pedidos`
--
ALTER TABLE `detalles_orden_pedidos`
  ADD PRIMARY KEY (`iddetalles_opedido`),
  ADD UNIQUE KEY `iddetalles_opedido_UNIQUE` (`iddetalles_opedido`),
  ADD KEY `idordenpedido_idx` (`idordenpedido`),
  ADD KEY `idproducto_fk_idx` (`idproductos`);

--
-- Indexes for table `detalle_cotizacion`
--
ALTER TABLE `detalle_cotizacion`
  ADD PRIMARY KEY (`id_detalle_ctz`);

--
-- Indexes for table `detalle_inventario`
--
ALTER TABLE `detalle_inventario`
  ADD PRIMARY KEY (`id_lotes`),
  ADD KEY `iproductos` (`idproductos`) USING BTREE;

--
-- Indexes for table `detalle_rol`
--
ALTER TABLE `detalle_rol`
  ADD KEY `rol_state` (`idrol_sistema`,`idwin_state`);

--
-- Indexes for table `det_boleta`
--
ALTER TABLE `det_boleta`
  ADD UNIQUE KEY `bolet_articulo` (`idboleta`,`idproductos`);

--
-- Indexes for table `det_ordencompras`
--
ALTER TABLE `det_ordencompras`
  ADD KEY `idorden_compra_fk_idx` (`idord_compra`),
  ADD KEY `idproductos_idx` (`idproductos`);

--
-- Indexes for table `distritos`
--
ALTER TABLE `distritos`
  ADD PRIMARY KEY (`id_distrito`);

--
-- Indexes for table `documento_relacionado_guiaremision`
--
ALTER TABLE `documento_relacionado_guiaremision`
  ADD PRIMARY KEY (`id_docrelaguiaremi`),
  ADD UNIQUE KEY `cod_docrelaguiaremi` (`cod_docrelaguiaremi`),
  ADD KEY `desc_docrelaguiaremi` (`desc_docrelaguiaremi`);

--
-- Indexes for table `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`idempleado`),
  ADD UNIQUE KEY `idempleado_UNIQUE` (`idempleado`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD UNIQUE KEY `doc_identidad` (`idtip_documento`,`num_documento`),
  ADD KEY `id_profesion_fk_idx` (`id_profesion`),
  ADD KEY `id_cargo_fk_idx` (`id_cargo`);

--
-- Indexes for table `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`num_fiscal`),
  ADD UNIQUE KEY `num_fiscal_UNIQUE` (`num_fiscal`);

--
-- Indexes for table `estados_pais`
--
ALTER TABLE `estados_pais`
  ADD PRIMARY KEY (`idestados_pais`),
  ADD UNIQUE KEY `idestados_pais_UNIQUE` (`idestados_pais`),
  ADD UNIQUE KEY `desc_estados_UNIQUE` (`desc_estados`);

--
-- Indexes for table `estatus_itemcomprobante`
--
ALTER TABLE `estatus_itemcomprobante`
  ADD PRIMARY KEY (`id_statusitem`),
  ADD UNIQUE KEY `cod_statusitem` (`cod_statusitem`),
  ADD KEY `desc_statusitem` (`desc_statusitem`);

--
-- Indexes for table `facturas_electronicas`
--
ALTER TABLE `facturas_electronicas`
  ADD PRIMARY KEY (`idfacturaelectronica`);

--
-- Indexes for table `forma_farmaceutica`
--
ALTER TABLE `forma_farmaceutica`
  ADD PRIMARY KEY (`id_formfarmaceutica`);

--
-- Indexes for table `generico_medicamentos`
--
ALTER TABLE `generico_medicamentos`
  ADD PRIMARY KEY (`id_genmedic`),
  ADD UNIQUE KEY `generico_med` (`cod_genmedic`,`desc_genmedic`);

--
-- Indexes for table `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`idgrupo`),
  ADD UNIQUE KEY `idgrupo_UNIQUE` (`idgrupo`),
  ADD UNIQUE KEY `descgrupo_UNIQUE` (`desc_grupo`);

--
-- Indexes for table `grupo_medica`
--
ALTER TABLE `grupo_medica`
  ADD PRIMARY KEY (`id_gmedica`),
  ADD UNIQUE KEY `cod_descrip` (`cod_gmedica`,`desc_gmedica`);

--
-- Indexes for table `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`idproductos`),
  ADD UNIQUE KEY `idproductos_UNIQUE` (`idproductos`),
  ADD KEY `idproductos_fk_idx` (`idproductos`);

--
-- Indexes for table `kardex`
--
ALTER TABLE `kardex`
  ADD PRIMARY KEY (`idkardex`);

--
-- Indexes for table `laboratorios`
--
ALTER TABLE `laboratorios`
  ADD PRIMARY KEY (`idlaboratorio`),
  ADD UNIQUE KEY `idlaboratorio_UNIQUE` (`idlaboratorio`),
  ADD UNIQUE KEY `desc_laboratorio_UNIQUE` (`desc_laboratorio`);

--
-- Indexes for table `linea`
--
ALTER TABLE `linea`
  ADD PRIMARY KEY (`idlinea`),
  ADD UNIQUE KEY `idlinea_UNIQUE` (`idlinea`),
  ADD UNIQUE KEY `desc_linea_UNIQUE` (`desc_linea`);

--
-- Indexes for table `modalidad_traslado`
--
ALTER TABLE `modalidad_traslado`
  ADD PRIMARY KEY (`id_modtraslado`),
  ADD UNIQUE KEY `cod_modtraslado` (`cod_modtraslado`),
  ADD KEY `desc_modtraslado` (`desc_modtraslado`);

--
-- Indexes for table `moneda`
--
ALTER TABLE `moneda`
  ADD PRIMARY KEY (`idmoneda`),
  ADD UNIQUE KEY `idmoneda_UNIQUE` (`idmoneda`);

--
-- Indexes for table `motivos_traslados`
--
ALTER TABLE `motivos_traslados`
  ADD PRIMARY KEY (`id_motivtraslad`),
  ADD UNIQUE KEY `cod_id_motivtraslad` (`cod_id_motivtraslad`),
  ADD KEY `desc_id_motivtraslad` (`desc_id_motivtraslad`);

--
-- Indexes for table `ordenes_compras`
--
ALTER TABLE `ordenes_compras`
  ADD PRIMARY KEY (`idord_compra`),
  ADD UNIQUE KEY `idord_compra_UNIQUE` (`idord_compra`),
  ADD KEY `idproveedor_fk_idx` (`idproveedor`),
  ADD KEY `idtipo_pago_fk_idx` (`id_tipo_pago`),
  ADD KEY `idempleado_fk_idx` (`idempleado`);

--
-- Indexes for table `orden_pedidos`
--
ALTER TABLE `orden_pedidos`
  ADD PRIMARY KEY (`idordenpedido`),
  ADD UNIQUE KEY `idordenpedido_UNIQUE` (`idordenpedido`),
  ADD KEY `idproveedor_fk_idx` (`idproveedor`);

--
-- Indexes for table `pagos_compras`
--
ALTER TABLE `pagos_compras`
  ADD UNIQUE KEY `idcompr_fecha` (`idcompra`,`fecha_pago`);

--
-- Indexes for table `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`idpais`),
  ADD UNIQUE KEY `idpais_UNIQUE` (`idpais`);

--
-- Indexes for table `privilegios_rol`
--
ALTER TABLE `privilegios_rol`
  ADD PRIMARY KEY (`idpriv_rol`),
  ADD UNIQUE KEY `idpriv_rol_UNIQUE` (`idpriv_rol`),
  ADD UNIQUE KEY `rol_privilegio` (`idrol_sistema`,`idwin_sistemas`);

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idproductos`),
  ADD UNIQUE KEY `idproductos_UNIQUE` (`idproductos`),
  ADD UNIQUE KEY `nom_producto_UNIQUE` (`nom_producto`,`idlaboratorio`) USING BTREE,
  ADD KEY `idlaboratorio_fk_idx` (`idlaboratorio`),
  ADD KEY `idlinea_fk_idx` (`idlinea`),
  ADD KEY `idumedida_fk_idx` (`idumedida`);

--
-- Indexes for table `profesion`
--
ALTER TABLE `profesion`
  ADD PRIMARY KEY (`id_profesion`),
  ADD UNIQUE KEY `id_profesion_UNIQUE` (`id_profesion`),
  ADD UNIQUE KEY `desc_profesion_UNIQUE` (`desc_profesion`);

--
-- Indexes for table `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`idproveedor`),
  ADD UNIQUE KEY `idproveedor_UNIQUE` (`idproveedor`);

--
-- Indexes for table `provincias`
--
ALTER TABLE `provincias`
  ADD PRIMARY KEY (`id_provincia`);

--
-- Indexes for table `regimen_percepcion`
--
ALTER TABLE `regimen_percepcion`
  ADD PRIMARY KEY (`id_regpercepcion`),
  ADD UNIQUE KEY `cod_regpercepcion` (`cod_regpercepcion`);

--
-- Indexes for table `regimen_retencion`
--
ALTER TABLE `regimen_retencion`
  ADD PRIMARY KEY (`id_regretencion`),
  ADD UNIQUE KEY `cod_regretencion` (`cod_regretencion`);

--
-- Indexes for table `rol_sistema`
--
ALTER TABLE `rol_sistema`
  ADD PRIMARY KEY (`idrol_sistema`),
  ADD UNIQUE KEY `idrol_sistema_UNIQUE` (`idrol_sistema`),
  ADD UNIQUE KEY `nom_rol_UNIQUE` (`nom_rol`);

--
-- Indexes for table `subgrupos`
--
ALTER TABLE `subgrupos`
  ADD PRIMARY KEY (`idsubgrupo`),
  ADD UNIQUE KEY `idsubgrupo_UNIQUE` (`idsubgrupo`);

--
-- Indexes for table `tarifa_serv_publico`
--
ALTER TABLE `tarifa_serv_publico`
  ADD PRIMARY KEY (`id_tarifaservpub`),
  ADD UNIQUE KEY `cod_tarifaservpub` (`cod_tarifaservpub`);

--
-- Indexes for table `tipoctasbancarias`
--
ALTER TABLE `tipoctasbancarias`
  ADD PRIMARY KEY (`idtipocta`),
  ADD UNIQUE KEY `idtipoctasbancarias_UNIQUE` (`idtipocta`),
  ADD UNIQUE KEY `desctipocta_UNIQUE` (`desctipocta`);

--
-- Indexes for table `tipo_afectacionimpuesto`
--
ALTER TABLE `tipo_afectacionimpuesto`
  ADD PRIMARY KEY (`id_afecimpuesto`),
  ADD UNIQUE KEY `cod_afecimpuesto` (`cod_afecimpuesto`);

--
-- Indexes for table `tipo_comprobante`
--
ALTER TABLE `tipo_comprobante`
  ADD PRIMARY KEY (`id_tipcomprobante`),
  ADD UNIQUE KEY `cod_tipcomprobante` (`cod_tipcomprobante`);

--
-- Indexes for table `tipo_documentotributario`
--
ALTER TABLE `tipo_documentotributario`
  ADD PRIMARY KEY (`id_tipdoctrib`),
  ADD UNIQUE KEY `cod_tipdoctrib` (`cod_tipdoctrib`),
  ADD KEY `desc_tipdoctrib` (`desc_tipdoctrib`);

--
-- Indexes for table `tipo_factura`
--
ALTER TABLE `tipo_factura`
  ADD PRIMARY KEY (`id_tipofactura`),
  ADD UNIQUE KEY `cod_tipofactura` (`cod_tipofactura`);

--
-- Indexes for table `tipo_isc`
--
ALTER TABLE `tipo_isc`
  ADD PRIMARY KEY (`id_tipoisc`),
  ADD UNIQUE KEY `cod_tipoisc` (`cod_tipoisc`),
  ADD KEY `desc_tipoisc` (`desc_tipoisc`);

--
-- Indexes for table `tipo_notacredito`
--
ALTER TABLE `tipo_notacredito`
  ADD PRIMARY KEY (`id_tiponotacred`),
  ADD UNIQUE KEY `cod_tiponotacred` (`cod_tiponotacred`),
  ADD KEY `desc_tiponotacred` (`desc_tiponotacred`);

--
-- Indexes for table `tipo_notadebito`
--
ALTER TABLE `tipo_notadebito`
  ADD PRIMARY KEY (`id_tiponotadeb`),
  ADD UNIQUE KEY `cod_tiponotadeb` (`cod_tiponotadeb`),
  ADD KEY `desc_tiponotadeb` (`desc_tiponotadeb`);

--
-- Indexes for table `tipo_operacion`
--
ALTER TABLE `tipo_operacion`
  ADD PRIMARY KEY (`id_tipoperacion`),
  ADD UNIQUE KEY `cod_tipoperacion` (`cod_tipoperacion`),
  ADD KEY `desc_tipoperacion` (`desc_tipoperacion`);

--
-- Indexes for table `tipo_pago`
--
ALTER TABLE `tipo_pago`
  ADD PRIMARY KEY (`id_tipo_pago`),
  ADD UNIQUE KEY `id_tipo_pago_UNIQUE` (`id_tipo_pago`);

--
-- Indexes for table `tipo_precioventa`
--
ALTER TABLE `tipo_precioventa`
  ADD PRIMARY KEY (`id_tipprecioventa`),
  ADD UNIQUE KEY `cod_tipprecioventa` (`cod_tipprecioventa`),
  ADD KEY `desc_tipprecioventa` (`desc_tipprecioventa`);

--
-- Indexes for table `tipo_series`
--
ALTER TABLE `tipo_series`
  ADD PRIMARY KEY (`id_serie`);

--
-- Indexes for table `tipo_tributo`
--
ALTER TABLE `tipo_tributo`
  ADD PRIMARY KEY (`id_tipotributo`),
  ADD UNIQUE KEY `cod_tipotributo` (`cod_tipotributo`),
  ADD KEY `desc_tipotributo` (`desc_tipotributo`);

--
-- Indexes for table `tipo_valorventa`
--
ALTER TABLE `tipo_valorventa`
  ADD PRIMARY KEY (`id_tipovalorventa`),
  ADD UNIQUE KEY `cod_tipovalorventa` (`cod_tipovalorventa`),
  ADD KEY `desc_tipovalorventa` (`desc_tipovalorventa`);

--
-- Indexes for table `tip_documento`
--
ALTER TABLE `tip_documento`
  ADD PRIMARY KEY (`idtip_documento`),
  ADD UNIQUE KEY `idtip_documento_UNIQUE` (`idtip_documento`),
  ADD UNIQUE KEY `desc_documento_UNIQUE` (`desc_documento`);

--
-- Indexes for table `ubicacion`
--
ALTER TABLE `ubicacion`
  ADD PRIMARY KEY (`idubicacion`),
  ADD UNIQUE KEY `desc_ubicacion_UNIQUE` (`desc_ubicacion`);

--
-- Indexes for table `unidadmedidad`
--
ALTER TABLE `unidadmedidad`
  ADD PRIMARY KEY (`idumedida`),
  ADD UNIQUE KEY `idumedida_UNIQUE` (`idumedida`),
  ADD UNIQUE KEY `desc_umedida_UNIQUE` (`desc_umedida`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuario`),
  ADD UNIQUE KEY `idusuarios_UNIQUE` (`idusuario`),
  ADD UNIQUE KEY `idempleado_UNIQUE` (`idempleado`),
  ADD UNIQUE KEY `login_UNIQUE` (`login`),
  ADD KEY `id_rolsistema_fk_idx` (`idrol_sistema`);

--
-- Indexes for table `ventas_perdidas`
--
ALTER TABLE `ventas_perdidas`
  ADD PRIMARY KEY (`id_vperdida`);

--
-- Indexes for table `ventas_temporal`
--
ALTER TABLE `ventas_temporal`
  ADD UNIQUE KEY `venta_unika` (`idproductos`,`idusuario`),
  ADD KEY `idproductos` (`idproductos`);

--
-- Indexes for table `win_sistemas`
--
ALTER TABLE `win_sistemas`
  ADD PRIMARY KEY (`idwin_state`),
  ADD UNIQUE KEY `idwin_sistemas_UNIQUE` (`idwin_state`),
  ADD UNIQUE KEY `name_state_UNIQUE` (`name_state`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `almacenes`
--
ALTER TABLE `almacenes`
  MODIFY `idalmacen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id_area` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `atc_medica`
--
ALTER TABLE `atc_medica`
  MODIFY `id_atc` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bancos`
--
ALTER TABLE `bancos`
  MODIFY `idbancos` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `boletas`
--
ALTER TABLE `boletas`
  MODIFY `idboleta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id_cargo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `idclientes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `compras`
--
ALTER TABLE `compras`
  MODIFY `idcompra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `conceptos_adicional_comprobantes`
--
ALTER TABLE `conceptos_adicional_comprobantes`
  MODIFY `id_conceptadicion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `conceptos_tributarios`
--
ALTER TABLE `conceptos_tributarios`
  MODIFY `id_conceptribut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `concepto_ventas_perdidas`
--
ALTER TABLE `concepto_ventas_perdidas`
  MODIFY `id_cvperdida` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `condicion_pago`
--
ALTER TABLE `condicion_pago`
  MODIFY `idcondicion_pago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cotizacion`
--
ALTER TABLE `cotizacion`
  MODIFY `id_cotizacion` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cuentasbancarias`
--
ALTER TABLE `cuentasbancarias`
  MODIFY `idcuentasbancarias` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `departamentos_pais`
--
ALTER TABLE `departamentos_pais`
  MODIFY `id_depto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `detalles_orden_pedidos`
--
ALTER TABLE `detalles_orden_pedidos`
  MODIFY `iddetalles_opedido` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `detalle_cotizacion`
--
ALTER TABLE `detalle_cotizacion`
  MODIFY `id_detalle_ctz` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `detalle_inventario`
--
ALTER TABLE `detalle_inventario`
  MODIFY `id_lotes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `distritos`
--
ALTER TABLE `distritos`
  MODIFY `id_distrito` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1839;
--
-- AUTO_INCREMENT for table `documento_relacionado_guiaremision`
--
ALTER TABLE `documento_relacionado_guiaremision`
  MODIFY `id_docrelaguiaremi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `empleado`
--
ALTER TABLE `empleado`
  MODIFY `idempleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `estados_pais`
--
ALTER TABLE `estados_pais`
  MODIFY `idestados_pais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `estatus_itemcomprobante`
--
ALTER TABLE `estatus_itemcomprobante`
  MODIFY `id_statusitem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `facturas_electronicas`
--
ALTER TABLE `facturas_electronicas`
  MODIFY `idfacturaelectronica` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `forma_farmaceutica`
--
ALTER TABLE `forma_farmaceutica`
  MODIFY `id_formfarmaceutica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=233;
--
-- AUTO_INCREMENT for table `generico_medicamentos`
--
ALTER TABLE `generico_medicamentos`
  MODIFY `id_genmedic` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `grupos`
--
ALTER TABLE `grupos`
  MODIFY `idgrupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `grupo_medica`
--
ALTER TABLE `grupo_medica`
  MODIFY `id_gmedica` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kardex`
--
ALTER TABLE `kardex`
  MODIFY `idkardex` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `laboratorios`
--
ALTER TABLE `laboratorios`
  MODIFY `idlaboratorio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=290;
--
-- AUTO_INCREMENT for table `linea`
--
ALTER TABLE `linea`
  MODIFY `idlinea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `modalidad_traslado`
--
ALTER TABLE `modalidad_traslado`
  MODIFY `id_modtraslado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `motivos_traslados`
--
ALTER TABLE `motivos_traslados`
  MODIFY `id_motivtraslad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `ordenes_compras`
--
ALTER TABLE `ordenes_compras`
  MODIFY `idord_compra` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orden_pedidos`
--
ALTER TABLE `orden_pedidos`
  MODIFY `idordenpedido` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `privilegios_rol`
--
ALTER TABLE `privilegios_rol`
  MODIFY `idpriv_rol` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `productos`
--
ALTER TABLE `productos`
  MODIFY `idproductos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1961;
--
-- AUTO_INCREMENT for table `profesion`
--
ALTER TABLE `profesion`
  MODIFY `id_profesion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `idproveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `provincias`
--
ALTER TABLE `provincias`
  MODIFY `id_provincia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;
--
-- AUTO_INCREMENT for table `regimen_percepcion`
--
ALTER TABLE `regimen_percepcion`
  MODIFY `id_regpercepcion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `regimen_retencion`
--
ALTER TABLE `regimen_retencion`
  MODIFY `id_regretencion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `rol_sistema`
--
ALTER TABLE `rol_sistema`
  MODIFY `idrol_sistema` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `subgrupos`
--
ALTER TABLE `subgrupos`
  MODIFY `idsubgrupo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tarifa_serv_publico`
--
ALTER TABLE `tarifa_serv_publico`
  MODIFY `id_tarifaservpub` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tipoctasbancarias`
--
ALTER TABLE `tipoctasbancarias`
  MODIFY `idtipocta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tipo_afectacionimpuesto`
--
ALTER TABLE `tipo_afectacionimpuesto`
  MODIFY `id_afecimpuesto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tipo_comprobante`
--
ALTER TABLE `tipo_comprobante`
  MODIFY `id_tipcomprobante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tipo_documentotributario`
--
ALTER TABLE `tipo_documentotributario`
  MODIFY `id_tipdoctrib` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tipo_factura`
--
ALTER TABLE `tipo_factura`
  MODIFY `id_tipofactura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tipo_isc`
--
ALTER TABLE `tipo_isc`
  MODIFY `id_tipoisc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tipo_notacredito`
--
ALTER TABLE `tipo_notacredito`
  MODIFY `id_tiponotacred` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tipo_notadebito`
--
ALTER TABLE `tipo_notadebito`
  MODIFY `id_tiponotadeb` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tipo_operacion`
--
ALTER TABLE `tipo_operacion`
  MODIFY `id_tipoperacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tipo_pago`
--
ALTER TABLE `tipo_pago`
  MODIFY `id_tipo_pago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tipo_precioventa`
--
ALTER TABLE `tipo_precioventa`
  MODIFY `id_tipprecioventa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tipo_series`
--
ALTER TABLE `tipo_series`
  MODIFY `id_serie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tipo_tributo`
--
ALTER TABLE `tipo_tributo`
  MODIFY `id_tipotributo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tipo_valorventa`
--
ALTER TABLE `tipo_valorventa`
  MODIFY `id_tipovalorventa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tip_documento`
--
ALTER TABLE `tip_documento`
  MODIFY `idtip_documento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `ubicacion`
--
ALTER TABLE `ubicacion`
  MODIFY `idubicacion` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `unidadmedidad`
--
ALTER TABLE `unidadmedidad`
  MODIFY `idumedida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ventas_perdidas`
--
ALTER TABLE `ventas_perdidas`
  MODIFY `id_vperdida` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `win_sistemas`
--
ALTER TABLE `win_sistemas`
  MODIFY `idwin_state` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `detalles_orden_pedidos`
--
ALTER TABLE `detalles_orden_pedidos`
  ADD CONSTRAINT `idordenpedido_fk` FOREIGN KEY (`idordenpedido`) REFERENCES `orden_pedidos` (`idordenpedido`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `idproducto_fk` FOREIGN KEY (`idproductos`) REFERENCES `productos` (`idproductos`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `det_boleta`
--
ALTER TABLE `det_boleta`
  ADD CONSTRAINT `idboletas_fk` FOREIGN KEY (`idboleta`) REFERENCES `boletas` (`idboleta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `det_ordencompras`
--
ALTER TABLE `det_ordencompras`
  ADD CONSTRAINT `idorden_compra_fk` FOREIGN KEY (`idord_compra`) REFERENCES `ordenes_compras` (`idord_compra`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idproductos` FOREIGN KEY (`idproductos`) REFERENCES `productos` (`idproductos`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `empleado`
--
ALTER TABLE `empleado`
  ADD CONSTRAINT `id_cargo_fk` FOREIGN KEY (`id_cargo`) REFERENCES `cargo` (`id_cargo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `id_profesion_fk` FOREIGN KEY (`id_profesion`) REFERENCES `profesion` (`id_profesion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `inventario`
--
ALTER TABLE `inventario`
  ADD CONSTRAINT `idproductos_fk` FOREIGN KEY (`idproductos`) REFERENCES `productos` (`idproductos`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ordenes_compras`
--
ALTER TABLE `ordenes_compras`
  ADD CONSTRAINT `idempleado_fk` FOREIGN KEY (`idempleado`) REFERENCES `empleado` (`idempleado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idproveedor_fk` FOREIGN KEY (`idproveedor`) REFERENCES `proveedores` (`idproveedor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idtipo_pago_fk` FOREIGN KEY (`id_tipo_pago`) REFERENCES `tipo_pago` (`id_tipo_pago`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pagos_compras`
--
ALTER TABLE `pagos_compras`
  ADD CONSTRAINT `idcompra_fk` FOREIGN KEY (`idcompra`) REFERENCES `compras` (`idcompra`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `idlaboratorio_fk` FOREIGN KEY (`idlaboratorio`) REFERENCES `laboratorios` (`idlaboratorio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idlinea_fk` FOREIGN KEY (`idlinea`) REFERENCES `linea` (`idlinea`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idumedida_fk` FOREIGN KEY (`idumedida`) REFERENCES `unidadmedidad` (`idumedida`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `id_empleado_fk` FOREIGN KEY (`idempleado`) REFERENCES `empleado` (`idempleado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idrol_sistema_fk` FOREIGN KEY (`idrol_sistema`) REFERENCES `rol_sistema` (`idrol_sistema`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
