(function() {
    'use strict';

    angular.module('BlurAdmin.getservice', [])
        .factory('getservice', getservice);
//ya va dejame ver porque creo que yo lo tengo de dos maneras una que se uso en el modulo de rrh y otra en comercial en clientes 
    /** @ngInject */
    function getservice($http, $httpParamSerializerJQLike) {
			var datas={};
			datas.getDespartamentos=function(){
				return $http({///osea si logro entrar y me busca la provincias pero no pinta ese es el tema ya va 
					url:'http://rbviin.localhost/api/estados.php', method:"POST", data:$httpParamSerializerJQLike({oper:'listar'}),
					headers:{'Content-Type':'application/x-www-form-urlencoded'}
				});
			};
            datas.getProvincias=function(cod_depart){
				return $http({
					url:'http://rbviin.localhost/api/estados.php', method:"POST", data:$httpParamSerializerJQLike({oper:'provincias', co_depart:cod_depart}),
					headers:{'Content-Type':'application/x-www-form-urlencoded'} 
				})
			};
			datas.getDistritos=function(id_distrito){
				console.log('entro get service');
				console.log(id_distrito);
				return $http({
					url:'http://rbviin.localhost/api/estados.php', method:"POST", data:$httpParamSerializerJQLike({oper:'distritos', cod_provin:id_distrito}),
					headers:{'Content-Type':'application/x-www-form-urlencoded'}
				});
			};
			datas.getPrimas=function(cod_regimen){
				return $http({
					url:'http://rbviin.localhost/api/estados.php', method:"POST", data:$httpParamSerializerJQLike({oper:'cargar_primas', co_regimen:cod_regimen}),
					headers:{'Content-Type':'application/x-www-form-urlencoded'}
				});
			};
			datas.getCargos=function(cod_area){
				return $http({
					url:'http://rbviin.localhost/api/estados.php', method:"POST", data:$httpParamSerializerJQLike({oper:'cargar_cargos_fil', id_area:cod_area}),
					headers:{'Content-Type':'application/x-www-form-urlencoded'}
				});
			};
			return datas;
    }
})();
