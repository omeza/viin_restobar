/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.home')
      .controller('DashboardMapCtrl', DashboardMapCtrl);

  /** @ngInject */
  function DashboardMapCtrl(baConfig, layoutPaths) {
    var layoutColors = baConfig.colors;
    var map = AmCharts.makeChart('amChartMap', {
      type: 'map',
      theme: 'blur',
      zoomControl: { zoomControlEnabled: false, panControlEnabled: false },

      dataProvider: {
        map: 'worldLow',
        zoomLevel: 3.5,
        zoomLongitude: 10,
        zoomLatitude: 52,
        areas: [
          { title: 'Lima', id: 'AT', color: layoutColors.primary, customData: '1 244', groupId: '1'},
          { title: 'Santa Anita', id: 'IE', color: layoutColors.primary, customData: '1 342', groupId: '1'},
          { title: 'Cuscu', id: 'DK', color: layoutColors.primary, customData: '1 973', groupId: '1'},
          { title: 'Mayorazgo', id: 'FI', color: layoutColors.primary, customData: '1 573', groupId: '1'},
          { title: 'Ate', id: 'SE', color: layoutColors.primary, customData: '1 084', groupId: '1'},
          { title: 'Surco', id: 'GB', color: layoutColors.primary, customData: '1 452', groupId: '1'},
          { title: 'Surquillo', id: 'IT', color: layoutColors.primary, customData: '1 321', groupId: '1'},
          { title: 'Miraflores', id: 'FR', color: layoutColors.primary, customData: '1 112', groupId: '1'},
          { title: 'San Borja', id: 'ES', color: layoutColors.primary, customData: '1 865', groupId: '1'},
          { title: 'San Isidro', id: 'GR', color: layoutColors.primary, customData: '1 453', groupId: '1'},
          { title: 'Chorrillos', id: 'DE', color: layoutColors.primary, customData: '1 957', groupId: '1'},
          { title: 'La Victoria', id: 'BE', color: layoutColors.primary, customData: '1 011', groupId: '1'},
          { title: 'Arequipa', id: 'LU', color: layoutColors.primary, customData: '1 011', groupId: '1'},
          { title: 'San Miguel', id: 'NL', color: layoutColors.primary, customData: '1 213', groupId: '1'},
          { title: 'EL Callao', id: 'PT', color: layoutColors.primary, customData: '1 291', groupId: '1'},
          { title: 'Ceres', id: 'LT', color: layoutColors.successLight, customData: '567', groupId: '2'},
          { title: 'PLaza Norte', id: 'LV', color: layoutColors.successLight, customData: '589', groupId: '2'},
          { title: 'Trujillo', id: 'CZ', color: layoutColors.successLight, customData: '785', groupId: '2'},
          { title: 'Ica', id: 'SK', color: layoutColors.successLight, customData: '965', groupId: '2'},
          { title: 'Vitarte', id: 'EE', color: layoutColors.successLight, customData: '685', groupId: '2'}
       
        ]
      },

      areasSettings: {
        rollOverOutlineColor: layoutColors.border,
        rollOverColor: layoutColors.primaryDark,
        alpha: 0.8,
        unlistedAreasAlpha: 0.2,
        unlistedAreasColor: layoutColors.defaultText,
        balloonText: '[[title]]: [[customData]] Clientes'
      },


      legend: {
        width: '100%',
        marginRight: 27,
        marginLeft: 27,
        equalWidths: false,
        backgroundAlpha: 0.3,
        backgroundColor: layoutColors.border,
        borderColor: layoutColors.border,
        borderAlpha: 1,
        top: 362,
        left: 0,
        horizontalGap: 10,
        data: [
          {
            title: 'over 1 000 users',
            color: layoutColors.primary
          },
          {
            title: '500 - 1 000 users',
            color: layoutColors.successLight
          },
          {
            title: '100 - 500 users',
            color: layoutColors.success
          },
          {
            title: '0 - 100 users',
            color: layoutColors.danger
          }
        ]
      },
      export: {
        enabled: true
      },
      creditsPosition: 'bottom-right',
      pathToImages: layoutPaths.images.amChart
    });
  }
})();