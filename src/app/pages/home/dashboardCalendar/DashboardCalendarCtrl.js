/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.home')
      .controller('DashboardCalendarCtrl', DashboardCalendarCtrl);

  /** @ngInject */
  function DashboardCalendarCtrl(baConfig) {
    var dashboardColors = baConfig.colors.dashboard;
    var $element = $('#calendar').fullCalendar({
      //height: 335,
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      defaultDate: '2018-06-14',
      selectable: true,
      selectHelper: true,
      select: function (start, end) {
        var title = prompt('Evento:');
        var eventData;
        if (title) {
          eventData = {
            title: title,
            start: start,
            end: end
          };
          $element.fullCalendar('renderEvent', eventData, true); // stick? = true
        }
        $element.fullCalendar('unselect');
      },
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: [
        {
          title: 'EVento todo el dia',
          start: '2018-06-12',
          color: dashboardColors.silverTree
        },
        {
          title: 'Evento Largo',
          start: '2018-06-07',
          end: '2018-06-15',
          color: dashboardColors.blueStone
        },
        {
          title: 'Feriados',
          start: '2018-05-14T20:00:00',
          color: dashboardColors.surfieGreen
        },
        {
          title: 'Fiesta Cumpleaños',
          start: '2018-06-021T07:00:00',
          color: dashboardColors.gossipDark
        }
      ]
    });
  }
})();