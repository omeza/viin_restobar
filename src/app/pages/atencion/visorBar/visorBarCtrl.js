(function(){
    'use strict';
    angular.module('BlurAdmin.pages.atencion.visorBar', [])    
    .controller('visorBarlistCtrl', [ '$scope', 'serviceAPI', 'toastr', '$stateParams', 'baSidebarService', '$timeout', '$log', function($scope, serviceAPI, toastr, $stateParams, baSidebarService, $timeout, $log){
        var vm=this;
        $scope.default={idusuario:0, idempleado:0, idtipdoc:'', num_doc: '', apenom:'', usuario:'', password:'', rep_passw:'', idrol_sistema:'', permission:[]};
        $scope.data={idusuario:0, idempleado:0, idtipdoc:'', num_doc:'', apenom:'', usuario:'', password:'', rep_passw:'', idrol_sistema:'', permission:[]};
        
        $scope.jpedidos=[];
        $scope.jmesas=[];
        $scope.valores=[];
        $scope.mesa='';


        $scope.consultar = function () {

           // setInterval(restarHoras, 1000);

            serviceAPI.getMesasAtendidasVisorBar().success(function (r) {     
                if (r.success && r.totalItemCount > 0) {
                    for(var i = 0; i < r.rows.length; i++){    
                        serviceAPI.consultarPedidoDetalleVisorBar({id_pedido_cabecera: r.rows[i].id_pedido_cabecera,no_mesa:r.rows[i].no_mesas}).success(function (b) {                  
                            console.log(b);
                            if (b.totalItemCount > 0 && b.success) {
                                console.log(b);
                                $scope.valores.push({data:b.no_mesa, pedidos:b.rows});                               
                            }
                        });
                    }
                }
            });
            console.log($scope.valores);      
        };

        $scope.guardar=function(eForm){
            serviceAPI.guardarUsuario($scope.data).success(function(r){
                var typeNotification='error';
                if(r.success){
                    typeNotification=(r.affected_rows!=0)?'success':'warning';
                }
                toastr[typeNotification](r.messages, 'Registro de Usuario');
                angular.extend($scope.data, $scope.default);
                eForm.$setPristine();
                eForm.$setUntouched();
            });
        };

        $scope.atendido=function(value){            
            serviceAPI.actualizarAtendido(value).success(function(r){
                var typeNotification='error';
                if(r.success){
                    typeNotification=(r.affected_rows!=0)?'success':'warning';
                    $scope.valores=[];
                    $scope.consultar();
                }
                toastr[typeNotification](r.messages, 'Atendido');
                angular.extend($scope.data, $scope.default);
            });
        };

      
    }]);
})();