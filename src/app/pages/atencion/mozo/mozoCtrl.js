(function () {
    'use strict';
    angular.module('BlurAdmin.pages.atencion.mozo', [])
        .controller('mozolistCtrl', ['$scope', 'serviceAPI', '$state', function ($scope, serviceAPI, $state) {
            $scope.displayed = [];
            $scope.callServer = function (tableState) {
                var params = angular.extend({}, tableState.pagination, tableState.search);
                serviceAPI.getMesasAtendidas(params).success(function (r) {
                    console.log(r);
                    if (r.success && r.totalItemCount > 0) {
                        $scope.displayed = r.rows;
                        tableState.pagination.numberOfPages = r.numberOfPages;
                    }
                });
            };
            $scope.agregar = function () {
                $state.go('dashboard.atencion.mozo-details');
            };
            $scope.editar = function (id) {
                $state.go('dashboard.atencion.mozo-details', { idPedidoCabecera: id_pedido_cabecera });
            };
            $scope.changeStatus = function (id) {

            };
        }])
        .controller('mozoCtrl', ['$scope', 'serviceAPI', 'toastr', '$stateParams', 'baSidebarService', '$timeout', '$log', '$filter', function ($scope, serviceAPI, toastr, $stateParams, baSidebarService, $timeout, $log, $filter) {
            var vm = this;
            $scope.default = {id_pedido_cabecera:0, id_mesas: '', id_categoria: '', id_producto: 0 };
            $scope.data = {id_pedido_cabecera:0, id_mesas: '', id_categoria: '', id_producto: 0 };
            $scope.jmesas = [];
            $scope.jcategoria = [];
            $scope.gmesas = [];
            $scope.jproductos = [];
            $scope.jpedidos = [];
            $scope.i = 1;

            $scope.getPedido = function (value) {
                var itemActual;
                for (var i = 0; i < $scope.jpedidos.length; i++) {
                    if ($scope.jpedidos[i].id_producto == value.id_producto) {
                        itemActual = $scope.jpedidos[i];
                    }
                }
                    if (!itemActual) {
                        $scope.jpedidos.push({
                            id_pedido: $scope.i++,
                            id_producto: value.id_producto,
                            no_producto: value.no_producto,
                            nu_cantidad: 0,
                            nu_precio:value.nu_precio,
                            nu_total:0,
                            stock:value.stock,
                            no_observacion: '',
                            id_producto_menu: value.id_producto_menu
                        });
                    } else {
                        if(!itemActual.stock){
                            itemActual.nu_cantidad++;
                            itemActual.nu_total=itemActual.nu_cantidad *itemActual.nu_precio ;
                        }else{
                            if(itemActual.nu_cantidad < itemActual.stock){
                                itemActual.nu_cantidad++;
                                itemActual.nu_total=itemActual.nu_cantidad *itemActual.nu_precio ;
                            }
                        }
                        
                    }                    
            };
            $scope.getMesas = function (value) {
                var itemActual;
                for (var i = 0; i < $scope.gmesas.length; i++) {
                    if ($scope.gmesas[i].id_mesa == value.id_mesas.id) {
                        itemActual = $scope.gmesas[i];
                    }
                }
                if (!itemActual) {
                    $scope.gmesas.push({
                        id_mesa: value.id_mesas.id,
                        no_mesas: value.id_mesas.nu_mesa,
                    });
                } else {
                    itemActual.Cantidad++;
                }
            };

            $scope.suma = function (value) {
                for (var i = 0; i <= $scope.jpedidos.length; i++) {                   
                    if ($scope.jpedidos[i].id_pedido == value.id_pedido) {
                        if(!$scope.jpedidos[i].stock){
                            $scope.jpedidos[i].nu_cantidad = $scope.jpedidos[i].nu_cantidad + 1;
                            $scope.jpedidos[i].nu_total = parseFloat($scope.jpedidos[i].nu_total) + parseFloat(value.nu_precio);
                        }else{
                            if($scope.jpedidos[i].nu_cantidad < $scope.jpedidos[i].stock){
                                $scope.jpedidos[i].nu_cantidad = $scope.jpedidos[i].nu_cantidad + 1;
                                $scope.jpedidos[i].nu_total = parseFloat($scope.jpedidos[i].nu_total) + parseFloat(value.nu_precio);
                            }  
                        }
                                             
                    }
                }
            }

            $scope.resta = function (value) {
                for (var i = 0; i <= $scope.jpedidos.length; i++) {
                    if ($scope.jpedidos[i].id_pedido == value.id_pedido) {
                        $scope.jpedidos[i].nu_cantidad = $scope.jpedidos[i].nu_cantidad - 1;
                        $scope.jpedidos[i].nu_total = parseFloat($scope.jpedidos[i].nu_total) - parseFloat(value.nu_precio);
                    }
                }
            }

            $scope.getProductos = function (value) {
                $scope.jproductos = [];
                serviceAPI.listarProductoxCategoria({ id: value.id_categoria.id }).then(function (r) {
                    if (r.data.totalItemCount > 0 && r.data.success) {
                        angular.extend($scope.jproductos, r.data.rows);
                        $scope.data.id_producto = _.findWhere($scope.jproductos);
                    }
                });
            }

            $scope.quitar_producto = function (id) {
                $scope.gmesas = $scope.gmesas.filter(function (item) {
                    return item.id_mesa != id;
                });
            };
            
            $scope.quitar_pedido = function (id) {
                $scope.jpedidos = $scope.jpedidos.filter(function (item) {
                    return item.id_pedido != id;
                });
            };
            
            $scope.listarMesasMozo = function(){
                serviceAPI.listarMesasSucursalMozo().then(function (r) {
                    if (r.data.totalItemCount > 0 && r.data.success) {                    
                        angular.extend($scope.jmesas, r.data.rows);
                        $scope.data.id_mesas = _.findWhere($scope.jmesas);
                    }
                });
            }
            
            serviceAPI.listarCategoriaProducto().then(function (r) {
                if (r.data.totalItemCount > 0 && r.data.success) {
                    angular.extend($scope.jcategoria, r.data.rows);
                    $scope.data.id_categoria = _.findWhere($scope.jcategoria);
                }
                $scope.listarMesasMozo();
            });

            $scope.guardar = function (eForm) {
                var params = {idPedido:$stateParams.idPedido, mesa: $scope.gmesas, detalle: $scope.jpedidos};               
                serviceAPI.guardarPedido(params).success(function (r) {                   
                    var typeNotification = 'error';
                    if (r.success) {
                        typeNotification = (r.affected_rows != 0) ? 'success' : 'warning';
                    }
                    toastr[typeNotification](r.messages, 'Realizar Pedido');
                    angular.extend($scope.data, $scope.default);
                    $scope.gmesas = [];
                    $scope.jpedidos = [];
                    $scope.jproductos = [];
                  
                    eForm.$setPristine();
                    eForm.$setUntouched();                    
                });
                $scope.jmesas = [];
                $scope.listarMesasMozo();
            };

            $scope.miFormato = function (valor) {
                return isNaN(valor) ? valor : parseFloat(valor).toFixed(2);
            }

            $scope.consultar = function () {
                if ($stateParams.idPedido != '') {
                    serviceAPI.consultarPedido({ id_pedido_cabecera: $stateParams.idPedido }).success(function (r) {
                        var typeNotification = (!r.success) ? 'error' : 'warning';
                        
                        if (r.totalItemCount > 0 && r.success) {
                            typeNotification = 'success';
                            $scope.gmesas = r.rows;  
                            serviceAPI.consultarPedidoDetalle({ id_pedido_cabecera: $stateParams.idPedido }).success(function (p) {
                                var typeNotification = (!p.success) ? 'error' : 'warning';
                                if (r.totalItemCount > 0 && p.success) {
                                    typeNotification = 'success';
                                    console.log(p.rows);
                                    $scope.jpedidos = p.rows;     
                                }
                                toastr[typeNotification](p.messages, 'Información de Pedido');
                            });
                        }
                        toastr[typeNotification](r.messages, 'Información de Pedido');
                    });                    
                }
            };
        }]);
})();