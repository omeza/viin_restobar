(function () {
  'use strict';
  angular.module('BlurAdmin.pages.atencion', [
    'BlurAdmin.pages.atencion.mozo',
    'BlurAdmin.pages.atencion.visor',
    'BlurAdmin.pages.atencion.visorBar'
  ]).config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('dashboard.atencion', {
        url: '/atencion',
        template: '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
        abstract: true,
        title: 'Atencion',
        sidebarMeta: {
          icon: 'ion-thumbsup',
          order: 500
        }
      }).state('dashboard.atencion.mozo', {
        url: '/mozo',
        templateUrl: 'app/pages/atencion/mozo/list/mozolist.html',
        controller: 'mozolistCtrl',
        title: 'Mozo',
        sidebarMeta: {
          order: 100
        }
      }).state('dashboard.atencion.mozo-details', {
        url: '/mozo/details/:idPedido',
        templateUrl: 'app/pages/atencion/mozo/mozo.html',
        controller: "mozoCtrl",
        title: 'Mozo',
        params: {
          idPedido: ''
        }
      }).state('dashboard.atencion.visor', {
        url: '/visor',
        templateUrl: 'app/pages/atencion/visor/visor.html',
        controller: 'visorlistCtrl',
        title: 'Visor Cocina',
        sidebarMeta: {
          order: 200
        }
      }).state('dashboard.atencion.visor-details', {
        url: '/visor/details/:idcajas',
        templateUrl: 'app/pages/atencion/visor/visor.html',
        controller: 'visorCtrl',
        controllerAs: 'vm',
        title: 'Visor Cocina',
        params: {
          idrol: ''
        }
      }).state('dashboard.atencion.visorBar', {
        url: '/visorBar',
        templateUrl: 'app/pages/atencion/visorBar/visorBar.html',
        controller: 'visorBarlistCtrl',
        title: 'Visor Bar',
        sidebarMeta: {
          order: 200
        }
      }).state('dashboard.atencion.visorBar-details', {
        url: '/visorBar/details/:idcajas',
        templateUrl: 'app/pages/atencion/visorBar/visorBar.html',
        controller: 'visorBarCtrl',
        controllerAs: 'vm',
        title: 'Visor Bar',
        params: {
          idrol: ''
        }
      });
  }
})();