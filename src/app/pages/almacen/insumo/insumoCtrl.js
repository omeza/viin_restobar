(function(){
    'use strict';
    angular.module('BlurAdmin.pages.almacen.insumo', [])    
    .controller('insumolistCtrl',[ '$scope', 'serviceAPI', '$state', function($scope, serviceAPI, $state){
        $scope.displayed=[];
        $scope.callServer=function(tableState){
            var params = angular.extend({}, tableState.pagination, tableState.search);
            serviceAPI.getInsumos(params).success(function(r){
                if(r.success && r.totalItemCount>0){
                    $scope.displayed=r.rows;
                    tableState.pagination.numberOfPages =r.numberOfPages;
                }
            });
        };
        $scope.agregar=function(){
            $state.go('dashboard.almacen.insumo-details');
        };
        $scope.editar=function(id){
            $state.go('dashboard.almacen.insumo-details', {idinsumo:id});
        };
        $scope.changeStatus=function(id){
            
        };
    }])
    .controller('insumoCtrl', [ '$scope', 'serviceAPI', 'toastr', '$stateParams', 'baSidebarService', '$timeout', '$log', function($scope, serviceAPI, toastr, $stateParams, baSidebarService, $timeout, $log){
        var vm=this;
        $scope.default={id_insumo:0,no_insumo:'', id_categoria_insumo:0, id_unidad: 0, id_medida: 0, nu_cantidad_medida:0, nu_stock_minimo:0,nu_stock_minimo_medida:0, nu_precio_sugerido:0};
        $scope.data={id_insumo:0,no_insumo:'', id_categoria_insumo:0, id_unidad: 0, id_medida: 0, nu_cantidad_medida:0, nu_stock_minimo:0,nu_stock_minimo_medida:0, nu_precio_sugerido:0};
             
        $scope.cCategoriaInsumos=[];
        $scope.cUnidad=[];
        $scope.cMedida=[];

        serviceAPI.listarCategoriaInsumo().then(function(r){
            if(r.data.totalItemCount>0 && r.data.success){
                var cCategoriaInsumos=[];
                cCategoriaInsumos=_.map(r.data.rows, function(item){
                    return {value:item.id, text:item.no_categoria_insumo};
                });
                $scope.cCategoriaInsumos=_.union($scope.cCategoriaInsumos, cCategoriaInsumos);
                $scope.data.id_categoria_insumo=_.findWhere($scope.cCategoriaInsumos);
            }
        });

        serviceAPI.listarUnidad().then(function(r){
            if(r.data.totalItemCount>0 && r.data.success){
                var cUnidad=[];
                cUnidad=_.map(r.data.rows, function(item){
                    return {value:item.id, text:item.no_unidad};
                });
                $scope.cUnidad=_.union($scope.cUnidad, cUnidad);
                $scope.data.id_unidad=_.findWhere($scope.cUnidad);
            }
        });

        serviceAPI.listarMedida().then(function(r){
            if(r.data.totalItemCount>0 && r.data.success){
                var cMedida=[];
                cMedida=_.map(r.data.rows, function(item){
                    return {value:item.id, text:item.no_medida};
                });
                $scope.cMedida=_.union($scope.cMedida, cMedida);
                $scope.data.id_Medida=_.findWhere($scope.cMedida);
            }
        });


        $scope.guardar=function(eForm){
            serviceAPI.guardarInsumo($scope.data).success(function(r){
                var typeNotification='error';
                if(r.success){
                    typeNotification=(r.affected_rows!=0)?'success':'warning';
                }
                toastr[typeNotification](r.messages, 'Registro de Insumo Correctamente');
                angular.extend($scope.data, $scope.default);
                eForm.$setPristine();
                eForm.$setUntouched();
            });
        };

        $scope.consultar=function(){
            var _this=this;
            if($stateParams.idInsumo!=''){
                serviceAPI.consultarInsumo({id:$stateParams.idInsumo}).success(function(r){
                    var typeNotification=(!r.success)?'error':'warning';
                    if(r.success && r.num_rows==1){
                        typeNotification='success';
                        angular.extend($scope.data, r.rows);
                        $scope.cCategoriaInsumos.map(function(a){
                            if(a.id==r.rows.id_categoria_insumo){ $scope.data.id_categoria_insumo=a; }
                        });
                        $scope.cUnidad.map(function(b){
                            if(b.id==r.rows.id_unidad){ $scope.data.id_unidad=b; }
                        });
                        $scope.cMedida.map(function(c){
                            if(c.id==r.rows.id_medida){ $scope.data.id_medida=c; }
                        });
                    }
                    toastr[typeNotification](r.messages, 'Información de Insumo');
                }); 
            }
        };

       
    }]);
})();