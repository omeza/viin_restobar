(function () {
  'use strict';
  angular.module('BlurAdmin.pages.almacen.stockInsumos', [])
    .controller('stockinsumolistCtrl', ['$scope', 'serviceAPI', '$state', 'baConfig', '$element', 'layoutPaths', function ($scope, serviceAPI, $state, baConfig, $element, layoutPaths) {
      $scope.displayed = [];
      $scope.datos = [];
      var layoutColors = baConfig.colors;
      var id = $element[0].getAttribute('id');
      $scope.callServer = function (tableState) {
        var params = angular.extend({}, tableState.pagination, tableState.search);
        serviceAPI.getInsumosStock(params).success(function (r) {
          if (r.success && r.totalItemCount > 0) {
            $scope.displayed = r.rows;
            tableState.pagination.numberOfPages = r.numberOfPages;
            for (var i = 0; i < $scope.displayed.length; i++) {
              $scope.notaproducto = {
                country: $scope.displayed[i].no_insumo,
                visits: $scope.displayed[i].nu_cantidad,
                color: layoutColors.primary
              };
              $scope.datos.push($scope.notaproducto);
            }
          }
        })
      };

      var barChart = AmCharts.makeChart(id, {
        type: 'serial',
        theme: 'blur',
        color: layoutColors.defaultText,
        dataProvider: [
          {
            country: 'LOMO DE TERNERA',
            visits: 10,
            color: layoutColors.primary
          },
          {
            country: 'TOMATE',
            visits: 5,
            color: layoutColors.danger

          },
          {
            country: 'CEBOLLA ROJA',
            visits: 8,
            color: layoutColors.info
          },
          {
            country: 'ARROZ BLANCO',
            visits: 50,
            color: layoutColors.success
          },
          {
            country: 'PAPA CANCHAN',
            visits: 15,
            color: layoutColors.primary
          },
          {
            country: 'SALSA DE SOJA',
            visits: 5,
            color: layoutColors.danger
          },
          {
            country: 'AZUCAR BLANCA',
            visits: 10,
            color: layoutColors.info
          }
          ,
          {
            country: 'LIMA',
            visits: 4,
            color: layoutColors.success
          }        
          ,
          {
            country: 'RON',
            visits: 12,
            color: layoutColors.primary
          }
          
          
        ],
        valueAxes: [
          {
            axisAlpha: 0,
            position: 'left',
            title: 'LISTA DE STOCK',
            gridAlpha: 0.5,
            gridColor: layoutColors.border,
          }
        ],
        startDuration: 1,
        graphs: [
          {
            balloonText: '<b>[[category]]: [[value]]</b>',
            fillColorsField: 'color',
            fillAlphas: 0.7,
            lineAlpha: 0.2,
            type: 'column',
            valueField: 'visits'
          }
        ],
        chartCursor: {
          categoryBalloonEnabled: false,
          cursorAlpha: 0,
          zoomable: false
        },
        categoryField: 'country',
        categoryAxis: {
          gridPosition: 'start',
          labelRotation: 45,
          gridAlpha: 0.5,
          gridColor: layoutColors.border,
        },
        export: {
          enabled: true
        },
        creditsPosition: 'top-right',
        pathToImages: layoutPaths.images.amChart
      });
      console.log();

      $scope.dataProvider = [];

    }])
    .controller('stockinsumoModelCtrl', ['$scope', 'serviceAPI', 'item', '$http', '$httpParamSerializerJQLike', '$uibModalInstance', function ($scope, serviceAPI, item, $http, $httpParamSerializerJQLike, $uibModalInstance) {
      $scope.item = $scope.$parent.item || false;
      $scope.jinsumos = [];

      $scope.consultar = function () {
        var i = 1;
        for (i = 1; i <= $scope.item.nu_cantidad; i++) {
          $scope.insumo = {
            id: i,
            no_insumo: $scope.item.no_insumo,
            fe_vencin: ''
          };
          console.log($scope.insumo);
          $scope.jinsumos.push($scope.insumo);
        }
      }

      $scope.opened = {};

      $scope.open = function ($event, elementOpened) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened[elementOpened] = !$scope.opened[elementOpened];
      };
      $scope.cerrar = function () {
        $uibModalInstance.close();
      }

    }])
    .controller('stockinsumoCtrl', ['$scope', '$uibModal', 'serviceAPI', 'toastr', '$stateParams', 'baSidebarService', '$timeout', '$log', function ($scope, $uibModal, serviceAPI, toastr, $stateParams, baSidebarService, $timeout, $log) {
      var vm = this;
      $scope.default = { id_insumo: 0, no_insumo: '', id_categoria_insumo: 0, id_unidad: 0, id_medida: 0, nu_cantidad_medida: 0, nu_stock_minimo: 0, nu_stock_minimo_medida: 0, nu_precio_sugerido: 0 };
      $scope.data = { id_insumo: 0, no_insumo: '', id_categoria_insumo: 0, id_unidad: 0, id_medida: 0, nu_cantidad_medida: 0, nu_stock_minimo: 0, nu_stock_minimo_medida: 0, nu_precio_sugerido: 0 };
      $scope.detalleCompra = [];

      $scope.consultar = function () {
        if ($stateParams.idInsumo != '') {
          serviceAPI.getInsumosDetalleCompras({ id: $stateParams.idInsumo }).success(function (r) {
            if (r.success && r.totalItemCount > 0) {
              $scope.detalleCompra = r.rows;
            }
          });
        }
      };

      $scope.openDetalle = function (size, item, index) {
        $scope.item = item;
        $uibModal.open({
          animation: true,
          templateUrl: "app/pages/almacen/stockInsumos/modal/modalDetalle.html",
          size: size,
          controller: 'stockinsumoModelCtrl',
          scope: $scope,
          resolve: {
            item: function () { }
          }
        }).result.then(function (item) {
          //angular.extend($scope.data, item);                    
        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });
      };

    }]);
})();