(function () {
    'use strict';
    angular.module('BlurAdmin.pages.almacen.categoriaInsumo', [])

        .controller('categoriaInsumoCtrl', ['$scope', 'serviceAPI', 'toastr', '$stateParams', 'baSidebarService', '$timeout', '$log', function ($scope, serviceAPI, toastr, $stateParams, baSidebarService, $timeout, $log) {
            var vm = this;
            $scope.default = { id_categoria_insumo: 0, no_categoria_insumo: '' };
            $scope.data = { id_categoria_insumo: 0, no_categoria_insumo: '' };


            $scope.displayed = [];
            $scope.callServer = function (tableState) {
                var params = angular.extend({}, tableState.pagination, tableState.search);
                serviceAPI.getCategoriaInsumo(params).success(function (r) {
                    if (r.success && r.totalItemCount > 0) {
                        $scope.displayed = r.rows;
                        tableState.pagination.numberOfPages = r.numberOfPages;
                    }
                });
            };

            $scope.changeStatus = function (idCategoria, actividad, index) {
                serviceAPI.cambiarStatusCategoriaInsumo({ id: idCategoria, status: actividad }).success(function (r) {
                    var typenotify = 'error';
                    if (r.num_rows == 0) {
                        typenotify = 'warning';
                        $scope.displayed = $scope.displayed.filter(function (i) { return (i.id_categoria_insumo != idCategoria); })
                    }
                    if (r.affected_rows == 1) {
                        typenotify = 'success';
                        $scope.displayed[index].status = r.rows.status;
                    }
                    toastr[typenotify](r.messages, 'Listado de Sucursal');
                });
            };

            $scope.guardar = function (eForm) {
                serviceAPI.guardarCategoriaInsumo($scope.data).success(function (r) {
                    var typeNotification = 'error';
                    if (r.success) {
                        typeNotification = (r.affected_rows != 0) ? 'success' : 'warning';
                    }
                    toastr[typeNotification](r.messages, 'Registro de Categoria Correctamente');
                    angular.extend($scope.data, $scope.default);
                    eForm.$setPristine();
                    eForm.$setUntouched();
                    $scope.callServer([]);
                });
            };

            $scope.editar = function (value) {
                $scope.data = value;
            };

            $scope.consultar = function () {
                var _this = this;
                if ($stateParams.id_categoria_insumo != '') {
                    serviceAPI.consultarCategoriaInsumo({ id: $stateParams.id_categoria_insumo }).success(function (r) {
                        var typeNotification = (!r.success) ? 'error' : 'warning';
                        if (r.success && r.num_rows == 1) {
                            typeNotification = 'success';
                            angular.extend($scope.data, r.rows);
                        }
                        toastr[typeNotification](r.messages, 'Información de Usuario');
                    });
                }
            };

            $scope.eliminar = function (id_categoria_insumo) {
                serviceAPI.eliminarCategoriaInsumo({ id: id_categoria_insumo }).success(function (r) {
                    var typenotify = (r.success) ? 'warning' : 'error';
                    if (r.affected_rows == 1) {
                        typenotify = 'success';
                        $scope.displayed = $scope.displayed.filter(function (i) { return (i.id_categoria_insumo != id_categoria_insumo) });
                    }
                    toastr[typenotify](r.messages, 'Listado de Categoria Insumo')
                });
            };

        }]);
})();