(function () {
  'use strict';
  angular.module('BlurAdmin.pages.almacen', [
    'BlurAdmin.pages.almacen.stockInsumos',
    'BlurAdmin.pages.almacen.categoriaInsumo',
    'BlurAdmin.pages.almacen.insumo'
  ]).config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('dashboard.almacen', {
        url: '/almacen',
        template: '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
        abstract: true,
        title: 'Almacen',
        sidebarMeta: {
          icon: 'ion-clipboard',
          order: 500,
        }
      }).state('dashboard.almacen.categoriaInsumo', {
        url: '/categoriaInsumo',
        templateUrl: 'app/pages/almacen/categoriaInsumo/categoriaInsumo.html',
        controller: 'categoriaInsumoCtrl',
        title: 'Categoria Insumo',
        sidebarMeta: {
          order: 100
        }
      }).state('dashboard.almacen.categoriaInsumo-details', {
        url: '/categoriaInsumo/details/:idalmacen',
        templateUrl: 'app/pages/almacen/categoriaInsumo/categoriaInsumo.html',
        controller: "categoriaInsumoCtrl",
        title: 'Categoria Insumo',
        params: {
          idempleado: ''
        }
      }).state('dashboard.almacen.insumo', {
        url: '/insumo',
        templateUrl: 'app/pages/almacen/insumo/list/insumoList.html',
        controller: 'insumolistCtrl',
        title: 'Insumo',
        sidebarMeta: {
          order: 200
        }
      }).state('dashboard.almacen.insumo-details', {
        url: '/insumo/details/:idInsumo',
        templateUrl: 'app/pages/almacen/insumo/insumo.html',
        controller: 'insumoCtrl',
        controllerAs: 'vm',
        title: 'Insumo',
        params: { idrol: '' }
      }).state('dashboard.almacen.stockInsumos', {
        url: '/stockInsumos',
        templateUrl: 'app/pages/almacen/stockInsumos/list/stockinsumolist.html',
        controller: 'stockinsumolistCtrl',
        title: 'Stock Insumo',
        sidebarMeta: {
          order: 200
        }
      }).state('dashboard.almacen.stockInsumos-details', {
        url: '/stockInsumos/details/:idInsumo',
        templateUrl: 'app/pages/almacen/stockInsumos/stockinsumo.html',
        controller: 'stockinsumoCtrl',
        controllerAs: 'vm',
        title: 'Stock Insumo',
        params: { 
          idInsumo: '' 
        }
      }).state('dashboard.almacen.stockInsumos-details2', {
        url: '/stockInsumos/details2/:idInsumo',
        templateUrl: 'app/pages/almacen/stockInsumos/listDetalle/stockinsumoDetalle.html',
        controller: 'stockinsumoCtrl',
        controllerAs: 'vm',
        title: 'Detalle Compras Insumo',
        params: { 
          idInsumo: '' 
        }
      });
  }

})();
