(function () {
    'use strict';
    angular.module('BlurAdmin.pages.empleado', [
        'BlurAdmin.pages.empleado.empleado'
    ]).config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
          .state('dashboard.empleado', {
            url: '/empleado',
            template : '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
            abstract: true,
            title: 'Empleado',
            sidebarMeta: {
              icon: 'ion-person',
              order: 500,
            }
          }).state('dashboard.empleado.empleado', {
            url: '/empleado',
            templateUrl: 'app/pages/empleado/empleado/list/empleadoList.html',
            controller: 'empleadolistCtrl',
            title: 'Empleado',
            sidebarMeta: {
                order: 100
            }
        }).state('dashboard.empleado.empleado-details', {
            url: '/empleado/details/:idempleado',
            templateUrl: 'app/pages/empleado/empleado/empleado.html',
            controller: "empleadoCtrl",
            title: 'Empleado',
            params:{
                idempleado:''
            }
        });
    }
  })();
  