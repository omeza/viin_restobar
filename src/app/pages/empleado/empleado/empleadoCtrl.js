(function(){
    'use strict';
    angular.module('BlurAdmin.pages.empleado.empleado', [])    
    .controller('empleadolistCtrl',[ '$scope', 'serviceAPI', '$state', function($scope, serviceAPI, $state){
        $scope.displayed=[];
        $scope.callServer=function(tableState){
            var params = angular.extend({}, tableState.pagination, tableState.search);
            serviceAPI.getEmpleado(params).success(function(r){
                if(r.success && r.totalItemCount>0){
                    $scope.displayed=r.rows;
                    tableState.pagination.numberOfPages =r.numberOfPages;
                }
            });
        };
        $scope.agregar=function(){
            $state.go('dashboard.empleado.empleado-details');
        };
        $scope.editar=function(id_empleado){
            $state.go('dashboard.empleado.empleado-details', {idempleado:id_empleado});
        };
        $scope.changeStatus=function(id_empleado){
            
        };
        $scope.eliminar=function(id_empleado){
            serviceAPI.eliminarEmpleado({id:id_empleado}).success(function(r){
                var typenotify=(r.success)?'warning':'error';
                if(r.affected_rows==1){
                    typenotify='success';
                    $scope.displayed=$scope.displayed.filter(function(i){ return (i.id_empleado!=id_empleado)});
                }
                toastr[typenotify](r.messages, 'Listado de Monedas')
            });
        };

        $scope.obtenerEmpleado = function (empleado) {
            $uibModalInstance.close(empleado);
        };
    }])
    .controller('empleadolistModaltrl',[ '$scope', 'serviceAPI', '$state', '$uibModalInstance', function($scope, serviceAPI, $state, $uibModalInstance){
        $scope.displayed=[];
        $scope.callServer=function(tableState){
            var params = angular.extend({}, tableState.pagination, tableState.search);
            serviceAPI.getEmpleado(params).success(function(r){
                if(r.success && r.totalItemCount>0){
                    $scope.displayed=r.rows;
                    tableState.pagination.numberOfPages =r.numberOfPages;
                }
            });
        };
       
        $scope.obtenerEmpleado = function (empleado) {
            $uibModalInstance.close(empleado);
        };
    }])
    .controller('empleadoCtrl', [ '$scope', 'serviceAPI', 'toastr', '$stateParams', 'baSidebarService', '$timeout', '$log', function($scope, serviceAPI, toastr, $stateParams, baSidebarService, $timeout, $log){
        var vm=this;
        $scope.default={id_tipo_documento:0,id_sexo:0,id_estado_civil:0,nu_documento:'',no_apepat:'',no_apemat:'',no_nombres:'',fe_nacimiento:'',
            no_direccion:'',nu_contacto:'',email:'', id_empleado:0};
        $scope.data={id_tipo_documento:0,id_sexo:0,id_estado_civil:0,nu_documento:'',no_apepat:'',no_apemat:'',no_nombres:'',fe_nacimiento:'',
        no_direccion:'',nu_contacto:'',email:'', id_empleado:0};
        
        $scope.cTipoDocumento=[];        
        $scope.cSexo=[];
        $scope.cEstadoCivil=[];


        serviceAPI.listarTipoDocumento().then(function(r){
            if(r.data.totalItemCount>0 && r.data.success){
                var cTipoDocumento=[];
                cTipoDocumento=_.map(r.data.rows, function(item){
                    return {value:item.id, text:item.no_tipo_documento};
                });
                $scope.cTipoDocumento=_.union($scope.cTipoDocumento, cTipoDocumento);
                $scope.data.id_tipo_documento=_.findWhere($scope.cTipoDocumento);
            }
        });

        serviceAPI.listarSexo().then(function(r){
            if(r.data.totalItemCount>0 && r.data.success){
                var cSexo=[];
                cSexo=_.map(r.data.rows, function(item){
                    return {value:item.id, text:item.no_sexo};
                });
                $scope.cSexo=_.union($scope.cSexo, cSexo);
                $scope.data.id_sexo=_.findWhere($scope.cSexo);
            }
        });

        serviceAPI.listarEstadoCivil().then(function(r){
            if(r.data.totalItemCount>0 && r.data.success){
                var cEstadoCivil=[];
                cEstadoCivil=_.map(r.data.rows, function(item){
                    return {value:item.id, text:item.no_estado_civil};
                });
                $scope.cEstadoCivil=_.union($scope.cEstadoCivil, cEstadoCivil);
                $scope.data.id_estado_civil=_.findWhere($scope.cEstadoCivil);
            }
        });

        $scope.guardar=function(eForm){
            serviceAPI.guardarEmpleado($scope.data).success(function(r){
                var typeNotification='error';
                if(r.success){
                    typeNotification=(r.affected_rows!=0)?'success':'warning';
                }
                toastr[typeNotification](r.messages, 'Registro de empleado');
                angular.extend($scope.data, $scope.default);
                eForm.$setPristine();
                eForm.$setUntouched();
            });
        };

        $scope.consultar=function(){
            var _this=this;
            if($stateParams.idempleado!=''){
                serviceAPI.consultarEmpleado({id_empleado:$stateParams.idempleado}).success(function(r){
                    var typeNotification=(!r.success)?'error':'warning';
                    if(r.success && r.num_rows==1){
                        typeNotification='success';
                        angular.extend($scope.data, r.rows);   
                        $scope.cTipoDocumento.map(function(a){
                            if(z.id==r.rows.id_tipo_documento){$scope.data.id_tipo_documento=a;}
                        });                   
                        $scope.cSexo.map(function(b){
                            if(z.id==r.rows.id_sexo){$scope.data.id_sexo=b;}
                        });
                        $scope.cEstadoCivil.map(function(c){
                            if(z.id==r.rows.id_estado_civil){$scope.data.id_estado_civil=c;}
                        });
                    }
                    toastr[typeNotification](r.messages, 'Información de la empleado');
                }); 
            }
        };

    }]);
})();