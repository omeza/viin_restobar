(function () {
  'use strict';
  angular.module('BlurAdmin.pages', [
    'ui.router',
    'BlurAdmin.pages.sides',
    'BlurAdmin.pages.home',
    'BlurAdmin.pages.sucursal',
    'BlurAdmin.pages.empleado',
    'BlurAdmin.pages.clientes',
    'BlurAdmin.pages.almacen',
    'BlurAdmin.pages.compras',
    'BlurAdmin.pages.producto',
    'BlurAdmin.pages.venta',
    'BlurAdmin.pages.atencion',
    'BlurAdmin.pages.seguridad',
    'BlurAdmin.pages.reportes',
    'BlurAdmin.pages.configuracion'
  ])
    .filter('summaryTotal', ['$filter', function ($filter) {
      return function (input, cantidad, precio, decimal) {
        var output = 0;
        if (_.isArray(input)) {
          _.each(input, function (item) {
            output += parseInt(item[cantidad]) * parseFloat(item[precio]);
          });
        }
        return output.toFixed(decimal);
      }
    }])
    .filter('unique', ['$filter', function ($filter) {
      return function (collection, keyname) {
        var output = [],
          keys = [];

        angular.forEach(collection, function (item) {
          var key = item[keyname];
          if (keys.indexOf(key) === -1) {
            keys.push(key);
            output.push(item);
          }
        });
        return output;
      }
    }])
    .filter('Total', ['$filter', function ($filter) {
      return function (input, peso, precio, decimal) {
        var output = 0;
        if (_.isArray(input)) {
          _.each(input, function (item) {
            output += parseFloat(item[precio]) * parseFloat(item[peso]);
          });
        }
        return output.toFixed(decimal);
      }
    }])
    .filter('numberFixedLen', ['$filter', function ($filter) {
      return function (num, size) {
        let s = num + "";
        s = num + "";
        while (s.length < size) s = "0" + s;
        return s;
      };
    }])
    .factory('Excel', ['$filter', function ($window) {
      var uri = 'data:application/vnd.ms-excel;base64,',
        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64 = function (s) { return $window.btoa(unescape(encodeURIComponent(s))); },
        format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) };
      return {
        tableToExcel: function (tableId, worksheetName) {
          console.log(tableId);
          console.log(worksheetName);
          var table = $(tableId),
            ctx = { worksheet: worksheetName, table: table.html() },
            href = uri + base64(format(template, ctx));
            console.log(table);
          return href;
        }
      }
    }])
    .factory('servFactory', ['$filter', function ($filter) {
      return {
        aplicar_porc: function (mto_compra, mto_porcentaje, decimal) {
          var mto = parseFloat(mto_compra);
          var mto_porc = parseFloat(mto_porcentaje);
          var calculo = parseFloat(mto) + (parseFloat(mto) * parseFloat(mto_porc) / 100);
          return calculo.toFixed(decimal || 2);
        },
        calularNeto: function (monto, excento_impuesto, porc_impuesto) {
          var monto = parseFloat(monto) || 0.00;
          var monto_neto = monto;
          if (!excento_impuesto) {
            monto_neto = monto / (1 + (parseFloat(porc_impuesto) / 100));
          }
          return monto_neto.toFixed(2);
        }
      }
    }])
    .factory('serviceAPI', ['$http', 'apiEndpoint', function ($http, apiEndpoint) {
      return {
        getFFarmaceuticas: function (params) { return $http.post(apiEndpoint + 'ffarmaceutica.php?oper=listar', params); },
        existCargo: function (params) { return $http.post(apiEndpoint + 'cargo.php?oper=existe', params); },
        getCondicionPago: function () { return $http.get(apiEndpoint + 'condicionpago.php?oper=listar'); },
        existFactCompra: function (params) { return $http.post(apiEndpoint + 'compras.php?oper=existeFactura', params); },
        guardarCompra: function (params) { return $http.post(apiEndpoint + 'compras.php?oper=guardar', params); },
        infoEmpresa: function () { return $http.get(apiEndpoint + 'empresa.php'); },
        getTiposDocumentos: function (params) { return $http.post(apiEndpoint + 'tipodocumentos.php?oper=listar', params); },
        guardarEmpresa: function (params) { return $http.post(apiEndpoint + 'empresa.php?oper=guardar', params) },
        changeDefaultModena: function (params) { return $http.post(apiEndpoint + 'monedas.php?oper=moneda_predeterminada', params); },
        validationMoneda: function (params) { return $http.post(apiEndpoint + 'monedas.php?oper=validation', params); },
        existCodeMoney: function (params) { return $http.post(apiEndpoint + 'monedas.php?oper=existCode', params) },
        existMoneda: function (params) { return $http.post(apiEndpoint + 'monedas.php?oper=existMoneda', params); },
        existSimbMoneda: function (params) { return $http.post(apiEndpoint + 'monedas.php?oper=existSimbolo', params); },
        addMonedaAditional: function (params) { return $http.post(apiEndpoint + 'monedas.php?oper=aditional', params); },
        getMonedas: function () { return $http.get(apiEndpoint + 'monedas.php?oper=listar'); },
        cambiarStatusMoneda: function (params) { return $http.post(apiEndpoint + 'monedas.php?oper=status', params); },
        getMoneda: function (params) { return $http.post(apiEndpoint + 'monedas.php?oper=consultar', params); },
        guardarMoneda: function (params) { return $http.post(apiEndpoint + 'monedas.php?oper=guardar', params); },
        eliminarMoneda: function (params) { return $http.post(apiEndpoint + 'monedas.php?oper=eliminar', params); },
        existsNomPais: function (params) { return $http.post(apiEndpoint + 'paises.php?oper=existPais', params) },
        getPaises: function (params) { return $http.post(apiEndpoint + 'paises.php?oper=listar', params); },
        getPais: function (params) { return $http.post(apiEndpoint + 'paises.php?oper=consultar', params); },
        guardarPais: function (params) { return $http.post(apiEndpoint + 'paises.php?oper=guardar', params); },
        borrarPais: function (params) { return $http.post(apiEndpoint + 'paises.php?oper=eliminar', params); },
        asignDefaultpais: function (params) { return $http.post(apiEndpoint + 'paises.php?oper=predeterminado', params); },
        getConfig: function () { return $http.get(apiEndpoint + 'configuracion.php'); },
        guardarConfig: function (paramas) { return $http.post(apiEndpoint + 'configuracion.php?oper=guardar', paramas) },
        signIn: function (data) { return $http.post(apiEndpoint + 'signin.php', data); },
        signInCaja: function (params) { return $http.post(apiEndpoint + 'cajas.php?oper=estadoCaja', params); },
        existRolUsuario: function (params) { return $http.post(apiEndpoint + 'roles.php?oper=existe', params); },
        guardarRolUsuario: function (params) { return $http.post(apiEndpoint + 'roles.php?oper=guardar', params); },
        consultarRolUsuario: function (params) { return $http.post(apiEndpoint + 'roles.php?oper=consultar', params); },
        getRoles: function () { return $http.get(apiEndpoint + 'roles.php?oper=listar'); },
        deleteRol: function (params) { return $http.post(apiEndpoint + 'roles.php?oper=eliminar', params) },
        cambiarStatusRol: function (params) { return $http.post(apiEndpoint + 'roles.php?oper=changestatus', params); },
        getUsuarios: function () { return $http.get(apiEndpoint + 'usuarios.php?oper=listar'); },
        exitUsuario: function (params) { return $http.post(apiEndpoint + 'usuarios.php?oper=existUser', params); },
        guardarUsuario: function (params) { return $http.post(apiEndpoint + 'usuarios.php?oper=guardar', params); },
        consultarUsuario: function (params) { return $http.post(apiEndpoint + 'usuarios.php?oper=consultar', params); },
        getProductoSalida: function (params) { return $http.post(apiEndpoint + 'productos.php?oper=listarProducSalida', params); },
        listSucursal: function (params) { return $http.post(apiEndpoint + 'sucursal.php?oper=listar', params); },
        guardarSucursal: function (params) { return $http.post(apiEndpoint + 'sucursal.php?oper=guardar', params); },
        consultarSucursal: function (params) { return $http.post(apiEndpoint + 'sucursal.php?oper=consultar', params); },
        getListaSucursal: function (params) { return $http.post(apiEndpoint + 'sucursal.php?oper=listarSucursal', params); },
        getCajas: function (params) { return $http.post(apiEndpoint + 'cajas.php?oper=listar', params); },
        getCajasSucursales: function (params) { return $http.post(apiEndpoint + 'cajas.php?oper=listarCajaSucursal', params); },
        guardarCajas: function (params) { return $http.post(apiEndpoint + 'cajas.php?oper=guardarCaja', params); },
        eliminarCajas: function (params) { return $http.post(apiEndpoint + 'cajas.php?oper=eliminar', params); },
        consultarCaja: function (params) { return $http.post(apiEndpoint + 'cajas.php?oper=consultar', params); },
        getMesas: function (params) { return $http.post(apiEndpoint + 'mesas.php?oper=listar', params); },
        guardarMesas: function (params) { return $http.post(apiEndpoint + 'mesas.php?oper=guardar', params); },
        eliminarMesas: function (params) { return $http.post(apiEndpoint + 'mesas.php?oper=eliminar', params); },
        consultarMesas: function (params) { return $http.post(apiEndpoint + 'mesas.php?oper=consultar', params); },
        getListarCajas: function (params) { return $http.post(apiEndpoint + 'cajas.php?oper=listarCajas', params); },
        getCategoriaInsumo: function (params) { return $http.post(apiEndpoint + 'categoriainsumo.php?oper=listar', params); },
        guardarCategoriaInsumo: function (params) { return $http.post(apiEndpoint + 'categoriainsumo.php?oper=guardar', params); },
        eliminarCategoriaInsumo: function (params) { return $http.post(apiEndpoint + 'categoriainsumo.php?oper=eliminar', params); },
        consultarCategoriaInsumo: function (params) { return $http.post(apiEndpoint + 'categoriainsumo.php?oper=consultar', params); },
        getInsumos: function (params) { return $http.post(apiEndpoint + 'insumos.php?oper=listar', params); },
        listarCategoriaInsumo: function (params) { return $http.post(apiEndpoint + 'categoriainsumo.php?oper=listarCategoriaInsumo', params); },
        listarUnidad: function (params) { return $http.post(apiEndpoint + 'unidad.php?oper=listarUnidad', params); },
        listarMedida: function (params) { return $http.post(apiEndpoint + 'medida.php?oper=listarMedida', params); },
        guardarInsumo: function (params) { return $http.post(apiEndpoint + 'insumos.php?oper=guardar', params); },
        consultarInsumo: function (params) { return $http.post(apiEndpoint + 'insumos.php?oper=consultar', params); },
        getProveedor: function (params) { return $http.post(apiEndpoint + 'proveedor.php?oper=listar', params); },
        listarTipoDocumento: function (params) { return $http.post(apiEndpoint + 'tipoDocumento.php?oper=listarTipoDocumento', params); },
        getDepartamento: function (params) { return $http.post(apiEndpoint + 'ubigeo.php?oper=listarDepartamento', params); },
        getProvincia: function (params) { return $http.post(apiEndpoint + 'ubigeo.php?oper=listarProvincia', params); },
        getDistrito: function (params) { return $http.post(apiEndpoint + 'ubigeo.php?oper=listarDistrito', params); },
        guardarUProveedor: function (params) { return $http.post(apiEndpoint + 'proveedor.php?oper=guardar', params); },
        consultarProveedor: function (params) { return $http.post(apiEndpoint + 'proveedor.php?oper=consultar', params); },
        getEmpleado: function (params) { return $http.post(apiEndpoint + 'empleado.php?oper=listar', params); },
        listarSexo: function (params) { return $http.post(apiEndpoint + 'sexo.php?oper=listarSexo', params); },
        listarEstadoCivil: function (params) { return $http.post(apiEndpoint + 'estadoCivil.php?oper=listarEstadoCivil', params); },
        guardarEmpleado: function (params) { return $http.post(apiEndpoint + 'empleado.php?oper=guardar', params); },
        consultarEmpleado: function (params) { return $http.post(apiEndpoint + 'empleado.php?oper=consultar', params); },
        eliminarEmpleado: function (params) { return $http.post(apiEndpoint + 'empleado.php?oper=eliminar', params); },
        getProductoCategoria: function (params) { return $http.post(apiEndpoint + 'productoCategoria.php?oper=listar', params); },
        guardarProductoCategoria: function (params) { return $http.post(apiEndpoint + 'productoCategoria.php?oper=guardar', params); },
        consultarCategoriaProducto: function (params) { return $http.post(apiEndpoint + 'productoCategoria.php?oper=consultar', params); },
        guardarCategoriaInsumo: function (params) { return $http.post(apiEndpoint + 'categoriaInsumo.php?oper=guardar', params); },
        getComprobante: function (params) { return $http.post(apiEndpoint + 'tipocomprobantes.php?oper=listar', params); },
        getProveedores: function (params) { return $http.post(apiEndpoint + 'proveedor.php?oper=listarProveedor', params); },
        guardarOrdenCompra: function (params) { return $http.post(apiEndpoint + 'compras.php?oper=guardar', params); },
        getCompras: function (params) { return $http.post(apiEndpoint + 'compras.php?oper=listar', params); },
        listarCategoriaProducto: function (params) { return $http.post(apiEndpoint + 'productoCategoria.php?oper=listarCategoria', params); },
        listarDestino: function (params) { return $http.post(apiEndpoint + 'productoCategoria.php?oper=listarDestino', params); },
        getProducto: function (params) { return $http.post(apiEndpoint + 'producto.php?oper=listar', params); },
        consultarTotales: function (params) { return $http.post(apiEndpoint + 'compras.php?oper=consultarTotal', params); },
        guardarProducto: function (params) { return $http.post(apiEndpoint + 'producto.php?oper=guardar', params); },
        consultarProductoV: function (params) { return $http.post(apiEndpoint + 'producto.php?oper=consultarV', params); },
        getInsumosProducto: function (params) { return $http.post(apiEndpoint + 'producto.php?oper=listarInsumosProducto', params); },
        listarMesasSucursal: function (params) { return $http.post(apiEndpoint + 'mesas.php?oper=listarMesasSucursal', params); },
        listarProductoxCategoria: function (params) { return $http.post(apiEndpoint + 'producto.php?oper=listarProductoxCategoria', params); },
        cambiarStatusSucursal: function (params) { return $http.post(apiEndpoint + 'sucursal.php?oper=status', params); },
        eliminarSucursal: function (params) { return $http.post(apiEndpoint + 'sucursal.php?oper=eliminar', params); },
        cambiarStatusProveedor: function (params) { return $http.post(apiEndpoint + 'proveedor.php?oper=status', params); },
        eliminarProveedor: function (params) { return $http.post(apiEndpoint + 'proveedor.php?oper=eliminar', params); },
        getInsumosStock: function (params) { return $http.post(apiEndpoint + 'insumos.php?oper=listaStock', params); },
        getInsumosDetalleCompras: function (params) { return $http.post(apiEndpoint + 'insumos.php?oper=listaInsumoCompras', params); },
        getCliente: function (params) { return $http.post(apiEndpoint + 'cliente.php?oper=listar', params); },
        guardarCliente: function (params) { return $http.post(apiEndpoint + 'cliente.php?oper=guardar', params); },
        consultarCliente: function (params) { return $http.post(apiEndpoint + 'cliente.php?oper=consultar', params); },
        guardarCliente: function (params) { return $http.post(apiEndpoint + 'cliente.php?oper=guardar', params); },
        eliminarCliente: function (params) { return $http.post(apiEndpoint + 'cliente.php?oper=eliminar', params); },
        guardarPedido: function (params) { return $http.post(apiEndpoint + 'mozo.php?oper=guardar', params); },
        cambiarStatusCategoriaInsumo: function (params) { return $http.post(apiEndpoint + 'categoriainsumo.php?oper=status', params); },
        getMesasAtendidas: function (params) { return $http.post(apiEndpoint + 'mozo.php?oper=listarAtendidas', params); },
        consultarPedido: function (params) { return $http.post(apiEndpoint + 'mozo.php?oper=consultar', params); },
        listarDetalleProductos: function (params) { return $http.post(apiEndpoint + 'mozo.php?oper=listarDetalleProductos', params); },
        consultarPedidoDetalle: function (params) { return $http.post(apiEndpoint + 'mozo.php?oper=consultarDetalle', params); },
        consultarCorrelativoSucursal: function (params) { return $http.post(apiEndpoint + 'sucursal.php?oper=consultarCorrelativoSucursal', params); },
        getMesasAtendidasVisor: function (params) { return $http.post(apiEndpoint + 'visor.php?oper=listarAtendidas', params); },
        listarMesasSucursalMozo: function (params) { return $http.post(apiEndpoint + 'mozo.php?oper=listarMesasSucursal', params); },
        consultarPedidoDetalleVisor: function (params) { return $http.post(apiEndpoint + 'visor.php?oper=consultarDetalle', params); },
        getTipoPago: function (params) { return $http.post(apiEndpoint + 'tipoPago.php?oper=listar', params); },
        getComboProducto: function (params) { return $http.post(apiEndpoint + 'producto.php?oper=comboProducto', params); },
        guardarProductoMenu: function (params) { return $http.post(apiEndpoint + 'productoMenu.php?oper=guardar', params); },
        getCabeceraMenu: function (params) { return $http.post(apiEndpoint + 'productoMenu.php?oper=listarCabecera', params); },
        getCabeceraMenuDetalle: function (params) { return $http.post(apiEndpoint + 'productoMenu.php?oper=listarDetalle', params); },
        getTotalesVentas: function (params) { return $http.post(apiEndpoint + 'cajas.php?oper=totales', params); },
        getTotalesVentasDia: function (params) { return $http.post(apiEndpoint + 'cajas.php?oper=totalesDia', params); },
        getVentas: function (params) { return $http.post(apiEndpoint + 'cajas.php?oper=listarVentas', params); },
        getVentasDia: function (params) { return $http.post(apiEndpoint + 'cajas.php?oper=listarVentasDia', params); },
        actualizarAtendido: function (params) { return $http.post(apiEndpoint + 'visor.php?oper=actualizarAtendido', params); },
        guardarVenta: function (params) { return $http.post(apiEndpoint + 'cajas.php?oper=guardar', params); },
        actualizarPago: function (params) { return $http.post(apiEndpoint + 'cajas.php?oper=actualizar', params); },
        getMesasAtendidasPago: function (params) { return $http.post(apiEndpoint + 'mozo.php?oper=listarAtendidasPago', params); },
        getMesasAtendidasVisorBar: function (params) { return $http.post(apiEndpoint + 'visor.php?oper=listarAtendidasBar', params); },
        consultarPedidoDetalleVisorBar: function (params) { return $http.post(apiEndpoint + 'visor.php?oper=consultarDetalleBar', params); },
        getCajasUsuario: function (params) { return $http.post(apiEndpoint + 'cajas.php?oper=listarCajasUsuario', params); },
        guardarAtertura: function (params) { return $http.post(apiEndpoint + 'cajas.php?oper=guardarAtertura', params); },
        consultarVentas: function (params) { return $http.post(apiEndpoint + 'reporte.php?oper=consultarVentas', params); },
        consultarCompras: function (params) { return $http.post(apiEndpoint + 'reporte.php?oper=consultarCompras', params); },
        getSucursalProducto: function (params) { return $http.post(apiEndpoint + 'producto.php?oper=sucursalProducto', params); }  
         
        
        
      }
    }]);
})();
