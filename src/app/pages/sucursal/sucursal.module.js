(function () {
    'use strict';
    angular.module('BlurAdmin.pages.sucursal', [
        'BlurAdmin.pages.sucursal.sucursales',
        'BlurAdmin.pages.sucursal.cajas',
        'BlurAdmin.pages.sucursal.mesas'
    ]).config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
          .state('dashboard.sucursal', {
            url: '/sucursal',
            template : '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
            abstract: true,
            title: 'Sucursal',
            sidebarMeta: {
              icon: 'fas fa-building',
              order: 500,
            }
          }).state('dashboard.sucursal.sucursales', {
            url: '/sucursales',
            templateUrl: 'app/pages/sucursal/sucursales/list/sucursalList.html',
            controller: 'sucursallistCtrl',
            title: 'Sucursal',
            sidebarMeta: {
                order: 100
            }
        }).state('dashboard.sucursal.sucursales-details', {
            url: '/sucursales/details/:idsucursal',
            templateUrl: 'app/pages/sucursal/sucursales/sucursales.html',
            controller: "sucursalesCtrl",
            title: 'Sucursal',
            params:{
                idempleado:''
            }
        }).state('dashboard.sucursal.cajas', {
            url: '/cajas',
            templateUrl: 'app/pages/sucursal/cajas/list/cajasList.html',
            controller: 'cajasCtrl',
            title: 'Caja',
            sidebarMeta: {
                order: 200
              }
        }).state('dashboard.sucursal.cajas-details', {
            url: '/cajas/details/:idcajas',
            templateUrl: 'app/pages/sucursal/cajas/cajas.html',
            controller: 'cajasCtrl',
            controllerAs: 'vm',
            title: 'Cajas',
            params:{ idrol:''}
        }).state('dashboard.sucursal.mesas', {
            url: '/mesas',
            templateUrl: 'app/pages/sucursal/mesas/list/mesasList.html',
            controller: 'mesasCtrl',
            title: 'Mesas',
            sidebarMeta: {
                order: 200
              }
        }).state('dashboard.sucursal.mesas-details', {
            url: '/mesas/details/:idmesas',
            templateUrl: 'app/pages/sucursal/mesas/mesas.html',
            controller: 'mesasCtrl',
            controllerAs: 'vm',
            title: 'Mesas',
            params:{ idrol:''}
        });
    }
  
  })();
  