(function(){
    'use strict';
    angular.module('BlurAdmin.pages.sucursal.mesas', [])    
    .controller('mesaslistCtrl',[ '$scope', 'serviceAPI', '$state', function($scope, serviceAPI, $state){
        $scope.displayed=[];
        $scope.callServer=function(tableState){
            var params = angular.extend({}, tableState.pagination, tableState.search);
            serviceAPI.getUsuarios(params).success(function(r){
                if(r.success && r.totalItemCount>0){
                    $scope.displayed=r.rows;
                    tableState.pagination.numberOfPages =r.numberOfPages;
                }
            });
        };
        $scope.agregar=function(){
            $state.go('dashboard.seguridad.usuario-details');
        };
        $scope.editar=function(id){
            $state.go('dashboard.seguridad.usuario-details', {idempleado:id});
        };
        $scope.changeStatus=function(id){
            
        };
    }])
    .controller('mesasCtrl', [ '$scope', 'serviceAPI', 'toastr', '$stateParams', 'baSidebarService', '$timeout', '$log','$filter','editableOptions','editableThemes', function($scope, serviceAPI, toastr, $stateParams, baSidebarService, $timeout, $log,$filter,editableOptions,
        editableThemes){
        var vm=this;
        $scope.default={id_mesa:0, id_sucursal:0, nu_mesa:''};
        $scope.data={id_mesa:0, id_sucursal:0, nu_mesa:''};
        $scope.rowNotaProducto = [];
        $scope.csucursal=[];

        serviceAPI.getMesas().then(function(r){
            if(r.data.success && r.data.totalItemCount>0){
                $scope.rowNotaProducto=r.data.rows;               
            }
        });
        $scope.a_sucursal=[{value:0, text:'Seleccionar', predeterminado:'1'}];
        $scope.a_sucursal_b=[{value:0, text:'Seleccionar', predeterminado:'1'}];

        $scope.addCajas = function () {
            console.log('entro');

            $scope.notaproducto = {
                id_mesa: $scope.rowNotaProducto.length + 1,
                id_sucursal: 0,
                nu_mesa: '',
            };
            console.log($scope.notaproducto);
           $scope.rowNotaProducto.push($scope.notaproducto);
         
        };

        $scope.showStatus = function(item) {
            var selected = [];
            if(item.id_sucursal) {
              selected = $filter('filter')($scope.a_sucursal, {value: item.id_sucursal});
            }
            return selected.length ? selected[0].text : 'Not set';
          };


        $scope.removeProducto = function(index) {
            $scope.rowNotaProducto.splice(index, 1);
          };

        

        serviceAPI.getListaSucursal().then(function(r){
            if(r.data.totalItemCount>0 && r.data.success){
                var a_sucursal=[];
                a_sucursal=_.map(r.data.rows, function(item){
                    return {value:item.id, text:item.no_sucursal};
                });
                $scope.a_sucursal=_.union($scope.a_sucursal, a_sucursal);
                $scope.data.id_sucursal=_.findWhere($scope.a_sucursal);
            }
        });
        
        $scope.guardar=function(eForm){
            var params={orden: $scope.data, detalle: $scope.rowNotaProducto};
            serviceAPI.guardarMesas(params).success(function(r){
                var typeNotification='error';
                if(r.success){
                    typeNotification=(r.affected_rows!=0)?'success':'warning';
                }
                toastr[typeNotification](r.messages, 'Registro de Mesas Correctamente');
                angular.extend($scope.data, $scope.default);
                eForm.$setPristine();
                eForm.$setUntouched();
            });
        };

        $scope.consultar=function(){
            var _this=this;
            if($stateParams.idempleado!=''){
                serviceAPI.consultarUsuario({id:$stateParams.idempleado}).success(function(r){
                    var typeNotification=(!r.success)?'error':'warning';
                    if(r.success && r.num_rows==1){
                        typeNotification='success';
                        angular.extend($scope.data, r.rows);
                        $scope.jTipDocumentos.map(function(a){
                            if(a.id==r.rows.idtipdoc){ $scope.data.idtipdoc=a; }
                        });
                        $scope.jRoles.map(function(b){
                            if(b.idrol_sistema==r.rows.idrol_sistema){ $scope.data.idrol_sistema=b; }
                        });
                    }
                    toastr[typeNotification](r.messages, 'Información de Usuario');
                }); 
            }
        };

        editableOptions.theme = 'bs3';
        editableThemes['bs3'].submitTpl = '<button type="submit" ng-click="calcularImporte(item)" class="btn btn-primary btn-with-icon"><i class="ion-checkmark-round"></i></button>';
        editableThemes['bs3'].cancelTpl = '<button type="button" ng-click="$form.$cancel()" class="btn btn-danger btn-with-icon"><i class="ion-close-round"></i></button>';

    }]);
})();