(function () {
    'use strict';
    angular.module('BlurAdmin.pages.sucursal.sucursales', [])
        .controller('sucursallistCtrl', ['$scope', 'serviceAPI', '$state', 'toastr', '$uibModal', function ($scope, serviceAPI, $state, toastr, $uibModal) {
            $scope.displayed = [];
            $scope.callServer = function (tableState) {
                var params = angular.extend({}, tableState.pagination, tableState.search);
                serviceAPI.listSucursal(params).success(function (r) {
                    if (r.success && r.totalItemCount > 0) {
                        $scope.displayed = r.rows;
                        tableState.pagination.numberOfPages = r.numberOfPages;
                    }
                });
            };
            $scope.agregar = function () {
                $state.go('dashboard.sucursal.sucursales-details');
            };
            $scope.editar = function (id) {
                $state.go('dashboard.sucursal.sucursales-details', { idsucursal: id });
            };

            $scope.eliminar = function (idSucursal) {
                serviceAPI.eliminarSucursal({ id: idSucursal }).success(function (r) {
                    console.log(r);
                    var typenotify = (r.success) ? 'warning' : 'error';
                    if (r.affected_rows == 1) {
                        typenotify = 'success';
                        $scope.displayed = $scope.displayed.filter(function (i) { return (i.id_sucursal != idSucursal) });
                    }
                    toastr[typenotify](r.messages, 'Listado de Sucursales')
                });
            };

            $scope.openModalExcel = function (size, data) {
                $scope.item = data;
                $uibModal.open({
                    animation: true,
                    size: size,
                    templateUrl: "app/pages/sucursal/sucursales/modal/generarSucursalExcel.html",
                    controller: 'sucursalModalctrl',
                    scope: $scope,
                    resolve: {
                        item: function () { }
                    }

                }).result.then(function (data) {
                    angular.extend($scope.data, data);
                }, function () {
                    console.log('Modal dismissed at: ' + new Date());
                });
            }

            $scope.changeStatus = function (idsucursal, actividad, index) {
                serviceAPI.cambiarStatusSucursal({ id: idsucursal, status: actividad }).success(function (r) {
                    var typenotify = 'error';
                    if (r.num_rows == 0) {
                        typenotify = 'warning';
                        $scope.displayed = $scope.displayed.filter(function (i) { return (i.id_sucursal != idsucursal); })
                    }
                    if (r.affected_rows == 1) {
                        typenotify = 'success';
                        $scope.displayed[index].status = r.rows.status;
                    }
                    toastr[typenotify](r.messages, 'Listado de Sucursal');
                });
            };
        }])
        .controller('sucursalModalctrl', ['$scope', 'item', 'Upload', '$http', '$httpParamSerializerJQLike', '$uibModalInstance', 'toastr', '$uibModal', function ($scope, item, Upload, $http, $httpParamSerializerJQLike, $uibModalInstance, toastr, $uibModal) {
            $scope.item = $scope.$parent.item || false;

            $scope.cerrar = function () {
                $uibModalInstance.close();
            }

        }])
        .controller('sucursalesCtrl', ['$scope', 'serviceAPI', 'toastr', '$stateParams', 'baSidebarService', '$timeout', '$log', function ($scope, serviceAPI, toastr, $stateParams, baSidebarService, $timeout, $log) {
            $scope.default = { id_sucursal: 0, no_sucursal: '', co_depart: '', co_provin: '', co_distri: '', no_direccion: '', no_observacion: '', status: 1 };
            $scope.data = { id_sucursal: 0, no_sucursal: '', co_depart: '', co_provin: '', co_distri: '', no_direccion: '', no_observacion: '', status: 1 };
            $scope.cdepartamento = [];
            $scope.cprovincia = [];
            $scope.cdistrito = [];

            $scope.a_departamento = [{ value: 0, text: 'Seleccionar', predeterminado: '1' }];
            $scope.a_provincia = [{ value: 0, text: 'Seleccionar', predeterminado: '1' }];
            $scope.a_distrito = [{ value: 0, text: 'Seleccionar', predeterminado: '1' }];

            serviceAPI.getDepartamento().then(function (r) {
                if (r.data.totalItemCount > 0 && r.data.success) {
                    var a_departamento = [];
                    a_departamento = _.map(r.data.rows, function (item) {
                        return { value: item.id, text: item.no_departamento };
                    });
                    $scope.a_departamento = _.union($scope.a_departamento, a_departamento);
                    $scope.data.co_depart = _.findWhere($scope.a_departamento);
                }
            });

            $scope.getpDepartamento = function (value) {
                serviceAPI.getProvincia(value).then(function (r) {
                    if (r.data.totalItemCount > 0 && r.data.success) {
                        var a_provincia = [];
                        a_provincia = _.map(r.data.rows, function (item) {
                            return { value: item.id, text: item.no_provincia };
                        });
                        $scope.a_provincia = _.union($scope.a_provincia, a_provincia);
                        $scope.data.co_provin = _.findWhere($scope.a_provincia);
                    }
                });
            }
            $scope.getpProvincia = function (value) {
                serviceAPI.getDistrito(value).then(function (r) {
                    if (r.data.totalItemCount > 0 && r.data.success) {
                        var a_distrito = [];
                        a_distrito = _.map(r.data.rows, function (item) {
                            return { value: item.id, text: item.no_distrito };
                        });
                        $scope.a_distrito = _.union($scope.a_distrito, a_distrito);
                        $scope.data.co_distri = _.findWhere($scope.a_distrito);
                    }
                });
            }

            $scope.guardar = function (eForm) {
                serviceAPI.guardarSucursal($scope.data).success(function (r) {
                    var typeNotification = 'error';
                    if (r.success) {
                        typeNotification = (r.affected_rows != 0) ? 'success' : 'warning';
                    }
                    toastr[typeNotification](r.messages, 'Registro de Sucursal Correctamente');
                    angular.extend($scope.data, $scope.default);
                    eForm.$setPristine();
                    eForm.$setUntouched();
                });
            };

            $scope.consultar = function () {
                var _this = this;
                if ($stateParams.idsucursal != '') {
                    serviceAPI.consultarSucursal({ id: $stateParams.idsucursal }).success(function (r) {
                        var typeNotification = (!r.success) ? 'error' : 'warning';
                        if (r.success && r.num_rows == 1) {
                            typeNotification = 'success';
                            angular.extend($scope.data, r.rows);
                            $scope.a_departamento.map(function (a) {
                                if (a.value == r.rows.co_depart) {
                                    $scope.data.co_depart = a.value;
                                }
                            });

                            serviceAPI.getProvincia($scope.data).then(function (r) {
                                if (r.data.totalItemCount > 0 && r.data.success) {
                                    var a_provincia = [];
                                    a_provincia = _.map(r.data.rows, function (item) {
                                        return { value: item.id, text: item.no_provincia };
                                    });
                                    $scope.a_provincia = _.union($scope.a_provincia, a_provincia);
                                    $scope.a_provincia.map(function (b) {
                                        if (b.value == r.rows.co_provin) {
                                            $scope.data.co_provin = b.value;
                                        }
                                    });

                                }
                            });
                            serviceAPI.getDistrito($scope.data).then(function (r) {
                                if (r.data.totalItemCount > 0 && r.data.success) {
                                    var a_distrito = [];
                                    a_distrito = _.map(r.data.rows, function (item) {
                                        return { value: item.id, text: item.no_distrito };
                                    });
                                    $scope.a_distrito = _.union($scope.a_distrito, a_distrito);
                                    $scope.a_distrito.map(function (c) {
                                        if (c.value == r.rows.co_distri) {
                                            $scope.data.co_distri = c.value;
                                        }
                                    });
                                }
                            });


                        }
                        toastr[typeNotification](r.messages, 'Información de la Sucursal');
                    });
                }
            };
        }]);
})();