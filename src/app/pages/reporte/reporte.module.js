(function(){
    'use strict';
    angular.module('BlurAdmin.pages.reportes',[
        'BlurAdmin.pages.reportes.compras',
        'BlurAdmin.pages.reportes.ventas',
        'BlurAdmin.pages.reportes.productos'
    ]).config(['$stateProvider', function($stateProvider){
        $stateProvider.state('dashboard.reportes', {
            url: '/reportes',
            abstract: true,
            template : '<ui-view autoscroll="true" autoscroll-body-top></ui-view>',
            title: 'Reportes',
            sidebarMeta: {
              icon: 'fa fa-line-chart',
              order: 700
            }  
        }).state('dashboard.reportes.compras', {
          url: '/compras',
          templateUrl: 'app/pages/reporte/compras/rptcompras.html',
          title: 'Compras',
          controller: 'rptcomprasCtrl',
          sidebarMeta: {
            order: 100
          }
        }).state('dashboard.reportes.ventas', {
            url: '/ventas',
            templateUrl: 'app/pages/reporte/ventas/rptventas.html',
            title: 'Ventas',
            controller: 'rptventasCtrl',
            sidebarMeta: {
              order: 200
            }
          }).state('dashboard.reportes.productos',{
            url:'/productos',
            templateUrl: 'app/pages/reporte/productos/rptproductos.html',
            title:'Productos',
            controller: 'rptproductosCtrl',
            sidebarMeta:{
                order: 300
            }
          });
    }])
})();