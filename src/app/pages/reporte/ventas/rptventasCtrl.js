(function () {
    'use strict';
    angular.module('BlurAdmin.pages.reportes.ventas', [])
        .controller('rptventasCtrl', ['$scope', 'serviceAPI', 'toastr', function ($scope, serviceAPI, toastr) {
            $scope.default = {tipdocumento: 0,num_doc: '',noclien: '',fec_inicio: '',fec_hasta: '',details_venta: false};
            $scope.data = {tipdocumento: 0,num_doc: '',noclien: '',fec_inicio: '',fec_hasta: '',details_venta: false};
            $scope.open = {fec_inicio: false,fec_hasta: false};
            $scope.jTipDocumentos = [];
            serviceAPI.getTiposDocumentos().success(function (r) {
                if (r.success && r.totalItemCount > 0) {
                    angular.extend($scope.jTipDocumentos, r.rows);
                    r.rows.map(function (s) {
                        if (s.predeterminado == 1) {
                            $scope.data.tipdocumento = s;
                        }
                    });
                }
            });

            $scope.consultar_empleado = function (eForm) {
                if (eForm.num_doc.$invalid || $scope.data.num_doc == "") {
                    if ($scope.data.num_doc == "") {
                        angular.extend($scope.data, {
                            apenom: '',
                            idempleado: 0
                        });
                    }
                    return true;
                }
            };


            $scope.rowVentas=[];
            
            $scope.consultar = function () {
                serviceAPI.consultarVentas($scope.data).success(function (r) {
                    var typeNotification=(!r.success)?'error':'warning';
                    if (r.success && r.totalItemCount > 0) {
                        $scope.rowVentas = r.rows;
                        tableState.pagination.numberOfPages = r.numberOfPages;
                    }
                    toastr[typeNotification](r.messages, 'Listado de Ventas');
                });
            };

        }]);
})();