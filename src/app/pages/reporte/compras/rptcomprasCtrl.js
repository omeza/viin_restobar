(function () {
    'use strict';
    angular.module('BlurAdmin.pages.reportes.compras', [])
        .controller('rptcomprasCtrl', ['$scope', 'serviceAPI', 'toastr', function ($scope, serviceAPI, toastr) {
            $scope.default = {
                tipdocumento: '',
                idempleado: 0,
                num_doc: '',
                apenom: '',
                fec_inicio: '',
                fec_hasta: '',
                details_venta: false
            };
            $scope.data = {
                tipdocumento: 0,
                num_doc: '',
                noclien: '',
                fec_inicio: '',
                fec_hasta: '',
                details_venta: false
            };
            $scope.open = {
                fec_inicio: false,
                fec_hasta: false
            }
            $scope.jTipDocumentos = [];
            serviceAPI.getTiposDocumentos().success(function (r) {
                if (r.success && r.totalItemCount > 0) {
                    angular.extend($scope.jTipDocumentos, r.rows);
                    r.rows.map(function (s) {
                        if (s.predeterminado == 1) {
                            $scope.data.tipdocumento = s;
                        }
                    });
                }
            });

            $scope.consultar_empleado = function (eForm) {
                if (eForm.num_doc.$invalid || $scope.data.num_doc == "") {
                    if ($scope.data.num_doc == "") {
                        angular.extend($scope.data, {
                            apenom: '',
                            idempleado: 0
                        });
                    }
                    return true;
                }
            };


            $scope.rowVentas=[];
            
            $scope.consultar = function () {
                serviceAPI.consultarCompras($scope.data).success(function (r) {
                    var typeNotify = (r.success) ? 'warning' : 'error';
                    if (r.success && r.num_rows == 1) {
                        $scope.rowVentas=r.rows;
                        tableState.pagination.numberOfPages =r.numberOfPages;
                        if (!r.rows.is_vendedor) {
                            r.messages = 'El empleado no es Vendedor';
                        } else {
                            typeNotify = '';
                        }
                    }
                    if (typeNotify != '')
                        toastr[typeNotify](r.messages, 'Consulta de Compras');
                });
            };

        }]);
})();