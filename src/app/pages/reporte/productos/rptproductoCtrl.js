(function(){
    'use strict';
    angular.module('BlurAdmin.pages.reportes.productos', [])
        .controller('rptproductosCtrl', [ '$scope', 'serviceAPI', function($scope, serviceAPI){
            $scope.data={id_categoria:''};
            $scope.jcategoria=[];
            serviceAPI.listarCategoriaProducto().success(function(r){
                console.log(r);
                if(r.success && r.totalItemCount>0){                    
                    angular.extend($scope.jcategoria, r.rows);
                    $scope.data.id_categoria = _.findWhere($scope.jcategoria);
                }
            });
        }]);
})();