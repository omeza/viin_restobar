(function() {
    'use strict';

    angular.module('BlurAdmin.signin')
        .controller('SignInCtrl', ['$scope', '$state', 'AuthenticationService', 'toastr', 'serviceAPI', 'commonService', function($scope, $state, AuthenticationService, toastr, serviceAPI, commonService){
            $scope.data={username:'', clave:''};
            $scope.signIn = function() {
                serviceAPI.signIn($scope.data).success(function(r){
                    AuthenticationService.setLoggedIn(r.auth);                    
                    if(r.auth){                        
                        AuthenticationService.setRol(r.rows.no_rol_sistema);
                        AuthenticationService.setUser($scope.data.username);
                        AuthenticationService.setToken(r.token);
                        AuthenticationService.setStates(r.rows.states);
                        $state.go('dashboard', {defaultState: r.rows.name_state});
                       
                    }else{
                        toastr.error("Datos de acceso inválidos", 'Error');
                    }
                });
            };  
        }]);

})();
