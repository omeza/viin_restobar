(function () {
    'use strict';
    angular.module('BlurAdmin.pages.producto.productoMenu', [])        
        .controller('productoMenuCtrl', ['$scope','Upload','$uibModal', 'serviceAPI', 'toastr', '$stateParams', 'baSidebarService', '$timeout', '$log', function ($scope,Upload, $uibModal, serviceAPI, toastr, $stateParams, baSidebarService, $timeout, $log) {
            $scope.default = { id_producto: 0, nu_cantidad_platos: 0};
            $scope.data = { id_producto: 0, nu_cantidad_platos: 0};

            $scope.jproducto = [];
            $scope.displayed = [];
            $scope.valores=[];
           
                serviceAPI.getComboProducto().then(function (r) {
                    if (r.data.totalItemCount > 0 && r.data.success) {
                        var jproducto = [];
                        jproducto = _.map(r.data.rows, function (item) {
                            return { value: item.id, text: item.no_producto };
                        });
                        $scope.jproducto = _.union($scope.jproducto, jproducto);
                        $scope.data.id_producto = _.findWhere($scope.jproducto);
                    }
                });
                
                serviceAPI.getCabeceraMenu().success(function (r) {     
                    if (r.success && r.totalItemCount > 0) {
                        for(var i = 0; i < r.rows.length; i++){    
                            serviceAPI.getCabeceraMenuDetalle({id_producto_menu: r.rows[i].id_producto_menu,no_producto:r.rows[i].no_producto, nu_cantidad_platos:r.rows[i].nu_cantidad_platos}).success(function (b) {                  
                                console.log(b);
                                if (b.totalItemCount > 0 && b.success) {
                                    console.log(b);
                                    $scope.valores.push({no_producto:b.no_producto, nu_cantidad_platos:b.nu_cantidad_platos, detalle:b.rows});                               
                                }
                            });
                        }
                    }
                });
                $scope.ListatInsumos=function(){
                    serviceAPI.getCabeceraMenu().success(function (r) {     
                        if (r.success && r.totalItemCount > 0) {
                            for(var i = 0; i < r.rows.length; i++){    
                                serviceAPI.getCabeceraMenuDetalle({id_producto_menu: r.rows[i].id_producto_menu,no_producto:r.rows[i].no_producto, nu_cantidad_platos:r.rows[i].nu_cantidad_platos}).success(function (b) {                  
                                    console.log(b);
                                    if (b.totalItemCount > 0 && b.success) {
                                        console.log(b);
                                        $scope.valores.push({no_producto:b.no_producto, nu_cantidad_platos:b.nu_cantidad_platos, detalle:b.rows});                               
                                    }
                                });
                            }
                        }
                    });
                }

            $scope.guardar=function(eForm){
                serviceAPI.guardarProductoMenu($scope.data).success(function(r){
                    var typeNotification='error';
                    if(r.success){
                        typeNotification=(r.affected_rows!=0)?'success':'warning';
                        $scope.valores=[];
                        $scope.ListatInsumos();
                    }
                    toastr[typeNotification](r.messages, 'Registro el Menu Correctamente');
                    angular.extend($scope.data, $scope.default);
                });
            };
        }]);
})();