(function(){
    'use strict';
    angular.module('BlurAdmin.pages.producto.productoCategoria', [])    
    
    .controller('productoCategorialistCtrl', [ '$scope', 'serviceAPI', 'toastr', '$stateParams', 'baSidebarService', '$timeout', '$log', function($scope, serviceAPI, toastr, $stateParams, baSidebarService, $timeout, $log){
        var vm=this;
        $scope.default={id_categoria_producto:0, no_categoria_producto:''};
        $scope.data={id_categoria_producto:0, no_categoria_producto:''};
        $scope.displayed=[];
        $scope.callServer=function(tableState){
            var params = angular.extend({}, tableState.pagination, tableState.search);
            serviceAPI.getProductoCategoria(params).success(function(r){
                if(r.success && r.totalItemCount>0){
                    $scope.displayed=r.rows;
                    tableState.pagination.numberOfPages =r.numberOfPages;
                }
            });
        };
        
        
        $scope.guardar=function(eForm){
            serviceAPI.guardarProductoCategoria($scope.data).success(function(r){
                var typeNotification='error';
                if(r.success){
                    typeNotification=(r.affected_rows!=0)?'success':'warning';
                }
                toastr[typeNotification](r.messages, 'Registro de Categoria Producto');
                angular.extend($scope.data, $scope.default);
                eForm.$setPristine();
                eForm.$setUntouched();                 
                $scope.callServer([]);
            });
        };

    }]);
})();