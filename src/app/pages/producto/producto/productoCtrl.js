(function () {
    'use strict';
    angular.module('BlurAdmin.pages.producto.producto', [])
        .controller('productolistCtrl', ['$scope', 'serviceAPI', '$state', function ($scope, serviceAPI, $state) {
            
            $scope.displayed = [];
            $scope.callServer = function (tableState) {
                var params = angular.extend({}, tableState.pagination, tableState.search);
                serviceAPI.getProducto(params).success(function (r) {
                    if (r.success && r.totalItemCount > 0) {
                        $scope.displayed = r.rows;
                        tableState.pagination.numberOfPages = r.numberOfPages;
                    }
                });
            };
            $scope.agregar = function () {
                $state.go('dashboard.producto.producto-details');
            };
            $scope.editar = function (id) {
                $state.go('dashboard.producto.producto-details', { idProducto: id });
            };          
            
            $scope.changeStatus = function (id) {

            };
        }])
        .controller('productoModelCtrl', ['$scope', 'serviceAPI', 'item', '$http', '$httpParamSerializerJQLike', '$uibModalInstance', function ($scope, serviceAPI, item, $http, $httpParamSerializerJQLike, $uibModalInstance) {

            $scope.item = $scope.$parent.item || false;
            $scope.status = 0;
            $scope.respuesta = 0;
            $scope.archivo = '';
            $scope.url = '';

            $scope.displayed = [];
            $scope.callServer = function (tableState) {
                var params = angular.extend({}, tableState.pagination, tableState.search);
                serviceAPI.getInsumos(params).success(function (r) {
                    if (r.success && r.totalItemCount > 0) {
                        $scope.displayed = r.rows;
                        tableState.pagination.numberOfPages = r.numberOfPages;
                    }
                });
            };
            $scope.obtenerInsumo = function (insumo) {
                $uibModalInstance.close(insumo);
            };

        }])
        .controller('productoCtrl', ['$scope','Upload','$uibModal', 'serviceAPI', 'toastr', '$stateParams', 'baSidebarService', '$timeout', '$log', function ($scope,Upload, $uibModal, serviceAPI, toastr, $stateParams, baSidebarService, $timeout, $log) {
            $scope.default = { id_producto: 0, id_categoria_producto: 0, id_sucursal:0,id_destino: 0, id_usuario: 1, no_producto: '', nu_precio: 0, ruta_imagen: '' };
            $scope.data = { id_producto: 0, id_categoria_producto: 0,id_sucursal:0, id_destino: 0, id_usuario: 1, no_producto: '', nu_precio: 0, ruta_imagen: '' };

            $scope.defaultp = { id_insumo: 0,  nu_cantidad: 0, no_categoria_insumo: '', no_insumo: '', no_medida: '', no_unidad: '', nu_cantidad_medida: 0, nu_precio_sugerido: 0, nu_stock_minimo: 0 };
            $scope.datap = { id_insumo: 0, nu_cantidad: 0, no_categoria_insumo: '', no_insumo: '', no_medida: '', no_unidad: '', nu_cantidad_medida: 0, nu_precio_sugerido: 0, nu_stock_minimo: 0 };

            $scope.dataproducto = {id_producto:0,   no_producto:'',no_categoria:'',nu_precio:0,ruta_imagen:''};
            $scope.rowCollection = [];
            $scope.rowInsumos = [];
            $scope.jcategoria = [];
            $scope.jdestino = [];
            $scope.jsucursal = [];
            $scope.selection=[];

            $scope.toggleSelection = function toggleSelection(fruitName) {
                console.log(fruitName);
                var idx = $scope.selection.indexOf(fruitName);
                if (idx > -1) {
                  $scope.selection.splice(idx, 1);
                }
                else {
                  $scope.selection.push(fruitName);
                }
                console.log($scope.selection);
            };   
            $scope.consultar=function(){
                if($stateParams.idProducto!=''){
                    serviceAPI.consultarProductoV({id:$stateParams.idProducto}).success(function(r){
                        var typeNotification=(!r.success)?'error':'warning';
                        if(r.success && r.num_rows==1){
                            typeNotification='success';
                            angular.extend($scope.dataproducto, r.rows);
                            serviceAPI.getInsumosProducto({id:$stateParams.idProducto}).success(function (r) {
                                if (r.success && r.totalItemCount > 0) {
                                    $scope.rowInsumos = r.rows;
                                    tableState.pagination.numberOfPages = r.numberOfPages;
                                }
                            });  
                        }
                        toastr[typeNotification](r.messages, 'Información del Producto');
                    }); 
                }
            };
            serviceAPI.getListaSucursal().then(function(r){
                if(r.data.totalItemCount>0 && r.data.success){
                    var jsucursal=[];
                    jsucursal=_.map(r.data.rows, function(item){
                        return {value:item.id, text:item.no_sucursal};
                    });
                    $scope.jsucursal=_.union($scope.jsucursal, jsucursal);
                    $scope.data.id_sucursal=_.findWhere($scope.jsucursal);
                }
            });            

            $scope.uploadFiles = function(file, errFiles) {
                console.log(file);
                $scope.f = file;
                $scope.errFile = errFiles && errFiles[0];
            }
            serviceAPI.listarCategoriaProducto().then(function (r) {
                if (r.data.totalItemCount > 0 && r.data.success) {
                    var jcategoria = [];
                    jcategoria = _.map(r.data.rows, function (item) {
                        return { value: item.id, text: item.no_categoria_producto };
                    });
                    $scope.jcategoria = _.union($scope.jcategoria, jcategoria);
                    $scope.data.id_categoria_producto = _.findWhere($scope.jcategoria);
                }
            });

            $scope.openInsumo = function (size, item) {
                $scope.item = item;
                $uibModal.open({
                    animation: true,
                    size: size,
                    templateUrl: "app/pages/producto/producto/modal/listInsumos.html",
                    controller: 'productoModelCtrl',
                    scope: $scope,
                    resolve: {
                        item: function () { }
                    }
                }).result.then(function (data) {
                    angular.extend($scope.datap, data);
                }, function () {
                    console.log('Modal dismissed at: ' + new Date());
                });
            };
            var $item = 0;
            $scope.addItem = function () {
                
                $scope.rowCollection.push(_.extend({
                    id: ++$item,
                    id_insumo:  $scope.datap.id_insumo,
                    no_insumo: $scope.datap.no_insumo,
                    no_medida: $scope.datap.no_medida,
                    nu_cantidad: $scope.datap.nu_cantidad,
                },
                    $scope.datap)
                );
                angular.extend($scope.datap, $scope.defaultp);
                console.log($scope.rowCollection);

            };

            $scope.quitar_producto = function (id) {
                $scope.rowCollection = $scope.rowCollection.filter(function (item) {
                    return item.id != id;
                });
            };

            serviceAPI.listarDestino().then(function (r) {
                if (r.data.totalItemCount > 0 && r.data.success) {
                    var jdestino = [];
                    jdestino = _.map(r.data.rows, function (item) {
                        return { value: item.id, text: item.no_destino };
                    });
                    $scope.jdestino = _.union($scope.jdestino, jdestino);
                    $scope.data.id_destino = _.findWhere($scope.jdestino);
                }
            });

            $scope.guardar = function (eForm) {
                var data=angular.extend({oper:'guardar_producto'}, {orden: $scope.data},{detalleLocal:$scope.selection} ,{detalle:$scope.rowCollection} , {file:$scope.f});
                Upload.upload({
                    url: 'api/productos.php',
                    method: 'POST',
                    headers: {'Content-Type': undefined},
                    data: data,
                }).then(function (r) {
                    var typeNotification = 'error';
                    if (r.data.success) {
                        typeNotification = (r.data.affected_rows != 0) ? 'success' : 'warning';
                    }
                    toastr[typeNotification](r.data.messages, 'Registro Producto Correctamente');
                    angular.extend($scope.data, $scope.default);
                    angular.extend($scope.datap, $scope.defaultp);
                    $scope.rowCollection = [];
                    $scope.selection = [];
                    eForm.$setPristine();
                    eForm.$setUntouched();
                }, function (response) {
                    toastr.error(response.data.message, 'Error');
                });
            };

            $scope.consultarV=function(){
                if($stateParams.idProducto!=''){
                    serviceAPI.consultarProductoV({id:$stateParams.idProducto}).success(function(r){
                        var typeNotification=(!r.success)?'error':'warning';
                        if(r.success && r.num_rows==1){
                            typeNotification='success';
                            angular.extend($scope.data, r.rows);
                            $scope.uploadFiles($scope.data.ruta_imagen,'');

                            $scope.jcategoria.map(function (a) {
                                if (a.value == r.rows.id_categoria_producto) {
                                     $scope.data.id_categoria_producto = a.value; 
                                    }
                            });
                            $scope.jdestino.map(function (b) {
                                if (b.value == r.rows.id_destino) {
                                    $scope.data.id_destino = b.value;
                                }
                            });

                            serviceAPI.getInsumosProducto({id:$stateParams.idProducto}).success(function (r) {
                                if (r.success && r.totalItemCount > 0) {
                                    $scope.rowCollection = r.rows;
                                }
                            });  
                            serviceAPI.getSucursalProducto({id:$stateParams.idProducto}).success(function (r) {                                
                                if (r.success && r.totalItemCount > 0) {    
                                    $scope.jsucursal.map(function (c) {
                                        for (var i=0; i < r.rows.length; i++) {  
                                            if (c.value == r.rows[i].id_sucursal) {
                                                $scope.toggleSelection(c.value);
                                            }
                                          };
                                        
                                    });
                                }
                            });  
                        }
                        toastr[typeNotification](r.messages, 'Información del Producto');
                    }); 
                }
            };

        }]);
})();