(function () {
    'use strict';
    angular.module('BlurAdmin.pages.producto', [
        'BlurAdmin.pages.producto.producto',
        'BlurAdmin.pages.producto.productoCategoria',
        'BlurAdmin.pages.producto.productoMenu'
    ]).config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
          .state('dashboard.producto', {
            url: '/producto',
            template : '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
            abstract: true,
            title: 'Producto',
            sidebarMeta: {
              icon: 'ion-beer',
              order: 500,
            }
          }).state('dashboard.producto.producto', {
            url: '/producto',
            templateUrl: 'app/pages/producto/producto/list/productoList.html',
            controller: 'productolistCtrl',
            title: 'Producto',
            sidebarMeta: {
                order: 100
            }
        }).state('dashboard.producto.producto-details', {
            url: '/producto/details/:idProducto',
            templateUrl: 'app/pages/producto/producto/producto.html',
            controller: "productoCtrl",
            title: 'Producto',
            params:{
              idProducto:''
            }
        }).state('dashboard.producto.producto-details2', {
          url: '/producto/details2/:idProducto',
          templateUrl: 'app/pages/producto/producto/productoItem.html',
          controller: "productoCtrl",
          title: 'Producto',
          params:{
            idProducto:''
          }
      }).state('dashboard.producto.productoCategoria', {
            url: '/productoCategoria',
            templateUrl: 'app/pages/producto/productoCategoria/productoCategoria.html',
            controller: 'productoCategorialistCtrl',
            title: 'Producto Categoria',
            sidebarMeta: {
                order: 200
              }
        }).state('dashboard.producto.productoCategoria-details', {
            url: '/productoCategoria/details/:idcajas',
            templateUrl: 'app/pages/producto/productoCategoria/productoCategoria.html',
            controller: 'productoCategoriaCtrl',
            controllerAs: 'vm',
            title: 'Producto Categoria',
            params:{ idrol:''}
        }).state('dashboard.producto.productoMenu', {
          url: '/productoMenu',
          templateUrl: 'app/pages/producto/productoMenu/list/productoMenuList.html',
          controller: 'productoMenuCtrl',
          title: 'Menu Producto',
          sidebarMeta: {
              order: 200
            }
      }).state('dashboard.producto.productoMenu-details', {
          url: '/productoMenu/details/:idProducto',
          templateUrl: 'app/pages/producto/productoMenu/productoMenu.html',
          controller: 'productoMenuCtrl',
          controllerAs: 'vm',
          title: 'Menu Producto',
          params:{ idrol:''}
      });
    }
  })();
  