(function(){
    'use strict';
    angular.module('BlurAdmin.pages.configuracion.dptopais', [])
    .directive('dptoExist', [ '$q', 'serviceAPI', function( $q, serviceAPI){
        return {
            require : 'ngModel',
            transclude : true,
            scope :{ idpais:'='},
            link : function($scope, $element, $attrs, ngModel){
                ngModel.$asyncValidators.dptoExist=function(modelValue, viewValue){
                    return serviceAPI.existNomDpto({nombre:viewValue, id: $scope.idpais}).success(function(r){
                            if(r.success && r.num_rows!=0){
                                return $q.reject();
                            }
                            return true;
                        })
                }
            }
        }
    }])
    .controller('DptoListCtrl', [ '$scope', 'toastr', '$state', 'serviceAPI', function($scope, toastr, $state, serviceAPI){
        $scope.smartTablePageSize = 10;
        $scope.data={idpais:''};
        $scope.jPaises=[];
        $scope.displayed=[];
        $scope.tableState=false;
        $scope.callServer=function(tableState){
            if($scope.data.idpais=='')
                return true;
            $scope.tableState=tableState;
            var params = angular.extend({id:$scope.data.idpais.idpais}, $scope.tableState.pagination, $scope.tableState.search);
            serviceAPI.getDepPais(params).success(function(r){
                if(r.success && r.totalItemCount>0){
                    $scope.displayed=r.rows;
                    tableState.pagination.numberOfPages =r.numberOfPages;
                }    
            });
        };

        serviceAPI.getPaises({}).success(function(r){
            if(r.success && r.totalItemCount>0){
                $scope.jPaises=r.rows;
                r.rows.map(function(item){ if(item.predeterminado=='1'){ $scope.data.idpais=item; } });
            }    
        });
        $scope.cargar_estados=function(){
            $scope.callServer($scope.tableState);
        };
        $scope.agregar=function(){
            $state.go('dashboard.configuracion.departamentopais-details');
        };
        $scope.editar=function(id){
            $state.go('dashboard.configuracion.departamentopais-details', {iddpto: id});
        };
        $scope.changeStatus=function(iddpto, actividad, index){
            serviceAPI.statusDptoPais({id:iddpto, status:actividad}).success(function(r){
                var typenotify='error';
                if(r.num_rows==0){
                    typenotify='warning';
                    $scope.displayed=$scope.displayed.filter(function(i){ return (i.id!=idtipdoc);})
                }
                if(r.affected_rows==1){
                    typenotify='success';
                    $scope.displayed[index].status=r.rows.status;
                }
                toastr[typenotify](r.messages, 'Listado de Departamentos Pais');
            });
        };
        $scope.eliminar=function(iddpto){
            serviceAPI.borrarDptoPais({id:iddpto}).success(function(r){
                var typenotify=(r.success)?'warning':'error';
                if(r.affected_rows==1){
                    typenotify='success';
                    $scope.displayed=$scope.displayed.filter(function(i){ return (i.idestados_pais!=iddpto)});
                }
                toastr[typenotify](r.messages, 'Listado de Departamentos Pais')
            });
        };


    }])
    .controller('DptoPaisCtrl', [ '$scope', 'serviceAPI', 'toastr', '$stateParams', function($scope, serviceAPI, toastr, $stateParams){
            $scope.default={id_dpto:0, id_pais: '', nom_estado:'', status:true};
            $scope.data={id_dpto:0, id_pais:'', nom_estado:'', status:true};
            $scope.jPais=[];
            $scope.messages={
                id_pais:{
                    required: 'Campo Obligatorio'
                },
                nom_estado:{
                    required: 'Campo Obligatorio',
                    dptoExist: 'Pertenece a otro registro'
                }
            };

            serviceAPI.getPaises({}).success(function(r){
                if(r.success && r.totalItemCount>0){
                    angular.extend($scope.jPais, r.rows);
                }
            });

            $scope.guardar=function(eForm){
                serviceAPI.guardarDptoPais($scope.data).success(function(response){
                        var typeNotification='error';
                        if(response.success){
                            typeNotification=(response.affected_rows!=0)?'success':'warning';
                        }
                        toastr[typeNotification](response.messages, 'Registro de Departamentos');
                        angular.extend($scope.data, $scope.default);
                        eForm.$setPristine();
                        eForm.$setUntouched();
                    });
            };

            $scope.consultar=function(){
                if($stateParams.iddpto=='')
                    return true;
                serviceAPI.consultarDptoPais({id: $stateParams.iddpto}).success(function(r){
                    if(r.success && r.num_rows==1){
                        angular.extend($scope.data, r.rows);
                        $scope.jPais.map(function(item){
                            if(item.idpais==r.rows.id_pais){
                                $scope.data.id_pais=item;
                            }
                        });
                    }else{
                        toastr.warning(r.messages, 'Información de Departamentos');
                    }
                });                
            };
        }]);
})();