(function(){
    'use strict';
    angular.module('BlurAdmin.pages.configuracion.empresa', [])
    .controller('empresaCtrl', [ '$scope', 'serviceAPI', 'toastr', '$stateParams', 'commonService', function($scope, serviceAPI, toastr, $stateParams, commonService){
        var empresa=commonService.getEmpresa();
        $scope.data={idtipdoc: '', num_doc: empresa.num_documento, nombre_empresa: empresa.razon_social, nombre_comercial: empresa.sucursal, direccion: empresa.direc_fiscal, email: empresa.email, telefono: empresa.telefono};
        $scope.idtipdoc='';
        $scope.jDocumentos=[];
        $scope.messages={
            idtipdoc:{ required: 'Campo Obligatorio' },
            num_doc:{ required: 'Campo Obligatorio'},
            nombre_empresa:{required: 'Campo Obligatorio'}
        };

        serviceAPI.getTiposDocumentos().success(function(r){
            if(r.success && r.totalItemCount>0){
                angular.extend($scope.jDocumentos, r.rows);
                if(empresa.desc_documento==''){ return true;}
                r.rows.map(function(item){
                    if(item.nombre==empresa.desc_documento && $scope.data.idtipdoc==''){ 
                        $scope.data.idtipdoc=item;
                        $scope.idtipdoc=item;
                    }
                })
            }
        });
            
        $scope.guardar=function(eForm){
            serviceAPI.guardarEmpresa($scope.data).success(function(r){
                var typeNotification='error';
                if(r.success)
                    typeNotification=(r.affected_rows==1)?'success':'warning';
                if(r.affected_rows==1){
                    commonService.setEmpresa(r.data);
                }else{
                    $scope.data={idtipdoc: '', num_doc: empresa.num_documento, nombre_empresa: empresa.razon_social, nombre_comercial: empresa.sucursal, direccion: empresa.direc_fiscal, email: empresa.email, telefono: empresa.telefono};
                }
                toastr[typeNotification](r.messages, 'Datos de la Empresa');
                angular.extend($scope.data, $scope.default);
                eForm.$setPristine();
                eForm.$setUntouched();
            });
        };
    }]);
})();