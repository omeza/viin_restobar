(function(){
    'use strict';
    angular.module('BlurAdmin.pages.configuracion.tipdocumento', [])
    .directive('tipdocExist', [ '$q', 'serviceAPI', function($q, serviceAPI){
        return {
            require : 'ngModel',
            transclude : true,
            scope :{ idtipdoc:'='},
            link : function($scope, $element, $attrs, ngModel){
                ngModel.$asyncValidators.tipdocExist=function(modelValue, viewValue){
                    return serviceAPI.existTipDocumento({nombre:viewValue, id: $scope.idtipdoc}).then(function(response){
                        if(response.data.success && response.data.num_rows!=0){
                            return $q.reject();
                        }
                        return $q.resolve();
                    });
                }
            }
        }
    }])
    .controller('tipdocumentListCtrl', [ '$scope', 'serviceAPI', '$state', 'toastr', function($scope, serviceAPI, $state, toastr){
        $scope.smartTablePageSize = 10;
        $scope.displayed=[];
        $scope.callServer=function(tableState){
            var params = angular.extend({}, tableState.pagination, tableState.search);
            serviceAPI.getTiposDocumentos(params).success(function(r){
                if(r.success && r.totalItemCount>0){
                    $scope.displayed=r.rows;
                    tableState.pagination.numberOfPages =r.numberOfPages;
                }    
            });
        };
        

        $scope.eliminar=function(idtipdoc){
            serviceAPI.eliminarTipDocumento({id:idtipdoc}).success(function(r){
                var typenotify=(r.success)?'warning':'error';
                if(r.affected_rows==1){
                    typenotify='success';
                    $scope.displayed=$scope.displayed.filter(function(i){ return (i.id!=idtipdoc)});
                }
                toastr[typenotify](r.messages, 'Listado de Tipo de Documentos')
            });
        };

        $scope.change_status=function(idtipdoc, actividad, index){
            serviceAPI.statusTipDocumento({id:idtipdoc, status:actividad}).success(function(r){
                var typenotify='error';
                if(r.num_rows==0){
                    typenotify='warning';
                    $scope.displayed=$scope.displayed.filter(function(i){ return (i.id!=idtipdoc);})
                }
                if(r.affected_rows==1){
                    typenotify='success';
                    $scope.displayed[index].status=r.rows.status;
                }
                toastr[typenotify](r.messages, 'Listado de Tipo de Documentos');
            });
        };

        $scope.agregarTipDocumento=function(){
            $state.go('dashboard.configuracion.tipodocumento-details');
        };

        $scope.editarTipDocumento=function(id){
            $state.go('dashboard.configuracion.tipodocumento-details', {idtipdoc: id});
        };

    }])
    .controller('tipdocumCtrl', [ '$scope', 'serviceAPI', 'toastr', '$stateParams', function($scope, serviceAPI, toastr, $stateParams){
        $scope.default={id_tipdoc:0, desc_documento: '', predeterminado: false, status:true};
        $scope.data={id_tipdoc:0, desc_documento: '', predeterminado: false, status:true};
        $scope.existdefault=false;
        $scope.messages={
            desc_documento:{
                required: 'Campo Obligatorio',
                tipdocExist: 'Pertenece a otro registro'
            }
        };
        
        serviceAPI.gettipdocpred().then(function(response){
            $scope.existdefault=(response.data.success && response.data.num_rows==1);
        });

        $scope.guardar=function(eForm){
            serviceAPI.guardarTipDocumento($scope.data).success(function(r){
                var typeNotification='error';
                if(r.success)
                    typeNotification=(r.affected_rows==1)?'success':'warning';
                toastr[typeNotification](r.messages, 'Registro de Tipo de Documento');
                angular.extend($scope.data, $scope.default);
                eForm.$setPristine();
                eForm.$setUntouched();
            });
        };

        $scope.consultar=function(){
            if($stateParams.idtipdoc=='')
                return true;
            serviceAPI.constularTipDocumento({id: $stateParams.idtipdoc}).success(function(r){
                if(r.success && r.num_rows==1){
                    angular.extend($scope.data, r.rows);
                }else{
                    toastr.warning(r.messages, 'Información de Tipo de Documento');
                }
            }); 
        };
    }]);
})();