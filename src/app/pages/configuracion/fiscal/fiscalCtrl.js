(function(){
    'use strict';
    angular.module('BlurAdmin.pages.configuracion.fiscal', [])
    .controller('fiscalCtrl', [ '$scope', 'serviceAPI', 'toastr', '$stateParams', 'commonService', function($scope, serviceAPI, toastr, $stateParams, commonService){
        var config=commonService.getConfig();
        $scope.data={ abrev_impuesto: '',  porc_impuesto: '',  idmoneda: '',  num_autorizacion: '', init_factura: '', init_boleta: '', autoidproducto: '',inicia_codigo: ''};
        angular.extend($scope.data, config);
        $scope.jMonedas=[];
        $scope.jImpresoras=[];
        $scope.idmoneda='';
        serviceAPI.getMonedas().success(function(r){
            if(r.success && r.totalItemCount>0){
                angular.extend($scope.jMonedas, r.rows);
                if(config.idmoneda==0){ return true;}
                r.rows.map(function(item){
                    if(item.idmoneda==config.idmoneda && $scope.idmoneda==''){ 
                        $scope.data.idmoneda=item;
                        $scope.idmoneda=item;
                    }
                })
            }
        });
        $scope.guardar=function(eForm, grupo){
            switch(grupo){
                case '0':
                    var params={abrev_impuesto: $scope.data.abrev_impuesto,  porc_impuesto: $scope.data.porc_impuesto,  idmoneda: $scope.data.idmoneda,  num_autorizacion: $scope.data.num_autorizacion, grupo:'0'};
                break;
                case '1':
                    var params={init_factura: $scope.data.init_factura, init_boleta: $scope.data.init_boleta, autoidproducto: $scope.data.autoidproducto, inicia_codigo: $scope.data.inicia_codigo, grupo:'1'};
                break;
            }
            serviceAPI.guardarConfig(params).success(function(r){
                var typeNotification='error';
                if(r.success){ typeNotification=(r.affected_rows==1)?'success':'warning';}
                if(typeNotification!='success'){
                    angular.extend($scope.data, config);
                    $scope.data.idmoneda=$scope.idmoneda;
                }else{ 
                    $scope.idmoneda=$scope.data.idmoneda;
                    commonService.updateConfig(params); 
                }
                toastr[typeNotification](r.messages, 'Datos de Configuración');
                angular.extend($scope.data, $scope.default);
                eForm.$setPristine();
                eForm.$setUntouched();
            });
        };
        serviceAPI.getPrinters().success(function(r){
            if(r.rows.length>0){$scope.jImpresoras=r.rows;}
        });
    }]);
})();