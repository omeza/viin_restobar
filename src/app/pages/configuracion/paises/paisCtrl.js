(function(){
    'use strict';
    angular.module('BlurAdmin.pages.configuracion.pais', [])
    .directive('paisExist', [ '$q', 'serviceAPI', function($q, serviceAPI){
        return {
            require : 'ngModel',
            transclude : true,
            scope :{ codpais:'='},
            link : function($scope, $element, $attrs, ngModel){
                ngModel.$asyncValidators.paisExist=function(modelValue, viewValue){
                    return serviceAPI.existsNomPais({nombre:viewValue, id: $scope.codpais}).success(function(r){
                            if(r.success && r.num_rows!=0){
                                return $q.reject();
                            }
                            return true;
                        })
                }
            }
        }
    }])
    .controller('paisListCtrl', [ '$scope', 'serviceAPI', '$state', 'toastr', function($scope, serviceAPI, $state, toastr){
        $scope.smartTablePageSize = 10;
        $scope.isDefault=0;
        $scope.displayed=[];
        $scope.callServer=function(tableState){
            var params = angular.extend({}, tableState.pagination, tableState.search);
            serviceAPI.getPaises(params).success(function(r){
                if(r.success && r.totalItemCount>0){
                    $scope.displayed=r.rows;
                    tableState.pagination.numberOfPages =r.numberOfPages;
                    r.rows.map(function(item){ if(item.predeterminado=='1' && $scope.isDefault==0){ $scope.isDefault=item.idpais; }});
                }    
            });
        };
        
        $scope.eliminar=function(idpais){
            serviceAPI.borrarPais({id:idpais}).success(function(r){
                var typenotify=(r.success)?'warning':'error';
                if(r.affected_rows==1){
                    typenotify='success';
                    $scope.displayed=$scope.displayed.filter(function(i){ return (i.id!=idpais)});
                }
                toastr[typenotify](r.messages, 'Listado de Paises')
            });
        };

        $scope.setdefault=function(idpais, index){
            serviceAPI.asignDefaultpais({id:idpais}).success(function(r){
                var typenotify='error';
                if(r.affected_rows!=1){
                    typenotify='warning';
                    $scope.displayed=$scope.displayed.filter(function(i){ return (i.id!=idtipdoc);})
                }else{
                    typenotify='success';
                    $scope.displayed[index].predeterminado='1';
                    $scope.isDefault=idpais;
                }
                toastr[typenotify](r.messages, 'Listado de Paises');
            });
        };

        $scope.agregar=function(){
            $state.go('dashboard.configuracion.pais-details');
        };

        $scope.editar=function(id){
            $state.go('dashboard.configuracion.pais-details', {idpais: id});
        };

    }])
    .controller('paisCtrl', [ '$scope', 'serviceAPI', 'toastr', '$stateParams', function($scope, serviceAPI, toastr, $stateParams){
        $scope.default={id_pais:0, nom_pais: '', pref_inter: '', default:false};
        $scope.data={id_pais:0, nom_pais: '', pref_inter: '', default:false};
        $scope.messages={
            nom_pais:{
                required: 'Campo Obligatorio',
                paisExist: 'Pais pertenece a otro registro'
            },
            pref_inter:{
                required: 'Campo Obligatorio',
                prefExist: 'Prefijo pertenece a otro registro'
            }
        };
        
        $scope.guardar=function(eForm){
            serviceAPI.guardarPais($scope.data).success(function(r){
                    var typeNotification='error';
                    if(r.success){
                        typeNotification=(r.affected_rows!=0)?'success':'warning';
                    }
                    toastr[typeNotification](r.messages, 'Registro de Pais');
                    angular.extend($scope.data, $scope.default);
                    if($stateParams.codigo===undefined){
                        eForm.$setPristine();
                        eForm.$setUntouched();
                    }
                });
        };

        $scope.consultar=function(){
            if($stateParams.idpais!=''){
                serviceAPI.getPais({id: $stateParams.idpais}).success(function(r){
                    if(r.success && r.num_rows==1){
                        angular.extend($scope.data, r.rows);
                    }else{
                        toastr.warning(r.messages, 'Información de Paises');
                    }
                }); 
            }
        };
    }]);
})();