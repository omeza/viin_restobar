(function () {
    'use strict';
    angular.module('BlurAdmin.pages.configuracion', [
        'BlurAdmin.pages.configuracion.pais',
        'BlurAdmin.pages.configuracion.dptopais',
        'BlurAdmin.pages.configuracion.tipdocumento',
        'BlurAdmin.pages.configuracion.empresa',
        'BlurAdmin.pages.configuracion.fiscal'
    ]).config([ '$stateProvider', function($stateProvider){
        $stateProvider.state('dashboard.configuracion', {
            url: '/configuracion',
            template : '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
            abstract: true,
            title: 'Configuracion',
            sidebarMeta: {
              icon: 'fa fa-cog',
              order: 1800,
            },
          }).state('dashboard.configuracion.paises', {
            url: '/paises',
            templateUrl: 'app/pages/configuracion/paises/list/paisList.html',
            controller: "paisListCtrl",
            title: 'Paises',
            sidebarMeta: {
              order: 100
            }
          }).state('dashboard.configuracion.pais-details', {
            url: '/paises/details/:idpais',
            templateUrl: 'app/pages/configuracion/paises/pais.html',
            controller: "paisCtrl",
            title: 'Pais',
            params: {
                idpais: ''
            }
        }).state('dashboard.configuracion.departamentospais', {
            url: '/dptospais',
            templateUrl: 'app/pages/configuracion/departamentospais/list/departamentoList.html',
            controller: "DptoListCtrl",
            title: 'Departamentos - Pais',
            sidebarMeta: {
                order: 300
            }
        }).state('dashboard.configuracion.departamentopais-details', {
            url: '/dptospais/details/:iddpto',
            templateUrl: 'app/pages/configuracion/departamentospais/dptopais.html',
            controller: "DptoPaisCtrl",
            title: 'Departamento/Pais',
            params: {
                iddpto:''
            }
        }).state('dashboard.configuracion.tiposdocumentos', {
            url: '/tiposdocumentos',
            templateUrl: 'app/pages/configuracion/tipodocumentos/list/tipodocumentosList.html',
            controller: "tipdocumentListCtrl",
            title: 'Tipo Documentos',
            sidebarMeta: {
              order: 100
            }
          }).state('dashboard.configuracion.tipodocumento-details', {
            url: '/tipodocumento/details/:idtipdoc',
            templateUrl: 'app/pages/configuracion/tipodocumentos/tipodocumento.html',
            controller: "tipdocumCtrl",
            title: 'Tipo Documentos',
            params: {
                idtipdoc:''
            }
        }).state('dashboard.configuracion.empresa', {
            url: '/empresa',
            templateUrl: 'app/pages/configuracion/empresa/empresa.html',
            controller: "empresaCtrl",
            title: 'Empresa',
            sidebarMeta: {
                order:500
            }
        }).state('dashboard.configuracion.fiscal', {
            url: '/fiscal',
            templateUrl: 'app/pages/configuracion/fiscal/fiscal.html',
            controller: "fiscalCtrl",
            title: 'Conf Fiscal',
            sidebarMeta: {
                order:600
            }
        });
    }]);  
  })();
  