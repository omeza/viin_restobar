(function () {
    'use strict';
    angular.module('BlurAdmin.pages.compras', [
        'BlurAdmin.pages.compras.proveedor',
        'BlurAdmin.pages.compras.compra'
    ]).config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
          .state('dashboard.compras', {
            url: '/compras',
            template : '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
            abstract: true,
            title: 'Compras',
            sidebarMeta: {
              icon: 'ion-bag',
              order: 500,
            }
          }).state('dashboard.compras.proveedor', {
            url: '/proveedor',
            templateUrl: 'app/pages/compras/proveedor/list/proveedorList.html',
            controller: 'proveedorlistCtrl',
            title: 'Proveedor',
            sidebarMeta: {
                order: 100
            }
        }).state('dashboard.compras.proveedor-details', {
            url: '/proveedor/details/:idProveedor',
            templateUrl: 'app/pages/compras/proveedor/proveedor.html',
            controller: "proveedorCtrl",
            title: 'Proveedor',
            params:{
                idempleado:''
            }
        }).state('dashboard.compras.compra', {
            url: '/compra',
            templateUrl: 'app/pages/compras/compra/list/comprasList.html',
            controller: 'compralistCtrl',
            title: 'Compra',
            sidebarMeta: {
                order: 200
              }
        }).state('dashboard.compras.compra-details', {
            url: '/compra/details/:idCompras',
            templateUrl: 'app/pages/compras/compra/compra.html',
            controller: 'compraCtrl',
            controllerAs: 'vm',
            title: 'Compra',
            params:{ idrol:''}
        });
    }
  })();
  