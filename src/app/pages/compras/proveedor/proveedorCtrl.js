(function () {
    'use strict';
    angular.module('BlurAdmin.pages.compras.proveedor', [])
        .controller('proveedorlistCtrl', ['$scope', 'serviceAPI', '$state', function ($scope, serviceAPI, $state) {
            $scope.displayed = [];
            $scope.callServer = function (tableState) {
                var params = angular.extend({}, tableState.pagination, tableState.search);
                serviceAPI.getProveedor(params).success(function (r) {
                    if (r.success && r.totalItemCount > 0) {
                        $scope.displayed = r.rows;
                        tableState.pagination.numberOfPages = r.numberOfPages;
                    }
                });
            };
            $scope.agregar = function () {
                $state.go('dashboard.compras.proveedor-details');
            };
            $scope.editar = function (id_proveedor) {
                $state.go('dashboard.compras.proveedor-details', { idProveedor: id_proveedor });
            };
            $scope.eliminar = function (idProveedor) {
                serviceAPI.eliminarProveedor({ id: idProveedor }).success(function (r) {
                    console.log(r);
                    var typenotify = (r.success) ? 'warning' : 'error';
                    if (r.affected_rows == 1) {
                        typenotify = 'success';
                        $scope.displayed = $scope.displayed.filter(function (i) { return (i.id_proveedor != idProveedor) });
                    }
                    toastr[typenotify](r.messages, 'Listado de Proveedores')
                });
            };



            $scope.changeStatus = function (idProveedor, actividad, index) {
                serviceAPI.cambiarStatusProveedor({ id: idProveedor, status: actividad }).success(function (r) {
                    var typenotify = 'error';
                    if (r.num_rows == 0) {
                        typenotify = 'warning';
                        $scope.displayed = $scope.displayed.filter(function (i) { return (i.id_proveedor != idProveedor); })
                    }
                    if (r.affected_rows == 1) {
                        typenotify = 'success';
                        $scope.displayed[index].status = r.rows.status;
                    }
                    toastr[typenotify](r.messages, 'Listado de Proveedores');
                });
            };
        }])
        .controller('proveedorCtrl', ['$scope', 'serviceAPI', 'toastr', '$stateParams', 'baSidebarService', '$timeout', '$log', function ($scope, serviceAPI, toastr, $stateParams, baSidebarService, $timeout, $log) {
            var vm = this;
            $scope.default = { id_proveedor: 0, id_tipo_documento: 0, id_ubigeo: '', no_proveedor: '', nu_documento: '', co_depart: '', co_provin: '', co_distri: '', no_direccion: '', nu_telefono: '', nu_celular: '', no_contacto: '', email: '', nu_cuenta_uno: '', nu_cuenta_dos: '', no_observacion: '' };
            $scope.data = { id_proveedor: 0, id_tipo_documento: 0, id_ubigeo: '', no_proveedor: '', nu_documento: '', co_depart: '', co_provin: '', co_distri: '', no_direccion: '', nu_telefono: '', nu_celular: '', no_contacto: '', email: '', nu_cuenta_uno: '', nu_cuenta_dos: '', no_observacion: '' };

            $scope.cTipoDocumento = [];
            $scope.a_departamento = [{ value: 0, text: 'Seleccionar', predeterminado: '1' }];
            $scope.a_provincia = [];
            $scope.a_distrito = [];


            serviceAPI.listarTipoDocumento().then(function (r) {
                if (r.data.totalItemCount > 0 && r.data.success) {
                    var cTipoDocumento = [];
                    cTipoDocumento = _.map(r.data.rows, function (item) {
                        return { value: item.id, text: item.no_tipo_documento };
                    });
                    $scope.cTipoDocumento = _.union($scope.cTipoDocumento, cTipoDocumento);
                    $scope.data.id_tipo_documento = _.findWhere($scope.cTipoDocumento);
                }
            });

            $scope.guardar = function (eForm) {
                serviceAPI.guardarUProveedor($scope.data).success(function (r) {
                    var typeNotification = 'error';
                    if (r.success) {
                        typeNotification = (r.affected_rows != 0) ? 'success' : 'warning';
                    }
                    toastr[typeNotification](r.messages, 'Registro de Proveedor');
                    angular.extend($scope.data, $scope.default);
                    eForm.$setPristine();
                    eForm.$setUntouched();
                });
            };


            serviceAPI.getDepartamento().then(function (r) {
                if (r.data.totalItemCount > 0 && r.data.success) {
                    var a_departamento = [];
                    a_departamento = _.map(r.data.rows, function (item) {
                        return { value: item.id, text: item.no_departamento };
                    });
                    $scope.a_departamento = _.union($scope.a_departamento, a_departamento);
                    $scope.data.id_departamento = _.findWhere($scope.a_departamento);
                }
            });

            serviceAPI.getProvincia().then(function (r) {
                if (r.data.totalItemCount > 0 && r.data.success) {
                    var a_provincia = [];
                    a_provincia = _.map(r.data.rows, function (item) {
                        return { value: item.id, text: item.no_provincia, predeterminado: item.predeterminado };
                    });
                    $scope.a_provincia = _.union($scope.a_provincia, a_provincia);
                    $scope.data.id_provincia = _.findWhere($scope.a_provincia);
                }
            });

            serviceAPI.getDistrito($scope.data).then(function (r) {
                if (r.data.totalItemCount > 0 && r.data.success) {
                    var a_distrito = [];
                    a_distrito = _.map(r.data.rows, function (item) {
                        return { value: item.id, text: item.no_distrito, predeterminado: item.predeterminado };
                    });
                    $scope.a_distrito = _.union($scope.a_distrito, a_distrito);
                    $scope.data.id_distrito = _.findWhere($scope.a_distrito, { predeterminado: '1' });
                }
            });

            $scope.getpDepartamento = function () {
                serviceAPI.getProvincia($scope.data).then(function (r) {
                    if (r.data.totalItemCount > 0 && r.data.success) {
                        $scope.a_provincia = _.map(r.data.rows, function (item) {
                            return { value: item.id, text: item.no_provincia, predeterminado: item.predeterminado };
                        });
                        $scope.data.id_provincia = _.findWhere($scope.a_provincia, { predeterminado: '1' });
                    }
                });
            }
            $scope.getpProvincia = function () {
                serviceAPI.getDistrito($scope.data).then(function (r) {
                    if (r.data.totalItemCount > 0 && r.data.success) {
                        $scope.a_distrito = _.map(r.data.rows, function (item) {
                            return { value: item.id, text: item.no_distrito, predeterminado: item.predeterminado };
                        });
                        $scope.data.id_distrito = _.findWhere($scope.a_distrito, { predeterminado: '1' });
                    }
                });
            }

            $scope.consultar = function () {
                var _this = this;
                if ($stateParams.idProveedor != '') {
                    serviceAPI.consultarProveedor({ id_proveedor: $stateParams.idProveedor }).success(function (r) {
                        var typeNotification = (!r.success) ? 'error' : 'warning';
                        if (r.success && r.num_rows == 1) {
                            typeNotification = 'success';
                            angular.extend($scope.data, r.rows);
                            $scope.cTipoDocumento.map(function (z) {
                                if (z.id == r.rows.co_depart) { $scope.data.id_tipo_documento = z; }
                            });
                            $scope.a_departamento.map(function (a) {
                                if (a.value == r.rows.co_depart) {
                                    $scope.data.co_depart = a.value;
                                }
                            });

                            serviceAPI.getProvincia($scope.data).then(function (r) {
                                if (r.data.totalItemCount > 0 && r.data.success) {
                                    var a_provincia = [];
                                    a_provincia = _.map(r.data.rows, function (item) {
                                        return { value: item.id, text: item.no_provincia };
                                    });
                                    $scope.a_provincia = _.union($scope.a_provincia, a_provincia);
                                    $scope.a_provincia.map(function (b) {
                                        if (b.value == r.rows.co_provin) {
                                            $scope.data.co_provin = b.value;
                                        }
                                    });

                                }
                            });
                            serviceAPI.getDistrito($scope.data).then(function (r) {
                                if (r.data.totalItemCount > 0 && r.data.success) {
                                    var a_distrito = [];
                                    a_distrito = _.map(r.data.rows, function (item) {
                                        return { value: item.id, text: item.no_distrito };
                                    });
                                    $scope.a_distrito = _.union($scope.a_distrito, a_distrito);
                                    $scope.a_distrito.map(function (c) {
                                        if (c.value == r.rows.co_distri) {
                                            $scope.data.co_distri = c.value;
                                        }
                                    });
                                }
                            });
                        }
                        toastr[typeNotification](r.messages, 'Información de la Proveedor');
                    });
                }
            };

        }]);
})();