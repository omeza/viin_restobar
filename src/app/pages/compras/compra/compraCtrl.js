(function () {
    'use strict';
    angular.module('BlurAdmin.pages.compras.compra', [])
        .controller('compralistCtrl', ['$scope', 'serviceAPI', '$state', function ($scope, serviceAPI, $state) {
            $scope.data = { nu_compras: 0, nu_total: 0 };
            $scope.displayed = [];
            $scope.callServer = function (tableState) {
                var params = angular.extend({}, tableState.pagination, tableState.search);
                serviceAPI.getCompras(params).success(function (r) {
                    if (r.success && r.totalItemCount > 0) {
                        $scope.displayed = r.rows;
                        tableState.pagination.numberOfPages = r.numberOfPages;
                    }
                });
            };

            $scope.consultar = function () {
                serviceAPI.consultarTotales().success(function (r) {
                    var typeNotification = (!r.success) ? 'error' : 'warning';
                    if (r.success && r.num_rows == 1) {
                        typeNotification = 'success';
                        angular.extend($scope.data, r.rows);
                    }
                    toastr[typeNotification](r.messages, 'Información Total');
                });
            };
            $scope.agregar = function () {
                $state.go('dashboard.compras.compra-details');
            };
            $scope.editar = function (id) {
                $state.go('dashboard.compras.compra-details', { idCompras: id_compras });
            };
            $scope.changeStatus = function (id) {

            };


        }])
        .controller('compraModelCtrl', ['$scope', 'serviceAPI', 'item', '$http', '$httpParamSerializerJQLike', '$uibModalInstance', function ($scope, serviceAPI, item, $http, $httpParamSerializerJQLike, $uibModalInstance) {

            $scope.item = $scope.$parent.item || false;
            $scope.status = 0;
            $scope.respuesta = 0;
            $scope.archivo = '';
            $scope.url = '';

            $scope.displayed = [];
            $scope.callServer = function (tableState) {
                var params = angular.extend({}, tableState.pagination, tableState.search);
                serviceAPI.getInsumos(params).success(function (r) {
                    if (r.success && r.totalItemCount > 0) {
                        $scope.displayed = r.rows;
                        tableState.pagination.numberOfPages = r.numberOfPages;
                    }
                });
            };
            $scope.obtenerInsumo = function (insumo) {
                $uibModalInstance.close(insumo);
            };

            $scope.miFormato = function(valor) {
                return isNaN(valor) ? valor : parseFloat(valor).toFixed(2);
              }

        }])

        .controller('compraCtrl', ['$scope', '$uibModal', 'serviceAPI', 'toastr', '$stateParams', 'baSidebarService', '$timeout', '$log', '$filter', function ($scope, $uibModal, serviceAPI, toastr, $stateParams, baSidebarService, $timeout, $log, $filter) {
            var vm = this;
            $scope.default = { id_insumo: 0, nu_cantidad: 0, no_categoria_insumo: '', no_insumo: '', no_medida: '', no_unidad: '', nu_cantidad_medida: 0, nu_precio_sugerido: 0, nu_stock_minimo: 0 , nu_stock:0};
            $scope.data = { id_insumo: 0, nu_cantidad: 0, no_categoria_insumo: '', no_insumo: '', no_medida: '', no_unidad: '', nu_cantidad_medida: 0, nu_precio_sugerido: 0, nu_stock_minimo: 0, nu_stock:0 };
            $scope.datac = { id_compra_cabecera: 0, id_sucursal: 0, id_proveedor: 0, id_tipo_comprobante: 0, id_usuario: 1, nu_serie: '', nu_correlativo: '', nu_sub_total: 0, nu_igv: 0, nu_total: 0, fe_emision: '', fe_registro: '', nu_serie_c: '', nu_correlativo_c: '' };
            $scope.defaultc = { id_compra_cabecera: 0, id_sucursal: 0, id_proveedor: 0, id_tipo_comprobante: 0, id_usuario: 1, nu_serie: '', nu_correlativo: '', nu_sub_total: 0, nu_igv: 0, nu_total: 0, fe_emision: '', fe_registro: '', nu_serie_c: '', nu_correlativo_c: '' };

            $scope.rowCollection = [];
            $scope.jcomprobante = [];
            $scope.jproveedor = [];
            $scope.jsucursal = [];

            serviceAPI.getListaSucursal().then(function (r) {
                if (r.data.totalItemCount > 0 && r.data.success) {
                    var jsucursal = [];
                    jsucursal = _.map(r.data.rows, function (item) {
                        return { value: item.id, text: item.no_sucursal };
                    });
                    $scope.jsucursal = _.union($scope.jsucursal, jsucursal);
                    $scope.datac.id_sucursal = _.findWhere($scope.jsucursal);
                }
            });

            $scope.getCorrelativo = function (value) {
                serviceAPI.consultarSucursal({ id: value }).then(function (r) {
                    console.log(r);
                    if (r.data.success && r.data.num_rows == 1) {
                        $scope.datac.nu_serie = $filter('numberFixedLen')(r.data.rows.nu_serie, '4');
                        $scope.datac.nu_correlativo = $filter('numberFixedLen')(r.data.rows.nu_correlativo, '8');
                        console.log($scope.datac);

                    }
                });
            }


            serviceAPI.getComprobante().then(function (r) {
                if (r.data.totalItemCount > 0 && r.data.success) {
                    var jcomprobante = [];
                    jcomprobante = _.map(r.data.rows, function (item) {
                        return { value: item.id, text: item.descripcion };
                    });
                    $scope.jcomprobante = _.union($scope.jcomprobante, jcomprobante);
                    $scope.datac.id_comprobante = _.findWhere($scope.jcomprobante);
                }
            });

            serviceAPI.getProveedores().then(function (r) {
                if (r.data.totalItemCount > 0 && r.data.success) {
                    var jproveedor = [];
                    jproveedor = _.map(r.data.rows, function (item) {
                        return { value: item.id, text: item.no_proveedor };
                    });
                    $scope.jproveedor = _.union($scope.jproveedor, jproveedor);
                    $scope.datac.id_proveedor = _.findWhere($scope.jproveedor);
                }
            });

            $scope.openInsumo = function (size, item) {
                $scope.item = item;
                $uibModal.open({
                    animation: true,
                    size: size,
                    templateUrl: "app/pages/compras/compra/modal/listInsumos.html",
                    controller: 'compraModelCtrl',
                    scope: $scope,
                    resolve: {
                        item: function () { }
                    }
                }).result.then(function (data) {
                    angular.extend($scope.data, data);
                }, function () {
                    console.log('Modal dismissed at: ' + new Date());
                });
            };
            var $item = 0;
            $scope.addItem = function () {
                $scope.rowCollection.push(_.extend({
                    id: ++$item,
                    no_insumo: $scope.data.no_insumo,
                    no_unidad: $scope.data.no_unidad,
                    nu_cantidad: $scope.data.nu_cantidad,
                    nu_precio: $scope.data.nu_precio_sugerido,
                    no_total: (parseFloat($scope.data.nu_cantidad) * parseFloat($scope.data.nu_precio_sugerido)).toFixed(2)
                },
                    $scope.data)
                );
                $scope.datac.nu_total = $filter('Total')($scope.rowCollection, 'nu_cantidad', 'nu_precio', 2);
                $scope.datac.nu_sub_total = parseFloat($scope.datac.nu_total / 1.18).toFixed(2);
                $scope.datac.nu_igv = parseFloat($scope.datac.nu_total - $scope.datac.nu_sub_total).toFixed(2);
                angular.extend($scope.data, $scope.default);

            };

            $scope.quitar_producto = function (id) {
                $scope.rowCollection = $scope.rowCollection.filter(function (item) {
                    return item.id != id;
                });
                $scope.datac.nu_total = $filter('Total')($scope.rowCollection, 'nu_cantidad', 'nu_precio', 2);
                $scope.datac.nu_sub_total = parseFloat($scope.datac.nu_total / 1.18).toFixed(2);
                $scope.datac.nu_igv = parseFloat($scope.datac.nu_total - $scope.datac.nu_sub_total).toFixed(2);
            };


            $scope.guardar = function (eForm) {
                var params = { orden: $scope.datac, detalle: $scope.rowCollection };
                serviceAPI.guardarOrdenCompra(params).success(function (r) {
                    var typeNotification = 'error';
                    if (r.success) {
                        typeNotification = (r.affected_rows != 0) ? 'success' : 'warning';
                    }
                    toastr[typeNotification](r.messages, 'Registro Orden de Compras');
                    angular.extend($scope.data, $scope.default);
                    angular.extend($scope.datac, $scope.defaultc);
                    $scope.rowCollection = [];
                    eForm.$setPristine();
                    eForm.$setUntouched();
                });
            };


        }]);
})();