(function(){
    'use strict';
    angular.module('BlurAdmin.pages.venta.aperturaCierre', [])    
    .controller('aperturaCierrelistCtrl',['$scope', '$uibModal', 'serviceAPI', 'toastr', '$stateParams', 'baSidebarService', '$timeout', '$log', '$filter', function ($scope, $uibModal, serviceAPI, toastr, $stateParams, baSidebarService, $timeout, $log, $filter){
        $scope.displayed=[];
        $scope.callServer=function(tableState){
            var params = angular.extend({}, tableState.pagination, tableState.search);
            serviceAPI.getCajasUsuario(params).success(function(r){
               
                if(r.success && r.totalItemCount>0){
                    $scope.displayed=r.rows;
                    tableState.pagination.numberOfPages =r.numberOfPages;
                }
            });
        };

        
        $scope.agregar=function(){
            $state.go('dashboard.seguridad.usuario-details');
        };
        $scope.editar=function(id){
            $state.go('dashboard.seguridad.usuario-details', {idempleado:id});
        };
        $scope.changeStatus=function(id){
            
        };
        $scope.openMoto = function (size, item) {
            $scope.item = item;
            $uibModal.open({
                animation: true,
                size: size,
                templateUrl: "app/pages/venta/aperturaCierre/modal/montoApertura.html",
                controller: 'montoModelCtrl',
                scope: $scope,
                resolve: {
                    item: function () { }
                }
            }).result.then(function (data) {
                $scope.displayed=[];
                $scope.callServer([]);
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });
        };
    }]).controller('montoModelCtrl', ['$scope', 'serviceAPI', 'item', '$http', '$httpParamSerializerJQLike', '$uibModalInstance', function ($scope, serviceAPI, item, $http, $httpParamSerializerJQLike, $uibModalInstance) {

        $scope.item = $scope.$parent.item || false;

        $scope.guardar=function(eForm){
            serviceAPI.guardarAtertura($scope.item).success(function(r){
                var typeNotification='error';
                if(r.success){
                    typeNotification=(r.affected_rows!=0)?'success':'warning';
                }
                toastr[typeNotification](r.messages, 'Registro de Usuario');
                
            });
            $scope.cerrar();
        };
        $scope.cerrar = function () {
            $uibModalInstance.close($scope.item);
        }

        $scope.miFormato = function(valor) {
            return isNaN(valor) ? valor : parseFloat(valor).toFixed(2);
          }

    }])
    .controller('aperturaCierreCtrl', [ '$scope', 'serviceAPI', 'toastr', '$stateParams', 'baSidebarService', '$timeout', '$log', function($scope, serviceAPI, toastr, $stateParams, baSidebarService, $timeout, $log){
        var vm=this;
        $scope.default={idusuario:0, idempleado:0, idtipdoc:'', num_doc: '', apenom:'', usuario:'', password:'', rep_passw:'', idrol_sistema:'', permission:[]};
        $scope.data={idusuario:0, idempleado:0, idtipdoc:'', num_doc:'', apenom:'', usuario:'', password:'', rep_passw:'', idrol_sistema:'', permission:[]};
        
        
        $scope.jTipDocumentos=[];
        $scope.show={password:false, re_passw:false};
        $scope.jRoles=[];
        $scope.class_password='';
        $scope.messages={
            num_doc:{ required: 'Campo Obligatorio' },
            apenom:{ required: 'Campo Obligatorio'}
        };

        serviceAPI.getRoles().success(function(r){
            if(r.success && r.totalItemCount>0){
                angular.extend($scope.jRoles, r.rows);
            }
        });

        serviceAPI.getTiposDocumentos().success(function(r){
            if(r.success && r.totalItemCount>0){
                angular.extend($scope.jTipDocumentos, r.rows);
                r.rows.filter(function(s){
                    if(s.predeterminado==1){ 
                        $scope.data.idtipdoc=s;
                        $scope.default.idtipdoc=s;
                    }
                })
            }
        });
        
        $scope.guardar=function(eForm){
            serviceAPI.guardarUsuario($scope.data).success(function(r){
                var typeNotification='error';
                if(r.success){
                    typeNotification=(r.affected_rows!=0)?'success':'warning';
                }
                toastr[typeNotification](r.messages, 'Registro de Usuario');
                angular.extend($scope.data, $scope.default);
                eForm.$setPristine();
                eForm.$setUntouched();
            });
        };

        $scope.consultar=function(){
            var _this=this;
            if($stateParams.idempleado!=''){
                serviceAPI.consultarUsuario({id:$stateParams.idempleado}).success(function(r){
                    var typeNotification=(!r.success)?'error':'warning';
                    if(r.success && r.num_rows==1){
                        typeNotification='success';
                        angular.extend($scope.data, r.rows);
                        $scope.jTipDocumentos.map(function(a){
                            if(a.id==r.rows.idtipdoc){ $scope.data.idtipdoc=a; }
                        });
                        $scope.jRoles.map(function(b){
                            if(b.idrol_sistema==r.rows.idrol_sistema){ $scope.data.idrol_sistema=b; }
                        });
                    }
                    toastr[typeNotification](r.messages, 'Información de Usuario');
                }); 
            }
        };
    }]);
})();