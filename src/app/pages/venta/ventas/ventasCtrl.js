(function () {
    'use strict';
    angular.module('BlurAdmin.pages.venta.ventas', [])
        .controller('ventaslistCtrl', ['$scope', 'serviceAPI', '$state', '$uibModal', 'toastr', function ($scope, serviceAPI, $state, $uibModal, toastr) {
            $scope.data = { nu_ventas: 0, nu_total: 0 };
            $scope.displayed = [];

            $scope.callServer = function (tableState) {
                var params = angular.extend({}, tableState.pagination, tableState.search);
                serviceAPI.getVentasDia(params).success(function (r) {
                    if (r.success && r.totalItemCount > 0) {
                        $scope.displayed = r.rows;
                        tableState.pagination.numberOfPages = r.numberOfPages;
                    }
                });
            };

            $scope.consultar = function () {
                serviceAPI.getTotalesVentasDia().success(function (r) {
                    var typeNotification = (!r.success) ? 'error' : 'warning';
                    console.log(r);
                    if (r.success && r.num_rows == 1) {
                        typeNotification = 'success';
                        angular.extend($scope.data, r.rows);
                        console.log($scope.data);
                    }
                    toastr[typeNotification](r.messages, 'Información Total');
                });
            };

            $scope.agregar = function () {
                $state.go('dashboard.venta.ventas-details');
            };
            $scope.editar = function (id) {
                $state.go('dashboard.venta.ventas-details', { idcajas: id });
            };
            $scope.changeStatus = function (id) {

            };

            $scope.openPdf = function (size, item) {
                $scope.item = item;
                $uibModal.open({
                    animation: true,
                    templateUrl: "app/pages/venta/caja/modal/modalPdf.html",
                    size: size,
                    controller: 'modalCtrl',
                    scope: $scope,
                    resolve: {
                        item: function () { }
                    }
                }).result.then(function (item) {
                    angular.extend($scope.data, item);
                }, function () {

                });
            };
            $scope.openGenerar = function (size, item, index) {
                $scope.item = item;
                $uibModal.open({
                    animation: true,
                    templateUrl: "app/pages/venta/caja/modal/generar.html",
                    size: size,
                    controller: 'modalCtrl',
                    scope: $scope,
                    resolve: {
                        item: function () { }
                    }
                }).result.then(function (item) {
                    angular.extend($scope.data, item);
                    if (item.success == true) {
                        $scope.rowCollection[index].no_estado = item.no_estado;
                        $scope.rowCollection[index].no_sunat = item.no_sunat;
                        $scope.rowCollection[index].id_respuesta = item.id_respuesta;
                    }
                    else {
                        $scope.rowCollection[index].no_estado = $scope.data.no_estado;
                        $scope.rowCollection[index].no_sunat = $scope.data.no_sunat;
                        $scope.rowCollection[index].id_respuesta = $scope.data.id_respuesta;
                    }

                }, function () {
                    console.log('Modal dismissed at: ' + new Date());
                });
            };

            $scope.openCorreo = function (size, item) {
                $scope.item = item;
                $uibModal.open({
                    animation: true,
                    size: size,
                    controller: 'modalCtrl',
                    scope: $scope,
                    templateUrl: "app/pages/venta/caja/modal/modalEmail.html",
                    resolve: {
                        item: function () { }
                    }
                })
            };

        }])
        .controller('modalCtrl', ['$scope', 'item', '$http', '$httpParamSerializerJQLike', '$uibModalInstance', function ($scope, item, $http, $httpParamSerializerJQLike, $uibModalInstance) {
            $scope.item = $scope.$parent.item || false;
            $scope.status = 0;
            $scope.respuesta = 0;
            $scope.archivo = '';
            $scope.url = '';
            $scope.gen = function () {
                if ($scope.item.id_tipocompro == 1) {
                    $scope.url = 'api/api-viin/examples/factura.php';
                }
                else if ($scope.item.id_tipocompro == 2) {
                    $scope.url = 'api/api-viin/examples/boleta.php';
                }
                else if ($scope.item.id_tipocompro == 3) {
                    $scope.url = 'api/api-viin/examples/nota-credito.php';
                }
                else if ($scope.item.id_tipocompro == 4) {
                    $scope.url = 'api/api-viin/examples/nota-debito.php';
                }
                $http({
                    url: $scope.url,
                    method: 'POST',
                    data: $httpParamSerializerJQLike({ oper: 'cargar', item: $scope.item }),
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                }).then(function (response) {
                    console.log();

                    if (response.data.cod == "0") {
                        $("#result").html(response.data.datos);
                        //$uibModalInstance.close(item);
                        angular.extend($scope.data, response.data);
                        $scope.status = response.data.status;
                        $scope.respuesta = response.data.id_respuesta;
                    }
                });
            }

            if (angular.isObject($scope.item)) {
                console.log($scope.item);

                if ($scope.item.id_tipo_comprobante == 1) {
                    $scope.archivo = 'api/api-viin/examples/report/Factura.php';
                } else if ($scope.item.id_tipo_comprobante == 2) {
                    $scope.archivo = 'api/api-viin/examples/report/Boleta.php';
                } else {
                    $scope.archivo = 'api/api-viin/examples/report/Boleta.php';
                }

            }

            $scope.pdf = function () {
                $uibModalInstance.close($scope.item);
            }


            $scope.cerrar = function (data) {
                $uibModalInstance.close($scope.data);

            }
            $scope.enviaCorreo = function () {
                $http({
                    url: 'api/ventas.php',
                    method: 'POST',
                    data: $httpParamSerializerJQLike({ oper: 'envioCorreo', info: $scope.item }),
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).then(function (response) {
                    if (response.data.success) {
                        $("#result").html(response.data.message);
                    } else
                        $("#result").html(response.data.message);
                }, function (response) {
                    toastr.error("ha fallado el proceso ", 'Error')
                });
            };



        }])
        .controller('ventasCtrl', ['$scope', '$uibModal', 'serviceAPI', 'toastr', '$stateParams', 'baSidebarService', '$timeout', '$log', '$filter', function ($scope, $uibModal, serviceAPI, toastr, $stateParams, baSidebarService, $timeout, $log, $filter) {
            var vm = this;
            $scope.default = { id_pedido_cabecera: 0, id_venta: 0, id_cliente: 0, no_cliente: '', id_usuario: 1, id_tipo_comprobante: 0, id_tipo_pago: 0, id_sucursal: 1, nu_correlativo: '', nu_serie: '', nu_total: 0, nu_efectivo: 0, nu_izipay: 0, nu_trasnferencia: 0, nu_credito: 0, nu_vuelto: 0, nu_sub_total: 0, nu_igv: 0 };
            $scope.data = { id_pedido_cabecera: 0, id_venta: 0, id_cliente: 0, no_cliente: '', id_usuario: 1, id_tipo_comprobante: 0, id_tipo_pago: 0, id_sucursal: 1, nu_correlativo: '', nu_serie: '', nu_total: 0, nu_efectivo: 0, nu_izipay: 0, nu_trasnferencia: 0, nu_credito: 0, nu_vuelto: 0, nu_sub_total: 0, nu_igv: 0 };
            $scope.id_pedido = 0;
            $scope.displayed = [];
            $scope.jproductos = [];
            $scope.jcomprobante = [];
            $scope.jtipopago = [];

            $scope.selection=[];

            $scope.toggleSelection = function toggleSelection(value) {
                var idx = $scope.selection.indexOf(value);
                if (idx > -1) {
                  $scope.selection.splice(idx, 1);
                }
                else {
                  $scope.selection.push(value);
                }
                $scope.data.nu_total = $filter('Total')($scope.selection, 'cantidad', 'nu_precio', 2);
                console.log($scope.selection);
            };   


            $scope.getVuelto = function (event) {
                if (event.keyCode == 13) {
                    $scope.data.nu_vuelto = $scope.data.nu_efectivo - $scope.data.nu_total;
                }
            }

            $scope.getCorrelativo = function (value) {
                serviceAPI.consultarCorrelativoSucursal(value).then(function (r) {
                    if (r.data.success && r.data.num_rows == 1) {
                        console.log(r);
                        $scope.data.nu_serie = r.data.rows.nu_serie;
                        $scope.data.nu_correlativo = $filter('numberFixedLen')(r.data.rows.nu_correlativo, '8');
                    }
                });
            }

            $scope.consultar = function () {
                serviceAPI.getMesasAtendidasPago().success(function (r) {
                    var typeNotification = (!r.success) ? 'error' : 'warning';
                    if (r.success && r.totalItemCount > 0) {
                        typeNotification = 'success';
                        $scope.displayed = r.rows;
                    }
                    toastr[typeNotification](r.messages, 'Información de Pedido');
                });
            };

            serviceAPI.getTipoPago().then(function (r) {
                console.log(r);
                if (r.data.num_rows > 0 && r.data.success) {
                    var jtipopago = [];
                    jtipopago = _.map(r.data.rows, function (item) {
                        return { value: item.id, text: item.no_tipo_pago };
                    });
                    $scope.jtipopago = _.union($scope.jtipopago, jtipopago);
                    $scope.data.id_tipo_pago = _.findWhere($scope.jtipopago);
                }
            });

            serviceAPI.getComprobante().then(function (r) {
                if (r.data.totalItemCount > 0 && r.data.success) {
                    var jcomprobante = [];
                    jcomprobante = _.map(r.data.rows, function (item) {
                        return { value: item.id, text: item.descripcion };
                    });
                    $scope.jcomprobante = _.union($scope.jcomprobante, jcomprobante);
                    $scope.data.id_tipo_documento = _.findWhere($scope.jcomprobante);
                }
            });

            $scope.getDetalle = function (value) {

                serviceAPI.listarDetalleProductos(value).success(function (r) {
                    var typeNotification = (!r.success) ? 'error' : 'warning';
                    if (r.success && r.totalItemCount > 0) {
                        console.log(r);
                        typeNotification = 'success';
                        $scope.jproductos = r.rows;
                        $scope.id_pedido = r.idPedido;
                        $scope.data.id_pedido_cabecera = r.idPedido;
                    }                   
                    $scope.data.nu_efectivo = 0;
                    $scope.data.nu_izipay = 0;
                    $scope.data.nu_trasnferencia = 0;
                    $scope.data.nu_vuelto = 0;
                    toastr[typeNotification](r.messages, 'Información de Productos');
                });

            };
            $scope.crearProduct = function (size) {
                $uibModal.open({
                    animation: true,
                    size: size,
                    templateUrl: "app/pages/clientes/cliente/clienteModal.html",
                    controller: 'clienteCtrl',
                    scope: $scope
                }).result.then(function (data) {
                    angular.extend($scope.data, data);
                }, function () {
                    console.log('Modal dismissed at: ' + new Date());
                });
            };

            $scope.modalProducts = function (size) {
                $uibModal.open({
                    animation: true,
                    size: size,
                    templateUrl: "app/pages/clientes/cliente/list/clienteListModal.html",
                    controller: 'clienteModalCtrl',
                    scope: $scope
                }).result.then(function (data) {
                    console.log(data);
                    angular.extend($scope.data, data);
                }, function () {
                    console.log('Modal dismissed at: ' + new Date());
                });
            };
            $scope.openPdf = function (size, item) {
                $scope.item = item;
                $uibModal.open({
                    animation: true,
                    templateUrl: "app/pages/venta/caja/modal/modalPdf.html",
                    size: size,
                    controller: 'modalCtrl',
                    scope: $scope,
                    resolve: {
                        item: function () { }
                    }
                }).result.then(function (item) {
                    angular.extend($scope.data, item);
                }, function () {

                });
            };



            $scope.guardar = function (eForm) {

                $scope.data.nu_sub_total = parseFloat($scope.data.nu_total / 1.18).toFixed(2);
                $scope.data.nu_igv = parseFloat($scope.data.nu_total - $scope.data.nu_sub_total).toFixed(2);
                var params = { orden: $scope.data, detalle: $scope.selection };
                serviceAPI.guardarVenta(params).success(function (r) {
                    console.log(r);
                    var typeNotification = 'error';
                    if (r.success) {
                        typeNotification = (r.affected_rows != 0) ? 'success' : 'warning';
                        angular.extend($scope.data.id_tipo_comprobante, r.id_tipo_comprobante);
                        var paramss = { orden: $scope.data, correlativo: r.correlativo, idComprobante: r.id_tipo_comprobante };
                        serviceAPI.actualizarPago(paramss).success(function (b) {
                            if (b.success) {
                                typeNotification = (b.affected_rows != 0) ? 'success' : 'warning';
                            }
                            toastr[typeNotification](b.messages, 'Registro Pagado Correctamente');
                        });
                        $scope.data.id_tipo_comprobante = r.id_tipo_comprobante;
                        angular.extend($scope.data.id_tipo_comprobante, r.id_tipo_comprobante);
                    }
                    angular.extend($scope.data.id_tipo_comprobante, r.id_tipo_comprobante);
                    var paramsd = { orden: $scope.data, idComprobante: r.id_tipo_comprobante };
                    toastr[typeNotification](r.messages, 'Registro de Venta Correctamente');
                    angular.extend($scope.data, $scope.default);
                    $scope.jproductos = [];
                    $scope.displayed = [];
                    $scope.consultar();
                    eForm.$setPristine();
                    eForm.$setUntouched();
                });
            };


        }]);
})();