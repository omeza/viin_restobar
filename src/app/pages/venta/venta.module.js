(function () {
  'use strict';
  angular.module('BlurAdmin.pages.venta', [
    'BlurAdmin.pages.venta.aperturaCierre',
    'BlurAdmin.pages.venta.caja',
    'BlurAdmin.pages.venta.ventas'

  ]).config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('dashboard.venta', {
        url: '/venta',
        template: '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
        abstract: true,
        title: 'Venta',
        sidebarMeta: {
          icon: 'ion-cash',
          order: 500,
        }
      }).state('dashboard.venta.aperturaCierre', {
        url: '/aperturaCierre',
        templateUrl: 'app/pages/venta/aperturaCierre/aperturaCierre.html',
        controller: 'aperturaCierrelistCtrl',
        title: 'Apertura/Cierre',
        sidebarMeta: {
          order: 100
        }
      }).state('dashboard.venta.aperturaCierre-details', {
        url: '/aperturaCierre/details/:idventa',
        templateUrl: 'app/pages/venta/aperturaCierre/aperturaCierre.html',
        controller: "aperturaCierreCtrl",
        title: 'Apertura/Cierre',
        params: {
          idempleado: ''
        }
      }).state('dashboard.venta.caja', {
        url: '/caja',
        templateUrl: 'app/pages/venta/caja/list/cajaList.html',
        controller: 'cajalistCtrl',
        title: 'Caja',
        sidebarMeta: {
          order: 200
        }
      })
      .state('dashboard.venta.caja-details', {
        url: '/caja/details/:idcajas',
        templateUrl: 'app/pages/venta/caja/caja.html',
        controller: 'cajaCtrl',
        controllerAs: 'vm',
        title: 'Caja',
        params: { idcajas: '' }
      }).state('dashboard.venta.ventas', {
        url: '/ventas',
        templateUrl: 'app/pages/venta/ventas/list/ventasList.html',
        controller: 'ventaslistCtrl',
        title: 'Ventas',
        sidebarMeta: {
          order: 200
        }
      })
      .state('dashboard.venta.ventas-details', {
        url: '/ventas/details/:idcajas',
        templateUrl: 'app/pages/venta/ventas/ventas.html',
        controller: 'ventasCtrl',
        controllerAs: 'vm',
        title: 'Ventas',
        params: { idcajas: '' }
      });
  }

})();
