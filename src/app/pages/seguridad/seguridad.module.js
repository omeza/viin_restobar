(function () {
    'use strict';
    angular.module('BlurAdmin.pages.seguridad', [
        'BlurAdmin.pages.seguridad.usuario',
        'BlurAdmin.pages.seguridad.roles'
    ]).config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
          .state('dashboard.seguridad', {
            url: '/seguridad',
            template : '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
            abstract: true,
            title: 'Seguridad',
            sidebarMeta: {
              icon: 'fas fa-user-shield',
              order: 500,
            }
          }).state('dashboard.seguridad.usuarios', {
            url: '/usuarios',
            templateUrl: 'app/pages/seguridad/usuarios/list/usuariosList.html',
            controller: 'usuarioslistCtrl',
            title: 'Usuarios',
            sidebarMeta: {
                order: 100
            }
        }).state('dashboard.seguridad.usuario-details', {
            url: '/usuario/details/:idempleado',
            templateUrl: 'app/pages/seguridad/usuarios/usuario.html',
            controller: "usuarioCtrl",
            title: 'Usuario',
            params:{
                idempleado:''
            }
        }).state('dashboard.seguridad.roles', {
            url: '/roles',
            templateUrl: 'app/pages/seguridad/roles/list/rolesList.html',
            controller: 'rolesListCtrl',
            title: 'Roles de Usuario',
            sidebarMeta: {
                order: 200
              }
        }).state('dashboard.seguridad.rol-details', {
            url: '/roles/details/:idrol',
            templateUrl: 'app/pages/seguridad/roles/roles.html',
            controller: 'rolesCtrl',
            controllerAs: 'vm',
            title: 'Rol de Usuario',
            params:{ idrol:''}
        });
    }
  
  })();
  