(function(){
    'use strict';
    angular.module('BlurAdmin.pages.seguridad.roles', [])
    .directive('rolExist', [ '$q', 'serviceAPI', function($q, serviceAPI){
        return {
            require : 'ngModel',
            transclude : true,
            scope :{ idrol:'='},
            link : function($scope, $element, $attrs, ngModel){
                ngModel.$asyncValidators.rolExist=function(modelValue, viewValue){
                    return serviceAPI.existRolUsuario({descripcion:viewValue, id: $scope.idrol}).success(function(r){
                            if(r.success && r.num_rows!=0){
                                return $q.reject();
                            }
                            return true;
                        })
                }
            }
        }
    }])
    .controller('rolesListCtrl',[ '$scope', 'serviceAPI', '$state', 'toastr', function($scope, serviceAPI, $state, toastr){
        $scope.displayed=[];
        $scope.callServer=function(tableState){
            var params = angular.extend({}, tableState.pagination, tableState.search);
            serviceAPI.getRoles(params).success(function(r){
                if(r.success && r.totalItemCount>0){
                    $scope.displayed=r.rows;
                    tableState.pagination.numberOfPages =r.numberOfPages;
                }
            });
        };
        $scope.agregar=function(){
            $state.go('dashboard.seguridad.rol-details');
        };
        $scope.editar=function(id){
            console.log(id);
            $state.go('dashboard.seguridad.rol-details', {idrol:id});
        };

        $scope.eliminar=function(idrol){
            serviceAPI.deleteRol({id:idrol}).success(function(r){
                var typenotify=(r.success)?'warning':'error';
                if(r.affected_rows==1){
                    typenotify='success';
                    $scope.displayed=$scope.displayed.filter(function(i){ return (i.idrol_sistema!=idrol)});
                }
                toastr[typenotify](r.messages, 'Listado de Roles')
            });
        };

        $scope.changeStatus=function(idrol, actividad, index){
            serviceAPI.cambiarStatusRol({id:idrol, status:actividad}).success(function(r){
                var typenotify='error';
                if(r.num_rows==0){
                    typenotify='warning';
                    $scope.displayed=$scope.displayed.filter(function(i){ return (i.idrol_sistema!=idrol);})
                }
                if(r.affected_rows==1){
                    typenotify='success';
                    $scope.displayed[index].status=r.rows.status;
                }
                toastr[typenotify](r.messages, 'Listado de Rol');
            });
        };

    }])
    .controller('rolesCtrl', [ '$timeout', '$scope', 'serviceAPI', 'toastr', '$stateParams', 'baSidebarService', function($timeout, $scope, serviceAPI, toastr, $stateParams, baSidebarService){
        var vm=this;
        vm.default={id_rol:0, nombre_rol:'', status:true};
        vm.data={id_rol:0, nombre_rol:'', status:true};
        vm.ignoreChanges = false;
        vm.treeData = [];
        vm.itemsUsuarios=[];
        vm.idrol=$stateParams.idrol || 0;
        $scope.messages={
            nombre_rol:{
                required: 'Campo Obligatorio',
                rolExist: 'Rol Pertenece a otro registro'
            }
        };

        vm.guardar=function(eForm){
            var sendData=angular.extend({}, vm.data, {items: $("#jstree").jstree('get_checked', null, true)});
            serviceAPI.guardarRolUsuario(sendData).success(function(r){
                var typeNotification='error';
                if(r.success){
                    typeNotification=(r.affected_rows==1)?'success':'warning';
                }
                toastr[typeNotification](r.messages, 'Registro de Rol de Usuario');
                angular.extend(vm.data, vm.default);
                eForm.$setPristine();
                eForm.$setUntouched();
                $('#jstree').jstree("uncheck_all");
            });
        };

        vm.consultar=function(){
            if($stateParams.idrol!=''){
                serviceAPI.consultarRolUsuario({id:$stateParams.idrol}).success(function(r){
                    if(r.success && r.num_rows==1){
                        console.log(r);
                        angular.extend(vm.data, r.rows);
                        
                        angular.copy(r.rows.items, vm.itemsUsuarios);
                    }else{
                        toastr.warning(r.messages, 'Información de Rol');
                    }
                }); 
            }
        };

    }]);
})();