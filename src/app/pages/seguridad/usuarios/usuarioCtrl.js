    (function(){
    'use strict';
    angular.module('BlurAdmin.pages.seguridad.usuario', [])
    .directive('equalToPassword', function(){
        return {
            require: 'ngModel',
            restrict: 'A',
            scope: { password:"="},
            link: function(scope, iElement, iAttrs, ngModel){
                ngModel.$validators.equalToPassword=function(modelValue, viewValue){
                    if(ngModel.$isEmpty(modelValue))
                        return false;
                    return (modelValue==scope.password);
                }
            }
        }
    })
    .directive('passwordStrong', function(){
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, iElement, iAttrs, ngModelCtrl){
                ngModelCtrl.$validators.passwordStrong=function(modelValue, viewValue){
                    var chars=0;
                    if(ngModelCtrl.$isEmpty(modelValue))
                        return false;
                    if((/[a-z]/).test(modelValue)) chars +=  26;
                    if((/[A-Z]/).test(modelValue)) chars +=  26;
                    if((/[0-9]/).test(modelValue)) chars +=  10;
                    if((/[^a-zA-Z0-9]/).test(modelValue)) chars +=  32;
                    chars += (modelValue.length>8)?6:(modelValue.length<5)?1:3;
                    return (chars>29);
                };
            }
        }
    })
    .directive('jsTree', ['$timeout', 'baSidebarService',  function($timeout, baSidebarService){
        return {
            require: 'ngModel',
            restrict: 'E',
            scope:{ idrol:"="},
            template:"<div id='jstree'></div>",
            link:function(scope, element, attrs, ngModelCtrl){
                var jMenu=[];
                var i=-1;
                baSidebarService.getMenuItems().map(function(item){
                    console.log(item);
                    if(angular.isArray(item.subMenu)){
                        if(!angular.isDefined(id_parent) || id_parent!=item.stateRef){
                            var id_parent=item.stateRef;
                            jMenu.push({id: item.stateRef, icon:item.icon, text: item.title, state:{ opened: true, selected:false}, children:[]});
                            i++;
                        }
                        angular.forEach(item.subMenu, function(value, key){
                            jMenu[i].children.push({id: value.stateRef, text: value.title, state:{selected:false}});
                        });
                    }else{
                        jMenu.push({id: item.stateRef, icon:item.icon, text: item.title, state:{selected:false}});
                        i++;
                    }
                });

                scope.treeInstance=$("div#jstree").jstree({
                    plugins: ["state", "types", "checkbox"],
                    checkbox: {
                        real_checkboxes: true,
                        two_state: true                        
                    },
                    core: {
                        data:{
                            url: 'api/roles.php?oper=menu_user', type: 'POST',
                            contentType: "application/json; charset=utf-8",
                            data: JSON.stringify({id: scope.idrol, menu:jMenu}),
                            dataType: 'json'
                        },
                        types:{
                            default:{
                                icon: 'fa fa-minus'
                            }
                        }
                    }
                }).on('ready.jstree', function(){
                    $('#jstree').jstree("uncheck_all");
                    if(scope.idrol!=''){
                        console.log(ngModelCtrl.$viewValue);
                        angular.forEach(ngModelCtrl.$viewValue, function(item){
                            $('#jstree').jstree("check_node", item.name_state);
                        });
                    }
                    if(angular.isDefined(attrs.hiddenCheckbox) && attrs.hiddenCheckbox){
                        $('#jstree').jstree("hide_checkboxes");
                    }
                });
                
                ngModelCtrl.$render=function(){
                    console.log('render: ', ngModelCtrl.$viewValue)

                }
            }
        }
    }])
    .directive('existUser', [ 'serviceAPI', '$q', function(serviceAPI, $q){
        return {
            require : 'ngModel',
            transclude : true,
            scope :{ id:'=idusuario'},
            link : function($scope, $element, $attrs, ngModel){
                ngModel.$asyncValidators.loginUser=function(modelValue, viewValue){
                    return serviceAPI.exitUsuario({usuario:viewValue, id: $scope.id}).then(function(r){
                            if(r.data.success && r.data.num_rows==1){ 
                                return $q.reject(); 
                            }
                            return $q.resolve();
                    });
                }
            }
        }
    }])
    .controller('usuarioslistCtrl',[ '$scope', 'serviceAPI', '$state', function($scope, serviceAPI, $state){
        $scope.displayed=[];
        $scope.callServer=function(tableState){
            var params = angular.extend({}, tableState.pagination, tableState.search);
            serviceAPI.getUsuarios(params).success(function(r){
                if(r.success && r.totalItemCount>0){
                    $scope.displayed=r.rows;
                    tableState.pagination.numberOfPages =r.numberOfPages;
                }
            });
        };
        $scope.agregar=function(){
            $state.go('dashboard.seguridad.usuario-details');
        };
        $scope.editar=function(id){
            $state.go('dashboard.seguridad.usuario-details', {idempleado:id});
        };
        $scope.changeStatus=function(id){
            
        };
    }])
    .controller('usuarioCtrl', [ '$scope','$uibModal', 'serviceAPI', 'toastr', '$stateParams', 'baSidebarService', '$timeout', '$log', function($scope, $uibModal,serviceAPI, toastr, $stateParams, baSidebarService, $timeout, $log){
        var vm=this;
        $scope.default={idusuario:0, idempleado:0,idSucursal:0, idCaja:0, idtipdoc:'', num_doc: '', apenom:'', usuario:'', password:'', rep_passw:'', idrol_sistema:'', permission:[]};
        $scope.data={idusuario:0, idempleado:0,idSucursal:0, idCaja:0, idtipdoc:'', num_doc:'', apenom:'', usuario:'', password:'', rep_passw:'', idrol_sistema:'', permission:[]};
        $scope.jTipDocumentos=[];
        $scope.show={password:false, re_passw:false};
        $scope.jRoles=[];
        $scope.jCaja=[];
        $scope.jSucursal=[];
        $scope.class_password='';
        $scope.messages={
            num_doc:{ required: 'Campo Obligatorio' },
            apenom:{ required: 'Campo Obligatorio'}
        };

        $scope.modalProducts=function(size){
            $uibModal.open({
                animation: true,
                size: size,
                templateUrl: "app/pages/empleado/empleado/list/empleadoModal.html",
                controller: 'empleadolistModaltrl',
                scope: $scope
            }).result.then(function (data) {
                angular.extend($scope.data, data);
                $scope.data.idempleado = data.id_empleado;
                $scope.data.num_doc=data.nu_documento;
                $scope.data.apenom=data.no_nombres;
                console.log(data);
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });
        };

        serviceAPI.getRoles().success(function(r){
            console.log(r);
            if(r.success && r.totalItemCount>0){
                angular.extend($scope.jRoles, r.rows);
            }
        });

        serviceAPI.getListaSucursal().success(function(r){
            console.log(r);
            if(r.success && r.totalItemCount>0){
                angular.extend($scope.jSucursal, r.rows);
            }
        });
        $scope.listarCajas = function(){
            serviceAPI.getCajasSucursales($scope.data).success(function(r){
                console.log(r);
                if(r.success && r.totalItemCount>0){
                    angular.extend($scope.jCaja, r.rows);
                }
            });
        }
        

        serviceAPI.getTiposDocumentos().success(function(r){
            console.log(r);
            if(r.success && r.totalItemCount>0){
                angular.extend($scope.jTipDocumentos, r.rows);
                r.rows.filter(function(s){
                    if(s.predeterminado==1){ 
                        $scope.data.idtipdoc=s;
                        $scope.default.idtipdoc=s;
                    }
                })
            }
        });
        
        $scope.guardar=function(eForm){
            serviceAPI.guardarUsuario($scope.data).success(function(r){
                var typeNotification='error';
                if(r.success){
                    typeNotification=(r.affected_rows!=0)?'success':'warning';
                }
                toastr[typeNotification](r.messages, 'Registro de Usuario');
                angular.extend($scope.data, $scope.default);
                eForm.$setPristine();
                eForm.$setUntouched();
            });
        };

        $scope.consultar=function(){
            var _this=this;
            if($stateParams.idempleado!=''){
                serviceAPI.consultarUsuario({id:$stateParams.idempleado}).success(function(r){
                    var typeNotification=(!r.success)?'error':'warning';
                    if(r.success && r.num_rows==1){
                        typeNotification='success';
                        angular.extend($scope.data, r.rows);
                        angular.extend($scope.data, r.rows);
                        console.log(r);
                        $scope.jTipDocumentos.map(function (a){
                            if(a.id==r.rows.idtipdoc){ $scope.data.idtipdoc=a; }
                        });
                        
                        $scope.jRoles.map(function (b){
                            console.log(b);
                            if(b.id_rol_sistema==r.rows.idrol_sistema){ $scope.data.idrol_sistema=b; }
                        });
                    }
                    toastr[typeNotification](r.messages, 'Información de Usuario');
                }); 
            }
        };
        $scope.cargarMenu=function(item){
            $scope.listarCajas();
            serviceAPI.consultarRolUsuario({id: item.id_rol_sistema}).success(function(r){
                $('#jstree').jstree("uncheck_all");
                if(r.success && r.num_rows==1){
                    if(r.rows.items.length>0){
                        angular.forEach(r.rows.items, function(item){
                            $('#jstree').jstree("check_node", item.no_win_sistemas);
                        });
                    }
                }
            })
        };
    }]);
})();