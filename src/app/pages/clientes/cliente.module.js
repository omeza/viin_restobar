(function () {
    'use strict';
    angular.module('BlurAdmin.pages.clientes', [
        'BlurAdmin.pages.clientes.cliente'
    ]).config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
          .state('dashboard.clientes', {
            url: '/clientes',
            template : '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
            abstract: true,
            title: 'Cliente',
            sidebarMeta: {
              icon: 'ion-happy',
              order: 500,
            }
          }).state('dashboard.clientes.cliente', {
            url: '/cliente',
            templateUrl: 'app/pages/clientes/cliente/list/clienteList.html',
            controller: 'clientelistCtrl',
            title: 'Cliente',
            sidebarMeta: {
                order: 100
            }
        }).state('dashboard.clientes.cliente-details', {
            url: '/cliente/details/:idCliente',
            templateUrl: 'app/pages/clientes/cliente/cliente.html',
            controller: "clienteCtrl",
            title: 'Cliente',
            params:{
                idempleado:''
            }
        });
    }
  })();
  