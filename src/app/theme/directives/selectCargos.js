(function () {
    'use strict';
  
    angular.module('BlurAdmin.theme')
        .directive('selectCargos', selectCargos);
  
    /** @ngInject */
    function selectCargos(getservice) {
        return {
            restrict:'A', scope:{cargos:'='}, require:'ngModel',
            link:function(scope, element, attrs, model){
                model.$render=function(){
                };
                angular.element(element).on('change', function(){
                    scope.cargos=[];
                    getservice.getCargos(model.$viewValue).then(function(response){
                        scope.cargos=response.data.cpuestos;
                    });
                });
            }
        }
    } 
   
  })();