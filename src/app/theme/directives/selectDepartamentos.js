(function () {
    'use strict';
  
    angular.module('BlurAdmin.theme')
        .directive('selectDepartamentos', selectDepartamentos);
  
    /** @ngInject */
    function selectDepartamentos(getservice) {
        return {
            restrict:'A', scope:{provincias:'=', distritos:'='  }, require:'ngModel',
            link:function(scope, element, attrs, model){
                model.$render=function(){
                };
                angular.element(element).on('change', function(){
                    console.log(model);
                    scope.provincias=[];
                    scope.distritos=[];
                    getservice.getProvincias(model.$viewValue).then(function(response){
                        scope.provincias=response.data.rows;
                    });
                });
            }
        }
    } 
   
  })();
