
(function () {
    'use strict';
  
    angular.module('BlurAdmin.theme')
        .directive('selectProvincias', selectProvincias);
  
    /** @ngInject */
    function selectProvincias(getservice) {
        return {
            restrict:'A', scope:{distritos:'='}, require:'ngModel',
            link:function(scope, element, attrs, model){                
                angular.element(element).on('change', function(){
                    console.log(model);
                    scope.distritos=[];
                    getservice.getDistritos(model.$viewValue).then(function(response){
                        scope.distritos=response.data.rows;
                    });
            });
            }
        }
    }
  
  })();