
(function () {
    'use strict';
  
    angular.module('BlurAdmin.theme')
        .directive('selectRegimen', selectRegimen);
  
    /** @ngInject */
    function selectRegimen(getservice) {
        return {
            restrict:'A', scope:{primas:'='}, require:'ngModel',
            link:function(scope, element, attrs, model){
                model.$render=function(){
                };
                angular.element(element).on('change', function(){
                    scope.primas=[];
                    console.log(model.$viewValue)
                    getservice.getPrimas(model.$viewValue).then(function(response){
                        scope.primas=response.data.cprimas;
        
                    });
                });
            }
        }
    } 
   
  })();
 