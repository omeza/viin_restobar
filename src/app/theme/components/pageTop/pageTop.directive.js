(function() {
    'use strict';

    angular.module('BlurAdmin.theme.components')
        .controller('SignOutCtrl', ['$scope', 'AuthenticationService', '$state', 'PermRoleStore', function($scope, AuthenticationService, $state, PermRoleStore){
            $scope.username = AuthenticationService.getUser();
            $scope.signOut = function(){
                AuthenticationService.setLoggedIn(false);
                AuthenticationService.signOut();
                $state.go('signin');
                /*$http.defaults.headers.common.Authorization='';*/
            };
        }])
        .directive('pageTop', function(){
            return {
                restrict: 'E',
                templateUrl: 'app/theme/components/pageTop/pageTop.html',
                controller: 'SignOutCtrl'
            };
        });

})();
