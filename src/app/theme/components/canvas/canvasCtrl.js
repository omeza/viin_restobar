(function() {
    'use strict';

    angular.module('BlurAdmin.theme.components')
    .controller('canvasCtrl', ['$scope', '$state', 'AuthenticationService', 'toastr', 'serviceAPI', 'commonService', function($scope, $state, AuthenticationService, toastr, serviceAPI, commonService){
        $scope.data={username:'', clave:''};
        $scope.signIn = function() {
            serviceAPI.signIn($scope.data).success(function(r){                
                AuthenticationService.setLoggedIn(r.auth);
                console.log(r);
                if(r.auth){
                    AuthenticationService.setRol(r.rows.no_rol_sistema);
                    AuthenticationService.setUser($scope.data.username);
                    AuthenticationService.setToken(r.token);
                    AuthenticationService.setStates(r.rows.states);
                    if(r.rows.id_rol_sistema == 3){
                        AuthenticationService.setCaja(r.rows.id_caja);
                    }
                    AuthenticationService.getSucursal(r.rows.id_sucursal);
                    if(r.rows.id_rol_sistema == 3){
                        serviceAPI.signInCaja(r.rows.id_caja).success(function(r){ 
                            var typeNotification = (!r.success) ? 'error' : 'warning';
                            if (r.success && r.totalItemCount > 0) {
                                typeNotification = 'success';
                                $state.go('dashboard', {defaultState: r.rows.name_state});  
                                toastr[typeNotification](r.messages, 'Informacion Caja Abierta');
                            }else{
                                toastr[typeNotification]('Por favor Abrir Caja', 'Informacion Caja Cerrada');
                            }                               
                        });
                    }else{
                        $state.go('dashboard', {defaultState: r.rows.name_state});  
                    }
                                        
                               
                }else{
                    toastr.error("Datos de acceso inválidos", 'Error');
                }
            });
        };  
    }]);

})();