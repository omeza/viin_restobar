/**
 * @author v.lugovsky
 * created on 10.12.2016
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.theme.inputs')
      .directive('baSwitcher', [function(){
        return {
          templateUrl: 'app/theme/inputs/baSwitcher/baSwitcher.html',
          scope: {
            switcherStyle: '@',
            switcherValue: '=',
            valueOn:'=',
            valueOff:'='
          },
          link:function(){
            console.log('baSwitcher')
          }    
        }
      }]);
})();
