(function() {
    'use strict';

    angular.module('BlurAdmin.facturaservice', [])
        .factory('facturaservice', facturaservice);

    /** @ngInject */
    function facturaservice($http, $httpParamSerializerJQLike) {
            var datas={};
            
            datas.getFacturas=function(id_anio, id_mes){
				return $http({
					url:'http://tlm.localhost/api/controller/rrhh/controller.facturas.php', method:"POST", data:$httpParamSerializerJQLike({oper:'facturas',id_anio:id_anio,id_mes:id_mes}),
					headers:{'Content-Type':'application/x-www-form-urlencoded'}
				});
			};
			return datas;
    }
})();
