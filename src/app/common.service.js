(function() {
    'use strict';

    angular.module('BlurAdmin.commonservice', [])
        .factory('permissions', [ 'AuthenticationService', function(AuthenticationService){
            return {
                hasPermissions:function(state){
                    var permissionList=AuthenticationService.getStatesusers();
                    if(!angular.isString(state)){
                        return false;
                    }
                    return (permissionList.indexOf(state)===-1);
                }
            }
        }])
        .directive('hasPermission', ['permissions', function(permissions){
            return {
                scope:{
                    name:'=namestate'
                },
                link:function(scope, element, attrs){
                    element[0].style.display=(permissions.hasPermissions(scope.name))?'none':'block';
                }
            }
        }])
        .factory('commonService',[ '$localStorage', function($localStorage){
            return {
                setConfig:function(infoConfig){
                    $localStorage.infoConfig=infoConfig;
                },
                setEmpresa:function(infoEmpresa){
                    $localStorage.infoEmpresa=infoEmpresa;
                },
                getConfig:function(){
                    return $localStorage.infoConfig;
                },
                getEmpresa:function(){
                    return $localStorage.infoEmpresa;
                },
                updateConfig:function(data){
                    angular.extend($localStorage.infoConfig, data);
                }
            }
        }]);
})();
