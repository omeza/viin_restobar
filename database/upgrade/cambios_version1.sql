DELIMITER $$
CREATE PROCEDURE `autoincidproduct`(IN `idproducto` INT, INOUT `codigo` CHAR(10))
BEGIN
  	DECLARE ncodigo CHAR(10) DEFAULT '';
    SELECT LPAD(inicia_codigo+1,10,'0') INTO ncodigo FROM configuracion;
    UPDATE configuracion SET inicia_codigo=inicia_codigo+1;
    UPDATE productos SET cod_producto=ncodigo WHERE idproductos=idproducto;
    SET codigo:=ncodigo;
END$$

CREATE PROCEDURE `get_lastcodproducto`()
BEGIN
    DECLARE lastnum INT;
    SELECT MAX(CAST(cod_producto AS INT)) INTO lastnum FROM productos WHERE cod_producto REGEXP '^[0-9]' = 1;
    UPDATE configuracion SET inicia_codigo=lastnum;
END$$
DELIMITER ;
---Cambios almacenes------
--------------------------

-----Cambios area---------
--------------------------

----Cambios atc_medica----
--------------------------

-------Cambios banco------
--------------------------

------Cambios boletas-----
ALTER TABLE `boletas` CHANGE `fecventa` `fecventa` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `boletas` CHANGE `tipo_comprobante` `id_tipcomprobante` INT NOT NULL;
ALTER TABLE `boletas`  ADD `num_ticket` VARCHAR(15) NULL DEFAULT NULL  AFTER `mto_impuesto`,  ADD `cod_cdr` VARCHAR(4) NULL DEFAULT NULL  AFTER `num_ticket`,  ADD `message_cdr` INT NULL DEFAULT NULL  AFTER `cod_cdr`,  ADD `id_baja` INT NULL DEFAULT NULL  AFTER `message_cdr`;
--------------------------

-------Cambios cargo------
--------------------------

-------Cambios cargo------
ALTER TABLE `configuracion`  ADD `idpais` INT NOT NULL  AFTER `inicia_codigo`;
--------------------------

------Cambios compras-----
--------------------------

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comprobantes_tip_documentos`
--

CREATE TABLE `comprobantes_tip_documentos` (
  `id_tipcomprobante` int(11) NOT NULL,
  `idtip_documento` int(11) NOT NULL,
  `status` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `comprobantes_tip_documentos`
--

INSERT INTO `comprobantes_tip_documentos` (`id_tipcomprobante`, `idtip_documento`, `status`) VALUES
(1, 1, '1'),
(1, 2, '1'),
(1, 3, '1'),
(1, 5, '1'),
(2, 1, '1'),
(2, 2, '1'),
(2, 3, '1'),
(2, 5, '1');
-- --------------------------------------------------------
SET FOREIGN_KEY_CHECKS=0;
--
-- Estructura de tabla para la tabla `conceptos_adicional_comprobantes`
--

CREATE TABLE `conceptos_adicional_comprobantes` (
  `id_conceptadicion` int(11) NOT NULL,
  `cod_conceptadicion` varchar(4) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_conceptadicion` varchar(90) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `conceptos_adicional_comprobantes`
--

INSERT INTO `conceptos_adicional_comprobantes` (`id_conceptadicion`, `cod_conceptadicion`, `desc_conceptadicion`, `estatus`) VALUES
(1, '1000', 'Monto en Letras', '1'),
(2, '1002', 'Leyenda \"TRANSFERENCIA GRATUITA DE UN BIEN Y/O SERVICIO PRESTADO GRATUITAMENTE\"', '1'),
(3, '2000', 'Leyenda \"COMPROBANTE DE PERCEPCIÓN\"', '1'),
(4, '2001', 'Leyenda \"BIENES TRANSFERIDOS EN LA AMAZONÍA REGIÓN SELVAPARA SER CONSUMIDOS EN LA MISMA\"', '1'),
(5, '2002', 'Leyenda \"SERVICIOS PRESTADOS EN LA AMAZONÍA REGIÓN SELVA PARA SER CONSUMIDOS EN LA MISMA\"', '1'),
(6, '2003', 'Leyenda \"CONTRATOS DE CONSTRUCCIÓN EJECUTADOS EN LA AMAZONÍA REGIÓN SELVA\"', '1'),
(7, '2004', 'Leyenda \"Agencia de Viaje - Paquete turístico\"', '1'),
(8, '2005', 'Leyenda \"Venta realizada por emisor itinerante\"', '1'),
(9, '2006', 'Leyenda: Operación sujeta a detracción', '1'),
(10, '2007', 'Leyenda: Operación sujeta a IVAP', '1'),
(11, '3000', 'Detracciones: CODIGO DE BB Y SS SUJETOS A DETRACCION', '1'),
(12, '3001', 'Detracciones: NUMERO DE CTA EN EL BN', '1'),
(13, '3002', 'Detracciones: Recursos Hidrobiológicos-Nombre y matrícula de la embarcación', '1'),
(14, '3003', 'Detracciones: Recursos Hidrobiológicos-Tipo y cantidad de especie vendida', '1'),
(15, '3004', 'Detracciones: Recursos Hidrobiológicos -Lugar de descarga', '1'),
(16, '3005', 'Detracciones: Recursos Hidrobiológicos -Fecha de descarga', '1'),
(17, '3006', 'Detracciones: Transporte Bienes vía terrestre – Numero Registro MTC', '1'),
(18, '3007', 'Detracciones: Transporte Bienes vía terrestre – configuración vehicular', '1'),
(19, '3008', 'Detracciones: Transporte Bienes vía terrestre – punto de origen', '1'),
(20, '3009', 'Detracciones: Transporte Bienes vía terrestre – punto destino', '1'),
(21, '3010', 'Detracciones: Transporte Bienes vía terrestre – valor referencial preliminar', '1'),
(22, '4000', 'Beneficio hospedajes: Código País de emisión del pasaporte', '1'),
(23, '4001', 'Beneficio hospedajes: Código País de residencia del sujeto no domiciliado', '1'),
(24, '4002', 'Beneficio Hospedajes: Fecha de ingreso al país', '1'),
(25, '4003', 'Beneficio Hospedajes: Fecha de ingreso al establecimiento', '1'),
(26, '4004', 'Beneficio Hospedajes: Fecha de salida del establecimiento', '1'),
(27, '4005', 'Beneficio Hospedajes: Número de días de permanencia', '1'),
(28, '4006', 'Beneficio Hospedajes: Fecha de consumo', '1'),
(29, '4007', 'Beneficio Hospedajes: Paquete turístico - Nombres y Apellidos del Huésped', '1'),
(30, '4008', 'Beneficio Hospedajes: Paquete turístico – Tipo documento identidad del huésped', '1'),
(31, '4009', 'Beneficio Hospedajes: Paquete turístico – Numero de documento identidad de huésped', '1'),
(32, '5000', 'Proveedores Estado: Número de Expediente', '1'),
(33, '5001', 'Proveedores Estado : Código de unidad ejecutora', '1'),
(34, '5002', 'Proveedores Estado : N° de proceso de selección', '1'),
(35, '5003', 'Proveedores Estado : N° de contrato', '1'),
(36, '6000', 'Comercialización de Oro : Código Unico Concesión Minera', '1'),
(37, '6001', 'Comercialización de Oro : N° declaración compromiso', '1'),
(38, '6002', 'Comercialización de Oro : N° Reg. Especial .Comerci. Oro', '1'),
(39, '6003', 'Comercialización de Oro : N° Resolución que autoriza Planta de Beneficio', '1'),
(40, '6004', 'Comercialización de Oro : Ley Mineral (% concent. oro)', '1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `conceptos_adicional_comprobantes`
--
ALTER TABLE `conceptos_adicional_comprobantes`
  ADD PRIMARY KEY (`id_conceptadicion`),
  ADD UNIQUE KEY `cod_conceptadicion` (`cod_conceptadicion`),
  ADD KEY `desc_conceptadicion` (`desc_conceptadicion`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `conceptos_adicional_comprobantes`
--
ALTER TABLE `conceptos_adicional_comprobantes`
  MODIFY `id_conceptadicion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conceptos_tributarios`
--

CREATE TABLE `conceptos_tributarios` (
  `id_conceptribut` int(11) NOT NULL,
  `cod_conceptribut` varchar(4) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_conceptribut` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `conceptos_tributarios`
--

INSERT INTO `conceptos_tributarios` (`id_conceptribut`, `cod_conceptribut`, `desc_conceptribut`, `estatus`) VALUES
(1, '1000', 'Total valor de venta – Exportaciones', '1'),
(2, '1001', 'Total valor de venta - operaciones gravadas', '1'),
(3, '1002', 'Total valor de venta - operaciones inafectas', '1'),
(4, '1003', 'Total valor de venta - operaciones exoneradas', '1'),
(5, '1004', 'Total valor de venta – Operaciones gratuitas', '1'),
(6, '1005', 'Sub total de venta', '1'),
(7, '2001', 'Percepciones', '1'),
(8, '2002', 'Retenciones', '1'),
(9, '2003', 'Detracciones', '1'),
(10, '2004', 'Bonificaciones', '1'),
(11, '2005', 'Total descuentos', '1'),
(12, '3001', 'FISE (Ley 29852) Fondo Inclusión Social Energético', '1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `conceptos_tributarios`
--
ALTER TABLE `conceptos_tributarios`
  ADD PRIMARY KEY (`id_conceptribut`),
  ADD UNIQUE KEY `cod_conceptribut` (`cod_conceptribut`),
  ADD KEY `desc_conceptribut` (`desc_conceptribut`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `conceptos_tributarios`
--
ALTER TABLE `conceptos_tributarios`
  MODIFY `id_conceptribut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacion`
--

CREATE TABLE `cotizacion` (
  `id_cotizacion` int(11) NOT NULL,
  `fec_cotizacion` date NOT NULL,
  `idclientes` int(11) NOT NULL,
  `neto_cotizacion` double(13,2) NOT NULL,
  `impuesto_cotizacion` double(13,2) NOT NULL,
  `total_cotizacion` double(13,2) NOT NULL,
  `id_tipo_pago` int(11) NOT NULL,
  `id_vendedor` int(11) NOT NULL COMMENT 'id usuario vendedor'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  ADD PRIMARY KEY (`id_cotizacion`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  MODIFY `id_cotizacion` int(11) NOT NULL AUTO_INCREMENT;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos_pais`
--

CREATE TABLE `departamentos_pais` (
  `id_depto` int(11) NOT NULL,
  `cod_depto` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_depto` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus_depto` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `departamentos_pais`
--

INSERT INTO `departamentos_pais` (`id_depto`, `cod_depto`, `desc_depto`, `estatus_depto`) VALUES
(1, '01', 'Amazonas', '1'),
(2, '02', 'Áncash', '1'),
(3, '03', 'Apurímac', '1'),
(4, '04', 'Arequipa', '1'),
(5, '05', 'Ayacucho', '1'),
(6, '06', 'Cajamarca', '1'),
(7, '07', 'Callao', '1'),
(8, '08', 'Cusco', '1'),
(9, '09', 'Huancavelica', '1'),
(10, '10', 'Huánuco', '1'),
(11, '11', 'Ica', '1'),
(12, '12', 'Junín', '1'),
(13, '13', 'La Libertad', '1'),
(14, '14', 'Lambayeque', '1'),
(15, '15', 'Lima', '1'),
(16, '16', 'Loreto', '1'),
(17, '17', 'Madre de Dios', '1'),
(18, '18', 'Moquegua', '1'),
(19, '19', 'Pasco', '1'),
(20, '20', 'Piura', '1'),
(21, '21', 'Puno', '1'),
(22, '22', 'San Martín', '1'),
(23, '23', 'Tacna', '1'),
(24, '24', 'Tumbes', '1'),
(25, '25', 'Ucayali', '1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `departamentos_pais`
--
ALTER TABLE `departamentos_pais`
  ADD PRIMARY KEY (`id_depto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `departamentos_pais`
--
ALTER TABLE `departamentos_pais`
  MODIFY `id_depto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_cotizacion`
--

CREATE TABLE `detalle_cotizacion` (
  `id_detalle_ctz` int(11) NOT NULL,
  `id_cotizacion` int(11) NOT NULL,
  `idproductos` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` double(13,2) NOT NULL,
  `precio_neto` double(13,2) NOT NULL,
  `mto_impuesto` double(13,2) NOT NULL,
  `exonerado_impuesto` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `detalle_cotizacion`
--
ALTER TABLE `detalle_cotizacion`
  ADD PRIMARY KEY (`id_detalle_ctz`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `detalle_cotizacion`
--
ALTER TABLE `detalle_cotizacion`
  MODIFY `id_detalle_ctz` int(11) NOT NULL AUTO_INCREMENT;

-- --------------------------------------------------------

--
-- Cambios para la tabla `detalle_inventario`
--

ALTER TABLE `detalle_inventario` ADD `serial_1` INT NULL DEFAULT NULL AFTER `idproductos`, ADD `num_serial1` VARCHAR(15) NULL DEFAULT NULL AFTER `serial_1`, ADD `serial_2` INT NULL DEFAULT NULL AFTER `num_serial1`, ADD `num_serial2` VARCHAR(15) NULL DEFAULT NULL AFTER `serial_2`, ADD `serial_3` INT NULL DEFAULT NULL AFTER `num_serial2`, ADD `num_serial3` VARCHAR(15) NULL DEFAULT NULL AFTER `serial_3`;
ALTER TABLE `detalle_inventario`  ADD `format_date` VARCHAR(10) NULL DEFAULT NULL  AFTER `lote`,  ADD `date_valid` DATE NULL DEFAULT NULL  AFTER `format_date`;

-- --------------------------------------------------------

--
-- Cambios para la tabla `det_boleta`
--

ALTER TABLE `det_boleta`  ADD `num_serie` VARCHAR(15) NULL DEFAULT NULL  AFTER `idproductos`,  ADD `lote` VARCHAR(10) NULL DEFAULT NULL  AFTER `num_serie`,  ADD `id_promocion` INT NULL DEFAULT NULL  AFTER `lote`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `distritos`
--

CREATE TABLE `distritos` (
  `id_distrito` int(11) NOT NULL,
  `id_provincia` int(11) NOT NULL,
  `cod_distrito` varchar(6) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_distrito` varchar(55) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus_distrito` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `distritos`
--

INSERT INTO `distritos` (`id_distrito`, `id_provincia`, `cod_distrito`, `desc_distrito`, `estatus_distrito`) VALUES
(1, 1, '010101', 'Chachapoyas', '1'),
(2, 1, '010102', 'Asunción', '1'),
(3, 1, '010103', 'Balsas', '1'),
(4, 1, '010104', 'Cheto', '1'),
(5, 1, '010105', 'Chiliquin', '1'),
(6, 1, '010106', 'Chuquibamba', '1'),
(7, 1, '010107', 'Granada', '1'),
(8, 1, '010108', 'Huancas', '1'),
(9, 1, '010109', 'La Jalca', '1'),
(10, 1, '010110', 'Leimebamba', '1'),
(11, 1, '010111', 'Levanto', '1'),
(12, 1, '010112', 'Magdalena', '1'),
(13, 1, '010113', 'Mariscal Castilla', '1'),
(14, 1, '010114', 'Molinopampa', '1'),
(15, 1, '010115', 'Montevideo', '1'),
(16, 1, '010116', 'Olleros', '1'),
(17, 1, '010117', 'Quinjalca', '1'),
(18, 1, '010118', 'San Francisco de Daguas', '1'),
(19, 1, '010119', 'San Isidro de Maino', '1'),
(20, 1, '010120', 'Soloco', '1'),
(21, 1, '010121', 'Sonche', '1'),
(22, 2, '010201', 'Bagua', '1'),
(23, 2, '010202', 'Aramango', '1'),
(24, 2, '010203', 'Copallin', '1'),
(25, 2, '010204', 'El Parco', '1'),
(26, 2, '010205', 'Imaza', '1'),
(27, 2, '010206', 'La Peca', '1'),
(28, 3, '010301', 'Jumbilla', '1'),
(29, 3, '010302', 'Chisquilla', '1'),
(30, 3, '010303', 'Churuja', '1'),
(31, 3, '010304', 'Corosha', '1'),
(32, 3, '010305', 'Cuispes', '1'),
(33, 3, '010306', 'Florida', '1'),
(34, 3, '010307', 'Jazan', '1'),
(35, 3, '010308', 'Recta', '1'),
(36, 3, '010309', 'San Carlos', '1'),
(37, 3, '010310', 'Shipasbamba', '1'),
(38, 3, '010311', 'Valera', '1'),
(39, 3, '010312', 'Yambrasbamba', '1'),
(40, 4, '010401', 'Nieva', '1'),
(41, 4, '010402', 'El Cenepa', '1'),
(42, 4, '010403', 'Río Santiago', '1'),
(43, 5, '010501', 'Lamud', '1'),
(44, 5, '010502', 'Camporredondo', '1'),
(45, 5, '010503', 'Cocabamba', '1'),
(46, 5, '010504', 'Colcamar', '1'),
(47, 5, '010505', 'Conila', '1'),
(48, 5, '010506', 'Inguilpata', '1'),
(49, 5, '010507', 'Longuita', '1'),
(50, 5, '010508', 'Lonya Chico', '1'),
(51, 5, '010509', 'Luya', '1'),
(52, 5, '010510', 'Luya Viejo', '1'),
(53, 5, '010511', 'María', '1'),
(54, 5, '010512', 'Ocalli', '1'),
(55, 5, '010513', 'Ocumal', '1'),
(56, 5, '010514', 'Pisuquia', '1'),
(57, 5, '010515', 'Providencia', '1'),
(58, 5, '010516', 'San Cristóbal', '1'),
(59, 5, '010517', 'San Francisco de Yeso', '1'),
(60, 5, '010518', 'San Jerónimo', '1'),
(61, 5, '010519', 'San Juan de Lopecancha', '1'),
(62, 5, '010520', 'Santa Catalina', '1'),
(63, 5, '010521', 'Santo Tomas', '1'),
(64, 5, '010522', 'Tingo', '1'),
(65, 5, '010523', 'Trita', '1'),
(66, 6, '010601', 'San Nicolás', '1'),
(67, 6, '010602', 'Chirimoto', '1'),
(68, 6, '010603', 'Cochamal', '1'),
(69, 6, '010604', 'Huambo', '1'),
(70, 6, '010605', 'Limabamba', '1'),
(71, 6, '010606', 'Longar', '1'),
(72, 6, '010607', 'Mariscal Benavides', '1'),
(73, 6, '010608', 'Milpuc', '1'),
(74, 6, '010609', 'Omia', '1'),
(75, 6, '010610', 'Santa Rosa', '1'),
(76, 6, '010611', 'Totora', '1'),
(77, 6, '010612', 'Vista Alegre', '1'),
(78, 7, '010701', 'Bagua Grande', '1'),
(79, 7, '010702', 'Cajaruro', '1'),
(80, 7, '010703', 'Cumba', '1'),
(81, 7, '010704', 'El Milagro', '1'),
(82, 7, '010705', 'Jamalca', '1'),
(83, 7, '010706', 'Lonya Grande', '1'),
(84, 7, '010707', 'Yamon', '1'),
(85, 1, '020101', 'Huaraz', '1'),
(86, 1, '020102', 'Cochabamba', '1'),
(87, 1, '020103', 'Colcabamba', '1'),
(88, 1, '020104', 'Huanchay', '1'),
(89, 1, '020105', 'Independencia', '1'),
(90, 1, '020106', 'Jangas', '1'),
(91, 1, '020107', 'La Libertad', '1'),
(92, 1, '020108', 'Olleros', '1'),
(93, 1, '020109', 'Pampas Grande', '1'),
(94, 1, '020110', 'Pariacoto', '1'),
(95, 1, '020111', 'Pira', '1'),
(96, 1, '020112', 'Tarica', '1'),
(97, 2, '020201', 'Aija', '1'),
(98, 2, '020202', 'Coris', '1'),
(99, 2, '020203', 'Huacllan', '1'),
(100, 2, '020204', 'La Merced', '1'),
(101, 2, '020205', 'Succha', '1'),
(102, 3, '020301', 'Llamellin', '1'),
(103, 3, '020302', 'Aczo', '1'),
(104, 3, '020303', 'Chaccho', '1'),
(105, 3, '020304', 'Chingas', '1'),
(106, 3, '020305', 'Mirgas', '1'),
(107, 3, '020306', 'San Juan de Rontoy', '1'),
(108, 4, '020401', 'Chacas', '1'),
(109, 4, '020402', 'Acochaca', '1'),
(110, 5, '020501', 'Chiquian', '1'),
(111, 5, '020502', 'Abelardo Pardo Lezameta', '1'),
(112, 5, '020503', 'Antonio Raymondi', '1'),
(113, 5, '020504', 'Aquia', '1'),
(114, 5, '020505', 'Cajacay', '1'),
(115, 5, '020506', 'Canis', '1'),
(116, 5, '020507', 'Colquioc', '1'),
(117, 5, '020508', 'Huallanca', '1'),
(118, 5, '020509', 'Huasta', '1'),
(119, 5, '020510', 'Huayllacayan', '1'),
(120, 5, '020511', 'La Primavera', '1'),
(121, 5, '020512', 'Mangas', '1'),
(122, 5, '020513', 'Pacllon', '1'),
(123, 5, '020514', 'San Miguel de Corpanqui', '1'),
(124, 5, '020515', 'Ticllos', '1'),
(125, 6, '020601', 'Carhuaz', '1'),
(126, 6, '020602', 'Acopampa', '1'),
(127, 6, '020603', 'Amashca', '1'),
(128, 6, '020604', 'Anta', '1'),
(129, 6, '020605', 'Ataquero', '1'),
(130, 6, '020606', 'Marcara', '1'),
(131, 6, '020607', 'Pariahuanca', '1'),
(132, 6, '020608', 'San Miguel de Aco', '1'),
(133, 6, '020609', 'Shilla', '1'),
(134, 6, '020610', 'Tinco', '1'),
(135, 6, '020611', 'Yungar', '1'),
(136, 7, '020701', 'San Luis', '1'),
(137, 7, '020702', 'San Nicolás', '1'),
(138, 7, '020703', 'Yauya', '1'),
(139, 8, '020801', 'Casma', '1'),
(140, 8, '020802', 'Buena Vista Alta', '1'),
(141, 8, '020803', 'Comandante Noel', '1'),
(142, 8, '020804', 'Yautan', '1'),
(143, 9, '020901', 'Corongo', '1'),
(144, 9, '020902', 'Aco', '1'),
(145, 9, '020903', 'Bambas', '1'),
(146, 9, '020904', 'Cusca', '1'),
(147, 9, '020905', 'La Pampa', '1'),
(148, 9, '020906', 'Yanac', '1'),
(149, 9, '020907', 'Yupan', '1'),
(150, 10, '021001', 'Huari', '1'),
(151, 10, '021002', 'Anra', '1'),
(152, 10, '021003', 'Cajay', '1'),
(153, 10, '021004', 'Chavin de Huantar', '1'),
(154, 10, '021005', 'Huacachi', '1'),
(155, 10, '021006', 'Huacchis', '1'),
(156, 10, '021007', 'Huachis', '1'),
(157, 10, '021008', 'Huantar', '1'),
(158, 10, '021009', 'Masin', '1'),
(159, 10, '021010', 'Paucas', '1'),
(160, 10, '021011', 'Ponto', '1'),
(161, 10, '021012', 'Rahuapampa', '1'),
(162, 10, '021013', 'Rapayan', '1'),
(163, 10, '021014', 'San Marcos', '1'),
(164, 10, '021015', 'San Pedro de Chana', '1'),
(165, 10, '021016', 'Uco', '1'),
(166, 11, '021101', 'Huarmey', '1'),
(167, 11, '021102', 'Cochapeti', '1'),
(168, 11, '021103', 'Culebras', '1'),
(169, 11, '021104', 'Huayan', '1'),
(170, 11, '021105', 'Malvas', '1'),
(171, 12, '021201', 'Caraz', '1'),
(172, 12, '021202', 'Huallanca', '1'),
(173, 12, '021203', 'Huata', '1'),
(174, 12, '021204', 'Huaylas', '1'),
(175, 12, '021205', 'Mato', '1'),
(176, 12, '021206', 'Pamparomas', '1'),
(177, 12, '021207', 'Pueblo Libre', '1'),
(178, 12, '021208', 'Santa Cruz', '1'),
(179, 12, '021209', 'Santo Toribio', '1'),
(180, 12, '021210', 'Yuracmarca', '1'),
(181, 13, '021301', 'Piscobamba', '1'),
(182, 13, '021302', 'Casca', '1'),
(183, 13, '021303', 'Eleazar Guzmán Barron', '1'),
(184, 13, '021304', 'Fidel Olivas Escudero', '1'),
(185, 13, '021305', 'Llama', '1'),
(186, 13, '021306', 'Llumpa', '1'),
(187, 13, '021307', 'Lucma', '1'),
(188, 13, '021308', 'Musga', '1'),
(189, 14, '021401', 'Ocros', '1'),
(190, 14, '021402', 'Acas', '1'),
(191, 14, '021403', 'Cajamarquilla', '1'),
(192, 14, '021404', 'Carhuapampa', '1'),
(193, 14, '021405', 'Cochas', '1'),
(194, 14, '021406', 'Congas', '1'),
(195, 14, '021407', 'Llipa', '1'),
(196, 14, '021408', 'San Cristóbal de Rajan', '1'),
(197, 14, '021409', 'San Pedro', '1'),
(198, 14, '021410', 'Santiago de Chilcas', '1'),
(199, 15, '021501', 'Cabana', '1'),
(200, 15, '021502', 'Bolognesi', '1'),
(201, 15, '021503', 'Conchucos', '1'),
(202, 15, '021504', 'Huacaschuque', '1'),
(203, 15, '021505', 'Huandoval', '1'),
(204, 15, '021506', 'Lacabamba', '1'),
(205, 15, '021507', 'Llapo', '1'),
(206, 15, '021508', 'Pallasca', '1'),
(207, 15, '021509', 'Pampas', '1'),
(208, 15, '021511', 'Tauca', '1'),
(209, 16, '021601', 'Pomabamba', '1'),
(210, 16, '021602', 'Huayllan', '1'),
(211, 16, '021603', 'Parobamba', '1'),
(212, 16, '021604', 'Quinuabamba', '1'),
(213, 17, '021701', 'Recuay', '1'),
(214, 17, '021702', 'Catac', '1'),
(215, 17, '021703', 'Cotaparaco', '1'),
(216, 17, '021704', 'Huayllapampa', '1'),
(217, 17, '021705', 'Llacllin', '1'),
(218, 17, '021706', 'Marca', '1'),
(219, 17, '021707', 'Pampas Chico', '1'),
(220, 17, '021708', 'Pararin', '1'),
(221, 17, '021709', 'Tapacocha', '1'),
(222, 17, '021710', 'Ticapampa', '1'),
(223, 18, '021801', 'Chimbote', '1'),
(224, 18, '021802', 'Cáceres del Perú', '1'),
(225, 18, '021803', 'Coishco', '1'),
(226, 18, '021804', 'Macate', '1'),
(227, 18, '021805', 'Moro', '1'),
(228, 18, '021806', 'Nepeña', '1'),
(229, 18, '021807', 'Samanco', '1'),
(230, 18, '021808', 'Santa', '1'),
(231, 18, '021809', 'Nuevo Chimbote', '1'),
(232, 19, '021901', 'Sihuas', '1'),
(233, 19, '021902', 'Acobamba', '1'),
(234, 19, '021903', 'Alfonso Ugarte', '1'),
(235, 19, '021904', 'Cashapampa', '1'),
(236, 19, '021905', 'Chingalpo', '1'),
(237, 19, '021906', 'Huayllabamba', '1'),
(238, 19, '021907', 'Quiches', '1'),
(239, 19, '021908', 'Ragash', '1'),
(240, 19, '021909', 'San Juan', '1'),
(241, 19, '021910', 'Sicsibamba', '1'),
(242, 20, '022001', 'Yungay', '1'),
(243, 20, '022002', 'Cascapara', '1'),
(244, 20, '022003', 'Mancos', '1'),
(245, 20, '022004', 'Matacoto', '1'),
(246, 20, '022005', 'Quillo', '1'),
(247, 20, '022006', 'Ranrahirca', '1'),
(248, 20, '022007', 'Shupluy', '1'),
(249, 20, '022008', 'Yanama', '1'),
(250, 1, '030101', 'Abancay', '1'),
(251, 1, '030102', 'Chacoche', '1'),
(252, 1, '030103', 'Circa', '1'),
(253, 1, '030104', 'Curahuasi', '1'),
(254, 1, '030105', 'Huanipaca', '1'),
(255, 1, '030106', 'Lambrama', '1'),
(256, 1, '030107', 'Pichirhua', '1'),
(257, 1, '030108', 'San Pedro de Cachora', '1'),
(258, 1, '030109', 'Tamburco', '1'),
(259, 2, '030201', 'Andahuaylas', '1'),
(260, 2, '030202', 'Andarapa', '1'),
(261, 2, '030203', 'Chiara', '1'),
(262, 2, '030204', 'Huancarama', '1'),
(263, 2, '030205', 'Huancaray', '1'),
(264, 2, '030206', 'Huayana', '1'),
(265, 2, '030207', 'Kishuara', '1'),
(266, 2, '030208', 'Pacobamba', '1'),
(267, 2, '030209', 'Pacucha', '1'),
(268, 2, '030210', 'Pampachiri', '1'),
(269, 2, '030211', 'Pomacocha', '1'),
(270, 2, '030212', 'San Antonio de Cachi', '1'),
(271, 2, '030213', 'San Jerónimo', '1'),
(272, 2, '030214', 'San Miguel de Chaccrampa', '1'),
(273, 2, '030215', 'Santa María de Chicmo', '1'),
(274, 2, '030216', 'Talavera', '1'),
(275, 2, '030217', 'Tumay Huaraca', '1'),
(276, 2, '030218', 'Turpo', '1'),
(277, 2, '030219', 'Kaquiabamba', '1'),
(278, 2, '030220', 'José María Arguedas', '1'),
(279, 3, '030301', 'Antabamba', '1'),
(280, 3, '030302', 'El Oro', '1'),
(281, 3, '030303', 'Huaquirca', '1'),
(282, 3, '030304', 'Juan Espinoza Medrano', '1'),
(283, 3, '030305', 'Oropesa', '1'),
(284, 3, '030306', 'Pachaconas', '1'),
(285, 3, '030307', 'Sabaino', '1'),
(286, 4, '030401', 'Chalhuanca', '1'),
(287, 4, '030402', 'Capaya', '1'),
(288, 4, '030403', 'Caraybamba', '1'),
(289, 4, '030404', 'Chapimarca', '1'),
(290, 4, '030405', 'Colcabamba', '1'),
(291, 4, '030406', 'Cotaruse', '1'),
(292, 4, '030407', 'Ihuayllo', '1'),
(293, 4, '030408', 'Justo Apu Sahuaraura', '1'),
(294, 4, '030409', 'Lucre', '1'),
(295, 4, '030410', 'Pocohuanca', '1'),
(296, 4, '030411', 'San Juan de Chacña', '1'),
(297, 4, '030412', 'Sañayca', '1'),
(298, 4, '030413', 'Soraya', '1'),
(299, 4, '030414', 'Tapairihua', '1'),
(300, 4, '030415', 'Tintay', '1'),
(301, 4, '030416', 'Toraya', '1'),
(302, 4, '030417', 'Yanaca', '1'),
(303, 5, '030501', 'Tambobamba', '1'),
(304, 5, '030502', 'Cotabambas', '1'),
(305, 5, '030503', 'Coyllurqui', '1'),
(306, 5, '030504', 'Haquira', '1'),
(307, 5, '030505', 'Mara', '1'),
(308, 5, '030506', 'Challhuahuacho', '1'),
(309, 6, '030601', 'Chincheros', '1'),
(310, 6, '030602', 'Anco_Huallo', '1'),
(311, 6, '030603', 'Cocharcas', '1'),
(312, 6, '030604', 'Huaccana', '1'),
(313, 6, '030605', 'Ocobamba', '1'),
(314, 6, '030606', 'Ongoy', '1'),
(315, 6, '030607', 'Uranmarca', '1'),
(316, 6, '030608', 'Ranracancha', '1'),
(317, 6, '030609', 'Rocchacc', '1'),
(318, 6, '030610', 'El Porvenir', '1'),
(319, 7, '030701', 'Chuquibambilla', '1'),
(320, 7, '030702', 'Curpahuasi', '1'),
(321, 7, '030703', 'Gamarra', '1'),
(322, 7, '030704', 'Huayllati', '1'),
(323, 7, '030705', 'Mamara', '1'),
(324, 7, '030706', 'Micaela Bastidas', '1'),
(325, 7, '030707', 'Pataypampa', '1'),
(326, 7, '030708', 'Progreso', '1'),
(327, 7, '030709', 'San Antonio', '1'),
(328, 7, '030711', 'Turpay', '1'),
(329, 7, '030712', 'Vilcabamba', '1'),
(330, 7, '030713', 'Virundo', '1'),
(331, 7, '030714', 'Curasco', '1'),
(332, 1, '040101', 'Arequipa', '1'),
(333, 1, '040102', 'Alto Selva Alegre', '1'),
(334, 1, '040103', 'Cayma', '1'),
(335, 1, '040104', 'Cerro Colorado', '1'),
(336, 1, '040105', 'Characato', '1'),
(337, 1, '040106', 'Chiguata', '1'),
(338, 1, '040107', 'Jacobo Hunter', '1'),
(339, 1, '040108', 'La Joya', '1'),
(340, 1, '040109', 'Mariano Melgar', '1'),
(341, 1, '040110', 'Miraflores', '1'),
(342, 1, '040111', 'Mollebaya', '1'),
(343, 1, '040112', 'Paucarpata', '1'),
(344, 1, '040113', 'Pocsi', '1'),
(345, 1, '040114', 'Polobaya', '1'),
(346, 1, '040115', 'Quequeña', '1'),
(347, 1, '040116', 'Sabandia', '1'),
(348, 1, '040117', 'Sachaca', '1'),
(349, 1, '040118', 'San Juan de Siguas', '1'),
(350, 1, '040119', 'San Juan de Tarucani', '1'),
(351, 1, '040120', 'Santa Isabel de Siguas', '1'),
(352, 1, '040121', 'Santa Rita de Siguas', '1'),
(353, 1, '040122', 'Socabaya', '1'),
(354, 1, '040123', 'Tiabaya', '1'),
(355, 1, '040124', 'Uchumayo', '1'),
(356, 1, '040125', 'Vitor', '1'),
(357, 1, '040126', 'Yanahuara', '1'),
(358, 1, '040127', 'Yarabamba', '1'),
(359, 1, '040128', 'Yura', '1'),
(360, 1, '040129', 'José Luis Bustamante Y Rivero', '1'),
(361, 2, '040201', 'Camaná', '1'),
(362, 2, '040203', 'Mariano Nicolás Valcárcel', '1'),
(363, 2, '040204', 'Mariscal Cáceres', '1'),
(364, 2, '040205', 'Nicolás de Pierola', '1'),
(365, 2, '040206', 'Ocoña', '1'),
(366, 2, '040207', 'Quilca', '1'),
(367, 2, '040208', 'Samuel Pastor', '1'),
(368, 3, '040301', 'Caravelí', '1'),
(369, 3, '040303', 'Atico', '1'),
(370, 3, '040304', 'Atiquipa', '1'),
(371, 3, '040305', 'Bella Unión', '1'),
(372, 3, '040306', 'Cahuacho', '1'),
(373, 3, '040307', 'Chala', '1'),
(374, 3, '040308', 'Chaparra', '1'),
(375, 3, '040309', 'Huanuhuanu', '1'),
(376, 3, '040310', 'Jaqui', '1'),
(377, 3, '040311', 'Lomas', '1'),
(378, 3, '040312', 'Quicacha', '1'),
(379, 3, '040313', 'Yauca', '1'),
(380, 4, '040401', 'Aplao', '1'),
(381, 4, '040402', 'Andagua', '1'),
(382, 4, '040403', 'Ayo', '1'),
(383, 4, '040404', 'Chachas', '1'),
(384, 4, '040405', 'Chilcaymarca', '1'),
(385, 4, '040406', 'Choco', '1'),
(386, 4, '040407', 'Huancarqui', '1'),
(387, 4, '040408', 'Machaguay', '1'),
(388, 4, '040409', 'Orcopampa', '1'),
(389, 4, '040410', 'Pampacolca', '1'),
(390, 4, '040411', 'Tipan', '1'),
(391, 4, '040412', 'Uñon', '1'),
(392, 4, '040413', 'Uraca', '1'),
(393, 4, '040414', 'Viraco', '1'),
(394, 5, '040501', 'Chivay', '1'),
(395, 5, '040502', 'Achoma', '1'),
(396, 5, '040503', 'Cabanaconde', '1'),
(397, 5, '040504', 'Callalli', '1'),
(398, 5, '040505', 'Caylloma', '1'),
(399, 5, '040506', 'Coporaque', '1'),
(400, 5, '040507', 'Huambo', '1'),
(401, 5, '040508', 'Huanca', '1'),
(402, 5, '040509', 'Ichupampa', '1'),
(403, 5, '040510', 'Lari', '1'),
(404, 5, '040511', 'Lluta', '1'),
(405, 5, '040512', 'Maca', '1'),
(406, 5, '040513', 'Madrigal', '1'),
(407, 5, '040514', 'San Antonio de Chuca', '1'),
(408, 5, '040515', 'Sibayo', '1'),
(409, 5, '040516', 'Tapay', '1'),
(410, 5, '040517', 'Tisco', '1'),
(411, 5, '040518', 'Tuti', '1'),
(412, 5, '040519', 'Yanque', '1'),
(413, 5, '040520', 'Majes', '1'),
(414, 6, '040601', 'Chuquibamba', '1'),
(415, 6, '040602', 'Andaray', '1'),
(416, 6, '040603', 'Cayarani', '1'),
(417, 6, '040604', 'Chichas', '1'),
(418, 6, '040605', 'Iray', '1'),
(419, 6, '040606', 'Río Grande', '1'),
(420, 6, '040607', 'Salamanca', '1'),
(421, 6, '040608', 'Yanaquihua', '1'),
(422, 7, '040701', 'Mollendo', '1'),
(423, 7, '040702', 'Cocachacra', '1'),
(424, 7, '040703', 'Dean Valdivia', '1'),
(425, 7, '040704', 'Islay', '1'),
(426, 7, '040705', 'Mejia', '1'),
(427, 7, '040706', 'Punta de Bombón', '1'),
(428, 8, '040801', 'Cotahuasi', '1'),
(429, 8, '040802', 'Alca', '1'),
(430, 8, '040803', 'Charcana', '1'),
(431, 8, '040804', 'Huaynacotas', '1'),
(432, 8, '040805', 'Pampamarca', '1'),
(433, 8, '040806', 'Puyca', '1'),
(434, 8, '040807', 'Quechualla', '1'),
(435, 8, '040808', 'Sayla', '1'),
(436, 8, '040809', 'Tauria', '1'),
(437, 8, '040810', 'Tomepampa', '1'),
(438, 8, '040811', 'Toro', '1'),
(439, 1, '050101', 'Ayacucho', '1'),
(440, 1, '050102', 'Acocro', '1'),
(441, 1, '050103', 'Acos Vinchos', '1'),
(442, 1, '050104', 'Carmen Alto', '1'),
(443, 1, '050105', 'Chiara', '1'),
(444, 1, '050106', 'Ocros', '1'),
(445, 1, '050107', 'Pacaycasa', '1'),
(446, 1, '050108', 'Quinua', '1'),
(447, 1, '050109', 'San José de Ticllas', '1'),
(448, 1, '050110', 'San Juan Bautista', '1'),
(449, 1, '050111', 'Santiago de Pischa', '1'),
(450, 1, '050112', 'Socos', '1'),
(451, 1, '050113', 'Tambillo', '1'),
(452, 1, '050114', 'Vinchos', '1'),
(453, 1, '050115', 'Jesús Nazareno', '1'),
(454, 1, '050116', 'Andrés Avelino Cáceres Dorregaray', '1'),
(455, 2, '050201', 'Cangallo', '1'),
(456, 2, '050202', 'Chuschi', '1'),
(457, 2, '050203', 'Los Morochucos', '1'),
(458, 2, '050204', 'María Parado de Bellido', '1'),
(459, 2, '050205', 'Paras', '1'),
(460, 2, '050206', 'Totos', '1'),
(461, 3, '050301', 'Sancos', '1'),
(462, 3, '050302', 'Carapo', '1'),
(463, 3, '050303', 'Sacsamarca', '1'),
(464, 3, '050304', 'Santiago de Lucanamarca', '1'),
(465, 4, '050401', 'Huanta', '1'),
(466, 4, '050402', 'Ayahuanco', '1'),
(467, 4, '050403', 'Huamanguilla', '1'),
(468, 4, '050404', 'Iguain', '1'),
(469, 4, '050405', 'Luricocha', '1'),
(470, 4, '050406', 'Santillana', '1'),
(471, 4, '050407', 'Sivia', '1'),
(472, 4, '050408', 'Llochegua', '1'),
(473, 4, '050409', 'Canayre', '1'),
(474, 4, '050410', 'Uchuraccay', '1'),
(475, 4, '050411', 'Pucacolpa', '1'),
(476, 4, '050412', 'Chaca', '1'),
(477, 5, '050501', 'San Miguel', '1'),
(478, 5, '050502', 'Anco', '1'),
(479, 5, '050503', 'Ayna', '1'),
(480, 5, '050504', 'Chilcas', '1'),
(481, 5, '050505', 'Chungui', '1'),
(482, 5, '050506', 'Luis Carranza', '1'),
(483, 5, '050507', 'Santa Rosa', '1'),
(484, 5, '050508', 'Tambo', '1'),
(485, 5, '050509', 'Samugari', '1'),
(486, 5, '050510', 'Anchihuay', '1'),
(487, 6, '050601', 'Puquio', '1'),
(488, 6, '050602', 'Aucara', '1'),
(489, 6, '050603', 'Cabana', '1'),
(490, 6, '050604', 'Carmen Salcedo', '1'),
(491, 6, '050605', 'Chaviña', '1'),
(492, 6, '050606', 'Chipao', '1'),
(493, 6, '050607', 'Huac-Huas', '1'),
(494, 6, '050608', 'Laramate', '1'),
(495, 6, '050609', 'Leoncio Prado', '1'),
(496, 6, '050610', 'Llauta', '1'),
(497, 6, '050611', 'Lucanas', '1'),
(498, 6, '050612', 'Ocaña', '1'),
(499, 6, '050613', 'Otoca', '1'),
(500, 6, '050614', 'Saisa', '1'),
(501, 6, '050615', 'San Cristóbal', '1'),
(502, 6, '050616', 'San Juan', '1'),
(503, 6, '050617', 'San Pedro', '1'),
(504, 6, '050618', 'San Pedro de Palco', '1'),
(505, 6, '050619', 'Sancos', '1'),
(506, 6, '050620', 'Santa Ana de Huaycahuacho', '1'),
(507, 6, '050621', 'Santa Lucia', '1'),
(508, 7, '050701', 'Coracora', '1'),
(509, 7, '050702', 'Chumpi', '1'),
(510, 7, '050703', 'Coronel Castañeda', '1'),
(511, 7, '050704', 'Pacapausa', '1'),
(512, 7, '050705', 'Pullo', '1'),
(513, 7, '050706', 'Puyusca', '1'),
(514, 7, '050707', 'San Francisco de Ravacayco', '1'),
(515, 7, '050708', 'Upahuacho', '1'),
(516, 8, '050801', 'Pausa', '1'),
(517, 8, '050802', 'Colta', '1'),
(518, 8, '050803', 'Corculla', '1'),
(519, 8, '050804', 'Lampa', '1'),
(520, 8, '050805', 'Marcabamba', '1'),
(521, 8, '050806', 'Oyolo', '1'),
(522, 8, '050807', 'Pararca', '1'),
(523, 8, '050808', 'San Javier de Alpabamba', '1'),
(524, 8, '050809', 'San José de Ushua', '1'),
(525, 8, '050810', 'Sara Sara', '1'),
(526, 9, '050901', 'Querobamba', '1'),
(527, 9, '050902', 'Belén', '1'),
(528, 9, '050903', 'Chalcos', '1'),
(529, 9, '050904', 'Chilcayoc', '1'),
(530, 9, '050905', 'Huacaña', '1'),
(531, 9, '050906', 'Morcolla', '1'),
(532, 9, '050907', 'Paico', '1'),
(533, 9, '050908', 'San Pedro de Larcay', '1'),
(534, 9, '050909', 'San Salvador de Quije', '1'),
(535, 9, '050910', 'Santiago de Paucaray', '1'),
(536, 9, '050911', 'Soras', '1'),
(537, 10, '051001', 'Huancapi', '1'),
(538, 10, '051002', 'Alcamenca', '1'),
(539, 10, '051003', 'Apongo', '1'),
(540, 10, '051004', 'Asquipata', '1'),
(541, 10, '051005', 'Canaria', '1'),
(542, 10, '051006', 'Cayara', '1'),
(543, 10, '051007', 'Colca', '1'),
(544, 10, '051008', 'Huamanquiquia', '1'),
(545, 10, '051009', 'Huancaraylla', '1'),
(546, 10, '051010', 'Huaya', '1'),
(547, 10, '051011', 'Sarhua', '1'),
(548, 10, '051012', 'Vilcanchos', '1'),
(549, 11, '051101', 'Vilcas Huaman', '1'),
(550, 11, '051102', 'Accomarca', '1'),
(551, 11, '051103', 'Carhuanca', '1'),
(552, 11, '051104', 'Concepción', '1'),
(553, 11, '051105', 'Huambalpa', '1'),
(554, 11, '051106', 'Independencia', '1'),
(555, 11, '051107', 'Saurama', '1'),
(556, 11, '051108', 'Vischongo', '1'),
(557, 1, '060101', 'Cajamarca', '1'),
(558, 1, '060103', 'Chetilla', '1'),
(559, 1, '060104', 'Cospan', '1'),
(560, 1, '060105', 'Encañada', '1'),
(561, 1, '060106', 'Jesús', '1'),
(562, 1, '060107', 'Llacanora', '1'),
(563, 1, '060108', 'Los Baños del Inca', '1'),
(564, 1, '060109', 'Magdalena', '1'),
(565, 1, '060110', 'Matara', '1'),
(566, 1, '060111', 'Namora', '1'),
(567, 1, '060112', 'San Juan', '1'),
(568, 2, '060201', 'Cajabamba', '1'),
(569, 2, '060202', 'Cachachi', '1'),
(570, 2, '060203', 'Condebamba', '1'),
(571, 2, '060204', 'Sitacocha', '1'),
(572, 3, '060301', 'Celendín', '1'),
(573, 3, '060302', 'Chumuch', '1'),
(574, 3, '060303', 'Cortegana', '1'),
(575, 3, '060304', 'Huasmin', '1'),
(576, 3, '060305', 'Jorge Chávez', '1'),
(577, 3, '060306', 'José Gálvez', '1'),
(578, 3, '060307', 'Miguel Iglesias', '1'),
(579, 3, '060308', 'Oxamarca', '1'),
(580, 3, '060309', 'Sorochuco', '1'),
(581, 3, '060310', 'Sucre', '1'),
(582, 3, '060311', 'Utco', '1'),
(583, 3, '060312', 'La Libertad de Pallan', '1'),
(584, 4, '060401', 'Chota', '1'),
(585, 4, '060402', 'Anguia', '1'),
(586, 4, '060403', 'Chadin', '1'),
(587, 4, '060404', 'Chiguirip', '1'),
(588, 4, '060405', 'Chimban', '1'),
(589, 4, '060406', 'Choropampa', '1'),
(590, 4, '060407', 'Cochabamba', '1'),
(591, 4, '060408', 'Conchan', '1'),
(592, 4, '060409', 'Huambos', '1'),
(593, 4, '060410', 'Lajas', '1'),
(594, 4, '060411', 'Llama', '1'),
(595, 4, '060412', 'Miracosta', '1'),
(596, 4, '060413', 'Paccha', '1'),
(597, 4, '060414', 'Pion', '1'),
(598, 4, '060415', 'Querocoto', '1'),
(599, 4, '060416', 'San Juan de Licupis', '1'),
(600, 4, '060417', 'Tacabamba', '1'),
(601, 4, '060418', 'Tocmoche', '1'),
(602, 4, '060419', 'Chalamarca', '1'),
(603, 5, '060501', 'Contumaza', '1'),
(604, 5, '060502', 'Chilete', '1'),
(605, 5, '060503', 'Cupisnique', '1'),
(606, 5, '060504', 'Guzmango', '1'),
(607, 5, '060505', 'San Benito', '1'),
(608, 5, '060506', 'Santa Cruz de Toledo', '1'),
(609, 5, '060507', 'Tantarica', '1'),
(610, 5, '060508', 'Yonan', '1'),
(611, 6, '060601', 'Cutervo', '1'),
(612, 6, '060602', 'Callayuc', '1'),
(613, 6, '060603', 'Choros', '1'),
(614, 6, '060604', 'Cujillo', '1'),
(615, 6, '060605', 'La Ramada', '1'),
(616, 6, '060606', 'Pimpingos', '1'),
(617, 6, '060607', 'Querocotillo', '1'),
(618, 6, '060608', 'San Andrés de Cutervo', '1'),
(619, 6, '060609', 'San Juan de Cutervo', '1'),
(620, 6, '060610', 'San Luis de Lucma', '1'),
(621, 6, '060611', 'Santa Cruz', '1'),
(622, 6, '060612', 'Santo Domingo de la Capilla', '1'),
(623, 6, '060613', 'Santo Tomas', '1'),
(624, 6, '060614', 'Socota', '1'),
(625, 6, '060615', 'Toribio Casanova', '1'),
(626, 7, '060701', 'Bambamarca', '1'),
(627, 7, '060702', 'Chugur', '1'),
(628, 7, '060703', 'Hualgayoc', '1'),
(629, 8, '060801', 'Jaén', '1'),
(630, 8, '060802', 'Bellavista', '1'),
(631, 8, '060803', 'Chontali', '1'),
(632, 8, '060804', 'Colasay', '1'),
(633, 8, '060805', 'Huabal', '1'),
(634, 8, '060806', 'Las Pirias', '1'),
(635, 8, '060807', 'Pomahuaca', '1'),
(636, 8, '060808', 'Pucara', '1'),
(637, 8, '060809', 'Sallique', '1'),
(638, 8, '060810', 'San Felipe', '1'),
(639, 8, '060811', 'San José del Alto', '1'),
(640, 8, '060812', 'Santa Rosa', '1'),
(641, 9, '060901', 'San Ignacio', '1'),
(642, 9, '060902', 'Chirinos', '1'),
(643, 9, '060903', 'Huarango', '1'),
(644, 9, '060904', 'La Coipa', '1'),
(645, 9, '060905', 'Namballe', '1'),
(646, 9, '060906', 'San José de Lourdes', '1'),
(647, 9, '060907', 'Tabaconas', '1'),
(648, 10, '061001', 'Pedro Gálvez', '1'),
(649, 10, '061002', 'Chancay', '1'),
(650, 10, '061003', 'Eduardo Villanueva', '1'),
(651, 10, '061004', 'Gregorio Pita', '1'),
(652, 10, '061005', 'Ichocan', '1'),
(653, 10, '061006', 'José Manuel Quiroz', '1'),
(654, 10, '061007', 'José Sabogal', '1'),
(655, 11, '061102', 'Bolívar', '1'),
(656, 11, '061103', 'Calquis', '1'),
(657, 11, '061104', 'Catilluc', '1'),
(658, 11, '061105', 'El Prado', '1'),
(659, 11, '061106', 'La Florida', '1'),
(660, 11, '061107', 'Llapa', '1'),
(661, 11, '061108', 'Nanchoc', '1'),
(662, 11, '061109', 'Niepos', '1'),
(663, 11, '061110', 'San Gregorio', '1'),
(664, 11, '061111', 'San Silvestre de Cochan', '1'),
(665, 11, '061112', 'Tongod', '1'),
(666, 11, '061113', 'Unión Agua Blanca', '1'),
(667, 12, '061201', 'San Pablo', '1'),
(668, 12, '061202', 'San Bernardino', '1'),
(669, 12, '061203', 'San Luis', '1'),
(670, 12, '061204', 'Tumbaden', '1'),
(671, 13, '061301', 'Santa Cruz', '1'),
(672, 13, '061302', 'Andabamba', '1'),
(673, 13, '061303', 'Catache', '1'),
(674, 13, '061304', 'Chancaybaños', '1'),
(675, 13, '061305', 'La Esperanza', '1'),
(676, 13, '061306', 'Ninabamba', '1'),
(677, 13, '061307', 'Pulan', '1'),
(678, 13, '061308', 'Saucepampa', '1'),
(679, 13, '061309', 'Sexi', '1'),
(680, 13, '061310', 'Uticyacu', '1'),
(681, 13, '061311', 'Yauyucan', '1'),
(682, 1, '070101', 'Callao', '1'),
(683, 1, '070103', 'Carmen de la Legua Reynoso', '1'),
(684, 1, '070104', 'La Perla', '1'),
(685, 1, '070105', 'La Punta', '1'),
(686, 1, '070106', 'Ventanilla', '1'),
(687, 1, '070107', 'Mi Perú', '1'),
(688, 1, '080101', 'Cusco', '1'),
(689, 1, '080102', 'Ccorca', '1'),
(690, 1, '080103', 'Poroy', '1'),
(691, 1, '080104', 'San Jerónimo', '1'),
(692, 1, '080105', 'San Sebastian', '1'),
(693, 1, '080106', 'Santiago', '1'),
(694, 1, '080107', 'Saylla', '1'),
(695, 1, '080108', 'Wanchaq', '1'),
(696, 2, '080201', 'Acomayo', '1'),
(697, 2, '080202', 'Acopia', '1'),
(698, 2, '080203', 'Acos', '1'),
(699, 2, '080204', 'Mosoc Llacta', '1'),
(700, 2, '080205', 'Pomacanchi', '1'),
(701, 2, '080206', 'Rondocan', '1'),
(702, 2, '080207', 'Sangarara', '1'),
(703, 3, '080301', 'Anta', '1'),
(704, 3, '080302', 'Ancahuasi', '1'),
(705, 3, '080303', 'Cachimayo', '1'),
(706, 3, '080304', 'Chinchaypujio', '1'),
(707, 3, '080305', 'Huarocondo', '1'),
(708, 3, '080306', 'Limatambo', '1'),
(709, 3, '080307', 'Mollepata', '1'),
(710, 3, '080308', 'Pucyura', '1'),
(711, 3, '080309', 'Zurite', '1'),
(712, 4, '080401', 'Calca', '1'),
(713, 4, '080402', 'Coya', '1'),
(714, 4, '080403', 'Lamay', '1'),
(715, 4, '080404', 'Lares', '1'),
(716, 4, '080405', 'Pisac', '1'),
(717, 4, '080406', 'San Salvador', '1'),
(718, 4, '080407', 'Taray', '1'),
(719, 4, '080408', 'Yanatile', '1'),
(720, 5, '080501', 'Yanaoca', '1'),
(721, 5, '080502', 'Checca', '1'),
(722, 5, '080503', 'Kunturkanki', '1'),
(723, 5, '080504', 'Langui', '1'),
(724, 5, '080505', 'Layo', '1'),
(725, 5, '080506', 'Pampamarca', '1'),
(726, 5, '080507', 'Quehue', '1'),
(727, 5, '080508', 'Tupac Amaru', '1'),
(728, 6, '080601', 'Sicuani', '1'),
(729, 6, '080602', 'Checacupe', '1'),
(730, 6, '080603', 'Combapata', '1'),
(731, 6, '080604', 'Marangani', '1'),
(732, 6, '080605', 'Pitumarca', '1'),
(733, 6, '080606', 'San Pablo', '1'),
(734, 6, '080607', 'San Pedro', '1'),
(735, 6, '080608', 'Tinta', '1'),
(736, 7, '080701', 'Santo Tomas', '1'),
(737, 7, '080702', 'Capacmarca', '1'),
(738, 7, '080703', 'Chamaca', '1'),
(739, 7, '080704', 'Colquemarca', '1'),
(740, 7, '080705', 'Livitaca', '1'),
(741, 7, '080706', 'Llusco', '1'),
(742, 7, '080707', 'Quiñota', '1'),
(743, 7, '080708', 'Velille', '1'),
(744, 8, '080801', 'Espinar', '1'),
(745, 8, '080802', 'Condoroma', '1'),
(746, 8, '080803', 'Coporaque', '1'),
(747, 8, '080804', 'Ocoruro', '1'),
(748, 8, '080805', 'Pallpata', '1'),
(749, 8, '080806', 'Pichigua', '1'),
(750, 8, '080807', 'Suyckutambo', '1'),
(751, 8, '080808', 'Alto Pichigua', '1'),
(752, 9, '080901', 'Santa Ana', '1'),
(753, 9, '080902', 'Echarate', '1'),
(754, 9, '080903', 'Huayopata', '1'),
(755, 9, '080904', 'Maranura', '1'),
(756, 9, '080906', 'Quellouno', '1'),
(757, 9, '080907', 'Kimbiri', '1'),
(758, 9, '080908', 'Santa Teresa', '1'),
(759, 9, '080909', 'Vilcabamba', '1'),
(760, 9, '080910', 'Pichari', '1'),
(761, 9, '080911', 'Inkawasi', '1'),
(762, 9, '080912', 'Villa Virgen', '1'),
(763, 9, '080913', 'Villa Kintiarina', '1'),
(764, 10, '081001', 'Paruro', '1'),
(765, 10, '081002', 'Accha', '1'),
(766, 10, '081003', 'Ccapi', '1'),
(767, 10, '081004', 'Colcha', '1'),
(768, 10, '081005', 'Huanoquite', '1'),
(769, 10, '081006', 'Omacha', '1'),
(770, 10, '081007', 'Paccaritambo', '1'),
(771, 10, '081008', 'Pillpinto', '1'),
(772, 10, '081009', 'Yaurisque', '1'),
(773, 11, '081101', 'Paucartambo', '1'),
(774, 11, '081102', 'Caicay', '1'),
(775, 11, '081103', 'Challabamba', '1'),
(776, 11, '081104', 'Colquepata', '1'),
(777, 11, '081105', 'Huancarani', '1'),
(778, 11, '081106', 'Kosñipata', '1'),
(779, 12, '081201', 'Urcos', '1'),
(780, 12, '081202', 'Andahuaylillas', '1'),
(781, 12, '081203', 'Camanti', '1'),
(782, 12, '081204', 'Ccarhuayo', '1'),
(783, 12, '081205', 'Ccatca', '1'),
(784, 12, '081206', 'Cusipata', '1'),
(785, 12, '081207', 'Huaro', '1'),
(786, 12, '081208', 'Lucre', '1'),
(787, 12, '081209', 'Marcapata', '1'),
(788, 12, '081210', 'Ocongate', '1'),
(789, 12, '081211', 'Oropesa', '1'),
(790, 12, '081212', 'Quiquijana', '1'),
(791, 13, '081301', 'Urubamba', '1'),
(792, 13, '081302', 'Chinchero', '1'),
(793, 13, '081303', 'Huayllabamba', '1'),
(794, 13, '081304', 'Machupicchu', '1'),
(795, 13, '081305', 'Maras', '1'),
(796, 13, '081306', 'Ollantaytambo', '1'),
(797, 13, '081307', 'Yucay', '1'),
(798, 1, '090101', 'Huancavelica', '1'),
(799, 1, '090102', 'Acobambilla', '1'),
(800, 1, '090103', 'Acoria', '1'),
(801, 1, '090104', 'Conayca', '1'),
(802, 1, '090105', 'Cuenca', '1'),
(803, 1, '090106', 'Huachocolpa', '1'),
(804, 1, '090107', 'Huayllahuara', '1'),
(805, 1, '090108', 'Izcuchaca', '1'),
(806, 1, '090109', 'Laria', '1'),
(807, 1, '090110', 'Manta', '1'),
(808, 1, '090111', 'Mariscal Cáceres', '1'),
(809, 1, '090112', 'Moya', '1'),
(810, 1, '090113', 'Nuevo Occoro', '1'),
(811, 1, '090114', 'Palca', '1'),
(812, 1, '090115', 'Pilchaca', '1'),
(813, 1, '090116', 'Vilca', '1'),
(814, 1, '090117', 'Yauli', '1'),
(815, 1, '090118', 'Ascensión', '1'),
(816, 1, '090119', 'Huando', '1'),
(817, 2, '090201', 'Acobamba', '1'),
(818, 2, '090203', 'Anta', '1'),
(819, 2, '090204', 'Caja', '1'),
(820, 2, '090205', 'Marcas', '1'),
(821, 2, '090206', 'Paucara', '1'),
(822, 2, '090207', 'Pomacocha', '1'),
(823, 2, '090208', 'Rosario', '1'),
(824, 3, '090301', 'Lircay', '1'),
(825, 3, '090302', 'Anchonga', '1'),
(826, 3, '090303', 'Callanmarca', '1'),
(827, 3, '090304', 'Ccochaccasa', '1'),
(828, 3, '090305', 'Chincho', '1'),
(829, 3, '090306', 'Congalla', '1'),
(830, 3, '090307', 'Huanca-Huanca', '1'),
(831, 3, '090308', 'Huayllay Grande', '1'),
(832, 3, '090309', 'Julcamarca', '1'),
(833, 3, '090310', 'San Antonio de Antaparco', '1'),
(834, 3, '090311', 'Santo Tomas de Pata', '1'),
(835, 3, '090312', 'Secclla', '1'),
(836, 4, '090401', 'Castrovirreyna', '1'),
(837, 4, '090402', 'Arma', '1'),
(838, 4, '090403', 'Aurahua', '1'),
(839, 4, '090404', 'Capillas', '1'),
(840, 4, '090405', 'Chupamarca', '1'),
(841, 4, '090406', 'Cocas', '1'),
(842, 4, '090407', 'Huachos', '1'),
(843, 4, '090408', 'Huamatambo', '1'),
(844, 4, '090409', 'Mollepampa', '1'),
(845, 4, '090410', 'San Juan', '1'),
(846, 4, '090411', 'Santa Ana', '1'),
(847, 4, '090412', 'Tantara', '1'),
(848, 4, '090413', 'Ticrapo', '1'),
(849, 5, '090501', 'Churcampa', '1'),
(850, 5, '090503', 'Chinchihuasi', '1'),
(851, 5, '090504', 'El Carmen', '1'),
(852, 5, '090505', 'La Merced', '1'),
(853, 5, '090506', 'Locroja', '1'),
(854, 5, '090507', 'Paucarbamba', '1'),
(855, 5, '090508', 'San Miguel de Mayocc', '1'),
(856, 5, '090509', 'San Pedro de Coris', '1'),
(857, 5, '090510', 'Pachamarca', '1'),
(858, 5, '090511', 'Cosme', '1'),
(859, 6, '090601', 'Huaytara', '1'),
(860, 6, '090602', 'Ayavi', '1'),
(861, 6, '090603', 'Córdova', '1'),
(862, 6, '090604', 'Huayacundo Arma', '1'),
(863, 6, '090605', 'Laramarca', '1'),
(864, 6, '090606', 'Ocoyo', '1'),
(865, 6, '090607', 'Pilpichaca', '1'),
(866, 6, '090608', 'Querco', '1'),
(867, 6, '090609', 'Quito-Arma', '1'),
(868, 6, '090610', 'San Antonio de Cusicancha', '1'),
(869, 6, '090611', 'San Francisco de Sangayaico', '1'),
(870, 6, '090612', 'San Isidro', '1'),
(871, 6, '090613', 'Santiago de Chocorvos', '1'),
(872, 6, '090614', 'Santiago de Quirahuara', '1'),
(873, 6, '090615', 'Santo Domingo de Capillas', '1'),
(874, 6, '090616', 'Tambo', '1'),
(875, 7, '090701', 'Pampas', '1'),
(876, 7, '090702', 'Acostambo', '1'),
(877, 7, '090703', 'Acraquia', '1'),
(878, 7, '090704', 'Ahuaycha', '1'),
(879, 7, '090706', 'Daniel Hernández', '1'),
(880, 7, '090707', 'Huachocolpa', '1'),
(881, 7, '090709', 'Huaribamba', '1'),
(882, 7, '090710', 'Ñahuimpuquio', '1'),
(883, 7, '090711', 'Pazos', '1'),
(884, 7, '090713', 'Quishuar', '1'),
(885, 7, '090714', 'Salcabamba', '1'),
(886, 7, '090715', 'Salcahuasi', '1'),
(887, 7, '090716', 'San Marcos de Rocchac', '1'),
(888, 7, '090717', 'Surcubamba', '1'),
(889, 7, '090718', 'Tintay Puncu', '1'),
(890, 7, '090719', 'Quichuas', '1'),
(891, 7, '090720', 'Andaymarca', '1'),
(892, 7, '090721', 'Roble', '1'),
(893, 7, '090722', 'Pichos', '1'),
(894, 1, '100101', 'Huanuco', '1'),
(895, 1, '100102', 'Amarilis', '1'),
(896, 1, '100103', 'Chinchao', '1'),
(897, 1, '100104', 'Churubamba', '1'),
(898, 1, '100105', 'Margos', '1'),
(899, 1, '100106', 'Quisqui (Kichki)', '1'),
(900, 1, '100107', 'San Francisco de Cayran', '1'),
(901, 1, '100108', 'San Pedro de Chaulan', '1'),
(902, 1, '100109', 'Santa María del Valle', '1'),
(903, 1, '100110', 'Yarumayo', '1'),
(904, 1, '100111', 'Pillco Marca', '1'),
(905, 1, '100112', 'Yacus', '1'),
(906, 1, '100113', 'San Pablo de Pillao', '1'),
(907, 2, '100201', 'Ambo', '1'),
(908, 2, '100202', 'Cayna', '1'),
(909, 2, '100203', 'Colpas', '1'),
(910, 2, '100204', 'Conchamarca', '1'),
(911, 2, '100205', 'Huacar', '1'),
(912, 2, '100206', 'San Francisco', '1'),
(913, 2, '100207', 'San Rafael', '1'),
(914, 2, '100208', 'Tomay Kichwa', '1'),
(915, 3, '100301', 'La Unión', '1'),
(916, 3, '100307', 'Chuquis', '1'),
(917, 3, '100311', 'Marías', '1'),
(918, 3, '100313', 'Pachas', '1'),
(919, 3, '100316', 'Quivilla', '1'),
(920, 3, '100317', 'Ripan', '1'),
(921, 3, '100321', 'Shunqui', '1'),
(922, 3, '100322', 'Sillapata', '1'),
(923, 3, '100323', 'Yanas', '1'),
(924, 4, '100401', 'Huacaybamba', '1'),
(925, 4, '100402', 'Canchabamba', '1'),
(926, 4, '100403', 'Cochabamba', '1'),
(927, 4, '100404', 'Pinra', '1'),
(928, 5, '100501', 'Llata', '1'),
(929, 5, '100502', 'Arancay', '1'),
(930, 5, '100503', 'Chavín de Pariarca', '1'),
(931, 5, '100504', 'Jacas Grande', '1'),
(932, 5, '100505', 'Jircan', '1'),
(933, 5, '100506', 'Miraflores', '1'),
(934, 5, '100507', 'Monzón', '1'),
(935, 5, '100508', 'Punchao', '1'),
(936, 5, '100509', 'Puños', '1'),
(937, 5, '100510', 'Singa', '1'),
(938, 5, '100511', 'Tantamayo', '1'),
(939, 6, '100601', 'Rupa-Rupa', '1'),
(940, 6, '100602', 'Daniel Alomía Robles', '1'),
(941, 6, '100603', 'Hermílio Valdizan', '1'),
(942, 6, '100604', 'José Crespo y Castillo', '1'),
(943, 6, '100605', 'Luyando', '1'),
(944, 6, '100606', 'Mariano Damaso Beraun', '1'),
(945, 6, '100607', 'Pucayacu', '1'),
(946, 6, '100608', 'Castillo Grande', '1'),
(947, 7, '100701', 'Huacrachuco', '1'),
(948, 7, '100702', 'Cholon', '1'),
(949, 7, '100703', 'San Buenaventura', '1'),
(950, 7, '100704', 'La Morada', '1'),
(951, 7, '100705', 'Santa Rosa de Alto Yanajanca', '1'),
(952, 8, '100801', 'Panao', '1'),
(953, 8, '100802', 'Chaglla', '1'),
(954, 8, '100803', 'Molino', '1'),
(955, 8, '100804', 'Umari', '1'),
(956, 9, '100901', 'Puerto Inca', '1'),
(957, 9, '100902', 'Codo del Pozuzo', '1'),
(958, 9, '100903', 'Honoria', '1'),
(959, 9, '100904', 'Tournavista', '1'),
(960, 9, '100905', 'Yuyapichis', '1'),
(961, 10, '101001', 'Jesús', '1'),
(962, 10, '101002', 'Baños', '1'),
(963, 10, '101003', 'Jivia', '1'),
(964, 10, '101004', 'Queropalca', '1'),
(965, 10, '101005', 'Rondos', '1'),
(966, 10, '101006', 'San Francisco de Asís', '1'),
(967, 10, '101007', 'San Miguel de Cauri', '1'),
(968, 11, '101101', 'Chavinillo', '1'),
(969, 11, '101102', 'Cahuac', '1'),
(970, 11, '101103', 'Chacabamba', '1'),
(971, 11, '101104', 'Aparicio Pomares', '1'),
(972, 11, '101105', 'Jacas Chico', '1'),
(973, 11, '101106', 'Obas', '1'),
(974, 11, '101107', 'Pampamarca', '1'),
(975, 11, '101108', 'Choras', '1'),
(976, 1, '110101', 'Ica', '1'),
(977, 1, '110102', 'La Tinguiña', '1'),
(978, 1, '110103', 'Los Aquijes', '1'),
(979, 1, '110104', 'Ocucaje', '1'),
(980, 1, '110105', 'Pachacutec', '1'),
(981, 1, '110106', 'Parcona', '1'),
(982, 1, '110107', 'Pueblo Nuevo', '1'),
(983, 1, '110108', 'Salas', '1'),
(984, 1, '110109', 'San José de Los Molinos', '1'),
(985, 1, '110111', 'Santiago', '1'),
(986, 1, '110112', 'Subtanjalla', '1'),
(987, 1, '110113', 'Tate', '1'),
(988, 1, '110114', 'Yauca del Rosario', '1'),
(989, 2, '110201', 'Chincha Alta', '1'),
(990, 2, '110202', 'Alto Laran', '1'),
(991, 2, '110203', 'Chavin', '1'),
(992, 2, '110204', 'Chincha Baja', '1'),
(993, 2, '110205', 'El Carmen', '1'),
(994, 2, '110206', 'Grocio Prado', '1'),
(995, 2, '110208', 'San Juan de Yanac', '1'),
(996, 2, '110209', 'San Pedro de Huacarpana', '1'),
(997, 2, '110210', 'Sunampe', '1'),
(998, 2, '110211', 'Tambo de Mora', '1'),
(999, 3, '110301', 'Nasca', '1'),
(1000, 3, '110302', 'Changuillo', '1'),
(1001, 3, '110303', 'El Ingenio', '1'),
(1002, 3, '110304', 'Marcona', '1'),
(1003, 3, '110305', 'Vista Alegre', '1'),
(1004, 4, '110401', 'Palpa', '1'),
(1005, 4, '110402', 'Llipata', '1'),
(1006, 4, '110403', 'Río Grande', '1'),
(1007, 4, '110404', 'Santa Cruz', '1'),
(1008, 4, '110405', 'Tibillo', '1'),
(1009, 5, '110501', 'Pisco', '1'),
(1010, 5, '110502', 'Huancano', '1'),
(1011, 5, '110503', 'Humay', '1'),
(1012, 5, '110504', 'Independencia', '1'),
(1013, 5, '110505', 'Paracas', '1'),
(1014, 5, '110506', 'San Andrés', '1'),
(1015, 5, '110507', 'San Clemente', '1'),
(1016, 5, '110508', 'Tupac Amaru Inca', '1'),
(1017, 1, '120101', 'Huancayo', '1'),
(1018, 1, '120104', 'Carhuacallanga', '1'),
(1019, 1, '120105', 'Chacapampa', '1'),
(1020, 1, '120106', 'Chicche', '1'),
(1021, 1, '120107', 'Chilca', '1'),
(1022, 1, '120108', 'Chongos Alto', '1'),
(1023, 1, '120111', 'Chupuro', '1'),
(1024, 1, '120112', 'Colca', '1'),
(1025, 1, '120113', 'Cullhuas', '1'),
(1026, 1, '120114', 'El Tambo', '1'),
(1027, 1, '120116', 'Huacrapuquio', '1'),
(1028, 1, '120117', 'Hualhuas', '1'),
(1029, 1, '120119', 'Huancan', '1'),
(1030, 1, '120120', 'Huasicancha', '1'),
(1031, 1, '120121', 'Huayucachi', '1'),
(1032, 1, '120122', 'Ingenio', '1'),
(1033, 1, '120124', 'Pariahuanca', '1'),
(1034, 1, '120125', 'Pilcomayo', '1'),
(1035, 1, '120126', 'Pucara', '1'),
(1036, 1, '120127', 'Quichuay', '1'),
(1037, 1, '120128', 'Quilcas', '1'),
(1038, 1, '120129', 'San Agustín', '1'),
(1039, 1, '120130', 'San Jerónimo de Tunan', '1'),
(1040, 1, '120132', 'Saño', '1'),
(1041, 1, '120133', 'Sapallanga', '1'),
(1042, 1, '120134', 'Sicaya', '1'),
(1043, 1, '120135', 'Santo Domingo de Acobamba', '1'),
(1044, 1, '120136', 'Viques', '1'),
(1045, 2, '120201', 'Concepción', '1'),
(1046, 2, '120203', 'Andamarca', '1'),
(1047, 2, '120204', 'Chambara', '1'),
(1048, 2, '120206', 'Comas', '1'),
(1049, 2, '120207', 'Heroínas Toledo', '1'),
(1050, 2, '120208', 'Manzanares', '1'),
(1051, 2, '120209', 'Mariscal Castilla', '1'),
(1052, 2, '120210', 'Matahuasi', '1'),
(1053, 2, '120211', 'Mito', '1'),
(1054, 2, '120212', 'Nueve de Julio', '1'),
(1055, 2, '120213', 'Orcotuna', '1'),
(1056, 2, '120214', 'San José de Quero', '1'),
(1057, 2, '120215', 'Santa Rosa de Ocopa', '1'),
(1058, 3, '120301', 'Chanchamayo', '1'),
(1059, 3, '120302', 'Perene', '1'),
(1060, 3, '120303', 'Pichanaqui', '1'),
(1061, 3, '120304', 'San Luis de Shuaro', '1'),
(1062, 3, '120305', 'San Ramón', '1'),
(1063, 3, '120306', 'Vitoc', '1'),
(1064, 4, '120401', 'Jauja', '1'),
(1065, 4, '120402', 'Acolla', '1'),
(1066, 4, '120403', 'Apata', '1'),
(1067, 4, '120404', 'Ataura', '1'),
(1068, 4, '120405', 'Canchayllo', '1'),
(1069, 4, '120406', 'Curicaca', '1'),
(1070, 4, '120407', 'El Mantaro', '1'),
(1071, 4, '120408', 'Huamali', '1'),
(1072, 4, '120409', 'Huaripampa', '1'),
(1073, 4, '120410', 'Huertas', '1'),
(1074, 4, '120411', 'Janjaillo', '1'),
(1075, 4, '120412', 'Julcán', '1'),
(1076, 4, '120413', 'Leonor Ordóñez', '1'),
(1077, 4, '120414', 'Llocllapampa', '1'),
(1078, 4, '120415', 'Marco', '1'),
(1079, 4, '120416', 'Masma', '1'),
(1080, 4, '120417', 'Masma Chicche', '1'),
(1081, 4, '120418', 'Molinos', '1'),
(1082, 4, '120419', 'Monobamba', '1'),
(1083, 4, '120420', 'Muqui', '1'),
(1084, 4, '120421', 'Muquiyauyo', '1'),
(1085, 4, '120422', 'Paca', '1'),
(1086, 4, '120423', 'Paccha', '1'),
(1087, 4, '120424', 'Pancan', '1'),
(1088, 4, '120425', 'Parco', '1'),
(1089, 4, '120426', 'Pomacancha', '1'),
(1090, 4, '120427', 'Ricran', '1'),
(1091, 4, '120428', 'San Lorenzo', '1'),
(1092, 4, '120429', 'San Pedro de Chunan', '1'),
(1093, 4, '120430', 'Sausa', '1'),
(1094, 4, '120431', 'Sincos', '1'),
(1095, 4, '120432', 'Tunan Marca', '1'),
(1096, 4, '120433', 'Yauli', '1'),
(1097, 4, '120434', 'Yauyos', '1'),
(1098, 5, '120501', 'Junin', '1'),
(1099, 5, '120502', 'Carhuamayo', '1'),
(1100, 5, '120503', 'Ondores', '1'),
(1101, 5, '120504', 'Ulcumayo', '1'),
(1102, 6, '120601', 'Satipo', '1'),
(1103, 6, '120602', 'Coviriali', '1'),
(1104, 6, '120603', 'Llaylla', '1'),
(1105, 6, '120604', 'Mazamari', '1'),
(1106, 6, '120605', 'Pampa Hermosa', '1'),
(1107, 6, '120606', 'Pangoa', '1'),
(1108, 6, '120607', 'Río Negro', '1'),
(1109, 6, '120608', 'Río Tambo', '1'),
(1110, 6, '120609', 'Vizcatan del Ene', '1'),
(1111, 7, '120701', 'Tarma', '1'),
(1112, 7, '120703', 'Huaricolca', '1'),
(1113, 7, '120704', 'Huasahuasi', '1'),
(1114, 7, '120705', 'La Unión', '1'),
(1115, 7, '120706', 'Palca', '1'),
(1116, 7, '120707', 'Palcamayo', '1'),
(1117, 7, '120708', 'San Pedro de Cajas', '1'),
(1118, 7, '120709', 'Tapo', '1'),
(1119, 8, '120801', 'La Oroya', '1'),
(1120, 8, '120802', 'Chacapalpa', '1'),
(1121, 8, '120803', 'Huay-Huay', '1'),
(1122, 8, '120804', 'Marcapomacocha', '1'),
(1123, 8, '120805', 'Morococha', '1'),
(1124, 8, '120806', 'Paccha', '1'),
(1125, 8, '120807', 'Santa Bárbara de Carhuacayan', '1'),
(1126, 8, '120808', 'Santa Rosa de Sacco', '1'),
(1127, 8, '120809', 'Suitucancha', '1'),
(1128, 8, '120810', 'Yauli', '1'),
(1129, 9, '120901', 'Chupaca', '1'),
(1130, 9, '120902', 'Ahuac', '1'),
(1131, 9, '120903', 'Chongos Bajo', '1'),
(1132, 9, '120904', 'Huachac', '1'),
(1133, 9, '120905', 'Huamancaca Chico', '1'),
(1134, 9, '120906', 'San Juan de Iscos', '1'),
(1135, 9, '120907', 'San Juan de Jarpa', '1'),
(1136, 9, '120908', 'Tres de Diciembre', '1'),
(1137, 9, '120909', 'Yanacancha', '1'),
(1138, 1, '130101', 'Trujillo', '1'),
(1139, 1, '130102', 'El Porvenir', '1'),
(1140, 1, '130103', 'Florencia de Mora', '1'),
(1141, 1, '130104', 'Huanchaco', '1'),
(1142, 1, '130106', 'Laredo', '1'),
(1143, 1, '130107', 'Moche', '1'),
(1144, 1, '130108', 'Poroto', '1'),
(1145, 1, '130109', 'Salaverry', '1'),
(1146, 1, '130110', 'Simbal', '1'),
(1147, 1, '130111', 'Victor Larco Herrera', '1'),
(1148, 2, '130201', 'Ascope', '1'),
(1149, 2, '130202', 'Chicama', '1'),
(1150, 2, '130203', 'Chocope', '1'),
(1151, 2, '130204', 'Magdalena de Cao', '1'),
(1152, 2, '130205', 'Paijan', '1'),
(1153, 2, '130206', 'Rázuri', '1'),
(1154, 2, '130207', 'Santiago de Cao', '1'),
(1155, 2, '130208', 'Casa Grande', '1'),
(1156, 3, '130301', 'Bolívar', '1'),
(1157, 3, '130302', 'Bambamarca', '1'),
(1158, 3, '130303', 'Condormarca', '1'),
(1159, 3, '130304', 'Longotea', '1'),
(1160, 3, '130305', 'Uchumarca', '1'),
(1161, 3, '130306', 'Ucuncha', '1'),
(1162, 4, '130401', 'Chepen', '1'),
(1163, 4, '130402', 'Pacanga', '1'),
(1164, 4, '130403', 'Pueblo Nuevo', '1'),
(1165, 5, '130501', 'Julcan', '1'),
(1166, 5, '130502', 'Calamarca', '1'),
(1167, 5, '130503', 'Carabamba', '1'),
(1168, 5, '130504', 'Huaso', '1'),
(1169, 6, '130601', 'Otuzco', '1'),
(1170, 6, '130602', 'Agallpampa', '1'),
(1171, 6, '130604', 'Charat', '1'),
(1172, 6, '130605', 'Huaranchal', '1'),
(1173, 6, '130606', 'La Cuesta', '1'),
(1174, 6, '130608', 'Mache', '1'),
(1175, 6, '130610', 'Paranday', '1'),
(1176, 6, '130611', 'Salpo', '1'),
(1177, 6, '130613', 'Sinsicap', '1'),
(1178, 6, '130614', 'Usquil', '1'),
(1179, 7, '130701', 'San Pedro de Lloc', '1'),
(1180, 7, '130702', 'Guadalupe', '1'),
(1181, 7, '130703', 'Jequetepeque', '1'),
(1182, 7, '130704', 'Pacasmayo', '1'),
(1183, 7, '130705', 'San José', '1'),
(1184, 8, '130802', 'Buldibuyo', '1'),
(1185, 8, '130803', 'Chillia', '1'),
(1186, 8, '130804', 'Huancaspata', '1'),
(1187, 8, '130805', 'Huaylillas', '1'),
(1188, 8, '130806', 'Huayo', '1'),
(1189, 8, '130807', 'Ongon', '1'),
(1190, 8, '130808', 'Parcoy', '1'),
(1191, 8, '130809', 'Pataz', '1'),
(1192, 8, '130810', 'Pias', '1'),
(1193, 8, '130811', 'Santiago de Challas', '1'),
(1194, 8, '130812', 'Taurija', '1'),
(1195, 8, '130813', 'Urpay', '1'),
(1196, 9, '130901', 'Huamachuco', '1'),
(1197, 9, '130902', 'Chugay', '1'),
(1198, 9, '130903', 'Cochorco', '1'),
(1199, 9, '130904', 'Curgos', '1'),
(1200, 9, '130905', 'Marcabal', '1'),
(1201, 9, '130906', 'Sanagoran', '1'),
(1202, 9, '130907', 'Sarin', '1'),
(1203, 9, '130908', 'Sartimbamba', '1'),
(1204, 10, '131001', 'Santiago de Chuco', '1'),
(1205, 10, '131002', 'Angasmarca', '1'),
(1206, 10, '131003', 'Cachicadan', '1'),
(1207, 10, '131004', 'Mollebamba', '1'),
(1208, 10, '131005', 'Mollepata', '1'),
(1209, 10, '131006', 'Quiruvilca', '1'),
(1210, 10, '131007', 'Santa Cruz de Chuca', '1'),
(1211, 10, '131008', 'Sitabamba', '1'),
(1212, 11, '131101', 'Cascas', '1'),
(1213, 11, '131102', 'Lucma', '1'),
(1214, 11, '131103', 'Marmot', '1'),
(1215, 11, '131104', 'Sayapullo', '1'),
(1216, 12, '131201', 'Viru', '1'),
(1217, 12, '131202', 'Chao', '1'),
(1218, 12, '131203', 'Guadalupito', '1'),
(1219, 1, '140101', 'Chiclayo', '1'),
(1220, 1, '140102', 'Chongoyape', '1'),
(1221, 1, '140103', 'Eten', '1'),
(1222, 1, '140104', 'Eten Puerto', '1'),
(1223, 1, '140105', 'José Leonardo Ortiz', '1'),
(1224, 1, '140106', 'La Victoria', '1'),
(1225, 1, '140107', 'Lagunas', '1'),
(1226, 1, '140108', 'Monsefu', '1'),
(1227, 1, '140109', 'Nueva Arica', '1'),
(1228, 1, '140110', 'Oyotun', '1'),
(1229, 1, '140111', 'Picsi', '1'),
(1230, 1, '140112', 'Pimentel', '1'),
(1231, 1, '140113', 'Reque', '1'),
(1232, 1, '140114', 'Santa Rosa', '1'),
(1233, 1, '140115', 'Saña', '1'),
(1234, 1, '140116', 'Cayalti', '1'),
(1235, 1, '140117', 'Patapo', '1'),
(1236, 1, '140118', 'Pomalca', '1'),
(1237, 1, '140119', 'Pucala', '1'),
(1238, 1, '140120', 'Tuman', '1'),
(1239, 2, '140201', 'Ferreñafe', '1'),
(1240, 2, '140202', 'Cañaris', '1'),
(1241, 2, '140203', 'Incahuasi', '1'),
(1242, 2, '140204', 'Manuel Antonio Mesones Muro', '1'),
(1243, 2, '140205', 'Pitipo', '1'),
(1244, 2, '140206', 'Pueblo Nuevo', '1'),
(1245, 3, '140301', 'Lambayeque', '1'),
(1246, 3, '140302', 'Chochope', '1'),
(1247, 3, '140303', 'Illimo', '1'),
(1248, 3, '140304', 'Jayanca', '1'),
(1249, 3, '140305', 'Mochumi', '1'),
(1250, 3, '140306', 'Morrope', '1'),
(1251, 3, '140307', 'Motupe', '1'),
(1252, 3, '140308', 'Olmos', '1'),
(1253, 3, '140309', 'Pacora', '1'),
(1254, 3, '140310', 'Salas', '1'),
(1255, 3, '140311', 'San José', '1'),
(1256, 1, '150101', 'Lima', '1'),
(1257, 1, '150102', 'Ancón', '1'),
(1258, 1, '150103', 'Ate', '1'),
(1259, 1, '150104', 'Barranco', '1'),
(1260, 1, '150105', 'Breña', '1'),
(1261, 1, '150106', 'Carabayllo', '1'),
(1262, 1, '150107', 'Chaclacayo', '1'),
(1263, 1, '150108', 'Chorrillos', '1'),
(1264, 1, '150109', 'Cieneguilla', '1'),
(1265, 1, '150110', 'Comas', '1'),
(1266, 1, '150111', 'El Agustino', '1'),
(1267, 1, '150112', 'Independencia', '1'),
(1268, 1, '150113', 'Jesús María', '1'),
(1269, 1, '150114', 'La Molina', '1'),
(1270, 1, '150115', 'La Victoria', '1'),
(1271, 1, '150116', 'Lince', '1'),
(1272, 1, '150117', 'Los Olivos', '1'),
(1273, 1, '150118', 'Lurigancho', '1'),
(1274, 1, '150119', 'Lurin', '1'),
(1275, 1, '150120', 'Magdalena del Mar', '1'),
(1276, 1, '150121', 'Pueblo Libre', '1'),
(1277, 1, '150122', 'Miraflores', '1'),
(1278, 1, '150123', 'Pachacamac', '1'),
(1279, 1, '150124', 'Pucusana', '1'),
(1280, 1, '150125', 'Puente Piedra', '1'),
(1281, 1, '150126', 'Punta Hermosa', '1'),
(1282, 1, '150127', 'Punta Negra', '1'),
(1283, 1, '150128', 'Rímac', '1'),
(1284, 1, '150129', 'San Bartolo', '1'),
(1285, 1, '150130', 'San Borja', '1'),
(1286, 1, '150131', 'San Isidro', '1'),
(1287, 1, '150132', 'San Juan de Lurigancho', '1'),
(1288, 1, '150133', 'San Juan de Miraflores', '1'),
(1289, 1, '150134', 'San Luis', '1'),
(1290, 1, '150135', 'San Martín de Porres', '1'),
(1291, 1, '150136', 'San Miguel', '1'),
(1292, 1, '150137', 'Santa Anita', '1'),
(1293, 1, '150138', 'Santa María del Mar', '1'),
(1294, 1, '150139', 'Santa Rosa', '1'),
(1295, 1, '150140', 'Santiago de Surco', '1'),
(1296, 1, '150141', 'Surquillo', '1'),
(1297, 1, '150142', 'Villa El Salvador', '1'),
(1298, 1, '150143', 'Villa María del Triunfo', '1'),
(1299, 2, '150201', 'Barranca', '1'),
(1300, 2, '150202', 'Paramonga', '1'),
(1301, 2, '150203', 'Pativilca', '1'),
(1302, 2, '150204', 'Supe', '1'),
(1303, 2, '150205', 'Supe Puerto', '1'),
(1304, 3, '150301', 'Cajatambo', '1'),
(1305, 3, '150302', 'Copa', '1'),
(1306, 3, '150303', 'Gorgor', '1'),
(1307, 3, '150304', 'Huancapon', '1'),
(1308, 3, '150305', 'Manas', '1'),
(1309, 4, '150401', 'Canta', '1'),
(1310, 4, '150402', 'Arahuay', '1'),
(1311, 4, '150403', 'Huamantanga', '1'),
(1312, 4, '150404', 'Huaros', '1'),
(1313, 4, '150405', 'Lachaqui', '1'),
(1314, 4, '150406', 'San Buenaventura', '1'),
(1315, 4, '150407', 'Santa Rosa de Quives', '1'),
(1316, 5, '150501', 'San Vicente de Cañete', '1'),
(1317, 5, '150502', 'Asia', '1'),
(1318, 5, '150503', 'Calango', '1'),
(1319, 5, '150504', 'Cerro Azul', '1'),
(1320, 5, '150505', 'Chilca', '1'),
(1321, 5, '150506', 'Coayllo', '1'),
(1322, 5, '150507', 'Imperial', '1'),
(1323, 5, '150508', 'Lunahuana', '1'),
(1324, 5, '150509', 'Mala', '1'),
(1325, 5, '150510', 'Nuevo Imperial', '1'),
(1326, 5, '150511', 'Pacaran', '1'),
(1327, 5, '150512', 'Quilmana', '1'),
(1328, 5, '150513', 'San Antonio', '1'),
(1329, 5, '150514', 'San Luis', '1'),
(1330, 5, '150515', 'Santa Cruz de Flores', '1'),
(1331, 5, '150516', 'Zúñiga', '1'),
(1332, 6, '150601', 'Huaral', '1'),
(1333, 6, '150602', 'Atavillos Alto', '1'),
(1334, 6, '150603', 'Atavillos Bajo', '1'),
(1335, 6, '150604', 'Aucallama', '1'),
(1336, 6, '150605', 'Chancay', '1'),
(1337, 6, '150606', 'Ihuari', '1'),
(1338, 6, '150607', 'Lampian', '1'),
(1339, 6, '150608', 'Pacaraos', '1'),
(1340, 6, '150609', 'San Miguel de Acos', '1'),
(1341, 6, '150610', 'Santa Cruz de Andamarca', '1'),
(1342, 6, '150611', 'Sumbilca', '1'),
(1343, 6, '150612', 'Veintisiete de Noviembre', '1'),
(1344, 7, '150701', 'Matucana', '1'),
(1345, 7, '150702', 'Antioquia', '1'),
(1346, 7, '150703', 'Callahuanca', '1'),
(1347, 7, '150704', 'Carampoma', '1'),
(1348, 7, '150705', 'Chicla', '1'),
(1349, 7, '150706', 'Cuenca', '1'),
(1350, 7, '150707', 'Huachupampa', '1'),
(1351, 7, '150708', 'Huanza', '1'),
(1352, 7, '150709', 'Huarochiri', '1'),
(1353, 7, '150710', 'Lahuaytambo', '1'),
(1354, 7, '150711', 'Langa', '1'),
(1355, 7, '150712', 'Laraos', '1'),
(1356, 7, '150713', 'Mariatana', '1'),
(1357, 7, '150714', 'Ricardo Palma', '1'),
(1358, 7, '150715', 'San Andrés de Tupicocha', '1'),
(1359, 7, '150716', 'San Antonio', '1'),
(1360, 7, '150717', 'San Bartolomé', '1'),
(1361, 7, '150719', 'San Juan de Iris', '1'),
(1362, 7, '150720', 'San Juan de Tantaranche', '1');
INSERT INTO `distritos` (`id_distrito`, `id_provincia`, `cod_distrito`, `desc_distrito`, `estatus_distrito`) VALUES
(1363, 7, '150721', 'San Lorenzo de Quinti', '1'),
(1364, 7, '150722', 'San Mateo', '1'),
(1365, 7, '150723', 'San Mateo de Otao', '1'),
(1366, 7, '150724', 'San Pedro de Casta', '1'),
(1367, 7, '150725', 'San Pedro de Huancayre', '1'),
(1368, 7, '150726', 'Sangallaya', '1'),
(1369, 7, '150727', 'Santa Cruz de Cocachacra', '1'),
(1370, 7, '150728', 'Santa Eulalia', '1'),
(1371, 7, '150729', 'Santiago de Anchucaya', '1'),
(1372, 7, '150730', 'Santiago de Tuna', '1'),
(1373, 7, '150731', 'Santo Domingo de Los Olleros', '1'),
(1374, 7, '150732', 'Surco', '1'),
(1375, 8, '150801', 'Huacho', '1'),
(1376, 8, '150802', 'Ambar', '1'),
(1377, 8, '150803', 'Caleta de Carquin', '1'),
(1378, 8, '150804', 'Checras', '1'),
(1379, 8, '150805', 'Hualmay', '1'),
(1380, 8, '150806', 'Huaura', '1'),
(1381, 8, '150807', 'Leoncio Prado', '1'),
(1382, 8, '150808', 'Paccho', '1'),
(1383, 8, '150809', 'Santa Leonor', '1'),
(1384, 8, '150810', 'Santa María', '1'),
(1385, 8, '150811', 'Sayan', '1'),
(1386, 8, '150812', 'Vegueta', '1'),
(1387, 9, '150901', 'Oyon', '1'),
(1388, 9, '150902', 'Andajes', '1'),
(1389, 9, '150903', 'Caujul', '1'),
(1390, 9, '150904', 'Cochamarca', '1'),
(1391, 9, '150905', 'Navan', '1'),
(1392, 9, '150906', 'Pachangara', '1'),
(1393, 10, '151001', 'Yauyos', '1'),
(1394, 10, '151002', 'Alis', '1'),
(1395, 10, '151003', 'Allauca', '1'),
(1396, 10, '151004', 'Ayaviri', '1'),
(1397, 10, '151005', 'Azángaro', '1'),
(1398, 10, '151006', 'Cacra', '1'),
(1399, 10, '151007', 'Carania', '1'),
(1400, 10, '151008', 'Catahuasi', '1'),
(1401, 10, '151009', 'Chocos', '1'),
(1402, 10, '151010', 'Cochas', '1'),
(1403, 10, '151011', 'Colonia', '1'),
(1404, 10, '151012', 'Hongos', '1'),
(1405, 10, '151013', 'Huampara', '1'),
(1406, 10, '151014', 'Huancaya', '1'),
(1407, 10, '151015', 'Huangascar', '1'),
(1408, 10, '151016', 'Huantan', '1'),
(1409, 10, '151017', 'Huañec', '1'),
(1410, 10, '151018', 'Laraos', '1'),
(1411, 10, '151019', 'Lincha', '1'),
(1412, 10, '151020', 'Madean', '1'),
(1413, 10, '151021', 'Miraflores', '1'),
(1414, 10, '151022', 'Omas', '1'),
(1415, 10, '151023', 'Putinza', '1'),
(1416, 10, '151024', 'Quinches', '1'),
(1417, 10, '151025', 'Quinocay', '1'),
(1418, 10, '151026', 'San Joaquín', '1'),
(1419, 10, '151027', 'San Pedro de Pilas', '1'),
(1420, 10, '151028', 'Tanta', '1'),
(1421, 10, '151029', 'Tauripampa', '1'),
(1422, 10, '151030', 'Tomas', '1'),
(1423, 10, '151031', 'Tupe', '1'),
(1424, 10, '151032', 'Viñac', '1'),
(1425, 10, '151033', 'Vitis', '1'),
(1426, 1, '160101', 'Iquitos', '1'),
(1427, 1, '160102', 'Alto Nanay', '1'),
(1428, 1, '160103', 'Fernando Lores', '1'),
(1429, 1, '160104', 'Indiana', '1'),
(1430, 1, '160105', 'Las Amazonas', '1'),
(1431, 1, '160106', 'Mazan', '1'),
(1432, 1, '160107', 'Napo', '1'),
(1433, 1, '160108', 'Punchana', '1'),
(1434, 1, '160110', 'Torres Causana', '1'),
(1435, 1, '160112', 'Belén', '1'),
(1436, 1, '160113', 'San Juan Bautista', '1'),
(1437, 2, '160201', 'Yurimaguas', '1'),
(1438, 2, '160202', 'Balsapuerto', '1'),
(1439, 2, '160205', 'Jeberos', '1'),
(1440, 2, '160206', 'Lagunas', '1'),
(1441, 2, '160210', 'Santa Cruz', '1'),
(1442, 2, '160211', 'Teniente Cesar López Rojas', '1'),
(1443, 3, '160301', 'Nauta', '1'),
(1444, 3, '160302', 'Parinari', '1'),
(1445, 3, '160303', 'Tigre', '1'),
(1446, 3, '160304', 'Trompeteros', '1'),
(1447, 3, '160305', 'Urarinas', '1'),
(1448, 4, '160401', 'Ramón Castilla', '1'),
(1449, 4, '160402', 'Pebas', '1'),
(1450, 4, '160403', 'Yavari', '1'),
(1451, 4, '160404', 'San Pablo', '1'),
(1452, 5, '160501', 'Requena', '1'),
(1453, 5, '160502', 'Alto Tapiche', '1'),
(1454, 5, '160503', 'Capelo', '1'),
(1455, 5, '160504', 'Emilio San Martín', '1'),
(1456, 5, '160505', 'Maquia', '1'),
(1457, 5, '160506', 'Puinahua', '1'),
(1458, 5, '160507', 'Saquena', '1'),
(1459, 5, '160508', 'Soplin', '1'),
(1460, 5, '160509', 'Tapiche', '1'),
(1461, 5, '160510', 'Jenaro Herrera', '1'),
(1462, 5, '160511', 'Yaquerana', '1'),
(1463, 6, '160601', 'Contamana', '1'),
(1464, 6, '160602', 'Inahuaya', '1'),
(1465, 6, '160603', 'Padre Márquez', '1'),
(1466, 6, '160604', 'Pampa Hermosa', '1'),
(1467, 6, '160605', 'Sarayacu', '1'),
(1468, 6, '160606', 'Vargas Guerra', '1'),
(1469, 7, '160702', 'Cahuapanas', '1'),
(1470, 7, '160703', 'Manseriche', '1'),
(1471, 7, '160704', 'Morona', '1'),
(1472, 7, '160705', 'Pastaza', '1'),
(1473, 7, '160706', 'Andoas', '1'),
(1474, 8, '160801', 'Putumayo', '1'),
(1475, 8, '160802', 'Rosa Panduro', '1'),
(1476, 8, '160803', 'Teniente Manuel Clavero', '1'),
(1477, 8, '160804', 'Yaguas', '1'),
(1478, 1, '170101', 'Tambopata', '1'),
(1479, 1, '170102', 'Inambari', '1'),
(1480, 1, '170103', 'Las Piedras', '1'),
(1481, 1, '170104', 'Laberinto', '1'),
(1482, 2, '170201', 'Manu', '1'),
(1483, 2, '170202', 'Fitzcarrald', '1'),
(1484, 2, '170203', 'Madre de Dios', '1'),
(1485, 2, '170204', 'Huepetuhe', '1'),
(1486, 3, '170301', 'Iñapari', '1'),
(1487, 3, '170302', 'Iberia', '1'),
(1488, 3, '170303', 'Tahuamanu', '1'),
(1489, 1, '180101', 'Moquegua', '1'),
(1490, 1, '180102', 'Carumas', '1'),
(1491, 1, '180103', 'Cuchumbaya', '1'),
(1492, 1, '180104', 'Samegua', '1'),
(1493, 1, '180105', 'San Cristóbal', '1'),
(1494, 1, '180106', 'Torata', '1'),
(1495, 2, '180201', 'Omate', '1'),
(1496, 2, '180202', 'Chojata', '1'),
(1497, 2, '180203', 'Coalaque', '1'),
(1498, 2, '180204', 'Ichuña', '1'),
(1499, 2, '180205', 'La Capilla', '1'),
(1500, 2, '180206', 'Lloque', '1'),
(1501, 2, '180207', 'Matalaque', '1'),
(1502, 2, '180208', 'Puquina', '1'),
(1503, 2, '180209', 'Quinistaquillas', '1'),
(1504, 2, '180210', 'Ubinas', '1'),
(1505, 2, '180211', 'Yunga', '1'),
(1506, 3, '180301', 'Ilo', '1'),
(1507, 3, '180302', 'El Algarrobal', '1'),
(1508, 3, '180303', 'Pacocha', '1'),
(1509, 1, '190101', 'Chaupimarca', '1'),
(1510, 1, '190102', 'Huachon', '1'),
(1511, 1, '190103', 'Huariaca', '1'),
(1512, 1, '190104', 'Huayllay', '1'),
(1513, 1, '190105', 'Ninacaca', '1'),
(1514, 1, '190106', 'Pallanchacra', '1'),
(1515, 1, '190107', 'Paucartambo', '1'),
(1516, 1, '190108', 'San Francisco de Asís de Yarusyacan', '1'),
(1517, 1, '190109', 'Simon Bolívar', '1'),
(1518, 1, '190110', 'Ticlacayan', '1'),
(1519, 1, '190111', 'Tinyahuarco', '1'),
(1520, 1, '190112', 'Vicco', '1'),
(1521, 1, '190113', 'Yanacancha', '1'),
(1522, 2, '190201', 'Yanahuanca', '1'),
(1523, 2, '190202', 'Chacayan', '1'),
(1524, 2, '190203', 'Goyllarisquizga', '1'),
(1525, 2, '190204', 'Paucar', '1'),
(1526, 2, '190205', 'San Pedro de Pillao', '1'),
(1527, 2, '190206', 'Santa Ana de Tusi', '1'),
(1528, 2, '190207', 'Tapuc', '1'),
(1529, 2, '190208', 'Vilcabamba', '1'),
(1530, 3, '190301', 'Oxapampa', '1'),
(1531, 3, '190302', 'Chontabamba', '1'),
(1532, 3, '190303', 'Huancabamba', '1'),
(1533, 3, '190304', 'Palcazu', '1'),
(1534, 3, '190305', 'Pozuzo', '1'),
(1535, 3, '190306', 'Puerto Bermúdez', '1'),
(1536, 3, '190307', 'Villa Rica', '1'),
(1537, 3, '190308', 'Constitución', '1'),
(1538, 1, '200101', 'Piura', '1'),
(1539, 1, '200104', 'Castilla', '1'),
(1540, 1, '200105', 'Catacaos', '1'),
(1541, 1, '200107', 'Cura Mori', '1'),
(1542, 1, '200108', 'El Tallan', '1'),
(1543, 1, '200109', 'La Arena', '1'),
(1544, 1, '200110', 'La Unión', '1'),
(1545, 1, '200111', 'Las Lomas', '1'),
(1546, 1, '200114', 'Tambo Grande', '1'),
(1547, 1, '200115', 'Veintiseis de Octubre', '1'),
(1548, 2, '200201', 'Ayabaca', '1'),
(1549, 2, '200202', 'Frias', '1'),
(1550, 2, '200203', 'Jilili', '1'),
(1551, 2, '200204', 'Lagunas', '1'),
(1552, 2, '200205', 'Montero', '1'),
(1553, 2, '200206', 'Pacaipampa', '1'),
(1554, 2, '200207', 'Paimas', '1'),
(1555, 2, '200208', 'Sapillica', '1'),
(1556, 2, '200209', 'Sicchez', '1'),
(1557, 2, '200210', 'Suyo', '1'),
(1558, 3, '200301', 'Huancabamba', '1'),
(1559, 3, '200302', 'Canchaque', '1'),
(1560, 3, '200303', 'El Carmen de la Frontera', '1'),
(1561, 3, '200304', 'Huarmaca', '1'),
(1562, 3, '200305', 'Lalaquiz', '1'),
(1563, 3, '200306', 'San Miguel de El Faique', '1'),
(1564, 3, '200307', 'Sondor', '1'),
(1565, 3, '200308', 'Sondorillo', '1'),
(1566, 4, '200401', 'Chulucanas', '1'),
(1567, 4, '200402', 'Buenos Aires', '1'),
(1568, 4, '200403', 'Chalaco', '1'),
(1569, 4, '200404', 'La Matanza', '1'),
(1570, 4, '200405', 'Morropon', '1'),
(1571, 4, '200406', 'Salitral', '1'),
(1572, 4, '200407', 'San Juan de Bigote', '1'),
(1573, 4, '200408', 'Santa Catalina de Mossa', '1'),
(1574, 4, '200409', 'Santo Domingo', '1'),
(1575, 4, '200410', 'Yamango', '1'),
(1576, 5, '200501', 'Paita', '1'),
(1577, 5, '200502', 'Amotape', '1'),
(1578, 5, '200503', 'Arenal', '1'),
(1579, 5, '200504', 'Colan', '1'),
(1580, 5, '200505', 'La Huaca', '1'),
(1581, 5, '200506', 'Tamarindo', '1'),
(1582, 5, '200507', 'Vichayal', '1'),
(1583, 6, '200601', 'Sullana', '1'),
(1584, 6, '200603', 'Ignacio Escudero', '1'),
(1585, 6, '200604', 'Lancones', '1'),
(1586, 6, '200605', 'Marcavelica', '1'),
(1587, 6, '200606', 'Miguel Checa', '1'),
(1588, 6, '200607', 'Querecotillo', '1'),
(1589, 6, '200608', 'Salitral', '1'),
(1590, 7, '200701', 'Pariñas', '1'),
(1591, 7, '200702', 'El Alto', '1'),
(1592, 7, '200703', 'La Brea', '1'),
(1593, 7, '200704', 'Lobitos', '1'),
(1594, 7, '200705', 'Los Organos', '1'),
(1595, 7, '200706', 'Mancora', '1'),
(1596, 8, '200801', 'Sechura', '1'),
(1597, 8, '200802', 'Bellavista de la Unión', '1'),
(1598, 8, '200803', 'Bernal', '1'),
(1599, 8, '200804', 'Cristo Nos Valga', '1'),
(1600, 8, '200805', 'Vice', '1'),
(1601, 8, '200806', 'Rinconada Llicuar', '1'),
(1602, 1, '210101', 'Puno', '1'),
(1603, 1, '210102', 'Acora', '1'),
(1604, 1, '210103', 'Amantani', '1'),
(1605, 1, '210104', 'Atuncolla', '1'),
(1606, 1, '210105', 'Capachica', '1'),
(1607, 1, '210106', 'Chucuito', '1'),
(1608, 1, '210107', 'Coata', '1'),
(1609, 1, '210108', 'Huata', '1'),
(1610, 1, '210109', 'Mañazo', '1'),
(1611, 1, '210110', 'Paucarcolla', '1'),
(1612, 1, '210111', 'Pichacani', '1'),
(1613, 1, '210112', 'Plateria', '1'),
(1614, 1, '210114', 'Tiquillaca', '1'),
(1615, 1, '210115', 'Vilque', '1'),
(1616, 2, '210201', 'Azángaro', '1'),
(1617, 2, '210202', 'Achaya', '1'),
(1618, 2, '210203', 'Arapa', '1'),
(1619, 2, '210204', 'Asillo', '1'),
(1620, 2, '210205', 'Caminaca', '1'),
(1621, 2, '210206', 'Chupa', '1'),
(1622, 2, '210207', 'José Domingo Choquehuanca', '1'),
(1623, 2, '210208', 'Muñani', '1'),
(1624, 2, '210209', 'Potoni', '1'),
(1625, 2, '210210', 'Saman', '1'),
(1626, 2, '210211', 'San Anton', '1'),
(1627, 2, '210212', 'San José', '1'),
(1628, 2, '210214', 'Santiago de Pupuja', '1'),
(1629, 2, '210215', 'Tirapata', '1'),
(1630, 3, '210301', 'Macusani', '1'),
(1631, 3, '210302', 'Ajoyani', '1'),
(1632, 3, '210303', 'Ayapata', '1'),
(1633, 3, '210304', 'Coasa', '1'),
(1634, 3, '210305', 'Corani', '1'),
(1635, 3, '210306', 'Crucero', '1'),
(1636, 3, '210307', 'Ituata', '1'),
(1637, 3, '210308', 'Ollachea', '1'),
(1638, 3, '210309', 'San Gaban', '1'),
(1639, 3, '210310', 'Usicayos', '1'),
(1640, 4, '210401', 'Juli', '1'),
(1641, 4, '210402', 'Desaguadero', '1'),
(1642, 4, '210403', 'Huacullani', '1'),
(1643, 4, '210404', 'Kelluyo', '1'),
(1644, 4, '210405', 'Pisacoma', '1'),
(1645, 4, '210406', 'Pomata', '1'),
(1646, 4, '210407', 'Zepita', '1'),
(1647, 5, '210501', 'Ilave', '1'),
(1648, 5, '210502', 'Capazo', '1'),
(1649, 5, '210503', 'Pilcuyo', '1'),
(1650, 5, '210504', 'Santa Rosa', '1'),
(1651, 5, '210505', 'Conduriri', '1'),
(1652, 6, '210601', 'Huancane', '1'),
(1653, 6, '210602', 'Cojata', '1'),
(1654, 6, '210603', 'Huatasani', '1'),
(1655, 6, '210604', 'Inchupalla', '1'),
(1656, 6, '210605', 'Pusi', '1'),
(1657, 6, '210606', 'Rosaspata', '1'),
(1658, 6, '210607', 'Taraco', '1'),
(1659, 6, '210608', 'Vilque Chico', '1'),
(1660, 7, '210701', 'Lampa', '1'),
(1661, 7, '210702', 'Cabanilla', '1'),
(1662, 7, '210703', 'Calapuja', '1'),
(1663, 7, '210704', 'Nicasio', '1'),
(1664, 7, '210705', 'Ocuviri', '1'),
(1665, 7, '210707', 'Paratia', '1'),
(1666, 7, '210709', 'Santa Lucia', '1'),
(1667, 7, '210710', 'Vilavila', '1'),
(1668, 8, '210801', 'Ayaviri', '1'),
(1669, 8, '210802', 'Antauta', '1'),
(1670, 8, '210803', 'Cupi', '1'),
(1671, 8, '210804', 'Llalli', '1'),
(1672, 8, '210805', 'Macari', '1'),
(1673, 8, '210806', 'Nuñoa', '1'),
(1674, 8, '210807', 'Orurillo', '1'),
(1675, 8, '210808', 'Santa Rosa', '1'),
(1676, 8, '210809', 'Umachiri', '1'),
(1677, 9, '210901', 'Moho', '1'),
(1678, 9, '210902', 'Conima', '1'),
(1679, 9, '210903', 'Huayrapata', '1'),
(1680, 9, '210904', 'Tilali', '1'),
(1681, 10, '211001', 'Putina', '1'),
(1682, 10, '211002', 'Ananea', '1'),
(1683, 10, '211003', 'Pedro Vilca Apaza', '1'),
(1684, 10, '211004', 'Quilcapuncu', '1'),
(1685, 10, '211005', 'Sina', '1'),
(1686, 11, '211101', 'Juliaca', '1'),
(1687, 11, '211102', 'Cabana', '1'),
(1688, 11, '211103', 'Cabanillas', '1'),
(1689, 11, '211104', 'Caracoto', '1'),
(1690, 12, '211201', 'Sandia', '1'),
(1691, 12, '211202', 'Cuyocuyo', '1'),
(1692, 12, '211203', 'Limbani', '1'),
(1693, 12, '211204', 'Patambuco', '1'),
(1694, 12, '211205', 'Phara', '1'),
(1695, 12, '211206', 'Quiaca', '1'),
(1696, 12, '211207', 'San Juan del Oro', '1'),
(1697, 12, '211208', 'Yanahuaya', '1'),
(1698, 12, '211209', 'Alto Inambari', '1'),
(1699, 12, '211210', 'San Pedro de Putina Punco', '1'),
(1700, 13, '211301', 'Yunguyo', '1'),
(1701, 13, '211302', 'Anapia', '1'),
(1702, 13, '211303', 'Copani', '1'),
(1703, 13, '211304', 'Cuturapi', '1'),
(1704, 13, '211305', 'Ollaraya', '1'),
(1705, 13, '211306', 'Tinicachi', '1'),
(1706, 13, '211307', 'Unicachi', '1'),
(1707, 1, '220101', 'Moyobamba', '1'),
(1708, 1, '220102', 'Calzada', '1'),
(1709, 1, '220103', 'Habana', '1'),
(1710, 1, '220104', 'Jepelacio', '1'),
(1711, 1, '220105', 'Soritor', '1'),
(1712, 1, '220106', 'Yantalo', '1'),
(1713, 2, '220201', 'Bellavista', '1'),
(1714, 2, '220202', 'Alto Biavo', '1'),
(1715, 2, '220203', 'Bajo Biavo', '1'),
(1716, 2, '220204', 'Huallaga', '1'),
(1717, 2, '220205', 'San Pablo', '1'),
(1718, 2, '220206', 'San Rafael', '1'),
(1719, 3, '220301', 'San José de Sisa', '1'),
(1720, 3, '220302', 'Agua Blanca', '1'),
(1721, 3, '220303', 'San Martín', '1'),
(1722, 3, '220305', 'Shatoja', '1'),
(1723, 4, '220401', 'Saposoa', '1'),
(1724, 4, '220402', 'Alto Saposoa', '1'),
(1725, 4, '220403', 'El Eslabón', '1'),
(1726, 4, '220404', 'Piscoyacu', '1'),
(1727, 4, '220405', 'Sacanche', '1'),
(1728, 4, '220406', 'Tingo de Saposoa', '1'),
(1729, 5, '220501', 'Lamas', '1'),
(1730, 5, '220502', 'Alonso de Alvarado', '1'),
(1731, 5, '220503', 'Barranquita', '1'),
(1732, 5, '220504', 'Caynarachi', '1'),
(1733, 5, '220505', 'Cuñumbuqui', '1'),
(1734, 5, '220506', 'Pinto Recodo', '1'),
(1735, 5, '220507', 'Rumisapa', '1'),
(1736, 5, '220508', 'San Roque de Cumbaza', '1'),
(1737, 5, '220509', 'Shanao', '1'),
(1738, 5, '220510', 'Tabalosos', '1'),
(1739, 5, '220511', 'Zapatero', '1'),
(1740, 6, '220601', 'Juanjuí', '1'),
(1741, 6, '220603', 'Huicungo', '1'),
(1742, 6, '220604', 'Pachiza', '1'),
(1743, 6, '220605', 'Pajarillo', '1'),
(1744, 7, '220701', 'Picota', '1'),
(1745, 7, '220703', 'Caspisapa', '1'),
(1746, 7, '220704', 'Pilluana', '1'),
(1747, 7, '220705', 'Pucacaca', '1'),
(1748, 7, '220706', 'San Cristóbal', '1'),
(1749, 7, '220707', 'San Hilarión', '1'),
(1750, 7, '220708', 'Shamboyacu', '1'),
(1751, 7, '220709', 'Tingo de Ponasa', '1'),
(1752, 7, '220710', 'Tres Unidos', '1'),
(1753, 8, '220801', 'Rioja', '1'),
(1754, 8, '220802', 'Awajun', '1'),
(1755, 8, '220803', 'Elías Soplin Vargas', '1'),
(1756, 8, '220804', 'Nueva Cajamarca', '1'),
(1757, 8, '220805', 'Pardo Miguel', '1'),
(1758, 8, '220806', 'Posic', '1'),
(1759, 8, '220807', 'San Fernando', '1'),
(1760, 8, '220808', 'Yorongos', '1'),
(1761, 8, '220809', 'Yuracyacu', '1'),
(1762, 9, '220901', 'Tarapoto', '1'),
(1763, 9, '220902', 'Alberto Leveau', '1'),
(1764, 9, '220903', 'Cacatachi', '1'),
(1765, 9, '220904', 'Chazuta', '1'),
(1766, 9, '220905', 'Chipurana', '1'),
(1767, 9, '220906', 'El Porvenir', '1'),
(1768, 9, '220907', 'Huimbayoc', '1'),
(1769, 9, '220908', 'Juan Guerra', '1'),
(1770, 9, '220909', 'La Banda de Shilcayo', '1'),
(1771, 9, '220910', 'Morales', '1'),
(1772, 9, '220911', 'Papaplaya', '1'),
(1773, 9, '220912', 'San Antonio', '1'),
(1774, 9, '220913', 'Sauce', '1'),
(1775, 9, '220914', 'Shapaja', '1'),
(1776, 10, '221001', 'Tocache', '1'),
(1777, 10, '221002', 'Nuevo Progreso', '1'),
(1778, 10, '221003', 'Polvora', '1'),
(1779, 10, '221004', 'Shunte', '1'),
(1780, 10, '221005', 'Uchiza', '1'),
(1781, 1, '230101', 'Tacna', '1'),
(1782, 1, '230102', 'Alto de la Alianza', '1'),
(1783, 1, '230103', 'Calana', '1'),
(1784, 1, '230104', 'Ciudad Nueva', '1'),
(1785, 1, '230105', 'Inclan', '1'),
(1786, 1, '230106', 'Pachia', '1'),
(1787, 1, '230107', 'Palca', '1'),
(1788, 1, '230108', 'Pocollay', '1'),
(1789, 1, '230109', 'Sama', '1'),
(1790, 1, '230110', 'Coronel Gregorio Albarracín Lanchipa', '1'),
(1791, 1, '230111', 'La Yarada los Palos', '1'),
(1792, 2, '230201', 'Candarave', '1'),
(1793, 2, '230202', 'Cairani', '1'),
(1794, 2, '230203', 'Camilaca', '1'),
(1795, 2, '230204', 'Curibaya', '1'),
(1796, 2, '230205', 'Huanuara', '1'),
(1797, 2, '230206', 'Quilahuani', '1'),
(1798, 3, '230301', 'Locumba', '1'),
(1799, 3, '230302', 'Ilabaya', '1'),
(1800, 3, '230303', 'Ite', '1'),
(1801, 4, '230401', 'Tarata', '1'),
(1802, 4, '230402', 'Héroes Albarracín', '1'),
(1803, 4, '230403', 'Estique', '1'),
(1804, 4, '230404', 'Estique-Pampa', '1'),
(1805, 4, '230405', 'Sitajara', '1'),
(1806, 4, '230406', 'Susapaya', '1'),
(1807, 4, '230407', 'Tarucachi', '1'),
(1808, 4, '230408', 'Ticaco', '1'),
(1809, 1, '240101', 'Tumbes', '1'),
(1810, 1, '240102', 'Corrales', '1'),
(1811, 1, '240103', 'La Cruz', '1'),
(1812, 1, '240104', 'Pampas de Hospital', '1'),
(1813, 1, '240105', 'San Jacinto', '1'),
(1814, 1, '240106', 'San Juan de la Virgen', '1'),
(1815, 2, '240201', 'Zorritos', '1'),
(1816, 2, '240202', 'Casitas', '1'),
(1817, 2, '240203', 'Canoas de Punta Sal', '1'),
(1818, 3, '240301', 'Zarumilla', '1'),
(1819, 3, '240302', 'Aguas Verdes', '1'),
(1820, 3, '240303', 'Matapalo', '1'),
(1821, 3, '240304', 'Papayal', '1'),
(1822, 1, '250101', 'Calleria', '1'),
(1823, 1, '250102', 'Campoverde', '1'),
(1824, 1, '250103', 'Iparia', '1'),
(1825, 1, '250104', 'Masisea', '1'),
(1826, 1, '250105', 'Yarinacocha', '1'),
(1827, 1, '250106', 'Nueva Requena', '1'),
(1828, 1, '250107', 'Manantay', '1'),
(1829, 2, '250201', 'Raymondi', '1'),
(1830, 2, '250202', 'Sepahua', '1'),
(1831, 2, '250203', 'Tahuania', '1'),
(1832, 2, '250204', 'Yurua', '1'),
(1833, 3, '250301', 'Padre Abad', '1'),
(1834, 3, '250302', 'Irazola', '1'),
(1835, 3, '250303', 'Curimana', '1'),
(1836, 3, '250304', 'Neshuya', '1'),
(1837, 3, '250305', 'Alexander Von Humboldt', '1'),
(1838, 4, '250401', 'Purus', '1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `distritos`
--
ALTER TABLE `distritos`
  ADD PRIMARY KEY (`id_distrito`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `distritos`
--
ALTER TABLE `distritos`
  MODIFY `id_distrito` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1839;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documento_relacionado_guiaremision`
--

CREATE TABLE `documento_relacionado_guiaremision` (
  `id_docrelaguiaremi` int(11) NOT NULL,
  `cod_docrelaguiaremi` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_docrelaguiaremi` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `detallar_concepto` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `documento_relacionado_guiaremision`
--

INSERT INTO `documento_relacionado_guiaremision` (`id_docrelaguiaremi`, `cod_docrelaguiaremi`, `desc_docrelaguiaremi`, `detallar_concepto`, `estatus`) VALUES
(1, '01', 'NUMERACION DAM', '0', '1'),
(2, '02', 'NUMERO DE ORDEN DE ENTREGA', '0', '1'),
(3, '03', 'NUMERO SCOP', '0', '1'),
(4, '04', 'NUMERO DE MANIFIESTO DE CARGA', '0', '1'),
(5, '05', 'NUMERO DE CONSTANCIA DE DETRACCION', '0', '1'),
(6, '06', 'OTROS', '1', '1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `documento_relacionado_guiaremision`
--
ALTER TABLE `documento_relacionado_guiaremision`
  ADD PRIMARY KEY (`id_docrelaguiaremi`),
  ADD UNIQUE KEY `cod_docrelaguiaremi` (`cod_docrelaguiaremi`),
  ADD KEY `desc_docrelaguiaremi` (`desc_docrelaguiaremi`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `documento_relacionado_guiaremision`
--
ALTER TABLE `documento_relacionado_guiaremision`
  MODIFY `id_docrelaguiaremi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado_tipo_documentos`
--

CREATE TABLE `empleado_tipo_documentos` (
  `id_empleado` int(11) NOT NULL,
  `idtip_documento` int(11) NOT NULL,
  `idwin_state` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `empleado_tipo_documentos`
--

INSERT INTO `empleado_tipo_documentos` (`id_empleado`, `idtip_documento`, `idwin_state`) VALUES
(1, 5, 51);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados_pais`
--

CREATE TABLE `estados_pais` (
  `idestados_pais` int(11) NOT NULL,
  `idpais` int(11) NOT NULL,
  `desc_estados` varchar(45) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estatus_itemcomprobante`
--

CREATE TABLE `estatus_itemcomprobante` (
  `id_statusitem` int(11) NOT NULL,
  `cod_statusitem` varchar(1) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_statusitem` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `estatus_itemcomprobante`
--

INSERT INTO `estatus_itemcomprobante` (`id_statusitem`, `cod_statusitem`, `desc_statusitem`, `estatus`) VALUES
(1, '1', 'Adicionar', '1'),
(2, '2', 'Modificar', '1'),
(3, '3', 'Anulado', '1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `estatus_itemcomprobante`
--
ALTER TABLE `estatus_itemcomprobante`
  ADD PRIMARY KEY (`id_statusitem`),
  ADD UNIQUE KEY `cod_statusitem` (`cod_statusitem`),
  ADD KEY `desc_statusitem` (`desc_statusitem`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `estatus_itemcomprobante`
--
ALTER TABLE `estatus_itemcomprobante`
  MODIFY `id_statusitem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forma_farmaceutica`
--

CREATE TABLE `forma_farmaceutica` (
  `id_formfarmaceutica` int(11) NOT NULL,
  `desc_formfarmaceutica` text COLLATE utf8_spanish2_ci NOT NULL,
  `status_formfarmace` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `forma_farmaceutica`
--

INSERT INTO `forma_farmaceutica` (`id_formfarmaceutica`, `desc_formfarmaceutica`, `status_formfarmace`) VALUES
(1, 'AEROSOL', '1'),
(2, 'AEROSOL PARA INHALACION', '1'),
(3, 'AEROSOL TOPICO', '1'),
(4, 'AGUA AROMATICA', '1'),
(5, 'ANILLO VAGINAL', '1'),
(6, 'BARRA', '1'),
(7, 'BOLSA', '1'),
(8, 'CAPSULA', '1'),
(9, 'CAPSULA BLANDA', '1'),
(10, 'CAPSULA BLANDA DE LIBERACION RETARDADA', '1'),
(11, 'CAPSULA BLANDA MASTICABLE', '1'),
(12, 'CAPSULA BLANDA VAGINAL', '1'),
(13, 'CAPSULA CON GRANULOS CON RECUBIERTA ENTERICA', '1'),
(14, 'CAPSULA CON GRANULOS DE LIBERACION PROLONGADA', '1'),
(15, 'CAPSULA DE LIBERACION CONTROLADA', '1'),
(16, 'CAPSULA DE LIBERACION EXTENDIDA', '1'),
(17, 'CAPSULA DE LIBERACION MODIFICADA', '1'),
(18, 'CAPSULA DE LIBERACION PROLONGADA', '1'),
(19, 'CAPSULA DE LIBERACION RETARDADA', '1'),
(20, 'CAPSULA DE LIBERACION RETARDADA CON CUBIERTA ENTERICA', '1'),
(21, 'CAPSULA DURA', '1'),
(22, 'CAPSULA DURA DE LIBERACION MODIFICADA', '1'),
(23, 'CAPSULA GASTRORRESISTENTE', '1'),
(24, 'CAPSULA VAGINAL', '1'),
(25, 'CATAPLASMA', '1'),
(26, 'CHAMPU', '1'),
(27, 'CHICLE', '1'),
(28, 'COLIRIO', '1'),
(29, 'COLUTORIO (GARGARISMOS) ', '1'),
(30, 'COMPRIMIDO', '1'),
(31, 'COMPRIMIDO BUCODISPERSABLE', '1'),
(32, 'COMPRIMIDO CON CUBIERTA ENTERICA', '1'),
(33, 'COMPRIMIDO CON CUBIERTA PELICULAR', '1'),
(34, 'COMPRIMIDO DE DISOLUCION BUCAL', '1'),
(35, 'COMPRIMIDO DE LIBERACION PROLONGADA', '1'),
(36, 'COMPRIMIDO DISPERSABLE', '1'),
(37, 'COMPRIMIDO DISPERSABLE/MASTICABLE', '1'),
(38, 'COMPRIMIDO EFERVESCENTE', '1'),
(39, 'COMPRIMIDO ESTRATIFICADO', '1'),
(40, 'COMPRIMIDO GASTRORRESISTENTE', '1'),
(41, 'COMPRIMIDO MASTICABLE', '1'),
(42, 'COMPRIMIDO RECUBIERTO', '1'),
(43, 'COMPRIMIDO RECUBIERTO CON PELICULA', '1'),
(44, 'COMPRIMIDO RECUBIERTO DE LIBERACION CONTROLADA', '1'),
(45, 'COMPRIMIDO RECUBIERTO DE LIBERACION PROLONGADA', '1'),
(46, 'COMPRIMIDO RECUBIERTO GASTRORRESISTENTE', '1'),
(47, 'COMPRIMIDO SUBLINGUAL', '1'),
(48, 'CONCENTRADO PARA SOLUCION INYECTABLE', '1'),
(49, 'CONCENTRADO PARA SOLUCIÓN ORAL ', '1'),
(50, 'CONCENTRADO PARA SOLUCION PARA INFUSION', '1'),
(51, 'CONCENTRADO PARA SOLUCION PARA PERFUSION', '1'),
(52, 'CONCENTRADO PARA SUSPENSION PARA PERFUSION', '1'),
(53, 'CONCENTRADO Y DISOLVENTE PARA SOLUCION PARA PERFUSION', '1'),
(54, 'CREMA', '1'),
(55, 'CREMA RECTAL', '1'),
(56, 'CREMA VAGINAL', '1'),
(57, 'DISOLVENTE PARA USO PARENTERAL', '1'),
(58, 'ELIXIR', '1'),
(59, 'EMPLASTO', '1'),
(60, 'EMULSION', '1'),
(61, 'EMULSION INYECTABLE', '1'),
(62, 'EMULSION LIQUIDA', '1'),
(63, 'EMULSION ORAL', '1'),
(64, 'EMULSION PARA INYECCION', '1'),
(65, 'EMULSION PARA PERFUSION', '1'),
(66, 'EMULSION SEMISOLIDA', '1'),
(67, 'EMULSION TOPICA', '1'),
(68, 'ENEMA', '1'),
(69, 'ESPONJA', '1'),
(70, 'ESPUMA', '1'),
(71, 'ESPUMA CUTANEA', '1'),
(72, 'EXTRACTO LIQUIDO', '1'),
(73, 'GALLETA', '1'),
(74, 'GAS COMPRIMIDO', '1'),
(75, 'GAS LICUADO', '1'),
(76, 'GEL', '1'),
(77, 'GEL BUCAL', '1'),
(78, 'GEL DENTAL', '1'),
(79, 'GEL OFTALMICO', '1'),
(80, 'GEL VAGINAL', '1'),
(81, 'GENERADOR DE RADIONUCLEIDO', '1'),
(82, 'GOMA', '1'),
(83, 'GOMA DE MASCAR (CHICLE)', '1'),
(84, 'GOTAS ORALES', '1'),
(85, 'GRAGEA', '1'),
(86, 'GRANULADO', '1'),
(87, 'GRANULADO PARA SOLUCION ORAL', '1'),
(88, 'GRANULOS', '1'),
(89, 'GRANULOS DE LIBERACION PROLONGADA', '1'),
(90, 'GRANULOS DE LIBERACION RETARDADA', '1'),
(91, 'GRANULOS EFERVESCENTES', '1'),
(92, 'GRANULOS GASTRORRESISTENTES', '1'),
(93, 'GRANULOS ORAL', '1'),
(94, 'GRANULOS PARA SOLUCION', '1'),
(95, 'GRANULOS PARA SOLUCION ORAL', '1'),
(96, 'GRANULOS PARA SUSPENSION', '1'),
(97, 'GRANULOS PARA SUSPENSION ORAL', '1'),
(98, 'IMPLANTE', '1'),
(99, 'IMPLANTE (PELLET) ', '1'),
(100, 'INFUSION INTRAVENOSA', '1'),
(101, 'INFUSION ORAL', '1'),
(102, 'INHALACION', '1'),
(103, 'INHALACION PRESURIZADA', '1'),
(104, 'INYECTABLE', '1'),
(105, 'JABON LIQUIDO', '1'),
(106, 'JABON SOLIDO', '1'),
(107, 'JALEA', '1'),
(108, 'JARABE', '1'),
(109, 'LAPIZ', '1'),
(110, 'LIQUIDO CRIOGENICO', '1'),
(111, 'LOCION', '1'),
(112, 'OVULO', '1'),
(113, 'PARCHE', '1'),
(114, 'PARCHE CUTANEO', '1'),
(115, 'PARCHE TRANSDERMICO', '1'),
(116, 'PASTA', '1'),
(117, 'PASTA DENTAL', '1'),
(118, 'PASTILLA', '1'),
(119, 'PASTILLA (CARAMELO)', '1'),
(120, 'PASTILLA DE GOMA', '1'),
(121, 'PASTILLA DURA PARA CHUPAR', '1'),
(122, 'PERLA', '1'),
(123, 'PILDORA', '1'),
(124, 'POLVO EFERVESCENTE', '1'),
(125, 'POLVO EFERVESCENTE PARA SOLUCION ORAL', '1'),
(126, 'POLVO LIOFILIZADO PARA INSTILACION INTRAVESICAL', '1'),
(127, 'POLVO LIOFILIZADO PARA SOLUCION INYECTABLE', '1'),
(128, 'POLVO LIOFILIZADO PARA SUSPENSION INYECTABLE', '1'),
(129, 'POLVO ORAL', '1'),
(130, 'POLVO PARA CONCENTRADO PARA DISPERSION PARA PERFUSION', '1'),
(131, 'POLVO PARA CONCENTRADO PARA SOLUCION PARA PERFUSION', '1'),
(132, 'POLVO PARA DISPERSION PARA PERFUSION', '1'),
(133, 'POLVO PARA INFUSION', '1'),
(134, 'POLVO PARA INHALACION', '1'),
(135, 'POLVO PARA INHALACION EN CAPSULA DURA', '1'),
(136, 'POLVO PARA INYECCION', '1'),
(137, 'POLVO PARA SOLUCION DE HEMODIALISIS', '1'),
(138, 'POLVO PARA SOLUCION INYECTABLE', '1'),
(139, 'POLVO PARA SOLUCION OFTALMICA', '1'),
(140, 'POLVO PARA SOLUCION ORAL', '1'),
(141, 'POLVO PARA SOLUCION PARA PERFUSION', '1'),
(142, 'POLVO PARA SOLUCION TOPICA', '1'),
(143, 'POLVO PARA SOLUCION VAGINAL', '1'),
(144, 'POLVO PARA SUSPENSION', '1'),
(145, 'POLVO PARA SUSPENSION INYECTABLE', '1'),
(146, 'POLVO PARA SUSPENSION ORAL', '1'),
(147, 'POLVO PARA SUSPENSION PARA PERFUSION', '1'),
(148, 'POLVO PARA SUSPENSION RECTAL', '1'),
(149, 'POLVO TOPICO', '1'),
(150, 'POLVO Y DISOLVENTE PARA SOLUCION INYECTABLE', '1'),
(151, 'POLVO Y DISOLVENTE PARA SUSPENSION INYECTABLE', '1'),
(152, 'POLVO Y DISOLVENTE PARA SUSPENSION INYECTABLE DE LIBERACION PROLONGADA', '1'),
(153, 'POLVO Y SOLUCION PARA SOLUCION INYECTABLE', '1'),
(154, 'POLVO Y SUSPENSION PARA SUSPENSION INYECTABLE', '1'),
(155, 'POMADA', '1'),
(156, 'POMADA OFTALMICA', '1'),
(157, 'POMADA RECTAL', '1'),
(158, 'SISTEMA DE LIBERACION INTRAUTERINO', '1'),
(159, 'SISTEMA INTRAUTERINO', '1'),
(160, 'SISTEMA TERAPEUTICO', '1'),
(161, 'SOLUCION', '1'),
(162, 'SOLUCION CUTANEA', '1'),
(163, 'SOLUCION INYECTABLE', '1'),
(164, 'SOLUCION INYECTABLE PARA INFUSION', '1'),
(165, 'SOLUCION NASAL', '1'),
(166, 'SOLUCION OFTALMICA', '1'),
(167, 'SOLUCION ORAL', '1'),
(168, 'SOLUCION OTICA', '1'),
(169, 'SOLUCION PARA', '1'),
(170, 'GARGARISMOS Y ENJUAGUE BUCAL', '1'),
(171, 'SOLUCION PARA DIALISIS PERITONEAL', '1'),
(172, 'SOLUCION PARA ENJUAGUE BUCAL', '1'),
(173, 'SOLUCION PARA GARGARISMOS', '1'),
(174, 'SOLUCION PARA HEMODIALISIS', '1'),
(175, 'SOLUCION PARA HEMOFILTRACION', '1'),
(176, 'SOLUCION PARA INHALACION', '1'),
(177, 'SOLUCION PARA INYECCION', '1'),
(178, 'SOLUCION PARA IRRIGACION', '1'),
(179, 'SOLUCION PARA NEBULIZACION', '1'),
(180, 'SOLUCION PARA PERFUSION', '1'),
(181, 'SOLUCION PARA PULVERIZACION BUCAL', '1'),
(182, 'SOLUCION PARA PULVERIZACION CUTANEA', '1'),
(183, 'SOLUCION PARA PULVERIZACION NASAL', '1'),
(184, 'SOLUCION PARA PULVERIZACION TRANSDERMICA', '1'),
(185, 'SOLUCION PARA UÑAS', '1'),
(186, 'SOLUCION RECTAL', '1'),
(187, 'SOLUCION TOPICA', '1'),
(188, 'SPRAY', '1'),
(189, 'SUPOSITORIO', '1'),
(190, 'SUSPENSION', '1'),
(191, 'SUSPENSION DE LIBERACION PROLONGADA INYECTABLE', '1'),
(192, 'SUSPENSION ENDOTRAQUEAL', '1'),
(193, 'SUSPENSION INYECTABLE', '1'),
(194, 'SUSPENSION OFTALMICA', '1'),
(195, 'SUSPENSION ORAL', '1'),
(196, 'SUSPENSION PARA INHALACION', '1'),
(197, 'SUSPENSION PARA INHALACION EN ENVASE A PRESION', '1'),
(198, 'SUSPENSION PARA INYECCION', '1'),
(199, 'SUSPENSION PARA NEBULIZACION', '1'),
(200, 'SUSPENSION PARA PULVERIZACION NASAL', '1'),
(201, 'SUSPENSION RECTAL', '1'),
(202, 'TABLETA', '1'),
(203, 'TABLETA CON CUBIERTA ENTERICA', '1'),
(204, 'TABLETA DE DESINTEGRACION ORAL', '1'),
(205, 'TABLETA DE DISOLUCION BUCAL', '1'),
(206, 'TABLETA DE LIBERACION CONTROLADA', '1'),
(207, 'TABLETA DE LIBERACION EXTENDIDA', '1'),
(208, 'TABLETA DE LIBERACION MODIFICADA', '1'),
(209, 'TABLETA DE LIBERACION PROLONGADA', '1'),
(210, 'TABLETA DE LIBERACION RETARDADA', '1'),
(211, 'TABLETA DE LIBERACION RETARDADA (CON CUBIERTA ENTERICA)', '1'),
(212, 'TABLETA DISPERSABLE', '1'),
(213, 'TABLETA EFERVESCENTE', '1'),
(214, 'TABLETA ESTRATIFICADA', '1'),
(215, 'TABLETA LAQUEADA', '1'),
(216, 'TABLETA MASTICABLE', '1'),
(217, 'TABLETA ORODISPERSABLE', '1'),
(218, 'TABLETA PARA SUSPENSION ORAL', '1'),
(219, 'TABLETA RECUBIERTA', '1'),
(220, 'TABLETA RECUBIERTA DE ACCION PROLONGADA', '1'),
(221, 'TABLETA RECUBIERTA DE LIBERACION PROLONGADA', '1'),
(222, 'TABLETA RECUBIERTA DE LIBERACION RETARDADA', '1'),
(223, 'TABLETA RECUBIERTA GASTRORRESISTENTE', '1'),
(224, 'TABLETA SOLUBLE', '1'),
(225, 'TABLETA SUBLINGUAL', '1'),
(226, 'TABLETA VAGINAL', '1'),
(227, 'TAMPON', '1'),
(228, 'TINTURA', '1'),
(229, 'TROCISCO', '1'),
(230, 'UNGUENTO', '1'),
(231, 'UNGUENTO DERMICO', '1'),
(232, 'UNGUENTO OFTALMICO', '1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `forma_farmaceutica`
--
ALTER TABLE `forma_farmaceutica`
  ADD PRIMARY KEY (`id_formfarmaceutica`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `forma_farmaceutica`
--
ALTER TABLE `forma_farmaceutica`
  MODIFY `id_formfarmaceutica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=233;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kardex`
--

CREATE TABLE `kardex` (
  `idkardex` int(11) NOT NULL,
  `idproductos` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fec_ingreso` date NOT NULL,
  `id_mov_ingreso` int(11) NOT NULL,
  `id_user_ing` int(11) DEFAULT NULL,
  `fec_egreso` date DEFAULT NULL,
  `id_mov_salida` int(11) DEFAULT NULL,
  `num_doc_salida` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `id_user_sal` int(11) DEFAULT NULL,
  `fec_ing_kardex` datetime DEFAULT CURRENT_TIMESTAMP,
  `fec_movimiento_kardex` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `kardex`
--
ALTER TABLE `kardex`
  ADD PRIMARY KEY (`idkardex`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `kardex`
--
ALTER TABLE `kardex`
  MODIFY `idkardex` int(11) NOT NULL AUTO_INCREMENT;

-- --------------------------------------------------------

--
-- Cambios para la tabla `laboratorios`
--

ALTER TABLE `laboratorios` CHANGE `num_sanitario` `reg_sanitario` VARCHAR(18) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

--
-- Volcado de datos para la tabla `linea`
--

INSERT INTO `linea` (`desc_linea`) VALUES
('AGENTE DE DIAGNOSTICO'),
('ESPECIALIDAD FARMACEUTICA'),
('MEDICAMENTO DE MARCA'),
('MEDICAMENTO GENERICO'),
('PRODUCTO DE ORIGEN BIOLOGICO'),
('PRODUCTO DIETETICO'),
('PRODUCTO EDULCORANTE'),
('PRODUCTO GALENICO'),
('PRODUCTO HOMEOPATICO'),
('PRODUCTO NATURAL'),
('PRODUCTO RADIOFARMACO'),
('RECURSO NATURAL');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modalidad_traslado`
--

CREATE TABLE `modalidad_traslado` (
  `id_modtraslado` int(11) NOT NULL,
  `cod_modtraslado` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_modtraslado` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `modalidad_traslado`
--

INSERT INTO `modalidad_traslado` (`id_modtraslado`, `cod_modtraslado`, `desc_modtraslado`, `estatus`) VALUES
(1, '01', 'Transporte público', '1'),
(2, '02', 'Transporte privado', '1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `modalidad_traslado`
--
ALTER TABLE `modalidad_traslado`
  ADD PRIMARY KEY (`id_modtraslado`),
  ADD UNIQUE KEY `cod_modtraslado` (`cod_modtraslado`),
  ADD KEY `desc_modtraslado` (`desc_modtraslado`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `modalidad_traslado`
--
ALTER TABLE `modalidad_traslado`
  MODIFY `id_modtraslado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Cambios tabla `moneda`
--
ALTER TABLE `moneda`  ADD `code_moneda` VARCHAR(3) NULL  AFTER `idmoneda`,  ADD `code_intern_moneda` VARCHAR(3) NOT NULL  AFTER `code_moneda`;

--
-- Volcado de datos para la tabla `moneda`
--
TRUNCATE TABLE `moneda`;

INSERT INTO `moneda` (`idmoneda`, `code_moneda`, `code_intern_moneda`, `simb_moneda`, `nom_moneda`, `status`) VALUES
(1, 'AFN', '971', '', '', '1'),
(2, 'EUR', '978', '€', '', '1'),
(3, 'ALL', '008', '', '', '1'),
(4, 'DZD', '012', '', '', '1'),
(5, 'USD', '840', '', '', '1'),
(6, 'AOA', '973', '', '', '1'),
(7, 'XCD', '951', '', '', '1'),
(8, 'ARS', '032', '$', '', '1'),
(9, 'AMD', '051', '', '', '1'),
(10, 'AWG', '533', '', '', '1'),
(11, 'AUD', '036', '', '', '1'),
(12, 'AZN', '944', '', '', '1'),
(13, 'BSD', '044', '', '', '1'),
(14, 'BHD', '048', '', '', '1'),
(15, 'BDT', '050', '', '', '1'),
(16, 'BBD', '052', '', '', '1'),
(17, 'BYN', '933', '', '', '1'),
(18, 'BZD', '084', '', '', '1'),
(19, 'XOF', '952', '', '', '1'),
(20, 'BMD', '060', '', '', '1'),
(21, 'INR', '356', '', '', '1'),
(22, 'BTN', '064', '', '', '1'),
(23, 'BOB', '068', '', '', '1'),
(24, 'BOV', '984', '', '', '1'),
(25, 'BAM', '977', '', '', '1'),
(26, 'BWP', '072', '', '', '1'),
(27, 'NOK', '578', '', '', '1'),
(28, 'BRL', '986', '', '', '1'),
(29, 'BND', '096', 'B$', 'BRUNEI DOLLAR', '1'),
(30, 'BGN', '975', '', '', '1'),
(31, 'BIF', '108', '', '', '1'),
(32, 'CVE', '132', '', '', '1'),
(33, 'KHR', '116', '', '', '1'),
(34, 'XAF', '950', '', '', '1'),
(35, 'CAD', '124', '', '', '1'),
(36, 'KYD', '136', '', '', '1'),
(37, 'CLP', '152', '', '', '1'),
(38, 'CLF', '990', '', '', '1'),
(39, 'CNY', '156', '', '', '1'),
(40, 'COP', '170', '', '', '1'),
(41, 'COU', '970', '', '', '1'),
(42, 'KMF', '174', '', '', '1'),
(43, 'CDF', '976', '', '', '1'),
(44, 'NZD', '554', '', '', '1'),
(45, 'CRC', '188', '', '', '1'),
(46, 'HRK', '191', '', '', '1'),
(47, 'CUP', '192', '', '', '1'),
(48, 'CUC', '931', '', '', '1'),
(49, 'ANG', '532', '', '', '1'),
(50, 'CZK', '203', '', '', '1'),
(51, 'DKK', '208', '', '', '1'),
(52, 'DJF', '262', '', '', '1'),
(53, 'DOP', '214', '', '', '1'),
(54, 'EGP', '818', '', '', '1'),
(55, 'SVC', '222', '', '', '1'),
(56, 'ERN', '232', '', '', '1'),
(57, 'ETB', '230', '', '', '1'),
(58, 'FKP', '238', '', '', '1'),
(59, 'FJD', '242', '', '', '1'),
(60, 'XPF', '953', '', '', '1'),
(61, 'GMD', '270', '', '', '1'),
(62, 'GEL', '981', '', '', '1'),
(63, 'GHS', '936', '', '', '1'),
(64, 'GIP', '292', '', '', '1'),
(65, 'GTQ', '320', '', '', '1'),
(66, 'GBP', '826', '', '', '1'),
(67, 'GNF', '324', '', '', '1'),
(68, 'GYD', '328', '', '', '1'),
(69, 'HTG', '332', '', '', '1'),
(70, 'HNL', '340', '', '', '1'),
(71, 'HKD', '344', '', '', '1'),
(72, 'HUF', '348', '', '', '1'),
(73, 'ISK', '352', '', '', '1'),
(74, 'IDR', '360', '', '', '1'),
(75, 'XDR', '960', '', '', '1'),
(76, 'IRR', '364', '', '', '1'),
(77, 'IQD', '368', '', '', '1'),
(78, 'ILS', '376', '', '', '1'),
(79, 'JMD', '388', '', '', '1'),
(80, 'JPY', '392', '', '', '1'),
(81, 'JOD', '400', '', '', '1'),
(82, 'KZT', '398', '', '', '1'),
(83, 'KES', '404', '', '', '1'),
(84, 'KPW', '408', '', '', '1'),
(85, 'KRW', '410', '', '', '1'),
(86, 'KWD', '414', '', '', '1'),
(87, 'KGS', '417', '', '', '1'),
(88, 'LAK', '418', '', '', '1'),
(89, 'LBP', '422', '', '', '1'),
(90, 'LSL', '426', '', '', '1'),
(91, 'ZAR', '710', '', '', '1'),
(92, 'LRD', '430', '', '', '1'),
(93, 'LYD', '434', '', '', '1'),
(94, 'CHF', '756', '', '', '1'),
(95, 'MOP', '446', '', '', '1'),
(96, 'MKD', '807', '', '', '1'),
(97, 'MGA', '969', '', '', '1'),
(98, 'MWK', '454', '', '', '1'),
(99, 'MYR', '458', '', '', '1'),
(100, 'MVR', '462', '', '', '1'),
(101, 'MRU', '929', '', '', '1'),
(102, 'MUR', '480', '', '', '1'),
(103, 'XUA', '965', '', '', '1'),
(104, 'MXN', '484', '', '', '1'),
(105, 'MXV', '979', '', '', '1'),
(106, 'MDL', '498', '', '', '1'),
(107, 'MNT', '496', '', '', '1'),
(108, 'MAD', '504', '', '', '1'),
(109, 'MZN', '943', '', '', '1'),
(110, 'MMK', '104', '', '', '1'),
(111, 'NAD', '516', '', '', '1'),
(112, 'NPR', '524', '', '', '1'),
(113, 'NIO', '558', '', '', '1'),
(114, 'NGN', '566', '', '', '1'),
(115, 'OMR', '512', '', '', '1'),
(116, 'PKR', '586', '', '', '1'),
(117, 'PAB', '590', '', '', '1'),
(118, 'PGK', '598', '', '', '1'),
(119, 'PYG', '600', '', '', '1'),
(120, 'PEN', '604', 'S/.', 'Soles', '1'),
(121, 'PHP', '608', '', '', '1'),
(122, 'PLN', '985', '', '', '1'),
(123, 'QAR', '634', '', '', '1'),
(124, 'RON', '946', '', '', '1'),
(125, 'RUB', '643', '', '', '1'),
(126, 'RWF', '646', '', '', '1'),
(127, 'SHP', '654', '', '', '1'),
(128, 'WST', '882', '', '', '1'),
(129, 'STN', '930', '', '', '1'),
(130, 'SAR', '682', '', '', '1'),
(131, 'RSD', '941', '', '', '1'),
(132, 'SCR', '690', '', '', '1'),
(133, 'SLL', '694', '', '', '1'),
(134, 'SGD', '702', '', '', '1'),
(135, 'XSU', '994', '', '', '1'),
(136, 'SBD', '090', '', '', '1'),
(137, 'SOS', '706', '', '', '1'),
(138, 'SSP', '728', '', '', '1'),
(139, 'LKR', '144', '', '', '1'),
(140, 'SDG', '938', '', '', '1'),
(141, 'SRD', '968', '', '', '1'),
(142, 'SZL', '748', '', '', '1'),
(143, 'SEK', '752', '', '', '1'),
(144, 'CHE', '947', '', '', '1'),
(145, 'CHW', '948', '', '', '1'),
(146, 'SYP', '760', '', '', '1'),
(147, 'TWD', '901', '', '', '1'),
(148, 'TJS', '972', '', '', '1'),
(149, 'TZS', '834', '', '', '1'),
(150, 'THB', '764', '', '', '1'),
(151, 'TOP', '776', '', '', '1'),
(152, 'TTD', '780', '', '', '1'),
(153, 'TND', '788', '', '', '1'),
(154, 'TRY', '949', '', '', '1'),
(155, 'TMT', '934', '', '', '1'),
(156, 'UGX', '800', '', '', '1'),
(157, 'UAH', '980', '', '', '1'),
(158, 'AED', '784', '', '', '1'),
(159, 'USN', '997', '', '', '1'),
(160, 'UYU', '858', '', '', '1'),
(161, 'UYI', '940', '', '', '1'),
(162, 'UYW', '927', '', '', '1'),
(163, 'UZS', '860', '', '', '1'),
(164, 'VUV', '548', '', '', '1'),
(165, 'VES', '928', 'Bs.S.', 'Bolivar Soberano', '1'),
(166, 'VND', '704', '', '', '1'),
(167, 'YER', '886', '', '', '1'),
(168, 'ZMW', '967', '', '', '1'),
(169, 'ZWL', '932', '', '', '1');

--
-- Estructura de tabla para la tabla `monedas_adicionales`
--

CREATE TABLE `monedas_adicionales` (
  `id_moneda` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `monedas_adicionales`
--

INSERT INTO `monedas_adicionales` (`id_moneda`) VALUES
(120),
(2),
(5);

--
-- Estructura de tabla para la tabla `motivos_traslados`
--

CREATE TABLE `motivos_traslados` (
  `id_motivtraslad` int(11) NOT NULL,
  `cod_id_motivtraslad` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_id_motivtraslad` varchar(80) COLLATE utf8_spanish2_ci NOT NULL,
  `detallar_concepto` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `motivos_traslados`
--

INSERT INTO `motivos_traslados` (`id_motivtraslad`, `cod_id_motivtraslad`, `desc_id_motivtraslad`, `detallar_concepto`, `estatus`) VALUES
(1, '01', 'VENTA', '0', '1'),
(2, '14', 'VENTA SUJETA A CONFIRMACION DEL COMPRADOR', '0', '1'),
(3, '02', 'COMPRA', '0', '1'),
(4, '04', 'TRASLADO ENTRE ESTABLECIMIENTOS DE LA MISMA EMPRESA', '0', '1'),
(5, '18', 'TRASLADO EMISOR ITINERANTE CP', '0', '1'),
(6, '08', 'IMPORTACION', '0', '1'),
(7, '09', 'EXPORTACION', '0', '1'),
(8, '19', 'TRASLADO A ZONA PRIMARIA', '0', '1'),
(9, '13', 'OTROS', '1', '1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `motivos_traslados`
--
ALTER TABLE `motivos_traslados`
  ADD PRIMARY KEY (`id_motivtraslad`),
  ADD UNIQUE KEY `cod_id_motivtraslad` (`cod_id_motivtraslad`),
  ADD KEY `desc_id_motivtraslad` (`desc_id_motivtraslad`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `motivos_traslados`
--
ALTER TABLE `motivos_traslados`
  MODIFY `id_motivtraslad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE `paises` (
  `idpais` int(11) NOT NULL,
  `desc_pais` varchar(70) NOT NULL,
  `cod_pais` varchar(2) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `id_moneda` int(11) NOT NULL,
  `predeterminado` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`idpais`, `desc_pais`, `cod_pais`, `id_moneda`, `predeterminado`) VALUES
(1, 'Afghanistan', 'AF', 1, '0'),
(2, 'Aland Islands', 'AX', 2, '0'),
(3, 'Albania', 'AL', 3, '0'),
(4, 'Algeria', 'DZ', 4, '0'),
(5, 'American Samoa', 'AS', 5, '0'),
(6, 'Andorra', 'AD', 2, '0'),
(7, 'Angola', 'AO', 6, '0'),
(8, 'Anguilla', 'AI', 7, '0'),
(9, 'Antarctica', 'AQ', 0, '0'),
(10, 'Antigua and Barbuda', 'AG', 7, '0'),
(11, 'Argentina', 'AR', 8, '0'),
(12, 'Armenia', 'AM', 9, '0'),
(13, 'Aruba', 'AW', 10, '0'),
(14, 'Australia', 'AU', 11, '0'),
(15, 'Austria', 'AT', 2, '0'),
(16, 'Azerbaijan', 'AZ', 12, '0'),
(17, 'Bahamas', 'BS', 13, '0'),
(18, 'Bahrain', 'BH', 14, '0'),
(19, 'Bangladesh', 'BD', 15, '0'),
(20, 'Barbados', 'BB', 16, '0'),
(21, 'Belarus', 'BY', 17, '0'),
(22, 'Belgium', 'BE', 2, '0'),
(23, 'Belize', 'BZ', 18, '0'),
(24, 'Benin', 'BJ', 19, '0'),
(25, 'Bermuda', 'BM', 20, '0'),
(26, 'Bhutan', 'BT', 21, '0'),
(27, 'Bolivia, Plurinational State of', 'BO', 23, '0'),
(28, 'Bosnia and Herzegovina', 'BA', 25, '0'),
(29, 'Botswana', 'BW', 26, '0'),
(30, 'Bouvet Island', 'BV', 27, '0'),
(31, 'Brazil', 'BR', 28, '0'),
(32, 'British Indian Ocean Territory', 'IO', 5, '0'),
(33, 'Brunei Darussalam', 'BN', 29, '0'),
(34, 'Bulgaria', 'BG', 30, '0'),
(35, 'Burkina Faso', 'BF', 19, '0'),
(36, 'Burundi', 'BI', 31, '0'),
(37, 'Cape Verde', 'CV', 32, '0'),
(38, 'Cambodia', 'KH', 33, '0'),
(39, 'Cameroon', 'CM', 34, '0'),
(40, 'Canada', 'CA', 35, '0'),
(41, 'Cayman Islands', 'KY', 36, '0'),
(42, 'Central African Republic', 'CF', 34, '0'),
(43, 'Chad', 'TD', 34, '0'),
(44, 'Chile', 'CL', 37, '0'),
(45, 'China', 'CN', 39, '0'),
(46, 'Christmas Island', 'CX', 11, '0'),
(47, 'Cocos (Keeling) Islands', 'CC', 11, '0'),
(48, 'Colombia', 'CO', 40, '0'),
(49, 'Comoros', 'KM', 42, '0'),
(50, 'Congo, the Democratic Republic of the', 'CD', 43, '0'),
(51, 'Congo', 'CG', 34, '0'),
(52, 'Cook Islands', 'CK', 44, '0'),
(53, 'Costa Rica', 'CR', 45, '0'),
(54, 'Cote d\'Ivoire', 'CI', 19, '0'),
(55, 'Croatia', 'HR', 46, '0'),
(56, 'Cuba', 'CU', 47, '0'),
(57, 'Netherlands Antilles', 'AN', 49, '0'),
(58, 'Cyprus', 'CY', 2, '0'),
(59, 'Czech Republic', 'CZ', 50, '0'),
(60, 'Denmark', 'DK', 51, '0'),
(61, 'Djibouti', 'DJ', 52, '0'),
(62, 'Dominica', 'DM', 7, '0'),
(63, 'Dominican Republic', 'DO', 53, '0'),
(64, 'Ecuador', 'EC', 5, '0'),
(65, 'Egypt', 'EG', 54, '0'),
(66, 'El Salvador', 'SV', 55, '0'),
(67, 'Equatorial Guinea', 'GQ', 34, '0'),
(68, 'Eritrea', 'ER', 56, '0'),
(69, 'Estonia', 'EE', 2, '0'),
(70, 'Ethiopia', 'ET', 57, '0'),
(71, 'Falkland Islands (Malvinas)', 'FK', 58, '0'),
(72, 'Faroe Islands', 'FO', 51, '0'),
(73, 'Fiji', 'FJ', 59, '0'),
(74, 'Finland', 'FI', 2, '0'),
(75, 'France', 'FR', 2, '0'),
(76, 'French Guiana', 'GF', 2, '0'),
(77, 'French Polynesia', 'PF', 60, '0'),
(78, 'French Southern Territories', 'TF', 2, '0'),
(79, 'Gabon', 'GA', 34, '0'),
(80, 'Gambia', 'GM', 61, '0'),
(81, 'Georgia', 'GE', 62, '0'),
(82, 'Germany', 'DE', 2, '0'),
(83, 'Ghana', 'GH', 63, '0'),
(84, 'Gibraltar', 'GI', 64, '0'),
(85, 'Greece', 'GR', 2, '0'),
(86, 'Greenland', 'GL', 51, '0'),
(87, 'Grenada', 'GD', 7, '0'),
(88, 'Guadeloupe', 'GP', 2, '0'),
(89, 'Guam', 'GU', 5, '0'),
(90, 'Guatemala', 'GT', 65, '0'),
(91, 'Guernsey', 'GG', 66, '0'),
(92, 'Guinea', 'GN', 67, '0'),
(93, 'Guinea-Bissau', 'GW', 19, '0'),
(94, 'Guyana', 'GY', 68, '0'),
(95, 'Haiti', 'HT', 69, '0'),
(96, 'Heard Island and McDonald Islands', 'HM', 11, '0'),
(97, 'Holy See (Vatican City State)', 'VA', 2, '0'),
(98, 'Honduras', 'HN', 70, '0'),
(99, 'Hong Kong', 'HK', 71, '0'),
(100, 'Hungary', 'HU', 72, '0'),
(101, 'Iceland', 'IS', 73, '0'),
(102, 'India', 'IN', 21, '0'),
(103, 'Indonesia', 'ID', 74, '0'),
(104, 'Iran, Islamic Republic of', 'IR', 76, '0'),
(105, 'Iraq', 'IQ', 77, '0'),
(106, 'Ireland', 'IE', 2, '0'),
(107, 'Isle of Man', 'IM', 66, '0'),
(108, 'Israel', 'IL', 78, '0'),
(109, 'Italy', 'IT', 2, '0'),
(110, 'Jamaica', 'JM', 79, '0'),
(111, 'Japan', 'JP', 80, '0'),
(112, 'Jersey', 'JE', 66, '0'),
(113, 'Jordan', 'JO', 81, '0'),
(114, 'Kazakhstan', 'KZ', 82, '0'),
(115, 'Kenya', 'KE', 83, '0'),
(116, 'Kiribati', 'KI', 11, '0'),
(117, 'Korea, Democratic People\'s Republic of', 'KP', 84, '0'),
(118, 'Korea, Republic of', 'KR', 85, '0'),
(119, 'Kuwait', 'KW', 86, '0'),
(120, 'Kyrgyzstan', 'KG', 87, '0'),
(121, 'Lao People\'s Democratic Republic', 'LA', 88, '0'),
(122, 'Latvia', 'LV', 2, '0'),
(123, 'Lebanon', 'LB', 89, '0'),
(124, 'Lesotho', 'LS', 90, '0'),
(125, 'Liberia', 'LR', 92, '0'),
(126, 'Libyan Arab Jamahiriya', 'LY', 93, '0'),
(127, 'Liechtenstein', 'LI', 94, '0'),
(128, 'Lithuania', 'LT', 2, '0'),
(129, 'Luxembourg', 'LU', 2, '0'),
(130, 'Macao', 'MO', 95, '0'),
(131, 'Macedonia, the former Yugoslav Republic of', 'MK', 96, '0'),
(132, 'Madagascar', 'MG', 97, '0'),
(133, 'Malawi', 'MW', 98, '0'),
(134, 'Malaysia', 'MY', 99, '0'),
(135, 'Maldives', 'MV', 100, '0'),
(136, 'Mali', 'ML', 19, '0'),
(137, 'Malta', 'MT', 2, '0'),
(138, 'Marshall Islands', 'MH', 5, '0'),
(139, 'Martinique', 'MQ', 2, '0'),
(140, 'Mauritania', 'MR', 101, '0'),
(141, 'Mauritius', 'MU', 102, '0'),
(142, 'Mayotte', 'YT', 2, '0'),
(143, 'Mexico', 'MX', 104, '0'),
(144, 'Micronesia, Federated States of', 'FM', 5, '0'),
(145, 'Moldova, Republic of', 'MD', 106, '0'),
(146, 'Monaco', 'MC', 2, '0'),
(147, 'Mongolia', 'MN', 107, '0'),
(148, 'Montenegro', 'ME', 2, '0'),
(149, 'Montserrat', 'MS', 7, '0'),
(150, 'Morocco', 'MA', 108, '0'),
(151, 'Mozambique', 'MZ', 109, '0'),
(152, 'Myanmar', 'MM', 110, '0'),
(153, 'Namibia', 'NA', 91, '0'),
(154, 'Nauru', 'NR', 11, '0'),
(155, 'Nepal', 'NP', 112, '0'),
(156, 'Netherlands', 'NL', 2, '0'),
(157, 'New Caledonia', 'NC', 60, '0'),
(158, 'New Zealand', 'NZ', 44, '0'),
(159, 'Nicaragua', 'NI', 113, '0'),
(160, 'Niger', 'NE', 19, '0'),
(161, 'Nigeria', 'NG', 114, '0'),
(162, 'Niue', 'NU', 44, '0'),
(163, 'Norfolk Island', 'NF', 11, '0'),
(164, 'Northern Mariana Islands', 'MP', 5, '0'),
(165, 'Norway', 'NO', 27, '0'),
(166, 'Oman', 'OM', 115, '0'),
(167, 'Pakistan', 'PK', 116, '0'),
(168, 'Palau', 'PW', 5, '0'),
(169, 'Palestinian Territory, Occupied', 'PS', 0, '0'),
(170, 'Panama', 'PA', 117, '0'),
(171, 'Papua New Guinea', 'PG', 118, '0'),
(172, 'Paraguay', 'PY', 119, '0'),
(173, 'Peru', 'PE', 120, '1'),
(174, 'Philippines', 'PH', 121, '0'),
(175, 'Pitcairn', 'PN', 44, '0'),
(176, 'Poland', 'PL', 122, '0'),
(177, 'Portugal', 'PT', 2, '0'),
(178, 'Puerto Rico', 'PR', 5, '0'),
(179, 'Qatar', 'QA', 123, '0'),
(180, 'Reunion  Réunion', 'RE', 2, '0'),
(181, 'Romania', 'RO', 124, '0'),
(182, 'Russian Federation', 'RU', 125, '0'),
(183, 'Rwanda', 'RW', 126, '0'),
(184, 'Saint Barthélemy', 'BL', 2, '0'),
(185, 'Saint Helena', 'SH', 127, '0'),
(186, 'Saint Kitts and Nevis', 'KN', 7, '0'),
(187, 'Saint Lucia', 'LC', 7, '0'),
(188, 'Saint Martin (French part)', 'MF', 2, '0'),
(189, 'Saint Pierre and Miquelon', 'PM', 2, '0'),
(190, 'Saint Vincent and the Grenadines', 'VC', 7, '0'),
(191, 'Samoa', 'WS', 128, '0'),
(192, 'San Marino', 'SM', 2, '0'),
(193, 'Sao Tome and Principe', 'ST', 129, '0'),
(194, 'Saudi Arabia', 'SA', 130, '0'),
(195, 'Senegal', 'SN', 19, '0'),
(196, 'Serbia', 'RS', 131, '0'),
(197, 'Seychelles', 'SC', 132, '0'),
(198, 'Sierra Leone', 'SL', 133, '0'),
(199, 'Singapore', 'SG', 134, '0'),
(200, 'Slovakia', 'SK', 2, '0'),
(201, 'Slovenia', 'SI', 2, '0'),
(202, 'Solomon Islands', 'SB', 136, '0'),
(203, 'Somalia', 'SO', 137, '0'),
(204, 'South Africa', 'ZA', 91, '0'),
(205, 'South Georgia and the South Sandwich Islands', 'GS', 0, '0'),
(206, 'Spain', 'ES', 2, '0'),
(207, 'Sri Lanka', 'LK', 139, '0'),
(208, 'Sudan', 'SD', 140, '0'),
(209, 'Suriname', 'SR', 141, '0'),
(210, 'Svalbard and Jan Mayen', 'SJ', 27, '0'),
(211, 'Swaziland', 'SZ', 142, '0'),
(212, 'Sweden', 'SE', 143, '0'),
(213, 'Switzerland', 'CH', 94, '0'),
(214, 'Syrian Arab Republic', 'SY', 146, '0'),
(215, 'Taiwan, Province of China', 'TW', 147, '0'),
(216, 'Tajikistan', 'TJ', 148, '0'),
(217, 'Tanzania, United Republic of', 'TZ', 149, '0'),
(218, 'Thailand', 'TH', 150, '0'),
(219, 'Timor-Leste', 'TL', 5, '0'),
(220, 'Togo', 'TG', 19, '0'),
(221, 'Tokelau', 'TK', 44, '0'),
(222, 'Tonga', 'TO', 151, '0'),
(223, 'Trinidad and Tobago', 'TT', 152, '0'),
(224, 'Tunisia', 'TN', 153, '0'),
(225, 'Turkey', 'TR', 154, '0'),
(226, 'Turkmenistan', 'TM', 155, '0'),
(227, 'Turks and Caicos Islands', 'TC', 5, '0'),
(228, 'Tuvalu', 'TV', 11, '0'),
(229, 'Uganda', 'UG', 156, '0'),
(230, 'Ukraine', 'UA', 157, '0'),
(231, 'United Arab Emirates', 'AE', 158, '0'),
(232, 'United Kingdom', 'GB', 66, '0'),
(233, 'United States Minor Outlying Islands', 'UM', 5, '0'),
(234, 'United States', 'US', 5, '0'),
(235, 'Uruguay', 'UY', 160, '0'),
(236, 'Uzbekistan', 'UZ', 163, '0'),
(237, 'Vanuatu', 'VU', 164, '0'),
(238, 'Venezuela, Bolivarian Republic of', 'VE', 165, '0'),
(239, 'Viet Nam', 'VN', 166, '0'),
(240, 'Virgin Islands, British', 'VG', 5, '0'),
(241, 'Virgin Islands, U.S.', 'VI', 5, '0'),
(242, 'Wallis and Futuna', 'WF', 60, '0'),
(243, 'Western Sahara', 'EH', 108, '0'),
(244, 'Yemen', 'YE', 167, '0'),
(245, 'Zambia', 'ZM', 168, '0'),
(246, 'Zimbabwe', 'ZW', 169, '0');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`idpais`),
  ADD UNIQUE KEY `idpais_UNIQUE` (`idpais`);

--
-- Estructura de tabla para la tabla `privilegios_rol`
--

CREATE TABLE `privilegios_rol` (
  `idpriv_rol` int(11) NOT NULL,
  `idrol_sistema` int(11) NOT NULL,
  `idwin_sistemas` int(11) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `privilegios_rol`
--
ALTER TABLE `privilegios_rol`
  ADD PRIMARY KEY (`idpriv_rol`),
  ADD UNIQUE KEY `idpriv_rol_UNIQUE` (`idpriv_rol`),
  ADD UNIQUE KEY `rol_privilegio` (`idrol_sistema`,`idwin_sistemas`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `privilegios_rol`
--
ALTER TABLE `privilegios_rol`
  MODIFY `idpriv_rol` int(11) NOT NULL AUTO_INCREMENT;


--
-- CAMBIOS en la tabla `productos`
--

ALTER TABLE `productos` CHANGE `concent_princ_activo` `concent_princ_activo` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
ALTER TABLE `productos`  ADD `id_formfarmaceutica` INT NULL DEFAULT NULL  AFTER `concent_princ_activo`,  ADD `reg_sanitario` VARCHAR(15) NULL DEFAULT NULL  AFTER `id_formfarmaceutica`;
ALTER TABLE `productos` DROP `form_farmaceu`;
ALTER TABLE `productos` CHANGE `serial_1` `serial_1` INT NULL DEFAULT NULL, CHANGE `serial_2` `serial_2` INT NULL DEFAULT NULL, CHANGE `serial_3` `serial_3` INT NULL DEFAULT NULL;

--
-- Estructura de tabla para la tabla `provincias`
--

CREATE TABLE `provincias` (
  `id_provincia` int(11) NOT NULL,
  `id_depto` int(11) NOT NULL,
  `cod_provincia` varchar(4) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `desc_provincia` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus_provincia` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `provincias`
--

INSERT INTO `provincias` (`id_provincia`, `id_depto`, `cod_provincia`, `desc_provincia`, `estatus_provincia`) VALUES
(1, 1, '0101', 'Chachapoyas', '1'),
(2, 1, '0102', 'Bagua', '1'),
(3, 1, '0103', 'Bongará', '1'),
(4, 1, '0105', 'Luya', '1'),
(5, 1, '0106', 'Rodríguez de Mendoza', '1'),
(6, 1, '0107', 'Utcubamba', '1'),
(7, 2, '0201', 'Huaraz', '1'),
(8, 2, '0202', 'Aija', '1'),
(9, 2, '0203', 'Antonio Raymondi', '1'),
(10, 2, '0204', 'Asunción', '1'),
(11, 2, '0205', 'Bolognesi', '1'),
(12, 2, '0206', 'Carhuaz', '1'),
(13, 2, '0207', 'Carlos Fermín Fitzcarrald', '1'),
(14, 2, '0208', 'Casma', '1'),
(15, 2, '0209', 'Corongo', '1'),
(16, 2, '0210', 'Huari', '1'),
(17, 2, '0211', 'Huarmey', '1'),
(18, 2, '0212', 'Huaylas', '1'),
(19, 2, '0213', 'Mariscal Luzuriaga', '1'),
(20, 2, '0214', 'Ocros', '1'),
(21, 2, '0215', 'Pallasca', '1'),
(22, 2, '0216', 'Pomabamba', '1'),
(23, 2, '0217', 'Recuay', '1'),
(24, 2, '0218', 'Santa', '1'),
(25, 2, '0219', 'Sihuas', '1'),
(26, 2, '0220', 'Yungay', '1'),
(27, 3, '0301', 'Abancay', '1'),
(28, 3, '0302', 'Andahuaylas', '1'),
(29, 3, '0303', 'Antabamba', '1'),
(30, 3, '0304', 'Aymaraes', '1'),
(31, 3, '0305', 'Cotabambas', '1'),
(32, 3, '0306', 'Chincheros', '1'),
(33, 3, '0307', 'Grau', '1'),
(34, 4, '0401', 'Arequipa', '1'),
(35, 4, '0402', 'Camaná', '1'),
(36, 4, '0404', 'Castilla', '1'),
(37, 4, '0405', 'Caylloma', '1'),
(38, 4, '0406', 'Condesuyos', '1'),
(39, 4, '0407', 'Islay', '1'),
(40, 4, '0408', 'La Unión', '1'),
(41, 5, '0501', 'Huamanga', '1'),
(42, 5, '0502', 'Cangallo', '1'),
(43, 5, '0503', 'Huanca Sancos', '1'),
(44, 5, '0504', 'Huanta', '1'),
(45, 5, '0505', 'La Mar', '1'),
(46, 5, '0506', 'Lucanas', '1'),
(47, 5, '0507', 'Parinacochas', '1'),
(48, 5, '0508', 'Páucar del Sara Sara', '1'),
(49, 5, '0509', 'Sucre', '1'),
(50, 5, '0510', 'Víctor Fajardo', '1'),
(51, 5, '0511', 'Vilcas Huamán', '1'),
(52, 6, '0601', 'Cajamarca', '1'),
(53, 6, '0602', 'Cajabamba', '1'),
(54, 6, '0603', 'Celendín', '1'),
(55, 6, '0604', 'Chota', '1'),
(56, 6, '0605', 'Contumazá', '1'),
(57, 6, '0607', 'Hualgayoc', '1'),
(58, 6, '0608', 'Jaén', '1'),
(59, 6, '0609', 'San Ignacio', '1'),
(60, 6, '0610', 'San Marcos', '1'),
(61, 6, '0611', 'San Miguel', '1'),
(62, 6, '0612', 'San Pablo', '1'),
(63, 6, '0613', 'Santa Cruz', '1'),
(64, 7, '0701', 'Prov. Const. del Callao', '1'),
(65, 8, '0801', 'Cusco', '1'),
(66, 8, '0802', 'Acomayo', '1'),
(67, 8, '0803', 'Anta', '1'),
(68, 8, '0804', 'Calca', '1'),
(69, 8, '0805', 'Canas', '1'),
(70, 8, '0806', 'Canchis', '1'),
(71, 8, '0807', 'Chumbivilcas', '1'),
(72, 8, '0808', 'Espinar', '1'),
(73, 8, '0809', 'La Convención', '1'),
(74, 8, '0810', 'Paruro', '1'),
(75, 8, '0811', 'Paucartambo', '1'),
(76, 8, '0812', 'Quispicanchi', '1'),
(77, 8, '0813', 'Urubamba', '1'),
(78, 9, '0901', 'Huancavelica', '1'),
(79, 9, '0902', 'Acobamba', '1'),
(80, 9, '0903', 'Angaraes', '1'),
(81, 9, '0904', 'Castrovirreyna', '1'),
(82, 9, '0905', 'Churcampa', '1'),
(83, 9, '0906', 'Huaytará', '1'),
(84, 10, '1001', 'Huánuco', '1'),
(85, 10, '1002', 'Ambo', '1'),
(86, 10, '1003', 'Dos de Mayo', '1'),
(87, 10, '1004', 'Huacaybamba', '1'),
(88, 10, '1005', 'Huamalíes', '1'),
(89, 10, '1006', 'Leoncio Prado', '1'),
(90, 10, '1007', 'Marañón', '1'),
(91, 10, '1008', 'Pachitea', '1'),
(92, 10, '1009', 'Puerto Inca', '1'),
(93, 10, '1010', 'Lauricocha', '1'),
(94, 10, '1011', 'Yarowilca', '1'),
(95, 11, '1101', 'Ica', '1'),
(96, 11, '1102', 'Chincha', '1'),
(97, 11, '1103', 'Nasca', '1'),
(98, 11, '1104', 'Palpa', '1'),
(99, 11, '1105', 'Pisco', '1'),
(100, 12, '1201', 'Huancayo', '1'),
(101, 12, '1202', 'Concepción', '1'),
(102, 12, '1203', 'Chanchamayo', '1'),
(103, 12, '1204', 'Jauja', '1'),
(104, 12, '1205', 'Junín', '1'),
(105, 12, '1206', 'Satipo', '1'),
(106, 12, '1207', 'Tarma', '1'),
(107, 12, '1208', 'Yauli', '1'),
(108, 12, '1209', 'Chupaca', '1'),
(109, 13, '1301', 'Trujillo', '1'),
(110, 13, '1302', 'Ascope', '1'),
(111, 13, '1303', 'Bolívar', '1'),
(112, 13, '1304', 'Chepén', '1'),
(113, 13, '1305', 'Julcán', '1'),
(114, 13, '1306', 'Otuzco', '1'),
(115, 13, '1307', 'Pacasmayo', '1'),
(116, 13, '1308', 'Pataz', '1'),
(117, 13, '1309', 'Sánchez Carrión', '1'),
(118, 13, '1310', 'Santiago de Chuco', '1'),
(119, 13, '1311', 'Gran Chimú', '1'),
(120, 13, '1312', 'Virú', '1'),
(121, 14, '1401', 'Chiclayo', '1'),
(122, 14, '1402', 'Ferreñafe', '1'),
(123, 14, '1403', 'Lambayeque', '1'),
(124, 15, '1501', 'Lima', '1'),
(125, 15, '1502', 'Barranca', '1'),
(126, 15, '1503', 'Cajatambo', '1'),
(127, 15, '1504', 'Canta', '1'),
(128, 15, '1505', 'Cañete', '1'),
(129, 15, '1506', 'Huaral', '1'),
(130, 15, '1507', 'Huarochirí', '1'),
(131, 15, '1509', 'Oyón', '1'),
(132, 15, '1510', 'Yauyos', '1'),
(133, 16, '1601', 'Maynas', '1'),
(134, 16, '1602', 'Alto Amazonas', '1'),
(135, 16, '1603', 'Loreto', '1'),
(136, 16, '1604', 'Mariscal Ramón Castilla', '1'),
(137, 16, '1605', 'Requena', '1'),
(138, 16, '1606', 'Ucayali', '1'),
(139, 16, '1607', 'Datem del Marañón', '1'),
(140, 16, '1608', 'Putumayo', '1'),
(141, 17, '1701', 'Tambopata', '1'),
(142, 17, '1702', 'Manu', '1'),
(143, 17, '1703', 'Tahuamanu', '1'),
(144, 18, '1801', 'Mariscal Nieto', '1'),
(145, 18, '1802', 'General Sánchez Cerro', '1'),
(146, 18, '1803', 'Ilo', '1'),
(147, 19, '1901', 'Pasco', '1'),
(148, 19, '1902', 'Daniel Alcides Carrión', '1'),
(149, 19, '1903', 'Oxapampa', '1'),
(150, 20, '2001', 'Piura', '1'),
(151, 20, '2002', 'Ayabaca', '1'),
(152, 20, '2003', 'Huancabamba', '1'),
(153, 20, '2004', 'Morropón', '1'),
(154, 20, '2005', 'Paita', '1'),
(155, 20, '2006', 'Sullana', '1'),
(156, 20, '2007', 'Talara', '1'),
(157, 20, '2008', 'Sechura', '1'),
(158, 21, '2101', 'Puno', '1'),
(159, 21, '2102', 'Azángaro', '1'),
(160, 21, '2103', 'Carabaya', '1'),
(161, 21, '2104', 'Chucuito', '1'),
(162, 21, '2105', 'El Collao', '1'),
(163, 21, '2106', 'Huancané', '1'),
(164, 21, '2108', 'Melgar', '1'),
(165, 21, '2109', 'Moho', '1'),
(166, 21, '2110', 'San Antonio de Putina', '1'),
(167, 21, '2111', 'San Román', '1'),
(168, 21, '2112', 'Sandia', '1'),
(169, 21, '2113', 'Yunguyo', '1'),
(170, 22, '2201', 'Moyobamba', '1'),
(171, 22, '2202', 'Bellavista', '1'),
(172, 22, '2203', 'El Dorado', '1'),
(173, 22, '2204', 'Huallaga', '1'),
(174, 22, '2205', 'Lamas', '1'),
(175, 22, '2206', 'Mariscal Cáceres', '1'),
(176, 22, '2207', 'Picota', '1'),
(177, 22, '2208', 'Rioja', '1'),
(178, 22, '2209', 'San Martín', '1'),
(179, 22, '2210', 'Tocache', '1'),
(180, 23, '2301', 'Tacna', '1'),
(181, 23, '2302', 'Candarave', '1'),
(182, 23, '2303', 'Jorge Basadre', '1'),
(183, 23, '2304', 'Tarata', '1'),
(184, 24, '2401', 'Tumbes', '1'),
(185, 24, '2402', 'Contralmirante Villar', '1'),
(186, 24, '2403', 'Zarumilla', '1'),
(187, 25, '2501', 'Coronel Portillo', '1'),
(188, 25, '2502', 'Atalaya', '1'),
(189, 25, '2503', 'Padre Abad', '1'),
(190, 25, '2504', 'Purús', '1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `provincias`
--
ALTER TABLE `provincias`
  ADD PRIMARY KEY (`id_provincia`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `provincias`
--
ALTER TABLE `provincias`
  MODIFY `id_provincia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- Estructura de tabla para la tabla `regimen_percepcion`
--

CREATE TABLE `regimen_percepcion` (
  `id_regpercepcion` int(11) NOT NULL,
  `cod_regpercepcion` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `desc_regpercepcion` varchar(90) COLLATE utf8_spanish2_ci NOT NULL,
  `mto_tasa` double(6,2) NOT NULL DEFAULT '0.00',
  `estatus` enum('0','1') COLLATE utf8_spanish2_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `regimen_percepcion`
--

INSERT INTO `regimen_percepcion` (`id_regpercepcion`, `cod_regpercepcion`, `desc_regpercepcion`, `mto_tasa`, `estatus`) VALUES
(1, '01', 'PERCEPCION VENTA INTERNA', 2.00, '1'),
(2, '02', 'PERCEPCION A LA ADQUISICION DE COMBUSTIBLE', 1.00, '1'),
(3, '03', 'PERCEPCION REALIZADA AL AGENTE DE PERCEPCION CON TASA ESPECIAL', 0.50, '1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `regimen_percepcion`
--
ALTER TABLE `regimen_percepcion`
  ADD PRIMARY KEY (`id_regpercepcion`),
  ADD UNIQUE KEY `cod_regpercepcion` (`cod_regpercepcion`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `regimen_percepcion`
--
ALTER TABLE `regimen_percepcion`
  MODIFY `id_regpercepcion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Estructura de tabla para la tabla `regimen_retencion`
--

CREATE TABLE `regimen_retencion` (
  `id_regretencion` int(11) NOT NULL,
  `cod_regretencion` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `monto_regretencion` double(6,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `regimen_retencion`
--

INSERT INTO `regimen_retencion` (`id_regretencion`, `cod_regretencion`, `monto_regretencion`) VALUES
(1, '01', 3.00);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `regimen_retencion`
--
ALTER TABLE `regimen_retencion`
  ADD PRIMARY KEY (`id_regretencion`),
  ADD UNIQUE KEY `cod_regretencion` (`cod_regretencion`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `regimen_retencion`
--
ALTER TABLE `regimen_retencion`
  MODIFY `id_regretencion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Cambios la tabla `proveedores`
--
ALTER TABLE `proveedores` ADD `id_provincia` INT NOT NULL AFTER `idestados_pais`, ADD `id_distrito` INT NOT NULL AFTER `id_provincia`;