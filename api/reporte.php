<?php
session_start();
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try {
    $response_json    = array('success' => false, 'num_rows' => -1, 'rows' => array(), "messages" => "Estas intentando algo inusual en el sistema");
    require_once("./class/GLibfunciones.php");
    $OConex = new GConector();
    $init_stmt = $OConex->stmt_init();
    $OReporte = new GReporte();
    $data = json_decode(file_get_contents('php://input'));
    switch ($_GET['oper']) {
        case 'consultarVentas': 
          
            $sql = $OReporte->consultarVentas();
            $likenum = '%'.$data->num_doc.'%';
            $likecli = '%'.$data->noclien.'%';
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if (!$init_stmt->bind_param('sssiissss', $data->fec_inicio, $data->fec_inicio,
                                              $data->fec_hasta, 
                                              $data->tipdocumento->id, $data->tipdocumento->id,
                                              $data->num_doc, $likenum,
                                              $data->noclien, $likecli))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
             
            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $response_json['success'] = true;
            $response_json['totalItemCount'] = $result->num_rows;
            $i = 0;
            while ($rows = $result->fetch_assoc()) {
                array_push($response_json['rows'], array_merge($rows, array("item" => ++$i)));
            }
            break;
            case 'consultarCompras': 
          
                $sql = $OReporte->consultarCompras();
                if (!$init_stmt->prepare($sql))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
                if (!$init_stmt->bind_param('sssiissss', $data->fec_inicio, $data->fec_inicio,
                                                  $data->fec_hasta, 
                                                  $data->tipdocumento->id, $data->tipdocumento->id,
                                                  $data->num_doc, $data->num_doc,
                                                  $data->noclien, $data->noclien))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
                 
                $init_stmt->execute();
                $result = $init_stmt->get_result();
                $response_json['success'] = TRUE;
                $response_json['num_rows'] = $result->num_rows;
                if ($response_json['num_rows'] != 1)
                    break;
                $response_json['rows'] = $result->fetch_object();
                break;
    }
    echo json_encode($response_json);
} catch (Exception $e) {
    echo $e;
}
