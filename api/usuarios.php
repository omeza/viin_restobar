<?php
session_start();
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
$a_events=array('listar', 'consultar', 'existUser', 'guardar');
if(isset($_GET['oper']) && in_array($_GET['oper'], $a_events)){
    try{
        require_once("./class/GLibfunciones.php");
        $oConector= new GConector();
        $oUsuario= new GUsuarios();
        $init_stmt=$oConector->stmt_init();
        $response_json =array('error'=>"","success"=>false, "data"=>array(), "num_rows"=>-1, "affected_rows"=>-1, "messages"=>"");
        $data = json_decode(file_get_contents('php://input'));
        switch($_GET['oper']){
            case 'consultar':
                if(!isset($data->id) || empty($data->id))
                    break;
                $sql= $oUsuario->getUsuario();
                if(!$init_stmt->prepare($sql))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $oConector->error, $oConector->errno);
                if(!$init_stmt->bind_param('i',$data->id))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $oConector->error, $oConector->errno);
                $init_stmt->execute();
                $result_stmt=$init_stmt->get_result();
                $response_json['num_rows']=$result_stmt->num_rows;
                $response_json['success']=true;
                if($result_stmt->num_rows!=1)
                    break;
                $rows=$result_stmt->fetch_assoc();
                $response_json['rows']=array_combine(array("idempleado", "idtipdoc", "num_doc", "apenom", "idusuario", "usuario", "idrol_sistema", "password"), array_values($rows));
                $response_json['rows']['rep_passw']=$response_json['rows']['password'];
                $sql_state="SELECT ws.no_win_sistemas, ws.no_parent FROM detalle_rol AS dr INNER JOIN win_sistemas AS ws ON ws.id_win_sistemas=dr.id_win_sistemas WHERE dr.id_rol_sistema=?";
                if(!$init_stmt->prepare($sql_state))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
                if(!$init_stmt->bind_param('i', $response_json['rows']['idrol_sistema']))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
                $init_stmt->execute();
                $result_permiso=$init_stmt->get_result();
                $response_json['rows']['permission']=array();
                if($result_permiso->num_rows==0)
                    break;
                while($rows_permisos=$result_permiso->fetch_assoc()){
                    array_push($response_json['rows']['permission'], $rows_permisos);
                }
            break;
            case 'changestatus':
                $changeTo=($data->status==1)?'0':'1';
                $sql=$oRol->getRol();
				if(!$init_stmt->prepare($sql))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $objConex->error, $objConex->errno);
                if(!$init_stmt->bind_param("i", $data->id))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $objConex->error, $objConex->errno);
                $init_stmt->execute();
                $store_result=$init_stmt->get_result();
                if($store_result->num_rows!=1){
                    $response_json['messages']=($store_result->num_rows==0)?"Vaya! no existe registro de rol en el sistema":"Vaya! al parecer hay varios rol con el ID especificado";
                    break;
                }
                $row_rol=$store_result->fetch_object();
                $response_json['success']=true;
                if($changeTo!=$row_rol->status){
                    $sql=$oRol->changeStatus();
                    if(!$init_stmt->prepare($sql))
                        throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $objConex->error, $objConex->errno);
                    if(!$init_stmt->bind_param("si", $changeTo, $data->id))
                        throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $objConex->error, $objConex->errno);
                    $init_stmt->execute();
                    $response_json['affected_rows']=$init_stmt->affected_rows;
                    if($response_json['affected_rows']!=1){
                        $response_json['messages']="No se realizaron cambios en el estado del rol";
                        break;
                    }
                }else
                    $response_json['affected_rows']=1;
                $response_json['messages']="Se cambio satisfactoriamente el estado del rol";
            break;
            case 'existUser':
                if(!isset($data->id, $data->usuario) || empty($data->usuario))
                    break;
                $sql=$oUsuario->existUsuario();
				if(!$init_stmt->prepare($sql))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $objConex->error, $objConex->errno);
                if(!$init_stmt->bind_param("si", $data->usuario, $data->id))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $objConex->error, $objConex->errno);
                $init_stmt->execute();
                $result=$init_stmt->get_result();
                $response_json['num_rows']=$result->num_rows;
                $response_json['success']=TRUE;
            break;
            case 'guardar':
               

                if(!isset($data->idempleado, $data->idrol_sistema, $data->idusuario, $data->usuario, $data->password, $data->rep_passw) || (empty($data->idempleado) || empty($data->idrol_sistema) || empty($data->usuario) || empty($data->password) || empty($data->rep_passw) || $data->rep_passw!=$data->password))
                    break;
                $oConector->setAutocommit(FALSE);
                $init_commit=$oConector->stmt_init();        
                $store_params=array(0=>"iiiis");
                $store_params[]=&$data->idempleado;
                $store_params[]=&$data->idrol_sistema->id_rol_sistema;
                $store_params[]=&$data->idSucursal->id;
                $store_params[]=&$data->idCaja->id_caja;
                $store_params[]=&$data->usuario;
                if(empty($data->idusuario)){
                    $password=md5(sha1($data->password));
                    $store_params[0].="s";
                    $store_params[]=&$password;
                    $sql= $oUsuario->agregarUsuario();
                }else{
                    if($data->password!="*******"){
                        $password=md5(sha1($data->password));
                        $store_params[0].="s";
                        $store_params[]=&$password;
                        $oUsuario->updatePassword(true);
                    }
                    $sql= $oUsuario->guardarUsuario();
                }
                $store_params[0].="i";
                $store_params[]=&$data->idusuario;

                if(!$init_commit->prepare($sql))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $objConex->error, $objConex->errno);
               
                if(!call_user_func_array(array($init_commit, 'bind_param'), $store_params))
                    throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
                
                $init_commit->execute();
                $response_json['error_mysql']=$init_commit->error;
                $response_json['affected_rows']=$init_commit->affected_rows;
                $response_json['success']=true;
				if($response_json['affected_rows']!=1)
                    $response_json['messages']="No se realizaron cambios en los registro de la tabla";
                else
                    $response_json['messages']=($data->idusuario==0)?"Se asignaron los permisos al usuario": "Se actualizaron los datos de acceso al Usuario";
                $oConector->commit();
            break;
            case 'listar':
                $response_json=array("totalItemCount"=>0, "numberOfPages"=>0, "rows"=>array(), "success"=>false);
                $store_params=array(0=>'');
                if(isset($data->predicateObject)){
                    foreach($data->predicateObject as $fields => $value){
                        $store_params[0].='s';
                        ${$fields}=sprintf("%%%s%%",$value);
                        $store_params[]=&${$fields};
                        $OProveedor->addFilter($fields);
                    }
                }
                $sql= $oUsuario->listar();
				if(!$init_stmt->prepare($sql))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $oConector->error, $oConector->errno);
                if(count($store_params)>1){
                    if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
                        throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $oConector->error, $oConector->errno);
                }
                $init_stmt->execute();
                $result=$init_stmt->get_result();
                $response_json['success']=true;
                $response_json['totalItemCount']=$result->num_rows;
                if($response_json['totalItemCount']==0)
                    break;
                if(isset($data->start, $data->number)){
                    $response_json['numberOfPages']=ceil($result->num_rows/$data->number);
                    $Opagination=new GPagination();
                    $Opagination->setInit($data->start);
                    $Opagination->setLimit($data->number);
                    $sql=$Opagination->prepareSQL($sql);
                }
                if(!$init_stmt->prepare($sql))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
                if(count($store_params)>1){
                    if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
                        throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
                }
                $init_stmt->execute();
                $result=$init_stmt->get_result();
                $i=(isset($data->start))?$data->start:0;
                while($rows=$result->fetch_assoc()){
                    array_push($response_json['rows'], array_merge($rows, array("item"=>++$i)));
                }
            break;
        }
    echo json_encode($response_json);
    }catch(Exception $e){
        echo $e->getOutMsg();
    }
}
?>