<?php
session_start();
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
	$response_json	=array('success'=>false, 'affected_rows'=>-1, 'num_rows'=>-1, 'rows'=>array(), "messages"=>"Estas intentando algo inusual en el sistema");
	require_once("./class/GLibfunciones.php");
	$token=GAuth::getAuthorization();
	GAuth::Check($token);
	$data_token=GAuth::GetData($token);
	$response_json['token']=GAuth::SignIn($data_token);
	$OConex = new GConector();
	$init_stmt=$OConex->stmt_init();
	$OMoneda = new GMoneda();
	$data = json_decode(file_get_contents('php://input'));
	switch($_GET['oper']){
		case 'aditional':
			if(!isset($data->id_moneda, $data->is_aditional) || empty($data->id_moneda))
				break;
			$sql_moneda=$OMoneda->getMoneda();
			if(!$init_stmt->prepare($sql_moneda))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id_moneda))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result_stmt=$init_stmt->get_result();
			if($result_stmt->num_rows!=1){
				$response_json['messages']="La moneda que desea asignar adicional no existe";
				break;
			}
			$row_moneda=$result_stmt->fetch_object();
			if($row_moneda->aditional==$data->is_aditional){
				$name_method=($data->is_aditional==0)?"addAditional":"removeAditional";
				$stmt_commit=$OConex->stmt_init();
				$sql_commit=call_user_func(array($OMoneda, $name_method));
				if(!$stmt_commit->prepare($sql_commit))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $stmt_commit->error, $stmt_commit->errno);
				if(!$stmt_commit->bind_param('i', $data->id_moneda))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $stmt_commit->error, $stmt_commit->errno);
				$stmt_commit->execute();
				$response_json['success']=true;
				$response_json['affected_rows']=$stmt_commit->affected_rows;
				$response_json['rows']['is_adicional']=($data->is_aditional==1)?0:1;
				$response_json['messages']=($response_json['rows']['is_adicional']==1)?"La moneda fue asignada adicional":"La moneda fue removida como adicional";
			}else{
				$response_json['rows']['is_adicional']=($data->is_aditional==1)?0:1;
				$response_json['messages']="La moneda fue cambiada adicional en otro proceso";
			}
		break;
		case 'validation':
			if(!isset($data->value, $data->validacion, $data->id) || empty($data->value) || empty($data->validacion))
				break;
			switch($data->validacion){
				case 'codigo_internacional':
					$name_method="existeCodInternacional";
				break;
			}
			if(empty($name_method))
				break;
			$sql=call_user_func(array($OMoneda, $name_method));
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('si', $data->value, $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result_stmt=$init_stmt->get_result();
			$response_json['success']=true;
			$response_json['num_rows']=$result_stmt->num_rows;
		break;
		case 'existCode':
			if(!isset($data->code, $data->id) || empty($data->code))
				break;
			$sql=$OMoneda->existeCodigo();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('si', $data->code, $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result_stmt=$init_stmt->get_result();
			$response_json['success']=TRUE;
			$response_json['num_rows']=$result_stmt->num_rows;
		break;
		case 'eliminar':
			if(!isset($data->id) || empty($data->id))
				break;
			$sql=$OMoneda->borrarMoneda();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$response_json['success']=TRUE;
			$response_json['affected_rows']=$init_stmt->affected_rows;
			$response_json['messages']=($init_stmt->affected_rows==1)?"Se elimino con éxito el registro":"";
			if($init_stmt->affected_rows!=1 && $init_stmt->errno!=0)
				$response_json['messages']=errorMySQL($init_stmt->errno);
		break;
		case 'status':
			if(!isset($data->id, $data->status) || empty($data->id))
				break;
			$sql=$OMoneda->getMoneda();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['num_rows']=$result->num_rows;
			if($result->num_rows==0){
				$response_json['messages']="Registro no se encuentra en el sistema";
				break;
			}
			$row=$result->fetch_object();
			if($row->status!=$data->status){
				$response_json['success']=TRUE;
				$response_json['messages']="Otro usuario realizo el cambio de status";
				break;
			}
			$status=($data->status=='0')?'1':'0';
			$sql=$OMoneda->updateStatusMoneda();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('si', $status, $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$response_json['affected_rows']=$init_stmt->affected_rows;
			$response_json['success']=TRUE;
			$response_json['messages']=($init_stmt->affected_rows==0)?"No se realizo cambio en el status": "Se realizo satisfactoriamente el cambio de status";
			if($init_stmt->affected_rows==1)
				$response_json['rows']['status']=$status;
		break;
		case 'moneda_predeterminada':
			if(!isset($data->id_moneda) || empty($data->id_moneda))
				break;
			$sql_moneda=$OMoneda->getMoneda();
			if(!$init_stmt->prepare($sql_moneda))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id_moneda))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result_stmt=$init_stmt->get_result();
			$response_json['success']=TRUE;
			if($result_stmt->num_rows==1){
				$row_moneda=$result_stmt->fetch_object();
				if($row_moneda->predeterminado==1){
					$response_json['messages']="Esta moneda ya esta asignada predeterminada";
					break;
				}
				$sql_default=$OMoneda->assignDefault();
				if(!$init_stmt->prepare($sql_default))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
				if(!$init_stmt->bind_param('i', $data->id_moneda))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
				$init_stmt->execute();
				$response_json['affected_rows']=$init_stmt->affected_rows;
				$response_json['messages']=($init_stmt->affected_rows==1)?"Se asigno nueva moneda predeterminada":"Ocurrio un problema al asignar moneda predeterminada";
				if($row_moneda->aditional==0){
					$sql_aditional=$OMoneda->addAditional();
					if(!$init_stmt->prepare($sql_default))
						throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
					if(!$init_stmt->bind_param('i', $data->id_moneda))
						throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
					$init_stmt->execute();
				}				
			}else
				$response_json['messages']="La moneda que desea usar como predeterminada no existe";
		break;
		case 'listar':
			$store_params=array(0=>'');
			if(isset($data->predicateObject)){
				foreach($data->predicateObject as $fields => $value){
					$store_params[0].='s';
					${$fields}=sprintf("%%%s%%",$value);
					$store_params[]=&${$fields};
					$OMoneda->addFilter($fields);
				}
			}
			$sql=$OMoneda->listarMonedas();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result_rows=$init_stmt->get_result();
			$response_json['totalItemCount']=$result_rows->num_rows;
			$response_json['success']=true;
			if($result_rows->num_rows==0)
				break;

			if(isset($data->start, $data->number)){
				$response_json['numberOfPages']=ceil($result_rows->num_rows/$data->number);
				$Opagination=new GPagination();
				$Opagination->setInit($data->start);
				$Opagination->setLimit($data->number);
				$sql=$Opagination->prepareSQL($sql);
			}
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$i=(isset($data->start))?$data->start:0;
			$number=(isset($data->number))?$data->number:10;
			while($row=$result->fetch_assoc()){
				//$row['simb_moneda']=$string = iconv('Windows-1252', 'UTF-8', $row['simb_moneda']);
				$row['nom_moneda']=utf8_encode($row['nom_moneda']);
				array_push($response_json['rows'], array_merge($row, array('item'=>++$i)));
			}
			$response_json['totalItemCount']=count($response_json['rows']);
		break;
		case 'consultar':
			if(!isset($data->id_moneda) || empty($data->id_moneda))
				break;
			$sql=$OMoneda->getMoneda();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id_moneda))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result_row=$init_stmt->get_result();
			$response_json['num_rows']=$result_row->num_rows;
			$response_json['success']=true;
			if($result_row->num_rows!=1){
				$response['messages']="No se encontraron registros que coincida en el sistema";
				break;
			}
			$response_json['rows']=$result_row->fetch_assoc();
			checked_json($response_json['rows']['simb_moneda']);
			checked_json($response_json['rows']['nom_moneda']);
			$response_json['rows']['predeterminado']=(bool)$response_json['rows']['predeterminado'];
			$response_json['rows']['aditional']=(bool)$response_json['rows']['aditional'];
			$response_json['rows']['status']=($response_json['rows']['status']=='1')?TRUE:FALSE;
		break;
		case 'guardar':
			if(!isset($data->cod_moneda, $data->cod_intern, $data->simb_moneda, $data->nombre, $data->predeterminado, $data->aditional, $data->id_moneda) || (empty($data->cod_moneda) || empty($data->cod_intern) || empty($data->nombre) ) )
				break;
			$OConex->setAutocommit(FALSE);
			$stmt_commit=$OConex->stmt_init();
			$name_method=(empty($data->id_moneda))?'agregarMoneda':'actualizarMoneda';
			$sql_commit=call_user_func(array($OMoneda, $name_method));
			$data->nombre=strtoupper($data->nombre);
			$data->simb_moneda=strtoupper($data->simb_moneda);
			if(!$stmt_commit->prepare($sql_commit))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $stmt_commit->error, $stmt_commit->errno);
			if(!$stmt_commit->bind_param('ssssi', $data->cod_moneda, $data->cod_intern, $data->simb_moneda, $data->nombre, $data->id_moneda))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $stmt_commit->error, $stmt_commit->errno);
			$stmt_commit->execute();
			$response_json['success']=true;
			$response_json['affected_rows']=$stmt_commit->affected_rows;
			if($stmt_commit->affected_rows!=1){
				$response_json['messages']="No se realizaron cambios en el registro de Moneda";
				break;
			}
			$id_moneda=(empty($data->id_moneda))?$init_stmt->insert_id:$data->id_moneda;
			if(!empty($data->id_moneda)){
				$sql_moneda=$OMoneda->getMoneda();
				if(!$init_stmt->prepare($sql_moneda))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
				if(!$init_stmt->bind_param('i', $data->id_moneda))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
				$init_stmt->execute();
				$result_stmt=$init_stmt->get_result();
				if($result_stmt->num_rows==1)
					$row_moneda=$result_stmt->fetch_assoc();
			}
			if($data->aditional){
				$sql=$OMoneda->addAditional();
				if(!$stmt_commit->prepare($sql))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
				if(empty($data->id_moneda) || (isset($row_moneda) && $row_moneda['aditional']==0)){
					if(!$stmt_commit->bind_param('i', $id_moneda))
						throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
					$stmt_commit->execute();
					if($stmt_commit->affected_rows!=1){
						$response_json['messages']="Ocurrio un problema al registrar la moneda como adicional";
						break;
					}
				}
			}
			if($data->predeterminado){
				$sql_default=$OMoneda->assignDefault();
				if(!$stmt_commit->prepare($sql_default))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
				if(!$stmt_commit->bind_param('i', $id_moneda))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
				$stmt_commit->execute();
				if($stmt_commit->affected_rows!=1){
					$response_json['messages']="Ocurrio un problema al registrar la moneda como adicional";
					break;
				}
			}
			$OConex->commit();
			$response_json['messages']="Los datos de la moneda se registraron con éxito";
		break;
		case 'existMoneda':
			$sql=$OMoneda->existeNombre();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('si', $data->nombre, $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result_row=$init_stmt->get_result();
			$response_json['num_rows']=$result_row->num_rows;
			$response_json['success']=true;
			if($result_row->num_rows==1){
				$response_json['rows']=$result_row->fetch_object();
			}
		break;
		case 'existSimbolo':
			$sql=$OMoneda->existeSimbol();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('si', $data->simbolo, $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result_rows=$init_stmt->get_result();
			$response_json['success']=true;
			$response_json['num_rows']=$result_rows->num_rows;
		break;
	}
	echo json_encode($response_json);
}catch(Exception $e){
	echo $e->getOutMsg();
}
?>