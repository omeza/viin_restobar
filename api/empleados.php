<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
	$response_json	=array('success'=>false, 'num_rows'=>-1, 'rows'=>array(), "messages"=>"Estas intentando algo inusual en el sistema");
	require_once("./class/GLibfunciones.php");
	$OConex = new GConector();
	$OEmpleado = new GEmpleados();
	$init_stmt=$OConex->stmt_init();
	$data = json_decode(file_get_contents('php://input'));
	switch($_GET['oper']){
		case 'visibility':
			if(!isset($data->id, $data->visible) || empty($data->id))
				break;
			$sql=$OEmpleado->getEmpleado();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['num_rows']=$result->num_rows;
			if($result->num_rows==0){
				$response_json['messages']="Registro no se encuentra en el sistema";
				break;
			}
			$row=$result->fetch_object();
			if($row->visible!=$data->visible){
				$response_json['success']=TRUE;
				$response_json['messages']="Otro usuario realizo el cambio de status";
				break;
			}
			$visible=($data->visible=='0')?'1':'0';
			$sql=$OEmpleado->changeVisibiity();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('si', $visible, $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$response_json['affected_rows']=$init_stmt->affected_rows;
			$response_json['success']=TRUE;
			$response_json['messages']=($init_stmt->affected_rows==0)?"No se realizo cambio en la acción": "Se realizo satisfactoriamente el cambio";
			if($init_stmt->affected_rows==1)
				$response_json['rows']['visible']=$visible;
		break;
		case 'status':
			if(!isset($data->id, $data->status) || empty($data->id))
				break;
			$sql=$OEmpleado->getEmpleado();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['num_rows']=$result->num_rows;
			if($result->num_rows==0){
				$response_json['messages']="Registro no se encuentra en el sistema";
				break;
			}
			$row=$result->fetch_object();
			if($row->status!=$data->status){
				$response_json['success']=TRUE;
				$response_json['messages']="Otro usuario realizo el cambio de status";
				break;
			}
			$status=($data->status=='0')?'1':'0';
			$sql=$OEmpleado->changeStatus();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('si', $status, $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$response_json['affected_rows']=$init_stmt->affected_rows;
			$response_json['success']=TRUE;
			$response_json['messages']=($init_stmt->affected_rows==0)?"No se realizo cambio en el status": "Se realizo satisfactoriamente el cambio de status";
			if($init_stmt->affected_rows==1)
				$response_json['rows']['status']=$status;
		break;
		case 'listar':
			$store_params=array(0=>'');
			if(isset($data->predicateObject)){
				foreach($data->predicateObject as $fields => $value){
					$store_params[0].='s';
					${$fields}=sprintf("%%%s%%",$value);
					$store_params[]=&${$fields};
					$OProducto->addFilter($fields);
				}
			}
			$sql=$OEmpleado->listarEmpleados();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=true;
			$response_json['totalItemCount']=$result->num_rows;
			if($response_json['totalItemCount']==0)
				break;
			if(isset($data->start, $data->number)){
				$response_json['numberOfPages']=ceil($result->num_rows/$data->number);
				$Opagination=new GPagination();
				$Opagination->setInit($data->start);
				$Opagination->setLimit($data->number);
				$sql=$Opagination->prepareSQL($sql);
			}
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$i=$data->start;
			while($rows=$result->fetch_assoc()){
				$rows['fec_ingreso']=DateTime::createFromFormat('Y-m-d H:i:s', $rows['fec_ingreso'])->format('d-m-Y');
				array_push($response_json['rows'], array_merge($rows, array("id"=>++$i)));
			}
		break;
		case 'validdocumento':
			if(!isset($data->tipdoc, $data->num_doc,  $data->idempleado) || (empty($data->tipdoc) || empty($data->num_doc)) )
				break;
			$sql=$OEmpleado->existeDocEmpleado();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('isi', $data->tipdoc, $data->num_doc, $data->idempleado))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=TRUE;
			$response_json['num_rows']=$result->num_rows;
			if($response_json['num_rows']!=1)
				break;
			$response_json['rows']=$result->fetch_object();
		break;
		case 'consultardocument':
			if(!isset($data->idtipdoc, $data->numdoc) || empty($data->idtipdoc) || empty($data->numdoc) )			
				break;
			$sql=$OEmpleado->consultDocumentoCliente();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('is', $data->idtipdoc, $data->numdoc))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=TRUE;
			$response_json['num_rows']=$result->num_rows;
			if($response_json['num_rows']!=1)
				break;
			$response_json['rows']=$result->fetch_object();				
		break;
		case 'consultar':
			if(!isset($data->id) || empty($data->id))
				break;
			$sql=$OEmpleado->getEmpleado();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=TRUE;
			$response_json['num_rows']=$result->num_rows;
			if($response_json['num_rows']!=1)
				break;
			$response_json['rows']=array_combine( array("idempleado", "tipdocumento", "desc_documento", "num_doc", "ape_paterno", "ape_materno", "nombres", "sexo", "fec_nacimiento", "edocivil", "direccion", "tlf_movil", "tlf_hab", "email", "id_profesion", "id_cargo", "status", "visible"), array_values($result->fetch_assoc()));
			$response_json['rows']['fec_nacimiento']=DateTime::createFromFormat('Y-m-d', $response_json['rows']['fec_nacimiento'])->format('d-m-Y');
			$response_json['messages']="Se encontro el registro con exito";
		break;
		case 'info':
			if(!isset($data->tipdoc, $data->numdoc) || (empty($data->tipdoc) || empty($data->numdoc)))
				break;
			$sql=$OEmpleado->infoEmpleado();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('is', $data->tipdoc, $data->numdoc))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['num_rows']=$result->num_rows;
			$response_json['success']=true;
			if($result->num_rows!=1){
				break;				
			}
			$response_json['rows']=array_combine(array("idempleado", "idtipdoc", "num_doc", "apenom", "idrol", "rol", "login", "password"), array_values($result->fetch_assoc()));
			$response_json['rows']['is_vendedor']=($response_json['rows']['idrol']==1 || $response_json['rows']['idrol']==3);
		break;
		case 'existe':
			$sql=$OEmpleado->existeDocCliente();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('si', $data->descripcion, $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result_cliente=$init_stmt->get_result();
			$response_json['num_rows']=$result_cliente->num_rows;
			$response_json['success']=true;
			if($result_cliente->num_rows==1){
				$response_json['rows']=$result_cliente->fetch_object();
			}
		break;
		case 'validemail':
			if(!isset($data->email, $data->idempleado) || empty($data->email))
				break;
			$sql=$OEmpleado->validEmail();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);																																									
			if(!$init_stmt->bind_param('si', $data->email, $data->idempleado))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['num_rows']=$result->num_rows;
			$response_json['success']=true;
		break;
		case 'guardar':
			if(!isset($data->idempleado, $data->tipdocumento, $data->num_doc, $data->ape_paterno, $data->ape_materno, $data->nombres, $data->sexo, $data->edocivil, $data->fec_nacimiento, $data->direccion, $data->tlf_hab, $data->tlf_movil, $data->email))
				break;
			if(empty($data->idempleado))
				$sql=$OEmpleado->agregarEmpleado();
			else
				$sql=$OEmpleado->actualizarEmpleado();
			$data->ape_paterno=strtoupper($data->ape_paterno);
			$data->ape_materno=strtoupper($data->ape_materno);
			$data->nombres=strtoupper($data->nombres);
			$data->email=strtolower($data->email);
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);																																									
			if(!$init_stmt->bind_param('isssssssssssi', $data->tipdocumento->id, $data->num_doc, $data->ape_paterno, $data->ape_materno, $data->nombres, $data->sexo->value, $data->fec_nacimiento, $data->edocivil->value, $data->direccion, $data->tlf_movil, $data->tlf_hab, $data->email, $data->idempleado))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$response_json['success']=true;
			$response_json['affected_rows']=$init_stmt->affected_rows;
			$response_json['error_mysql']=$init_stmt->error;
			$response_json['sql']=$sql;
			if($init_stmt->affected_rows!=1){
				$response_json['messages']="No se realizaron cambios en el registro";
				break;
			}
			$response_json['rows']['id']=($init_stmt->affected_rows==1 && $data->idempleado==0)?$init_stmt->insert_id:NULL;
			$response_json['messages']="El registro fue guardaron satisfactoriamente";
		break;
		case 'existdocumento':
			if(!isset($data->idtipdoc, $data->numero, $data->idempleado) || (empty($data->numero) || empty($data->idtipdoc)))
				break;
			$sql=$OEmpleado->existeDocEmpleado();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('isi', $data->idtipdoc, $data->numero, $data->idempleado))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=TRUE;
			$response_json['num_rows']=$result->num_rows;
			if($response_json['num_rows']!=1)
				break;
			$response_json['rows']=$result->fetch_object();
		break;
	}
	echo json_encode($response_json);
}catch(Exception $e){
	echo $e->getOutMsg();
}
?>