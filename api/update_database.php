<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
require_once './class/GLibfunciones.php';
try{
    $OConector=new GConector();
    $show_tables=sprintf("SHOW TABLES FROM %s", OELFCADB);
    $json_database=array();
    if($result_show=$OConector->query($show_tables)){
        if($result_show->num_rows>0){
            $i=0;
            while($row_table=$result_show->fetch_array(MYSQLI_NUM)){
                $show_columns=sprintf("SHOW COLUMNS FROM %s", $row_table[0]);
                if(!$result_columns=$OConector->query($show_columns))
                    continue;
                if($result_columns->num_rows==0)
                    continue;
                array_push($json_database, array("name_table"=>$row_table[0], "fields"=>array()) );
                $a_field=array();
                while($row_columns=$result_columns->fetch_array(MYSQLI_ASSOC)){
                    array_push($a_field, array('name'=>$row_columns['Field'], 'type'=>$row_columns['Type'], 'accept_null'=>$row_columns['Null'], 'key'=>$row_columns['Key'], 'value_default'=>$row_columns['Default'], 'autoincrement'=>$row_columns['Extra'], 'comment'=>(isset($row_columns['Comment']))?$row_columns['Comment']:NULL));
                }
                $json_database[$i]['fields']=$a_field;
                $i++;
            }
            var_dump($json_database);
        }
    }
}catch(Exception $e){
    echo "Hubo una excepcion";
}

?>