<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
	$response_json	=array('success'=>false, 'num_rows'=>-1, 'rows'=>array(), "msg"=>"Estas intentando algo inusual en el sistema");
	require_once("./class/GLibfunciones.php");
	$OConex = new GConector();
	$init_stmt=$OConex->stmt_init();
	$OUmedida = new GUnidadmedida();
	$data = json_decode(file_get_contents('php://input'));
	switch($_GET['oper']){
		case 'consultar':
			if(!isset($data->idumedida) || empty($data->idumedida))
				break;
			$sql=$OUmedida->consultarUMedida();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->idumedida))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result_cliente=$init_stmt->get_result();
			$response_json['num_rows']=$result_cliente->num_rows;
			$response_json['success']=true;
			if($result_cliente->num_rows!=1){
				break;				
			}
			$response_json['rows']=array_combine(array('id_gumedida', 'abrev_umedida', 'desc_umedida', 'status'), array_values($result_cliente->fetch_assoc()));
		break;
		case 'listar':
			$store_params=array(0=>'');
			if(isset($data->predicateObject)){
				foreach($data->predicateObject as $fields => $value){
					$store_params[0].='s';
					${$fields}=sprintf("%%%s%%",$value);
					$store_params[]=&${$fields};
					$OProveedor->addFilter($fields);
				}
			}
			$sql=$OUmedida->listarUMedida();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=true;
			$response_json['totalItemCount']=$result->num_rows;
			if($response_json['totalItemCount']==0)
				break;
			if(isset($data->start, $data->number)){
				$response_json['numberOfPages']=ceil($result->num_rows/$data->number);
				$Opagination=new GPagination();
				$Opagination->setInit($data->start);
				$Opagination->setLimit($data->number);
				$sql=$Opagination->prepareSQL($sql);
			}
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$i=(isset($data->start))?$data->start:0;
			while($rows=$result->fetch_assoc()){
				array_push($response_json['rows'], array_merge($rows, array("item"=>++$i)));
			}
		break;
		case 'guardar':
			if($data->id_gumedida==0)
				$sql=$OUmedida->agregarUMedida();
			else
				$sql=$OUmedida->actualizarUMedida();
			$data->desc_umedida=strtoupper($data->desc_umedida);
			$OConex->setAutocommit(FALSE);
			$init_commit=$OConex->stmt_init();
			if(!$init_commit->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_commit->bind_param('ssi', $data->abrev_umedida, $data->desc_umedida, $data->id_gumedida))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_commit->execute();
			$response_json['success']=true;
			$response_json['affected_rows']=$init_commit->affected_rows;
			if($init_commit->affected_rows!=1){
				$response_json['messages']="No se realizaron cambios en el registro del Grupo";
				break;
			}
			if($data->id_gumedida==0)
				$response_json['data']['id']=$init_commit->insert_id;
			$response_json['messages']="Los datos de Unidad de Medida se guardaron con éxito";
			$OConex->commit();
		break;
		case 'existnombre':
			$sql=$OGrupo->existeNombre();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('si', $data->nombre, $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['num_rows']=$result->num_rows;
			$response_json['success']=true;
			if($result->num_rows==1)
				$response_json['rows']=$result->fetch_object();
		break;
		case 'existCode':
			$sql=$OGrupo->existeCodigo();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('si', $data->codigo, $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['num_rows']=$result->num_rows;
			$response_json['success']=true;
			if($result->num_rows==1)
				$response_json['rows']=$result->fetch_object();
		break;
	}
	echo json_encode($response_json);
}catch(Exception $e){
	echo $e->getOutMsg();
}
?>