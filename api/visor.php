<?php
session_start();
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try {
    $response_json    = array('success' => false, 'num_rows' => -1, 'rows' => array(), "messages" => "Estas intentando algo inusual en el sistema");
    require_once("./class/GLibfunciones.php");
    $OConex = new GConector();
    $init_stmt = $OConex->stmt_init();
    $OVisor = new GVisor();
    $data = json_decode(file_get_contents('php://input'));
    switch ($_GET['oper']) {
        case 'listarAtendidas':
            $sql = $OVisor->listarAtendidas();
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
        
            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $response_json['success'] = true;
            $response_json['totalItemCount'] = $result->num_rows;
            if ($response_json['totalItemCount'] == 0)
                break;
            if (isset($data->start, $data->number)) {
                $response_json['numberOfPages'] = ceil($result->num_rows / $data->number);
                $Opagination = new GPagination();
                $Opagination->setInit($data->start);
                $Opagination->setLimit($data->number);
                $sql = $Opagination->prepareSQL($sql);
            }
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
           
            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $i = (isset($data->start)) ? $data->start : 0;
            while ($rows = $result->fetch_assoc()) {
                array_push($response_json['rows'], array_merge($rows, array("item" => ++$i)));
            }
            break;
            case 'listarAtendidasBar':
            $sql = $OVisor->listarAtendidasBar();
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
        
            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $response_json['success'] = true;
            $response_json['totalItemCount'] = $result->num_rows;
            if ($response_json['totalItemCount'] == 0)
                break;
            if (isset($data->start, $data->number)) {
                $response_json['numberOfPages'] = ceil($result->num_rows / $data->number);
                $Opagination = new GPagination();
                $Opagination->setInit($data->start);
                $Opagination->setLimit($data->number);
                $sql = $Opagination->prepareSQL($sql);
            }
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
           
            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $i = (isset($data->start)) ? $data->start : 0;
            while ($rows = $result->fetch_assoc()) {
                array_push($response_json['rows'], array_merge($rows, array("item" => ++$i)));
            }
            break;
        case 'consultarDetalle':
            if (!isset($data->id_pedido_cabecera) || empty($data->id_pedido_cabecera))
                break;

            $sql = $OVisor->listarPedidos();
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
            if (!$init_stmt->bind_param('i', $data->id_pedido_cabecera))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);

            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $response_json['success'] = true;
            $response_json['no_mesa'] = $data->no_mesa;
            $response_json['totalItemCount'] = $result->num_rows;
            if ($response_json['totalItemCount'] == 0)
                break;
            if (isset($data->start, $data->number)) {
                $response_json['numberOfPages'] = ceil($result->num_rows / $data->number);
                $Opagination = new GPagination();
                $Opagination->setInit($data->start);
                $Opagination->setLimit($data->number);
                $sql = $Opagination->prepareSQL($sql);
            }
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if (!$init_stmt->bind_param('i', $data->id_pedido_cabecera))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);

            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $i = (isset($data->start)) ? $data->start : 0;
            while ($rows = $result->fetch_assoc()) {
                $rows['nu_total'] = $rows['nu_cantidad'] * $rows['nu_precio'];
                array_push($response_json['rows'], array_merge($rows, array("id_pedido" => ++$i)));
            }
            break;
            case 'consultarDetalleBar':
            if (!isset($data->id_pedido_cabecera) || empty($data->id_pedido_cabecera))
                break;

            $sql = $OVisor->listarPedidosBar();
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
            if (!$init_stmt->bind_param('i', $data->id_pedido_cabecera))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);

            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $response_json['success'] = true;
            $response_json['no_mesa'] = $data->no_mesa;
            $response_json['totalItemCount'] = $result->num_rows;
            if ($response_json['totalItemCount'] == 0)
                break;
            if (isset($data->start, $data->number)) {
                $response_json['numberOfPages'] = ceil($result->num_rows / $data->number);
                $Opagination = new GPagination();
                $Opagination->setInit($data->start);
                $Opagination->setLimit($data->number);
                $sql = $Opagination->prepareSQL($sql);
            }
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if (!$init_stmt->bind_param('i', $data->id_pedido_cabecera))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);

            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $i = (isset($data->start)) ? $data->start : 0;
            while ($rows = $result->fetch_assoc()) {
                $rows['nu_total'] = $rows['nu_cantidad'] * $rows['nu_precio'];
                array_push($response_json['rows'], array_merge($rows, array("id_pedido" => ++$i)));
            }
            break;
            case 'actualizarAtendido':
                if(!isset($data->id_pedido_detalle))
                break;		
			    $OConex->setAutocommit(FALSE);
				$init_commit=$OConex->stmt_init();
				$method="actualizarAtendido";
				$sql=call_user_func(array($OVisor, $method));
				if(!$init_commit->prepare($sql))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
				if(!$init_commit->bind_param('i', $data->id_pedido_detalle))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
				$init_commit->execute();           
				$response_json['messages']="Se registro satisfactoriamente";
				$response_json['success']=TRUE;
				$response_json['affected_rows']=$init_commit->affected_rows;
				$OConex->commit();
        break;
        
    }
    echo json_encode($response_json);
} catch (Exception $e) {
    echo $e->getOutMsg();
}
