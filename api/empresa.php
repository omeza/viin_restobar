<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
$response_json	=array('success'=>false, 'num_rows'=>-1, 'data'=>array(), "messages"=>"Estas intentando algo inusual en el sistema");
require_once("./class/GLibfunciones.php");
$OConex = new GConector();
$init_stmt=$OConex->stmt_init();
$oper=(isset($_GET['oper']))?$_GET['oper']:'get';
switch($oper){
    case 'guardar':
        $data = json_decode(file_get_contents('php://input'));
        if(!isset($data->idtipdoc, $data->num_doc, $data->nombre_comercial, $data->nombre_empresa, $data->direccion, $data->email, $data->telefono) || (empty($data->idtipdoc) || empty($data->num_doc) || empty($data->nombre_comercial)))
            break;
        $data->email=(!empty($data->email))?strtolower($data->email):'';
        $sql="UPDATE empresa SET idtip_documento=?, num_fiscal=?, nombre_empresa=?, nombre_comercial=?, direccion=?, tlf_fiscal=?, email=?";
        if(!$init_stmt->prepare($sql))
            throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
        if(!$init_stmt->bind_param('issssss', $data->idtipdoc->id, $data->num_doc, $data->nombre_empresa, $data->nombre_comercial, $data->direccion, $data->telefono, $data->email))
            throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
        $init_stmt->execute();
        $response_json['affected_rows']=$init_stmt->affected_rows;
        $response_json['success']=true;
        $response_json['messages']=($init_stmt->affected_rows==1)?'Se actualizarón con éxito los datos':'No se actualizarón los datos';
        if($init_stmt->affected_rows==1)
            $response_json['data']=array('desc_documento'=>$data->idtipdoc->nombre, 'num_documento'=>$data->num_doc, 'razon_social'=>$data->nombre_empresa, 'sucursal'=>$data->nombre_comercial, 'direc_fiscal'=>$data->direccion, 'direc_sucursal'=>'', 'email'=>$data->email, 'telefono'=>$data->telefono);
    break;
    case 'get':
        $sql="SELECT IFNULL(td.desc_documento, '') AS desc_documento, e.num_fiscal, e.nombre_empresa, e.nombre_comercial, e.direccion, e.direccion_local, IFNULL(e.email, '') AS email, IFNULL(e.tlf_fiscal, '') AS tlf_fiscal FROM empresa AS e LEFT OUTER JOIN tip_documento AS td ON td.idtip_documento=e.idtip_documento LIMIT 0,1";
        if(!$init_stmt->prepare($sql))
            throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
        $init_stmt->execute();
        $result=$init_stmt->get_result();
        $response_json['success']=true;
        $response_json['num_rows']=$result->num_rows;
        if($result->num_rows==1)
            $response_json['data']=array_combine( array('desc_documento', 'num_documento', 'razon_social', 'sucursal', 'direc_fiscal', 'direc_sucursal', 'email', 'telefono'), array_values($result->fetch_assoc()));
}
echo json_encode($response_json);
?>