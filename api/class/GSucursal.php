<?php

class GSucursal{
	
	public function __construct(){

    }
    
    public function listar(){
        return "SELECT *  FROM tb_sucursal ";        
    }

    public function agregarSucursal(){
        return "INSERT INTO tb_sucursal(no_sucursal, no_direccion, id_ubigeo, no_observacion, status,id_sucursal) VALUES (?,?,?,?,?,?)";
    }
    public function actualizarSucursal(){
      return "UPDATE tb_sucursal SET no_sucursal = ?,no_direccion = ?, id_ubigeo = ?, no_observacion = ?, status = ? WHERE id_sucursal = ?;
      ";
    }

    public function getSucursal(){
		  return "SELECT *  FROM tb_sucursal  WHERE id_sucursal = ?";
    }
    
    public function listarSucursal(){
	  	return sprintf("SELECT id_sucursal as id ,  no_sucursal FROM tb_sucursal");
    }
    
    public function deleteSucursal(){
      return "DELETE FROM tb_sucursal WHERE id_sucursal=?";
  }

  public function changeStatus(){
      return "UPDATE tb_sucursal SET status=? WHERE id_sucursal=?";
  }

  public function consultarCorrelativo(){
    return "SELECT  nu_serie , nu_correlativo FROM tb_comprobante_sucursal WHERE id_sucursal = ? and id_tipo_comprobante = ?";
  }
}
?>