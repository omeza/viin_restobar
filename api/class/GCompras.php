<?php

class GCompras{
	
	public function __construct(){
	}
	
	
	
	public function agregar(){
		return "INSERT INTO tb_compra_cabecera(id_proveedor, id_tipo_comprobante, id_usuario, nu_serie, nu_correlativo, nu_sub_total, nu_igv, nu_total, fe_emision, fe_registro, nu_serie_c, nu_correlativo_c, id_sucursal,id_compra_cabecera) VALUES (?,?,?,?,?,?,?,?,now(),now(),?,?,?,?)";
    }

    public function agregarDetalle(){
		return "INSERT INTO tb_compra_detalle(id_compra_cabecera, id_insumo, nu_cantidad, nu_precio_unitario, nu_total) VALUES (?,?,?,?,?)";
	}

	public function listar(){
		return "SELECT id_compra_cabecera , no_proveedor, no_tipo_comprobante , CONCAT(no_nombres,' ',no_apepat,' ',no_apemat) as no_empleado, CONCAT(nu_serie_c,'-',nu_correlativo) as no_correlativo,cc.fe_registro, nu_total
		FROM tb_compra_cabecera as cc 
		INNER JOIN tb_proveedor as tp ON cc.id_proveedor = tp.id_proveedor
		INNER JOIN tc_tipo_comprobante as tc ON cc.id_tipo_comprobante = tc.id_tipo_comprobante
		INNER JOIN tb_usuario as tu ON cc.id_usuario = tu.id_usuario
		INNER JOIN tb_empleado as te ON tu.id_empleado =  te.id_empleado";
	}

	public function consultar(){
		return "SELECT * FROM tb_proveedor WHERE id_proveedor = ?";
	}
	public function consultarTotales(){
		return "SELECT COUNT(*) as nu_compras , SUM(nu_total) as nu_total FROM tb_compra_cabecera";
	}

	
}
?>