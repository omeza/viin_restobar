<?php

class GCajas{
	
	public function __construct(){
    }
    
	public function listar(){
		return "SELECT id_caja ,c.id_sucursal, no_sucursal , no_caja FROM tb_caja as c inner join tb_sucursal as s on c.id_sucursal = s.id_sucursal";
    }

    public function listarCajaSucursal(){
      return "SELECT id_caja , no_caja FROM tb_caja where id_sucursal = ?";
      }
    
    public function agregar(){
		return "INSERT INTO tb_caja (id_caja, id_sucursal, no_caja) VALUES (?, ?, ?)";
    }
    public function consultar(){
		return "SELECT id_caja , id_sucursal , no_caja FROM tb_caja WHERE id_caja=?";
	}
    public function eliminar(){
        return "DELETE FROM tb_caja WHERE id_caja=?";
    }
    public function listarCajas(){
        return sprintf("SELECT  id_caja as id , no_caja  FROM tb_caja WHERE id_sucursal = ?");
    }

    public function eliminarTotal(){
      return "DELETE FROM tb_caja";
    }
    public function guardarCabecera(){
      return "INSERT INTO tb_venta_cabecera(id_cliente, id_tipo_comprobante, id_usuario, nu_serie, nu_correlativo, nu_sub_total, nu_igv, nu_total, fe_emision, fe_registro, status,id_pedido_cabecera,id_venta_cabecera) VALUES (?,?,?,?,?,?,?,?,now(),now(),1,?,?)";
    }

    public function actualizarCorrelativo(){
      return "UPDATE tb_comprobante_sucursal SET nu_correlativo = ? WHERE id_tipo_comprobante = ? and id_sucursal = ?";
    }

    public function guardarDetalle(){
      return "INSERT INTO tb_venta_detalle(id_venta_cabecera, id_producto, nu_cantidad, nu_precio_unitario,id_pedido_detalle) VALUES (?,?,?,?,?)";
    }

    public function guardarParcial(){
      return "INSERT INTO tb_venta_parcial(id_venta_cabecera, nu_efectivo, nu_tarjeta, nu_tranferencia, nu_credito,nu_vuelto) VALUES (?,?,?,?,?,?)";
    }

    public function actualizarPedido(){
      return "UPDATE tb_pedido_detalle SET il_pagado = '1'  WHERE id_pedido_cabecera = ?";

    }
    public function actualizarEstadoMesa(){
      return "UPDATE tb_mesa SET il_disponible = '0'  WHERE id_mesa in (?)";
    }
    public function cajaAbierta(){
      return "SELECT id_caja_accion, id_caja, nu_monto_apertura, fh_apertura FROM tb_caja_accion WHERE DATE_FORMAT(fh_apertura, '%Y-%m-%d') = CURDATE() and id_caja = ?";
    }

    public function totales(){
      return "SELECT count(*) as nu_ventas, ROUND(sum(nu_total),2) as nu_total from tb_venta_cabecera";
    }

    public function totalesDia(){
      return "SELECT count(*) as nu_ventas, ROUND(sum(nu_total),2) as nu_total from tb_venta_cabecera  where fe_emision = CURDATE();";
    }

    public function listaVentas(){
      return "SELECT id_venta_cabecera, vc.id_cliente, vc.id_tipo_comprobante, no_tipo_comprobante, no_cliente, concat(nu_serie,'-',nu_correlativo) as numero_comprobante,fe_emision, ROUND(nu_total,2) as nu_total  , nu_sub_total , nu_igv , nu_serie , nu_correlativo , id_tipo_documento , nu_documento , no_direccion , status , no_estado_sunat , no_estado , email
      FROM  tb_venta_cabecera vc
      INNER JOIN tc_tipo_comprobante tc ON vc.id_tipo_comprobante = tc.id_tipo_comprobante  
      INNER JOIN tb_cliente cl ON vc.id_cliente = cl.id_cliente
      INNER JOIN tc_estado_sunat es ON es.id_estado_sunat = vc.id_estado_sunat
      INNER JOIN tc_estado e ON e.id_estado = vc.id_estado
      order by id_venta_cabecera";
    }

    public function listaVentasDia(){
      return "SELECT id_venta_cabecera, vc.id_cliente, vc.id_tipo_comprobante, no_tipo_comprobante, no_cliente, concat(nu_serie,'-',nu_correlativo) as numero_comprobante,fe_emision, ROUND(nu_total,2) as nu_total  , nu_sub_total , nu_igv , nu_serie , nu_correlativo , id_tipo_documento , nu_documento , no_direccion , status , no_estado_sunat , no_estado , email
      FROM  tb_venta_cabecera vc
      INNER JOIN tc_tipo_comprobante tc ON vc.id_tipo_comprobante = tc.id_tipo_comprobante  
      INNER JOIN tb_cliente cl ON vc.id_cliente = cl.id_cliente
      INNER JOIN tc_estado_sunat es ON es.id_estado_sunat = vc.id_estado_sunat
      INNER JOIN tc_estado e ON e.id_estado = vc.id_estado
      WHERE fe_emision = CURDATE() and vc.id_usuario = ?
      order by id_venta_cabecera";
    }

    public function listarVentasDetalle(){
      return "SELECT id_venta_detalle , id_venta_cabecera , vd.id_producto , no_producto , nu_cantidad , nu_precio_unitario 
      FROM tb_venta_detalle AS vd INNER JOIN tb_producto AS p ON vd.id_producto = p.id_producto
      WHERE id_venta_cabecera = ?";
    }

    public function listaCajaUsuario(){
      return "SELECT c.id_caja, no_caja, no_sucursal, concat(no_nombres, ' ', no_apepat, ' ', no_apemat) as no_empleado, 
                    (case when (select id_caja_accion from tb_caja_accion where id_caja = c.id_caja and Date_format(fh_apertura,'%Y-%m-%d') = curDate()) is null then 'ion-unlocked' else 'ion-locked' end) as icono,
                    (case when (select id_caja_accion from tb_caja_accion where id_caja = c.id_caja and Date_format(fh_apertura,'%Y-%m-%d') = curDate()) is null then 'btn-success' else 'btn-danger' end) as color,
                    (select id_caja_accion from tb_caja_accion where id_caja = c.id_caja and Date_format(fh_apertura,'%Y-%m-%d') = curDate()) as id_caja_accion,
                    (case when (select id_caja_accion from tb_caja_accion where id_caja = c.id_caja and Date_format(fh_apertura,'%Y-%m-%d') = curDate()) is null then 'Abrir Caja' else 'Cerrar Caja' end) as titulo,
                    (case when (select id_caja_accion from tb_caja_accion where id_caja = c.id_caja and Date_format(fh_apertura,'%Y-%m-%d') = curDate()) is null then 'Monto Inicial' else 'Monto Cierre' end) as titulo_monto,
                    (select nu_monto_apertura from tb_caja_accion where id_caja = c.id_caja and Date_format(fh_apertura,'%Y-%m-%d') = curDate()) as nu_monto_apertura,
                    (select ROUND(sum(nu_efectivo),2) from tb_venta_parcial) as nu_monto_cierre,
                    (case when (select nu_monto_cierre from tb_caja_accion where id_caja = c.id_caja and Date_format(fh_apertura,'%Y-%m-%d') = curDate()) is null then true else null end) as bloqueo,
                    (case when (select nu_monto_cierre from tb_caja_accion where id_caja = c.id_caja and Date_format(fh_apertura,'%Y-%m-%d') = curDate()) is null then null else 'Caja Cerrada' end) as mensaje
              from   tb_caja c,
                    tb_usuario u,
                    tb_sucursal s, 
                    tb_empleado e
              where  c.id_caja = u.id_caja
                    and c.id_sucursal = s.id_sucursal
                    and u.id_empleado = e.id_empleado ";
    }

    public function guardarAtertura(){
      return " INSERT INTO tb_caja_accion (id_caja, nu_monto_apertura, fh_apertura, nu_monto_cierre, id_caja_accion) values (?,?,now(),?,?)";
    }

    public function actualizar(){
      return "UPDATE tb_caja_accion set id_caja = ?, nu_monto_apertura = ?, fh_cierre = now(), nu_monto_cierre = ? where id_caja_accion = ? ";
    }

    
    
}
?>