<?php

class GTipodocumentos{
	private $id;
	private	$siglas;
	private $nombre;
	private $activo;
	
	public function __construct(){
		$this->id=0;
		$this->siglas='';
		$this->nombre='';
		$this->activo=TRUE;
	}
	public function ListarTipoSalida(){
		return "SELECT * FROM tipo_salida";
	}

	public function getTipDocumento4comprobantes(){
		return "SELECT DISTINCT ctd.idtc_tipo_documento, td.abrev_tipdocumento, td.no_tipo_documento, td.status, td.il_predeterminado FROM comprobantes_tc_tipo_documentos AS ctd INNER JOIN tc_tipo_documento AS td ON td.idtc_tipo_documento=ctd.idtc_tipo_documento %s";
	}

	public function borrarTDocumento(){
		return "DELETE FROM tc_tipo_documento WHERE idtc_tipo_documento=?";
	}

	public function updateStatusTDocumento(){
		return "UPDATE tc_tipo_documento SET status=? WHERE idtc_tipo_documento=?";
	}

	public function getTipoDocumentos(){
		return "SELECT * FROM tc_tipo_documento WHERE idtc_tipo_documento=?";
	}
	
	public function existeTipDocumento(){
		return "SELECT idtc_tipo_documento FROM tc_tipo_documento WHERE no_tipo_documento=? AND idtc_tipo_documento!=?";
	}
	
	public function consultDocumentoCliente(){
		return "SELECT * FROM tc_tipo_documento WHERE idtc_tipo_documento=?";
	}
	
	public function agregarTipoDocumento(){
		return "INSERT INTO tc_tipo_documento (no_tipo_documento, il_predeterminado, idtc_tipo_documento) VALUES (?, ?, ?)";
	}
	
	public function actualizarTipoDocumento(){
		return "UPDATE tc_tipo_documento SET no_tipo_documento=?, il_predeterminado=? WHERE idtc_tipo_documento=?";
	}
	
	public function listarTipodocumentos($status='all'){
		$filtro="WHERE status='1'";
		if($status!="all")
			$filtro=sprintf("WHERE status='%s'", $status);
		return sprintf("SELECT * FROM tc_tipo_documento %s ORDER BY no_tipo_documento", $filtro);
	}
	
	public function getTipDocpredterminado(){
		return "SELECT id_tipo_documento FROM tc_tipo_documento WHERE il_predeterminado='1' AND status='1'";
	}
}
?>