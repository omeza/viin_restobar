<?php
class GProfesion{
	protected $id;
	private $nombre;
    private $activo;
    private $filter;
	
	public function __construct(){
		$this->id=0;
		$this->nombre='';
        $this->activo=TRUE;
        $this->filter='';
	}
	
	public function getIdProfesion(){ return $this->id; }

	public function setIdProfesion($id){ $this->id=$id; }

	public function existeNombre(){
		return "SELECT id_profesion FROM profesion WHERE desc_profesion=? AND id_profesion!=?";
	}
	
	public function consultarProfesion(){
		return "SELECT * FROM profesion WHERE id_profesion=?";
	}
	
	public function agregarProfesion(){
		return "INSERT INTO profesion (desc_profesion, id_profesion) VALUES (?, ?)";
	}
	
	public function actualizarProfesion(){
		return "UPDATE profesion SET desc_profesion=? WHERE id_profesion=?";
	}
	
	public function listarProfesion(){
		return sprintf("SELECT * FROM profesion %s ORDER BY desc_profesion", $this->filter);
	}
}
?>