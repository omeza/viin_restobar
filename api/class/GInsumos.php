<?php

class GInsumos{
	
	public function __construct(){
    }
    
	  public function listar(){
		return "SELECT ti.id_insumo, no_insumo , no_categoria_insumo , no_unidad , no_medida , ti.nu_cantidad_medida , ti.nu_stock_minimo , ti.nu_precio_sugerido
        FROM tb_insumo as ti INNER JOIN tb_categoria_insumo as tci on ti.id_categoria_insumo = tci.id_categoria_insumo INNER JOIN tc_unidad as tu on ti.id_unidad = tu.id_unidad
        INNER JOIN tc_medida as tm on ti.id_medida = tm.id_medida ";
    }
    
    public function agregar(){
		return "INSERT INTO tb_insumo(id_categoria_insumo, id_unidad, id_medida, no_insumo, nu_cantidad_medida, nu_stock_minimo, nu_stock_minimo_medida, nu_precio_sugerido,id_insumo) VALUES (?,?,?,?,?,?,?,?,?)";
    }

    public function actualizar(){
      return "UPDATE tb_insumo SET id_categoria_insumo = ?, id_unidad = ?, id_medida = ?, no_insumo = ?, nu_cantidad_medida = ?, nu_stock_minimo = ?, nu_stock_minimo_medida = ?, nu_precio_sugerido = ? WHERE id_insumo = ?;
      ";
    }

    public function consultar(){
		return "SELECT id_insumo, id_categoria_insumo, id_unidad, id_medida, no_insumo, nu_cantidad_medida, nu_stock_minimo, nu_stock_minimo_medida, nu_precio_sugerido FROM tb_insumo WHERE id_insumo=?";
    }
    
    public function eliminar(){
        return "DELETE FROM tb_categoria_insumo WHERE id_categoria_insumo=?";
    }

    public function listarStock(){
      return "SELECT ai.id_insumo , no_insumo ,no_categoria_insumo , sum(nu_cantidad) as  nu_cantidad, no_abreviatura, 
                    (case when (ROUND((sum(nu_cantidad)/nu_cantidad_medida),2)-floor(ROUND((sum(nu_cantidad)/nu_cantidad_medida),2))) = 0 then 
                          cast(floor(ROUND((sum(nu_cantidad)/nu_cantidad_medida),2)) as char)
                    else cast(ROUND((sum(nu_cantidad)/nu_cantidad_medida),2) as char) end) as unidades, 
                    no_unidad
              FROM tb_almacen_insumo as ai 
                  INNER JOIN tb_insumo as i on ai.id_insumo = i.id_insumo 
                  INNER JOIN tb_categoria_insumo as ci on i.id_categoria_insumo = ci.id_categoria_insumo
                  INNER JOIN tc_medida as m on i.id_medida = m.id_medida
                  INNER JOIN tc_unidad as u on i.id_unidad = u.id_unidad
              GROUP BY 1 ,2 , 3, 5, 7";
    }
    public function listaInsumoCompras(){
      return "SELECT cc.id_compra_cabecera , nu_serie , nu_correlativo , fe_emision , i.no_insumo , cd.nu_cantidad , cd .nu_precio_unitario , cd.nu_total    
      FROM tb_compra_cabecera as cc 
      INNER JOIN tb_compra_detalle as cd on cc.id_compra_cabecera = cd.id_compra_cabecera 
      INNER JOIN tb_insumo as i on cd.id_insumo = i.id_insumo WHERE cd.id_insumo = ?";
    }
}
?>