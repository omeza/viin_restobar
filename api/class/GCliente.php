<?php

class GCliente extends GTipodocumentos{
	private $id;
	private $razon_social;
	private $direccion;
	private $id_departamento;
	private $rep_legal;
	private $email;
	private $telefono;
	private $celular;
	private $fecingreso;
	private $activo;
    protected $filter;

	
	public function __construct(){
		$this->id=0;
		$this->razon_social='';
		$this->direccion='';
		$this->id_departamento= new GDepartamentos();
		$this->rep_legal= '';
		$this->email='';
		$this->telefono='';
		$this->celular='';
		$this->fecingreso='';
		$this->activo=TRUE;
		$this->filter='';
	}

    public function addFilter($stringfilter, $condition="AND"){
        $a_filter=!is_array($stringfilter)?array($stringfilter):$stringfilter;
        foreach($a_filter as $i => $columns){
            $this->filter=(empty($this->filter))?" WHERE %s": $condition;
            switch($stringfilter){
                case 'razon_social':
                    $this->filter=sprintf($this->filter, "p.razon_social LIKE ?");
                break;
                case 'num_documento':
                    $this->filter=sprintf($this->filter, "p.num_documento LIKE ?");
                break;
            }
        }
    }
	
	function getRUC(){
		return "SELECT p.idproveedor, p.idtip_documento, p.num_documento, p.razon_social FROM proveedores AS p WHERE p.num_documento=? AND p.idtip_documento=?";
	}

	public function getCliente(){
		return "SELECT p.idproveedor, p.idtip_documento, p.num_documento, p.razon_social, p.direccion, p.idestados_pais, p.id_provincia, p.id_distrito, p.repr_legal, p.email, p.tlf_hab, p.tlf_movil, p.status FROM proveedores AS p WHERE p.idproveedor=?";
	}
	
	public function consultDocumentoCliente(){
		return "SELECT * FROM proveedores WHERE idclientes=?";
	}
	
	public function listarProveedores(){
		return sprintf("SELECT p.idproveedor, p.idtip_documento, td. abrev_tipdocumento, td.desc_documento, p.num_documento, p.razon_social, p.direccion, p.repr_legal, p.email, p.tlf_hab, p.tlf_movil, ep.desc_estados, p.status FROM proveedores AS p LEFT OUTER JOIN tip_documento AS td ON td.idtip_documento=p.idtip_documento LEFT OUTER JOIN estados_pais AS ep ON ep.idestados_pais=p.idestados_pais %s ORDER BY p.razon_social", $this->filter);
	}

	public function actualizar(){
		return "UPDATE tb_cliente SET id_tipo_documento = ?, id_ubigeo = ?, nu_documento = ?, no_cliente = ?, no_direccion = ?, nu_telefono = ?, email = ? WHERE id_cliente = ?";
	}
	
	public function agregar(){
		return "INSERT INTO tb_cliente(id_tipo_documento,id_ubigeo,nu_documento,no_cliente,no_direccion,nu_telefono,email,id_cliente) VALUES (?,?,?,?,?,?,?,?)";
	}

	public function existeDocProveedor(){
		return "SELECT idproveedor FROM proveedores WHERE (num_documento=? AND idtip_documento=?) AND idproveedor!=?";
	}	

	public function listar(){
		return "SELECT * FROM tb_cliente";
	}

	public function consultar(){
		return "SELECT * FROM tb_cliente WHERE id_cliente = ?";
	}

	public function listarProveedor(){
		return "SELECT id_cliente as id , no_proveedor FROM tb_cliente";
	}
	public function deleteCliente(){
		return "DELETE FROM tb_cliente WHERE id_cliente=?";
	}
  
	public function changeStatus(){
		return "UPDATE tb_cliente SET status=? WHERE id_cliente=?";
	}


}
?>