<?php
class GNumeroctas extends GBancos{
	private $id_cuenta;
	private $num_cuenta;
	private $otipocuenta;
	private $omoneda;
	private $status;
	
	public function __construct(){
		$this->id_cuenta=0;
		$this->num_cuenta='';
		$this->otipocuenta= new GTipocuentabancos();
		$this->omoneda= new GMoneda();
		$this->status=TRUE;
	}
	
	public function existeNumctabanco(){ 
		return "SELECT idcuentasbancarias FROM cuentasbancarias WHERE numctabancaria=? AND idcuentasbancarias!=?"; 
	}

	public function agregarNumctabanco(){ 
		return "INSERT INTO cuentasbancarias (idbancos, numctabancaria, idtipocta, idmoneda, idcuentasbancarias) VALUES (?, ?, ?, ?, ?)"; 
	}
	
	public function actualizarNumctabanco(){ 
		return "UPDATE cuentasbancarias SET idbancos=?, numctabancaria=?, idtipocta=?, idmoneda=? WHERE idcuentasbancarias=?"; 
	}

	public function getNumctabanco(){ 
		return "SELECT idcuentasbancarias, idbancos, numctabancaria, idtipocta, idmoneda, status FROM cuentasbancarias WHERE idcuentasbancarias=?"; 
	}
	
	public function listarNumctabanco($status="all"){
		$add_filter="";
		if($status!="all"){
			$add_filter=sprintf("AND status='%s'", $status);
		}
		return sprintf("SELECT cb.idcuentasbancarias, cb.idbancos, b.nombre_banco, cb.numctabancaria, cb.idtipocta, tcb.desctipocta, cb.idmoneda, m.simb_moneda, m.nom_moneda, cb.status  FROM cuentasbancarias AS cb INNER JOIN bancos AS b ON b.idbancos=cb.idbancos AND b.status='1' INNER JOIN tipoctasbancarias AS tcb ON tcb.idtipocta=cb.idtipocta AND tcb.status='1' INNER JOIN moneda AS m ON m.idmoneda=cb.idmoneda AND m.status='1' WHERE cb.idbancos=? %s ORDER BY cb.numctabancaria", $add_filter);
	}

	public function eliminarNumctabanco(){
		return "DELETE FROM cuentasbancarias WHERE idcuentasbancarias=?";
	}

	public function updateStatusnumctabanco(){
		return "UPDATE cuentasbancarias SET status=? WHERE idcuentasbancarias=?";
	}
}
?>