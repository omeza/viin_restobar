<?php
class GEmail{
  private $header_mail;
  private $replytoemail;
  private $cc;
  private $fromemail;
  private $msg;
  private $apenom;
  private $bodyHTML;

  public function __construct(){
    $this->replyto="no-reply@daceos.uptag.edu.ve";
    $this->fromemail="no-reply@daceos.uptag.edu.ve";
    $this->cc="daceiutag1@gmail.com";
    $this->msg="";
    $this->apenom="";
    $this->bodyHTML="";
  }

  public function setApeNom($apenom){
    $this->apenom=$apenom;
  }

  public function setCc($email){
    $this->cc=strtolower($email);
  }

  public function setFrom($email){
    $this->fromemail=strtolower($email);
  }

  public function setReply($email){
    $this->replytoemail=strtolower($email);
  }

  public function setMessage($msg){
    $this->msg=$msg;
  }

  public function bodyEmail($addbody){
    $this->bodyHTML=$addbody;
  }

  public function templateHTML(){
    $html ='<!DOCTYPE html><html><head><meta charset="utf-8"><title>DACEOS</title><style type="text/css">body{font-family: Arial; font-size: 13px;} td{ padding: 8px;}</style></head>';
    $html.=sprintf('<body style="padding:25px;"><table style="width:800px;"><tr><td style="width:90px;">Ciudadano (a):</td><td><div>%s</div></td></tr><tr><td colspan="2" style="text-align:center;">%s<br/><span>Departameno de Admisión y Control de Estudios UPTFAG - DACEOS</span></td></tr></table></body>', strtoupper($this->apenom), $this->bodyHTML);
    $html.="</html>";
    return $html;
  }
  public function sendEmailHTML($email, $asunto){
    if(empty($this->msg))
      $this->msg=$this->templateHTML();
    $email=strtolower($email);
    $headers_email=sprintf("From: %s \r\n Reply-To: %s \r\n Cc: %s", $this->fromemail, $this->replytoemail, $this->cc);
    $headers_email.="MIME-Version: 1.0\r\n";
    $headers_email.="Content-Type: text/html; charset=ISO-8859-1\r\n";
    return mail($email, $asunto, $this->msg, $headers_email);
  }

}
?>
