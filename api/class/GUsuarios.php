<?php
class GUsuarios extends GEmpleados{
    protected $idusuario;
    protected $idrol;
    protected $aliasuser;
    protected $pass;
    protected $change_password;

    public function __construct(){
        $this->idusuario=0;
        $this->idrol=0;
        $this->aliasuser="";
        $this->pass="";
        $this->change_password=false;
    }

    public function getIdrol(){ return $this->idrol; }
    public function getNombrerol() {return $this->nombre; }
    public function getStatus(){ return $this->status; }

    public function updatePassword($enabled){
        $this->change_password=(is_bool($enabled))?$enabled:false;
    }

    public function setIdrol($value){ $this->idrol=$value; }
    public function setNombrerol($value){ $this->nombre=$value;}
    public function setStatus($value){ $this->status=$value;}

    public function changeStatus(){
        return "UPDATE rol_sistema SET status=? WHERE id_rol_sistema=?";
    }


    public function agregarUsuario(){
        return "INSERT INTO tb_usuario (id_empleado, id_rol_sistema, id_sucursal , id_caja, login, password ,id_usuario ) VALUES (?, ?, ?, ?, ? , ? , ?)";
    }

    public function guardarUsuario(){
        return sprintf("UPDATE tb_usuario SET id_empleado=?, id_rol_sistema=?, login=?%s WHERE id_usuario=?", ($this->change_password)?', password=?':'');
    }

    public function existUsuario(){
        return "SELECT id_usuario FROM tb_usuario WHERE login=? AND id_usuario!=?";
    }

    public function getUsuario(){
        return "SELECT e.id_empleado, e.id_tipo_documento, e.nu_documento, CONCAT_WS(', ', TRIM(CONCAT_WS(' ', e.no_apepat, no_apemat)), e.no_nombres) AS apenom_empleado, IFNULL(u.id_usuario, 0) AS id_usuario, IFNULL(u.login, '') AS login, IFNULL(u.id_rol_sistema, 0) AS id_rol_sistema, IF(ISNULL(u.password), '', '*******') AS password FROM tb_empleado AS e LEFT OUTER JOIN tb_usuario AS u ON u.id_empleado=e.id_empleado LEFT OUTER JOIN rol_sistema AS rs ON rs.id_rol_sistema=u.id_rol_sistema WHERE e.id_empleado=?";
    }

    public function listar(){
        return "SELECT e.id_empleado, IFNULL(u.id_usuario, 0) AS id_usuario, e.no_apepat, e.no_apemat, e.no_nombres, IFNULL(u.login, '') AS login, IF(ISNULL(u.fe_ingreso), '' , DATE_FORMAT(u.fe_ingreso, '%d-%m-%Y')) AS fe_ingreso, IFNULL(u.fe_baje, '') AS fe_baje, IFNULL(u.id_rol_sistema, 0) AS id_rol_sistema, IFNULL(rs.no_rol_sistema, '') AS no_rol_sistema, IFNULL(u.status,'-') AS status FROM tb_empleado AS e LEFT OUTER JOIN tb_usuario AS u ON e.id_empleado=u.id_empleado LEFT OUTER JOIN rol_sistema AS rs ON rs.id_rol_sistema=u.id_rol_sistema WHERE e.il_visible='1' ORDER BY rs.no_rol_sistema";
    }

}
?>