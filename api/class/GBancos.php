<?php

class GBancos{
	private $id;
	private	$siglas;
	private $nombre;
	private $activo;
	
	public function __construct(){
		$this->id=0;
		$this->siglas='';
		$this->nombre='';
		$this->activo=TRUE;
		$this->filter='';
	}
	
    public function addFilter($stringfilter, $condition="AND"){
        $a_filter=!is_array($stringfilter)?array($stringfilter):$stringfilter;
        foreach($a_filter as $i => $columns){
            $this->filter=(empty($this->filter))?" WHERE %s": $condition;
            switch($stringfilter){
                case 'sigla_banco':
                    $this->filter=sprintf($this->filter, "sigla_banco LIKE ?");
                break;
                default:
                    $this->filter=sprintf($this->filter, "nombre_banco LIKE ?");
            }
        }
    }

	public function getBanco(){
		return "SELECT idbancos AS id_banco, sigla_banco, nombre_banco AS nombre, status FROM bancos WHERE idbancos=?";
	}
	
	public function existeSigla(){
		return "SELECT idbancos FROM bancos WHERE sigla_banco=? AND idbancos!=?";
	}

	public function existeNombre(){
		return "SELECT idbancos FROM bancos WHERE nombre_banco=? AND idbancos!=?";
	}
	
	public function consultDocumentoCliente(){
		return "SELECT * FROM clientes WHERE idcliente=?";
	}
	
	public function agregarBanco(){
		return "INSERT INTO bancos (sigla_banco, nombre_banco, idbancos) VALUES (?, ?, ?)";
	}
	
	public function actualizarBanco(){
		return "UPDATE bancos SET sigla_banco=?, nombre_banco=? WHERE idbancos=?";
	}
	
	public function listarBancos($status="all"){
		if($status!="all"){
			$field=sprintf("status='%s'", $status);
			$this->filter.=(empty($this->filter))?"WHERE %s": " AND %s";
			$this->filter=sprintf($this->filter, $field);
		}
		return sprintf("SELECT * FROM bancos %s ORDER BY nombre_banco", $this->filter);
	}

	public function borrarBanco(){
		return "DELETE FROM bancos WHERE idbancos=?";
	}

	public function updateStatusBanco(){
		return "UPDATE bancos SET status=? WHERE idbancos=?";
	}
}
?>