<?php
require_once dirname(__FILE__).'/vendor/autoload.php';
class GTemplate extends Twig_Environment {

    public function __construct(){
        $loader = new Twig_Loader_Filesystem(__DIR__.'/../Templates');
        parent::__construct($loader);
    }
}
?>