<?php

class GUbigeo{
	
	public function __construct(){
	}	
    
    public function listarDepartamento(){
		return sprintf("SELECT  SUBSTRING(id_ubigeo,1,2) as id , no_departamento  FROM tc_ubigeo WHERE no_provincia IS NULL");
	}
	public function listarProvincia(){
		return sprintf("SELECT  SUBSTRING(id_ubigeo,3,2) as id , no_provincia  FROM tc_ubigeo WHERE no_provincia is not null and no_distrito IS NULL and  SUBSTRING(id_ubigeo,1,2) = ?");
	}
	public function listarDistrito(){
		return sprintf("SELECT  SUBSTRING(id_ubigeo,5,2) as id , no_distrito  FROM tc_ubigeo WHERE SUBSTRING(id_ubigeo,1,2) = ? and  SUBSTRING(id_ubigeo,3,2) = ? and SUBSTRING(id_ubigeo,5,2) != '00'");
	}
}
?>