<?php

class GProductoCategoria{
	
	public function __construct(){
    }
    
	  public function listar(){
		return "select id_categoria_producto, no_categoria_producto from tb_categoria_producto";
    }
    
    public function agregar(){
		return "INSERT INTO tb_categoria_producto(no_categoria_producto,id_categoria_producto) VALUES (?,?)";
    }

    public function actualizar(){
      return "UPDATE tb_categoria_producto SET no_categoria_producto = ? WHERE id_categoria_producto = ?";
    }

    public function consultar(){
		return "SELECT id_insumo, id_categoria_insumo, id_unidad, id_medida, no_insumo, nu_cantidad_medida, nu_stock_minimo, nu_stock_minimo_medida, nu_precio_sugerido FROM tb_insumo WHERE id_insumo=?";
    }
    
    public function eliminar(){
        return "DELETE FROM tb_categoria_producto WHERE id_categoria_producto=?";
    }
    public function listarCategoria(){
      return sprintf("SELECT 0 as id, 'Seleccione' as no_categoria_producto
                      UNION
                    SELECT id_categoria_producto as id ,  no_categoria_producto FROM tb_categoria_producto");
    }
    public function listarDestino(){
      return sprintf("SELECT id_destino as id ,  no_destino FROM tb_destino");
    }
}
?>