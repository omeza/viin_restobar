<?php
class GRoles {
    protected $idrol;
    protected $nombre;
    protected $status;

    public function __construct(){
        $this->idrol=0;
        $this->nombre="";
        $this->status='1';
    }

    public function getIdrol(){ return $this->idrol; }
    public function getNombrerol() {return $this->nombre; }
    public function getStatus(){ return $this->status; }

    public function setIdrol($value){ $this->idrol=$value; }
    public function setNombrerol($value){ $this->nombre=$value;}
    public function setStatus($value){ $this->status=$value;}

    public function deleteRol(){
        return "DELETE FROM rol_sistema WHERE id_rol_sistema=?";
    }

    public function changeStatus(){
        return "UPDATE rol_sistema SET status=? WHERE id_rol_sistema=?";
    }

    public function getRol(){
        return "SELECT id_rol_sistema, no_rol_sistema, status FROM rol_sistema WHERE id_rol_sistema=?";
    }

    public function listar($status='all'){
        $filter="";
        if($status!='all' && is_numeric($status))
            $filter=sprintf("AND status='%d'", $status);
        return sprintf("SELECT * FROM rol_sistema WHERE hidden!='1' %s ORDER BY no_rol_sistema", $filter);
    }

    public function agregarRol(){
        return "INSERT INTO rol_sistema (no_rol_sistema, id_rol_sistema) VALUES (?, ?)";
    }

    public function editarRol(){
        return "UPDATE rol_sistema SET no_rol_sistema=? WHERE id_rol_sistema=?";
    }

    public function existRol(){
        return "SELECT id_rol_sistema FROM rol_sistema WHERE no_rol_sistema=? AND id_rol_sistema!=?";
    }

}
?>