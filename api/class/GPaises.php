<?php
class GPaises {
    protected $id;
    protected $nombre;
	protected $prefijo;
    protected $_default;
    protected $filter;

    public function __construct(){
        $this->id=0;
        $this->nombre="";
		$this->prefijo="";
        $this->_default='1';
        $this->filter='';
    }

    public function getId(){ return $this->id; }
    public function getNombre() {return $this->nombre; }
    public function getPrefijo() {return $this->prefijo; }
    public function getDefault(){ return $this->_default; }

    public function setId($value){ $this->id=$value; }
    public function setNombre($value){ $this->nombre=$value;}
    public function setPrefijo($value){ $this->prefijo=$value;}
    public function setDefault($value){ $this->_default=$value;}

    public function addFilter($stringfilter, $condition="AND"){
        $a_filter=!is_array($stringfilter)?array($stringfilter):$stringfilter;
        foreach($a_filter as $i => $columns){
            $this->filter=(empty($this->filter))?" WHERE %s": $condition;
            switch($stringfilter){
                case 'desc_pais':
                    $this->filter=sprintf($this->filter, "desc_pais LIKE ?");
                break;
            }
        }
    }


    public function updateDefault(){
        return "UPDATE paises SET predeterminado=? WHERE idpais=?";
    }

    public function consultarPais(){
        return "SELECT * FROM paises WHERE idpais=?";
    }

    public function eliminarPais(){
        return "DELETE FROM paises WHERE idpais=?";
    }

    public function existNombre(){
        return "SELECT idpais FROM paises WHERE desc_pais=? AND idpais!=?";
    }

    public function existPrefijo(){
        return "SELECT idpais FROM paises WHERE prefijo=? AND idpais!=?";
    }

	public function agregarPais(){ 
		return "INSERT INTO paises (desc_pais, prefijo, predeterminado, idpais) VALUES (?, ?, ?, ?)"; 
	}

    public function actualizarPais(){ 
		return "UPDATE paises SET desc_pais=?, prefijo=?, predeterminado=? WHERE idpais=?"; 
	}

    public function listar(){
        return sprintf("SELECT * FROM paises %s ORDER BY desc_pais", $this->filter);
    }

    public function asignDefault(){ return "UPDATE paises SET predeterminado='1' WHERE idpais=?"; }
    public function removeDefault(){ return "UPDATE paises SET predeterminado='0' WHERE predeterminado='1'"; }

}
?>