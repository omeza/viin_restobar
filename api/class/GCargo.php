<?php

class GCargo{
	protected $id;
	private $nombre;
    private $activo;
    private $filter;
	
	public function __construct(){
		$this->id=0;
		$this->nombre='';
        $this->activo=TRUE;
        $this->filter='';
	}
	
	public function getIdCargo(){ return $this->id; }

	public function setIdCargo($id){ $this->id=$id; }

	public function existeNombre(){
		return "SELECT id_cargo FROM cargo WHERE desc_cargo=? AND id_cargo!=?";
	}
	
	public function consultarCargo(){
		return "SELECT * FROM cargo WHERE id_cargo=?";
	}
	
	public function agregarCargo(){
		return "INSERT INTO cargo (desc_cargo, id_cargo) VALUES (?, ?)";
	}
	
	public function actualizarCargo(){
		return "UPDATE cargo SET desc_cargo=? WHERE id_cargo=?";
	}
	
	public function listarCargo(){
		return sprintf("SELECT * FROM cargo %s ORDER BY desc_cargo", $this->filter);
	}
}
?>