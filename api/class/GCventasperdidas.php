<?php
class GCventasperdidas{
	private $id;
	private	$siglas;
	private $nombre;
	private $activo;
	
	public function __construct(){
		$this->id=0;
		$this->siglas='';
		$this->nombre='';
		$this->activo=TRUE;
		$this->filter='';
	}
	
	public function consultDocumentoCliente(){
		return "SELECT * FROM clientes WHERE idcliente=?";
	}
	
	

    public function updateStatusCVPerdidas(){
		return "UPDATE concepto_ventas_perdidas SET status=? WHERE id_cvperdida=?";
	}

	public function borrarCVPerdidas(){
		return "DELETE FROM concepto_ventas_perdidas WHERE id_cvperdida=?";
	}

    public function getCVPerdidas(){
		return "SELECT * FROM concepto_ventas_perdidas WHERE id_cvperdida=?";
	}

    public function actualizarCVPerdidas(){
		return "UPDATE concepto_ventas_perdidas SET desc_cvperdida=?, det_cvperdida=? WHERE id_cvperdida=?";
	}

    public function agregarCVPerdidas(){
		return "INSERT INTO concepto_ventas_perdidas (desc_cvperdida, id_cvperdida, det_cvperdida) VALUES ( ?, ?, ?)";
	}

    public function existeDescripcion(){
		return "SELECT id_cvperdida FROM concepto_ventas_perdidas WHERE desc_cvperdida=? AND id_cvperdida!=?";
	}
	
	public function listarCVPerdidas(){
		return sprintf("SELECT * FROM concepto_ventas_perdidas ORDER BY desc_cvperdida", $this->filter);
	}

    public function addFilter($stringfilter, $condition="AND"){
        $a_filter=!is_array($stringfilter)?array($stringfilter):$stringfilter;
        foreach($a_filter as $i => $columns){
            $this->filter=(empty($this->filter))?" WHERE %s": $condition;
            switch($stringfilter){
                case 'desc_cvperdida':
                    $this->filter=sprintf($this->filter, "desc_cvperdida LIKE ?");
                break;
            }
        }
    }

}
?>