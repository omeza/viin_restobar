<?php
class GProducto extends GSubgrupo{
    protected $id_producto;
    protected $cod_producto;
    protected $nom_producto;
    protected $id_marca;
    protected $id_subgrupo;
    protected $id_linea;
    protected $descripcion;
    protected $mto_ult_compra;
    protected $mtodo_venta;
    protected $mto_minimo;
    protected $mto_sugerido;
    protected $mto_porc;
    protected $mto_venta;
    protected $facturawithoutexist;
    protected $msgwithoutstock;
    protected $stock_minimo;
    protected $medida;
    protected $peso;
    protected $id_umedida;
    protected $id_categoria;
    protected $used_serial;
    protected $filter;
    protected $orderBy;

    function __construct(){
        $this->filter='';
        $this->orderBy=' nom_producto ASC';
    }

    public function addFilterVentas(){
        $this->filter=(empty($this->filter))?"WHERE p.facturarsinstock=? OR !ISNULL(i.stock)":"";
    }

    public function setOrder($field, $reverse){
        $order=($reverse)?'ASC':'DESC';
        switch($field){
            case 'cod_producto':
                $fieldname="cod_producto";
            break;
            default:
                $fieldname="nom_producto";
            break;
        }
        $this->orderBy=sprintf(" %s %s", $fieldname , $order);
    }

    public function addFilter($stringfilter, $condition="AND"){
        $a_filter=!is_array($stringfilter)?array($stringfilter):$stringfilter;
        foreach($a_filter as $i => $columns){
            $this->filter=(empty($this->filter))?" WHERE %s": $condition;
            switch($stringfilter){
                case 'no_producto':
                    $this->filter=sprintf($this->filter, "p.no_producto LIKE ?");
                break;
                case 'id_producto':
                    $this->filter=sprintf($this->filter, "p.id_producto LIKE ?");
                break;
            }
        }
    }

    public function addDetailinventario(){
        return "INSERT INTO detalle_inventario (lote, serial_1, num_serial1, serial_2, num_serial2, serial_3, num_serial3, fecha_vencimiento, format_date, date_valid, idalmacen, idubicacion, cantidad, idproductos) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    }

    public function seekCode(){
        return "SELECT idproductos FROM productos WHERE cod_producto=?";
    }

    public function addInventario(){
		return "INSERT INTO inventario (idproductos) VALUES (?)";
	}
    
    public function existCodigo(){
        return "SELECT idproductos FROM productos WHERE cod_producto=? AND idproductos!=?";
    }

    public function existeDescripcion(){
        return "SELECT idproductos FROM productos WHERE nom_producto=? AND (idproductos!=? AND idlaboratorio!=?)";
    }

    public function addProducto(){
        return "INSERT INTO productos (cod_producto, nom_producto, is_principio_activo, principio_activo, concent_princ_activo, id_formfarmaceutica, reg_sanitario, idlaboratorio, idgrupo, idsubgrupo, idlinea, descripcion, mto_compra, exento_impuesto, mto_impuesto, metod_venta, mto_minimo, mto_sugerido, porc_ganancia, facturarsinstock, avisostockminimo, stockminimo, medida, peso, idumedida, categoria, usa_serial, serial_1, serial_2, serial_3, etiqueta_label, etiqueta_precio, etiqueta_codigo,stock_min,stock_max, imagen, ocultar_venta, idproductos) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?, ?, ?, ?)";
    }

    public function updateProducto(){
        return "UPDATE productos SET cod_producto=?, nom_producto=?, is_principio_activo=?, principio_activo=?, concent_princ_activo=?, id_formfarmaceutica=?, reg_sanitario=?, idlaboratorio=?, idgrupo=?, idsubgrupo=?, idlinea=?, descripcion=?, mto_compra=?, exento_impuesto=?, mto_impuesto=?, metod_venta=?, mto_minimo=?, mto_sugerido=?, porc_ganancia=?, facturarsinstock=?, avisostockminimo=?, stockminimo=?, medida=?, peso=?, idumedida=?, categoria=?, usa_serial=?, serial_1=?, serial_2=?, serial_3=?, etiqueta_label=?, etiqueta_precio=?, etiqueta_codigo=?, stock_min=?,stock_max=?, imagen=?, ocultar_venta=? WHERE idproductos=?";
    }
    
    public function listarProducto(){
		return sprintf("SELECT id_producto, no_categoria_producto, no_destino, no_producto, nu_precio FROM tb_producto p, tb_categoria_producto cp, tb_destino d where	p.id_categoria_producto = cp.id_categoria_producto
                and p.id_destino = d.id_destino %s", $this->filter);
	}

    public function detailProducto(){
        return "SELECT p.idproductos, p.cod_producto, p.nom_producto, p.is_principio_activo, p.principio_activo, p.concent_princ_activo, 
                    p.id_formfarmaceutica, p.reg_sanitario, p.mto_compra, p.exento_impuesto, p.mto_impuesto, p.metod_venta, p.mto_minimo, 
                    p.porc_ganancia, p.mto_sugerido, IF(p.metod_venta=1, p.mto_sugerido, ROUND((p.mto_compra+(p.mto_compra*p.porc_ganancia/100)),2)) AS mto_venta, 
                    p.facturarsinstock, p.idlaboratorio, p.idgrupo, p.idsubgrupo, p.idlinea, p.descripcion, p.fec_ult_compra, dt.costo_unidad AS mto_venta_sin_imp, 
                    (dt.costo_unidad+dt.mto_impuesto) AS mto_ult_venta, b.fecventa, p.avisostockminimo, p.stockminimo, p.medida, p.peso, p.idumedida, p.categoria, 
                    p.usa_serial, p.serial_1, p.serial_2, p.serial_3, p.ocultar_venta 
                FROM productos AS p LEFT OUTER JOIN det_boleta AS dt ON dt.idproductos=p.idproductos  
                    LEFT OUTER JOIN boletas AS b ON b.idboleta=dt.idboleta 
                    WHERE p.idproductos=? ORDER BY dt.idboleta DESC LIMIT 0,1";
    }

    public function consultarStock(){
        return "SELECT p.idproductos, IFNULL(i.stock, '-') AS stock FROM productos AS p LEFT OUTER JOIN inventario AS i ON i.idproductos=p.idproductos WHERE p.idproductos=?";
    }

    public function stockVenta(){
        return "SELECT i.idproductos, i.stock-IFNULL(SUM(vt.cantidad), 0) AS cnt_disponible FROM inventario AS i LEFT OUTER JOIN ventas_temporal AS vt ON vt.idproductos=i.idproductos WHERE i.idproductos=? GROUP BY i.idproductos";
    }
    public function getVentas(){
        return "SELECT b.idboleta, b.idticket, b.fecventa, b.idclientes, c.apellidos_nombres, b.total, b.subtotal, b.mto_impuesto, b.importe_impuesto, CONCAT_WS(', ', TRIM(CONCAT_WS(' ', e.apellido_paterno, e.apellido_materno)), e.nombres) AS apenom_empleado FROM boletas AS b LEFT OUTER JOIN clientes AS c ON c.idclientes=b.idclientes LEFT OUTER JOIN empleado AS e ON e.idempleado=b.idempleado WHERE b.fecventa BETWEEN ? AND ? ORDER BY b.idempleado, b.idticket";
    }

    public function getVentas4Empleado(){
        return "SELECT b.idboleta, b.idticket, b.fecventa, b.idclientes, c.apellidos_nombres, b.total, b.subtotal, b.mto_impuesto, b.importe_impuesto FROM boletas AS b LEFT OUTER JOIN clientes AS c ON c.idclientes=b.idclientes WHERE (b.fecventa BETWEEN ? AND ?) AND b.idempleado=? ORDER BY b.idempleado, b.idticket";
    }

    public function detailsVenta(){
        return "SELECT dbol.idproductos, p.cod_producto, p.nom_producto, p.idlaboratorio, fab.desc_laboratorio AS desc_fabricante, dbol.num_serie, dbol.cantidad, dbol.costo_unidad, dbol.importe_impuesto, dbol.exento_impuesto, dbol.mto_impuesto, dbol.lote FROM det_boleta AS dbol LEFT OUTER JOIN productos AS p ON p.idproductos=dbol.idproductos LEFT OUTER JOIN laboratorios AS fab ON fab.idlaboratorio=p.idlaboratorio WHERE dbol.idboleta=?";
    }

    public function guardarCabecera(){
        return "INSERT INTO tb_producto(id_categoria_producto, id_destino, id_usuario, no_producto, nu_precio, ruta_imagen, fe_registo, id_producto) VALUES (?,?,?,?,?,?,now(),?)";
    }   
    public function actualizarCabecera(){
        return "  UPDATE tb_producto SET id_categoria_producto = ?, id_destino = ?, id_usuario = ?, no_producto = ?, nu_precio = ?, ruta_imagen = ? WHERE id_producto = ?";
    }   
  
    public function agregarDetalle(){
        return "INSERT INTO tb_producto_insumo(id_producto, id_insumo, nu_cantidad) VALUES (?,?,?)";
    } 
    public function agregarSucursal(){
        return "INSERT INTO tb_producto_sucursal(id_producto,id_sucursal) VALUES (?,?)";
    } 

    public function getProducto(){
        return "SELECT id_producto , no_producto , no_categoria_producto  as no_categoria , nu_precio , ruta_imagen , p.id_categoria_producto,p.id_destino FROM tb_producto as p INNER JOIN tb_categoria_producto as cp on p.id_categoria_producto = cp.id_categoria_producto WHERE id_producto = ?";
    }    

    public function getInsumosProducto(){
        return "SELECT i.id_insumo , no_insumo , nu_cantidad , no_medida FROM tb_producto_insumo as pi INNER JOIN tb_insumo as i on pi.id_insumo = i.id_insumo
        INNER JOIN tc_medida as m on i.id_medida = m.id_medida  WHERE id_producto = ?";
    }
    
    public function getProductosxCategoriaTotal(){
        return "SELECT p.id_producto as id_producto , no_producto , nu_precio , ruta_imagen, null as id_producto_menu  FROM 
        tb_producto AS p INNER JOIN tb_producto_sucursal AS ps on p.id_producto = ps.id_producto
        WHERE ps.id_sucursal = ? and '0' = ?";
    }

    public function getSucursalProducto(){
        return "SELECT * FROM tb_producto_sucursal WHERE id_producto = ?";
    }

    public function getProductosxCategoriaUnidad(){
        return "SELECT p.id_producto as id_producto  , no_producto , nu_precio , ruta_imagen , null as id_producto_menu FROM tb_producto AS p INNER JOIN tb_producto_sucursal AS ps on p.id_producto = ps.id_producto
        WHERE ps.id_sucursal = ? and id_categoria_producto = ?";
    }

    public function comboProducto(){
        return "SELECT 0 as id, 'Seleccione' as no_producto
                  union 
                  SELECT  id_producto as id , no_producto FROM tb_producto order by 1 desc";
    }

    public function getProductosMenu(){
        return "SELECT * FROM ( SELECT pr.id_producto as id_producto, (concat('MENÚ-',no_producto))as no_producto , nu_precio , 
                ruta_imagen , (nu_cantidad_platos - nu_platos_despachados) as stock, pm.id_producto_menu as id_producto_menu
        FROM   tb_producto_menu pm,
               tb_producto pr
        where  pm.id_producto = pr.id_producto 
                and  24 = ?) AS tabla
            WHERE stock !=0";
    }

    public function geteliminarProductoInsumo(){
        return "DELETE FROM tb_producto_insumo WHERE id_producto = ?";
    }
    public function geteliminarProductoSucursal(){
        return "DELETE FROM tb_producto_sucursal WHERE id_producto = ?";
    }

}
?>