<?php

class GReporte{
	public function __construct(){
    }
	  public function consultarVentas(){
		    return "SELECT vc.fe_registro, no_cliente, no_tipo_comprobante, nu_total, concat(nu_serie,'-',nu_correlativo) as nu_comprobante
                    from   tb_venta_cabecera vc,
                        tb_cliente cl,
                        tc_tipo_comprobante tc       
                    where vc.id_cliente = cl.id_cliente 
                          and vc.id_tipo_comprobante = tc.id_tipo_comprobante
                          and ( ? = ' ' or  vc.fe_registro between ? and ? )
                          and ( ? = 0 or  cl.id_tipo_documento = ? )
                          and ( ? = ' ' or  cl.nu_documento LIKE ?)
                          and ( ? = ' '  or  cl.no_cliente LIKE ?) 
                    order by 1";
    }
    public function consultarComprqas(){
        return "SELECT cc.fe_registro, no_proveedor, no_tipo_comprobante, nu_total, concat(nu_serie,'-',nu_correlativo) as nu_comprobante, nu_documento
                from   tb_compra_cabecera cc,
                    tb_proveedor pr,
                    tc_tipo_comprobante tc       
                where cc.id_proveedor = pr.id_proveedor 
                    and cc.id_tipo_comprobante = tc.id_tipo_comprobante
                    and ( ? = '' or  cc.fe_registro between ? and ? )
                    and ( ? = 0 or  pr.id_tipo_documento = ? )
                    and ( ? = '' or  pr.nu_documento = ?)
                    and ( ? = ''  or     pr.no_proveedor = ?)                         

                order by 1";
}
}
