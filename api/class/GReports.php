<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
require_once dirname(__FILE__).'/vendor/autoload.php';
class GReports extends TCPDF {
	protected $con_logo;
	protected $lMargin;
	protected $tMargin;
	protected $titulo;
	protected $append_header;
	protected $groupby;
	protected $label_group;
	protected $name_group;
	protected $header_html;
	protected $headHTML;

	public function __construct($orientation='P', $unit='mm', $format='A4', $unicode=TRUE, $encoding='UTF-8', $diskcache=FALSE, $pdfa=FALSE, $logo=FALSE, $titulo=''){
		parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
		$this->lMargin=0;
		$this->groupby=false;
		$this->label_group='';
		$this->name_group='';
		$this->tMargin=0;
		$this->append_header=false;
		$this->titulo=$titulo;
	}

	function addHeader($header){
		if(is_array($header)){ $this->append_header=$header;}
	}

	function setTitulo($title){
		$this->titulo=$title;
	}

	function getTitulo(){
		return $this->titulo;
	}

	public function Footer() {
        $this->SetY(-15);
        $this->SetFont('', 'I', 8);
        $this->Cell(0, 10, 'Página '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
	public function Header(){
		$fecha=new DateTime('now');
		$this->SetFont(PDF_FONT_NAME_MAIN, 'B', 6);
		$img=K_PATH_IMAGES.PDF_HEADER_LOGO;
		//$this->Image($img, 5, 5, 15, 15);
		$this->Ln(10);
		$this->Multicell(0, 3, "Botica Trebol\n\rExcelencia en Servicios", 0, 'L');
		$this->SetFont('', 'B', 10);		$this->SetFont('', 'B', 6);
		//$this->writeHTMLCell($w = 0, $h = 50, $x = '', $y = 8, $this->headHTML, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'C', $autopadding = false);

		$this->Cell(0, 5, $this->titulo, 0, 1, 'C', 0);
		$this->SetFont('', '', 6);
		$this->Cell(0, 5, $fecha->format('d/m/Y'), 0, 1, 'R');
		if($this->groupby){
			if(!empty($this->label_group)){
				$this->SetFont('', 'B', 7);
				$this->Cell(40, 5, $this->label_group,0, 0,'C');
			}
			$this->SetFont('', '');
			$this->Cell(0, 5, $this->name_group, 0, 1,'L');
		}
		if(is_array($this->append_header)){
			$this->SetFont('', 'B', 7);
			foreach($this->append_header as $i => $header){
				$this->Cell($header['width'], 5, $header['title'], $header['border'] , $header['endl'], $header['text-align']);
			}
		}
	}
	
}
?>