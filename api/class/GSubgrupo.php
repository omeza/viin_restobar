<?php
class GSubgrupo extends GGrupo{
	private $id_subgrupo;
	private	$cod_subgrupo;
	private $nom_subgrupo;
	private $activo;
	
	public function __construct(){
        parent::__construct();
		$this->id_subgrupo=0;
		$this->cod_subgrupo='';
		$this->nom_subgrupo='';
		$this->activo=TRUE;
	}
	
	public function getBanco(){
		return "SELECT idgrupo AS id_banco, sigla_banco, desc_grupo AS nombre, status FROM subgrupos WHERE idbanco=?";
	}
	
	public function existeNomsg(){
		return "SELECT idsubgrupo FROM subgrupos WHERE desc_subgrupo=? AND idsubgrupo!=?";
	}
	
	public function consultarSubgrupo(){
		return "SELECT * FROM subgrupos WHERE idsubgrupo=?";
	}
	
	public function agregarSubgrupo(){
		return "INSERT INTO subgrupos (idgrupo, desc_subgrupo, idsubgrupo) VALUES (?, ?, ?)";
	}
	
	public function actualizarSubrupo(){
		return "UPDATE subgrupos SET idgrupo=?, desc_subgrupo=? WHERE idsubgrupo=?";
	}
	
	public function listarSubgrupo($status="all"){
		$add_filter="";
		if($status!="all"){
			$add_filter=sprintf("AND sbg.status='%s'", $status);
		}
		return sprintf("SELECT sbg.idgrupo, gr.desc_grupo, sbg.idsubgrupo, sbg.desc_subgrupo, sbg.status FROM subgrupos AS sbg LEFT OUTER JOIN grupos AS gr ON gr.idgrupo=sbg.idgrupo WHERE sbg.idgrupo=? ORDER BY gr.desc_grupo, sbg.desc_subgrupo", $add_filter);
	}
}
?>