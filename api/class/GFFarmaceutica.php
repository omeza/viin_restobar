<?php

class GFFarmaceutica{
	private $id;
    private $descripcion;
    private $desc_simplif;
	private $activo;
	
	public function __construct(){
		$this->id=0;
        $this->descripcion='';
        $this->desc_simplif='';
		$this->activo=TRUE;
	}
	
	public function listar(){
		return "SELECT * FROM forma_farmaceutica ORDER BY desc_formfarmaceutica";
	}
}
?>