<?php

class GVisor{
	
	public function __construct(){
	}
	
	public function agregar(){
		return "INSERT INTO tb_pedido_cabecera(id_usuario,total,id_pedido_cabecera) VALUES (?,?, ?)";
    }

    public function eliminarParaActualizar(){
		return "DELETE FROM tb_pedido_detalle WHERE id_pedido_cabecera = ?";
    }

    public function eliminarMesas(){
		return "DELETE FROM tb_pedido_mesa WHERE id_pedido_cabecera = ?";
    }

    public function actualizarCabeceraPedido(){
		return "UPDATE tb_pedido_cabecera
                SET    id_usuario = ?, total = ?
                WHERE  id_pedido_cabecera = ?";
    }

    public function agregarDetalle(){
		return "INSERT INTO tb_pedido_detalle(id_pedido_cabecera, id_producto, cantidad, no_observacion) 
                        VALUES (?,?,?,?)";
    }
    
    public function agregarMesas(){
		return "INSERT INTO tb_pedido_mesa(id_pedido_cabecera, id_mesa) VALUES (?,?)";
	}

	public function listar(){
		return "SELECT id_compra_cabecera , no_proveedor, no_tipo_comprobante , CONCAT(no_nombres,' ',no_apepat,' ',no_apemat) as no_empleado, CONCAT(nu_serie_c,'-',nu_correlativo) as no_correlativo,cc.fe_registro, nu_total
		FROM tb_compra_cabecera as cc 
		INNER JOIN tb_proveedor as tp ON cc.id_proveedor = tp.id_proveedor
		INNER JOIN tc_tipo_comprobante as tc ON cc.id_tipo_comprobante = tc.id_tipo_comprobante
		INNER JOIN tb_usuario as tu ON cc.id_usuario = tu.id_usuario
		INNER JOIN tb_empleado as te ON tu.id_empleado =  te.id_empleado";
    }
    
    public function listarAtendidas(){
		return " SELECT GROUP_CONCAT(m.id_mesa) as ids_mesas, GROUP_CONCAT(m.nu_mesa) as no_mesas,  pc.id_pedido_cabecera as id_pedido_cabecera
                        from    tb_pedido_mesa pm,
                                tb_mesa m,
                                tb_pedido_cabecera pc,
                                tb_pedido_detalle pd,
                                tb_producto pr
                        where   pm.id_mesa = m.id_mesa
                                and pm.id_pedido_cabecera = pc.id_pedido_cabecera
                                and pc.id_pedido_cabecera = pd.id_pedido_cabecera
                                and pd.id_producto = pr.id_producto
                                and pr.id_destino = 1
                        group by 3";
        }
        
        public function listarAtendidasBar(){
		return " SELECT GROUP_CONCAT(m.id_mesa) as ids_mesas, GROUP_CONCAT(m.nu_mesa) as no_mesas,  pc.id_pedido_cabecera as id_pedido_cabecera
                        from    tb_pedido_mesa pm,
                                tb_mesa m,
                                tb_pedido_cabecera pc,
                                tb_pedido_detalle pd,
                                tb_producto pr
                        where   pm.id_mesa = m.id_mesa
                                and pm.id_pedido_cabecera = pc.id_pedido_cabecera
                                and pc.id_pedido_cabecera = pd.id_pedido_cabecera
                                and pd.id_producto = pr.id_producto
                                and pr.id_destino = 2
                        group by 3";
	}

	public function listarMesaPedido(){
		return "SELECT m.id_mesa , m.nu_mesa as no_mesas
                from   tb_pedido_mesa pm,
                        tb_mesa m,
                        tb_pedido_cabecera pc
                where   pm.id_mesa = m.id_mesa
                        and pm.id_pedido_cabecera = pc.id_pedido_cabecera
                        and pc.id_pedido_cabecera = ?";
    }
    
    public function listarPedidos(){
		return "SELECT  pd.id_producto, concat(case when pd.id_producto_menu is not null then 'MENU - ' else '' end,p.no_producto) as no_producto, 
                                p.nu_precio, p.ruta_imagen, pd.cantidad as nu_cantidad, no_observacion, id_pedido_detalle
                        from    tb_pedido_detalle pd,
                                tb_producto p
                        where   pd.id_pedido_cabecera = ?
                                and pd.id_producto = p.id_producto
                                and pd.il_atendido = 0
                                and p.id_destino = 1    
                        ORDER BY pd.id_pedido_cabecera";
    }

    public function listarPedidosBar(){
        return "SELECT  pd.id_producto, p.no_producto, p.nu_precio, p.ruta_imagen, pd.cantidad as nu_cantidad, no_observacion, id_pedido_detalle
        from    tb_pedido_detalle pd,
                tb_producto p
        where   pd.id_pedido_cabecera = ?
                and pd.id_producto = p.id_producto
                and pd.il_atendido = 0
                and p.id_destino = 2    
        ORDER BY pd.id_pedido_cabecera";
}
    
        public function consultarTotales(){
                return "SELECT COUNT(*) as nu_compras , SUM(nu_total) as nu_total FROM tb_compra_cabecera";
        }

        public function actualizarAtendido(){
                return " UPDATE tb_pedido_detalle set il_atendido = '1' where id_pedido_detalle = ? ";
        }

	
}
