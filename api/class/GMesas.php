<?php

class GMesas{
	
	public function __construct(){
    }
    
	  public function listar(){
		    return "SELECT id_mesa ,c.id_sucursal, no_sucursal , nu_mesa FROM tb_mesa as c inner join tb_sucursal as s on c.id_sucursal = s.id_sucursal";
    }
    
    public function agregar(){
	    	return "INSERT INTO tb_mesa (id_mesa,id_sucursal, nu_mesa) VALUES (?, ?, ?)";
    }
    public function consultar(){
		    return "SELECT id_mesa , id_sucursal , nu_mesa FROM tb_mesa WHERE id_mesa=?";
	  }
    public function eliminar(){
        return "DELETE FROM tb_mesa WHERE id_mesa=?";
    }
    public function eliminarTotal(){
        return "DELETE FROM tb_mesa";
    }
    public function listarMesasSucursal(){
      return "SELECT 0 as id, '' as id_sucursal,'Seleccione' as nu_mesa
                union 
                SELECT  id_mesa as id , id_sucursal , nu_mesa FROM tb_mesa";
  }
}
?>