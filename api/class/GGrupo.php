<?php

class GGrupo{
	protected $id;
	private	$codigo;
	private $nombre;
	private $activo;
	
	public function __construct(){
		$this->id=0;
		$this->codigo='';
		$this->nombre='';
		$this->activo=TRUE;
	}
	
	public function getIdGrupo(){
		return $this->id;
	}

	public function setIdGrupo($id){
		$this->id=$id;
	}

	public function existeCodigo(){
		return "SELECT idgrupo FROM grupos WHERE cod_grupo=? AND idgrupo!=?";
	}

	public function existeNombre(){
		return "SELECT idgrupo FROM grupos WHERE desc_grupo=? AND idgrupo!=?";
	}
	
	public function consultarGrupo(){
		return "SELECT * FROM grupos WHERE idgrupo=?";
	}
	
	public function agregarGrupo(){
		return "INSERT INTO grupos (cod_grupo, desc_grupo, idgrupo) VALUES (?, ?, ?)";
	}
	
	public function actualizarGrupo(){
		return "UPDATE grupos SET cod_grupo=?, desc_grupo=? WHERE idgrupo=?";
	}
	
	public function listarGrupos($status="all"){
		$add_filter="";
		if($status!="all"){
			$add_filter=sprintf("WHERE status='%s'", $status);
		}
		return sprintf("SELECT * FROM grupos %s ORDER BY desc_grupo", $add_filter);
	}
}
?>