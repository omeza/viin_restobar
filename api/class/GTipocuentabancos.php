<?php

class GTipocuentabancos{
	private $id;
	private $descripcion;
	private $status;
	
	public function __construct(){
		$this->id=0;
		$this->descripcion='';
		$this->status=TRUE;
	}
	public function existeDescripcion(){ 
		return "SELECT idtipocta FROM tipoctasbancarias WHERE desctipocta=? AND idtipocta!=?"; 
	}

	public function agregarTipoCtaBancaria(){ 
		return "INSERT INTO tipoctasbancarias (desctipocta, idtipocta) VALUES (?, ?)"; 
	}
	
	public function actualizarTipoCtaBancaria(){ 
		return "UPDATE tipoctasbancarias SET desctipocta=? WHERE idtipocta=?"; 
	}

	public function getTipoCtaBancaria(){ 
		return "SELECT idtipocta AS idtipcta, desctipocta AS descripcion, status FROM tipoctasbancarias WHERE idtipocta=?"; 
	}
	
	public function listarTipoCtaBancaria($status="all"){
		$add_filter="";
		if($status!="all"){
			$add_filter=sprintf("WHERE status='%s'", $status);
		}
		return sprintf("SELECT * FROM tipoctasbancarias %s ORDER BY desctipocta", $add_filter);
	}

	public function changeStatusTCta(){
		return "UPDATE tipoctasbancarias SET status=? WHERE idtipocta=?";
	}

	public function eliminarTCta(){
		return "DELETE FROM tipoctasbancarias WHERE idtipocta=?";
	}
}
?>