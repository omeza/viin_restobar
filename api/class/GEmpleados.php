<?php

class GEmpleados {
	private $id;
	private	$doc_identidad;
    private $apellido_paterno;
    private $apellido_materno;
    private $nombres;
    private $sexo;
    private $edocivil;
	private $direccion;
	private $telefono;
	private $celular;
    private $email;
    private $ocargo;
    private $oprofesion;
    private $oarea;
	private $fecingreso;
	private $activo;
	
	public function __construct(){
        $this->id=0;
        $this->doc_identidad='';
        $this->apellido_paterno='';
        $this->apellido_materno='';
        $this->nombres='';
        $this->sexo='0';
        $this->edocivil='S';
        $this->direccion='';
        $this->telefono='';
        $this->celular='';
        $this->email='';
        $this->ocargo= new GCargo();
        $this->oprofesion= new GProfesion();
        $this->oarea= new GArea();
        $this->fecingreso='';
        $this->activo=TRUE;
    }
    
    public function changeVisibiity(){
        return "UPDATE empleado SET visible=? WHERE idempleado=?";
    }

    public function changeStatus(){
        return "UPDATE empleado SET status=? WHERE idempleado=?";
    }

	public function getEmpleado(){
		return "SELECT e.idempleado, e.idtip_documento, td.desc_documento, e.num_documento, e.apellido_paterno, e.apellido_materno, e.nombres, e.sexo, e.fecnacimiento, e.edocivil, e.direccion_hab, e.tlf_movil, e.tlf_habitacion, e.email, e.id_profesion, e.id_cargo, e.status, e.visible FROM empleado AS e LEFT OUTER JOIN tip_documento AS td ON td.idtip_documento=e.idtip_documento WHERE e.idempleado=?";
	}
	
	public function validEmail(){
        return "SELECT idempleado FROM empleado WHERE email=? AND idempleado!=?";
    }

	public function agregarEmpleado(){
		return "INSERT INTO empleado (idtip_documento, num_documento, apellido_paterno, apellido_materno, nombres, sexo, fecnacimiento, edocivil, direccion_hab, tlf_movil, tlf_habitacion, email, idempleado) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	}
	
	public function actualizarEmpleado(){
		return "UPDATE empleado SET idtip_documento=?, num_documento=?, apellido_paterno=?, apellido_materno=?, nombres=?, sexo=?, fecnacimiento=?, edocivil=?, direccion_hab=?, tlf_movil=?, tlf_habitacion=?, email=? WHERE idempleado=?";
	}
	
	public function listarEmpleados(){
		return "SELECT e.idempleado, e.idtip_documento, td.desc_documento, e.num_documento, e.apellido_paterno, e.apellido_materno, e.nombres, e.sexo, e.fecnacimiento, e.edocivil, e.direccion_hab, e.tlf_movil, e.tlf_habitacion, e.id_profesion, e.id_cargo, c.desc_cargo, e.fec_ingreso, e.status, e.visible FROM empleado AS e LEFT OUTER JOIN tip_documento AS td ON td.idtip_documento=e.idtip_documento LEFT OUTER JOIN cargo AS c ON c.id_cargo=e.id_cargo ORDER BY CONCAT_WS(' ', e.apellido_paterno, e.apellido_materno)";
    }

    public function infoEmpleado(){
		return "SELECT e.idempleado, e.idtip_documento, e.num_documento, CONCAT_wS(', ', TRIM(CONCAT_WS(' ', e.apellido_paterno, e.apellido_materno)), e.nombres) AS ape_nom, u.idrol_sistema, rs.nom_rol, u.login, '**********' AS password FROM empleado AS e LEFT OUTER JOIN usuarios AS u ON u.idempleado=e.idempleado LEFT OUTER JOIN rol_sistema AS rs ON rs.idrol_sistema=u.idrol_sistema WHERE e.status='1' AND (e.idtip_documento=? AND e.num_documento=?)";
	}

	public function existeDocEmpleado(){
		return "SELECT idempleado FROM empleado WHERE idtip_documento=? AND num_documento=? AND idempleado!=?";
	}    
}
?>