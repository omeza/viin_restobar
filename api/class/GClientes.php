<?php

class GClientes extends GDepartamentos{
	private $id;
	private	$doc_identidad;
	private $nombre;
	private $direccion;
	private $cod_postal;
	private $ciudad;
	private $idestado;
	private $telefono;
	private $celular;
	private $email;
	private $descuento;
	private $omitir_dscto;
	private $observacion;
	private $fecingreso;
	private $activo;
	
	public function __construct(){
		$this->id=0;
		$this->doc_identidad='';
		$this->nombre='';
		$this->direccion='';
		$this->cod_postal=0;
		$this->ciudad=0;
		$this->idestado= new GDepartamentos();
		$this->telefono='';
		$this->celular='';
		$this->email='';
		$this->descuento=0.00;
		$this->omitir_dscto=FALSE;
		$this->observacion='';
		$this->fecingreso='';
		$this->activo=TRUE;
	}
	
	public function getClienteID(){
		return "SELECT c.*, td.desc_documento  FROM clientes AS c LEFT OUTER JOIN tip_documento AS td ON td.idtip_documento=c.idtip_documento WHERE c.idclientes=?";
	}
	
	public function existeDocCliente(){
		return "SELECT idclientes FROM clientes WHERE num_documento=? AND idtip_documento=? AND idclientes!=?";
	}
	
	public function consultDocumentoCliente(){
		return "SELECT c.idclientes, c.apellidos_nombres FROM clientes AS c WHERE c.idtip_documento=? AND c.num_documento=?";
	}
	
	public function agregarCliente(){
		return "INSERT INTO clientes (idtip_documento, num_documento, apellidos_nombres, direccion, tlf_hab, tlf_movil, email, idclientes) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	}
	
	public function actualizarCliente(){
		return "UPDATE clientes SET idtip_documento=?, num_documento=?, apellidos_nombres=?, direccion=?, tlf_hab=?, tlf_movil=?, email=? WHERE idclientes=?";
	}
	
	public function listarClientes(){
		return "SELECT c.idclientes, c.idtip_documento, td.abrev_tipdocumento, td.desc_documento, c.num_documento, c.apellidos_nombres, c.tlf_hab, c.tlf_movil, c.email  FROM clientes AS c LEFT OUTER JOIN tip_documento AS td ON td.idtip_documento=c.idtip_documento ORDER BY c.apellidos_nombres";
	}
}
?>