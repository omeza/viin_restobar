<?php

class GMoneda{
	private $id;
	private	$simbolo;
	private $nombre;
	private $status;
	
	public function __construct(){
		$this->id=0;
		$this->simbolo='';
		$this->nombre='';
		$this->status=TRUE;
	}

	public function assignDefault(){
		return "UPDATE configuracion SET id_moneda=?";
	}
	public function removeAditional(){
		return "DELETE FROM monedas_adicionales WHERE id_moneda=?";
	}

	public function addAditional(){
		return "INSERT INTO monedas_adicionales (id_moneda) VALUES(?)";
	}

	public function existeCodInternacional(){
		return "SELECT idmoneda FROM moneda WHERE code_intern_moneda=? AND idmoneda!=?";
	}

	public function monedaAdditionals(){
		return "SELECT ma.id_moneda, IF(ISNULL(c.id_moneda),'0','1') AS predeterminada FROM monedas_adicionales AS ma LEFT OUTER JOIN configuracion AS c ON c.id_moneda=ma.id_moneda";
	}

	public function existeCodigo(){
		return "SELECT idmoneda FROM moneda WHERE code_moneda=? AND idmoneda!=?";
	}

	public function existeSimbol(){ 
		return "SELECT idmoneda FROM moneda WHERE simb_moneda=? AND idmoneda!=?"; 
	}

	public function existeNombre(){ 
		return "SELECT idmoneda FROM moneda WHERE nom_moneda=? AND idmoneda!=?"; 
	}

	public function agregarMoneda(){ 
		return "INSERT INTO moneda (code_moneda, code_intern_moneda, simb_moneda, nom_moneda, idmoneda) VALUES (?, ?, ?, ?, ?)"; 
	}
	
	public function actualizarMoneda(){ 
		return "UPDATE moneda SET code_moneda=?, code_intern_moneda=?, simb_moneda=?, nom_moneda=? WHERE idmoneda=?"; 
	}

	public function getMoneda(){ 
		return "SELECT m.idmoneda AS id_moneda, m.code_moneda AS cod_moneda, m.code_intern_moneda AS cod_intern, m.simb_moneda, m.nom_moneda AS nombre, m.status, IF(ISNULL(c.id_moneda), 0, 1) AS predeterminado, IF(ISNULL(ma.id_moneda),0,1) AS aditional FROM moneda AS m LEFT OUTER JOIN configuracion AS c ON c.id_moneda=m.idmoneda LEFT OUTER JOIN monedas_adicionales AS ma ON ma.id_moneda=m.idmoneda WHERE idmoneda=?"; 
	}

	public function constularM(){ return "SELECT * FROM clientes WHERE idcliente=?"; }
	
	public function listarMonedas($status="all"){
		$add_filter="";
		if($status!="all"){
			$add_filter=sprintf("WHERE status='%s'", $status);
		}
		return sprintf("SELECT m.*, IF(ISNULL(c.id_moneda),0,1) AS is_default, IF(ISNULL(ma.id_moneda),0,1) AS is_adicional FROM moneda AS m LEFT OUTER JOIN configuracion AS c ON c.id_moneda=m.idmoneda LEFT OUTER JOIN monedas_adicionales AS ma ON ma.id_moneda=m.idmoneda %s ORDER BY nom_moneda DESC", $add_filter);
	}

	public function updateStatusMoneda(){
		return "UPDATE moneda SET status=? WHERE idmoneda=?";
	}

	public function borrarMoneda(){
		return "DELETE FROM moneda WHERE idmoneda=?";
	}
}
?>