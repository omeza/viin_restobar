<?php
class GProductoMenu{
       function __construct(){
    }

    public function guardarProductoMenu(){
        return "INSERT INTO tb_producto_menu (id_producto, nu_cantidad_platos)values(?,?)";
    }

    public function listarCabeceraMenu(){
        return "SELECT id_producto_menu, no_producto, nu_cantidad_platos
                from   tb_producto pr,
                    tb_producto_menu me
                where  pr.id_producto = me.id_producto ORDER BY 1 desc";
    }

    public function listarDetalle(){
        return "SELECT id_producto_menu_detalle, no_insumo, nu_cantidad, no_medida
                from   tb_producto_menu_detalle md,
                    tb_insumo i, 
                    tc_medida me
                where  md.id_insumo = i.id_insumo
                    and i.id_medida = me.id_medida
                    and id_producto_menu = ?";
    }
    
    
}
?>