<?php

class GUnidadmedida{
	protected $id_unidadmedida;
	private $nom_unidadmedida;
	private $status;
	
	public function __construct(){
		$this->id_unidadmedida=0;
		$this->nom_unidadmedida='';
		$this->status=TRUE;
	}
	
	public function getIdUnimedida(){
		return $this->id_unidadmedida;
	}

	public function setIdUnimedida($id){
		$this->id_unidadmedida=$id;
	}

	public function existeAbrevUnimedida(){
		return "SELECT idumedida FROM unidadmedidad WHERE abrev_umedida=? AND idumedida!=?";
	}

	public function existeUnimedida(){
		return "SELECT idumedida FROM unidadmedidad WHERE desc_umedida=? AND idumedida!=?";
	}
	
	public function consultarUMedida(){
		return "SELECT * FROM unidadmedidad WHERE idumedida=?";
	}

	public function agregarUMedida(){
		return "INSERT INTO unidadmedidad (abrev_umedida, desc_umedida, idumedida) VALUES (?, ?, ?)";
	}
	
	public function actualizarUMedida(){
		return "UPDATE unidadmedidad SET abrev_umedida=?, desc_umedida=? WHERE idumedida=?";
	}
	
	public function listarUMedida($status="all"){
		$add_filter="";
		if($status!="all"){
			$add_filter=sprintf("WHERE status='%s'", $status);
		}
		return sprintf("SELECT * FROM unidadmedidad %s ORDER BY desc_umedida", $add_filter);
	}
}
?>