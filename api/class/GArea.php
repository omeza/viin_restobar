<?php
class GArea{
	protected $id;
	private $nombre;
    private $activo;
    private $filter;
	
	public function __construct(){
		$this->id=0;
		$this->nombre='';
        $this->activo=TRUE;
        $this->filter='';
	}
	
	public function getIdArea(){ return $this->id; }

	public function setIdArea($id){ $this->id=$id; }

	public function existeNombre(){
		return "SELECT id_area FROM area WHERE desc_area=? AND id_area!=?";
	}
	
	public function consultarArea(){
		return "SELECT * FROM area WHERE id_area=?";
	}
	
	public function agregarArea(){
		return "INSERT INTO area (desc_area, id_area) VALUES (?, ?)";
	}
	
	public function actualizarArea(){
		return "UPDATE area SET desc_area=? WHERE id_area=?";
	}
	
	public function listarArea(){
		return sprintf("SELECT * FROM area %s ORDER BY desc_area", $this->filter);
	}
}
?>