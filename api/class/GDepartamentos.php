<?php
class GDepartamentos{
	private $id_departamento;
	private	$desc_departamento;
	protected $filter;
	
	public function __construct(){
		$this->id_departamento=0;
		$this->desc_departamento='';
		$this->filter="";
	}
	
	public function setIdDepartamento($id_departamento){ $this->id_departamento=$id_departamento; }
	public function getIdDepartamento(){return $this->id_departamento; }
	public function setDescDepartamento($desc_departamento){ $this->desc_departamento=$desc_departamento; }
	public function getDescDepartamento(){return $this->desc_departamento; }
	
    public function addFilter($stringfilter, $condition="AND"){
        $a_filter=!is_array($stringfilter)?array($stringfilter):$stringfilter;
        foreach($a_filter as $i => $columns){
			$this->filter.=(empty($this->filter))?" WHERE %s": $condition;
            switch($columns){
                case 'desc_estados':
                    $this->filter.=" ep.desc_estados LIKE ?";
				break;
				case 'idpais':
					$this->filter=sprintf($this->filter, 'ep.idpais=? ');
				break;
            }
        }
    }

	public function getDistritos(){//arregla eso jajjaja me tengo que ir loco debo buscar a hilary en el colegio  dale ntp ya vi como solucionar gracias 
		return "SELECT dtr.id_distrito, dtr.cod_distrito, dtr.desc_distrito FROM distritos AS dtr WHERE dtr.id_provincia=?";
	}

	public function getProvincias(){
		return "SELECT prv.id_provincia, prv.cod_provincia, prv.desc_provincia FROM provincias AS prv WHERE prv.id_depto=?";
	}

	public function listarDepartamentos(){
		if(empty($this->filter))
			$this->filter="AND p.predeterminado='1'";
		return sprintf("SELECT ep.idestados_pais, ep.desc_estados, ep.status FROM estados_pais AS ep LEFT OUTER JOIN paises AS p ON p.idpais=ep.idpais %s ORDER BY 2", $this->filter);
	}
	
	public function existEstado(){
		return "SELECT idestados_pais FROM estados_pais WHERE desc_estados=? AND idestados_pais!=?";
	}
	
	public function guardarEstado(){
		return "INSERT INTO estados_pais (idpais, desc_estados, idestados_pais) VALUES (?, ?, ?)";
	}
	
	public function updateEstado(){
		return "UPDATE estados_pais SET idpais=?, desc_estados=? WHERE idestados_pais=?";
	}

	public function consultarEstado(){
		return "SELECT * FROM estados_pais WHERE idestados_pais=?";
	}
	
	public function updatestatusEstado(){
		return "UPDATE estados_pais SET status=? WHERE idestados_pais=?";
	}

	public function borrarEstado(){
		return "DELETE FROM estados_pais WHERE idestados_pais=?";
	}
}
?>