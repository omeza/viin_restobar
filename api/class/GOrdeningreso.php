<?php
class GOrdeningreso{
    protected $idproveedor;
    protected $fechaorden;
    protected $observacion;
    protected $idusuario;
    protected $fecregistro;
    protected $fecaprobacion;
    protected $fecanulado;
    protected $status;
    protected $detalle_items;
    protected $filter;

    function __construct(){
        $this->idproveedor=0;
        $this->fechaorden='';
        $this->observacion='';
        $this->idusuario=0;
        $this->fecregistro='';
        $this->fecaprobacion='';
        $this->fecanulado='';
        $this->status='A';
        $this->filter='';
    }

    public function addIngresoDet(){
		return "INSERT INTO detalles_orden_ingreso (idordeningreso,idproductos, cantidad,peso) VALUES (?, ?,?,?)";
	}
    public function addOrdenIngreso(){
		return "INSERT INTO orden_ingreso (idproveedor,id_tipo_salida, fe_ingreso, observacion) VALUES (?, ?, ?, ?)";
	}
    public function consultarNotaProvedor(){
        return "SELECT idordensalida,fe_emision FROM `orden_salida` where idproveedor=?";
    }
    function addProducto(){
        return "INSERT INTO detalles_orden_pedidos (idproductos, cantidad, mto_ult_compra, porc_impuesto, exento_impuesto, idordenpedido) VALUES (?, ?, ?, ?, ?, ?)";
    }
}
?>