<?php

class GCategoriaInsumo{
	
	  public function __construct(){
    }
    
	  public function listar(){
		    return "SELECT id_categoria_insumo ,no_categoria_insumo, il_visible , status FROM tb_categoria_insumo";
    }
    
    public function agregar(){
		    return "INSERT INTO tb_categoria_insumo (no_categoria_insumo, id_categoria_insumo) VALUES (?, ?)";
    }

    public function actualizar(){
      return "UPDATE tb_categoria_insumo SET no_categoria_insumo = ? WHERE id_categoria_insumo = ?";
    }

    public function consultar(){
		    return "SELECT id_categoria_insumo , no_categoria_insumo , il_visible , status FROM tb_categoria_insumo WHERE id_categoria_insumo=?";
    }
    
    public function eliminar(){
        return "DELETE FROM tb_categoria_insumo WHERE id_categoria_insumo=?";
    }

    public function eliminarTotal(){
        return "DELETE FROM tb_categoria_insumo";
    }
    
    public function getSucursal(){
        return "SELECT * FROM tb_categoria_insumo WHERE id_categoria_insumo = ? ";
    }

    public function listarCategoria(){
        return "SELECT id_categoria_insumo AS id, no_categoria_insumo  FROM tb_categoria_insumo";
    }
    public function changeStatus(){
        return "UPDATE tb_categoria_insumo SET status=? WHERE id_categoria_insumo=?";
    }
}
?>