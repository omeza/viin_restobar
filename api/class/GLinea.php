<?php

class GLinea{
	private $id;
	private $descripcion;
	private $activo;
	
	public function __construct(){
		$this->id=0;
		$this->descripcion='';
		$this->activo=TRUE;
	}
	
	public function getLinea(){
		return "SELECT * FROM linea WHERE idlinea=?";
	}
	
	public function consultDocumentoCliente(){
		return "SELECT * FROM linea WHERE idlinea=?";
	}
	
	public function listarLineas($status="all"){
		$add_filter="";
		if($status!="all"){
			$add_filter=sprintf("WHERE status='%s'", $status);
		}
		return sprintf("SELECT * FROM linea %s ORDER BY desc_linea", $add_filter);
	}

	public function actualizarLinea(){
		return "UPDATE linea SET desc_linea=?  WHERE idlinea=?";
	}
	
	public function agregarLinea(){
		return "INSERT INTO linea (desc_linea, idlinea) VALUES (?, ?)";
	}

	public function existeDescLinea(){
		return "SELECT idlinea FROM linea WHERE desc_linea=? AND idlinea!=?";
	}	
}
?>