<?php
class GOrdenpedidos{
    protected $idproveedor;
    protected $fechaorden;
    protected $observacion;
    protected $idusuario;
    protected $fecregistro;
    protected $fecaprobacion;
    protected $fecanulado;
    protected $status;
    protected $detalle_items;
    protected $filter;

    function __construct(){
        $this->idproveedor=0;
        $this->fechaorden='';
        $this->observacion='';
        $this->idusuario=0;
        $this->fecregistro='';
        $this->fecaprobacion='';
        $this->fecanulado='';
        $this->status='A';
        $this->filter='';
    }

    public function addFilter($stringfilter, $condition="AND"){
        $a_filter=!is_array($stringfilter)?array($stringfilter):$stringfilter;
        foreach($a_filter as $i => $columns){
            $this->filter=(empty($this->filter))?" WHERE %s": $condition;
            switch($stringfilter){
                case 'nom_producto':
                    $this->filter=sprintf($this->filter, "p.nom_producto LIKE ?");
                break;
                case 'cod_producto':
                    $this->filter=sprintf($this->filter, "p.cod_producto LIKE ?");
                break;
            }
        }
    }
    public function addSalidaDet(){
		return "INSERT INTO detalles_orden_salida (idordensalida,idproductos, cantidad,peso) VALUES (?, ?,?,?)";
	}
    public function addOrdenSalida(){
		return "INSERT INTO orden_salida (idproveedor,id_tipo_salida, fe_emision, nu_guia,peso_sug,cant_sug,observacion) VALUES (?, ?, ?, ?,?,?,?)";
	}
    public function listarOrdenesCompra (){
        return "SELECT tc.idcompra,tc.id_proveedor,tc.fecha_ocompra,tc.idcondicion_pago,tc.fecha_pago,tc.porc_descuento,tc.dia_prontopago,tc.subtotal, tc.importe_impuesto,tc.cancelado,
        tc.fec_cancelado,tc.entregado, tc.fec_entrega, tp.razon_social FROM `ordenes_compras` as tc left outer join proveedores as tp on tc.id_proveedor=tp.idproveedor";
    }
    public function listarOrdenes(){
        return "SELECT op.idordenpedido, op.idproveedor, p.razon_social, op.fecha_orden, op.fecha_aprobada, op.fecha_anulada, op.status, SUM(dop.mto_ult_compra*dop.cantidad) AS total,dop.cantidad,dop.peso FROM orden_pedidos AS op LEFT OUTER JOIN proveedores AS p ON p.idproveedor=op.idproveedor LEFT OUTER JOIN detalles_orden_pedidos AS dop ON dop.idordenpedido=op.idordenpedido GROUP BY op.idordenpedido ORDER BY op.fecha_orden DESC";
    }

    public function addOredenPedido(){
		return "INSERT INTO orden_pedidos (idproveedor, fecha_orden, observacion, idusuario, idordenpedido) VALUES (?, ?, ?, ?, ?)";
	}
    public function updateOrdenPedido(){
        return "";
    }
    function addProducto(){
        return "INSERT INTO detalles_orden_pedidos (idproductos, cantidad, mto_ult_compra, porc_impuesto, exento_impuesto, idordenpedido) VALUES (?, ?, ?, ?, ?, ?)";
    }
}
?>