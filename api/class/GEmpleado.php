<?php

class GEmpleado extends GTipodocumentos{
	private $id;
	private $razon_social;
	private $direccion;
	private $id_departamento;
	private $rep_legal;
	private $email;
	private $telefono;
	private $celular;
	private $fecingreso;
	private $activo;
    protected $filter;

	
	public function __construct(){
		$this->id=0;
		$this->razon_social='';
		$this->direccion='';
		$this->id_departamento= new GDepartamentos();
		$this->rep_legal= '';
		$this->email='';
		$this->telefono='';
		$this->celular='';
		$this->fecingreso='';
		$this->activo=TRUE;
		$this->filter='';
	}

    public function addFilter($stringfilter, $condition="AND"){
        $a_filter=!is_array($stringfilter)?array($stringfilter):$stringfilter;
        foreach($a_filter as $i => $columns){
            $this->filter=(empty($this->filter))?" WHERE %s": $condition;
            switch($stringfilter){
                case 'razon_social':
                    $this->filter=sprintf($this->filter, "p.razon_social LIKE ?");
                break;
                case 'num_documento':
                    $this->filter=sprintf($this->filter, "p.num_documento LIKE ?");
                break;
            }
        }
    }

	public function listar(){
		return "SELECT id_empleado,nu_documento,concat_ws(' ',no_apepat,no_apemat,no_nombres) as no_nombres, no_direccion, nu_contacto, email, fe_ingreso, fe_baja,status 
        FROM tb_empleado";
	}

	public function agregar(){
		return "INSERT INTO tb_empleado (id_tipo_documento,id_sexo,id_estado_civil,nu_documento,no_apepat,no_apemat,no_nombres,fe_nacimiento,
		no_direccion,nu_contacto,email,fe_ingreso,id_empleado)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
	}

	public function getEmpleado(){
		return "SELECT id_empleado,id_tipo_documento,id_sexo,id_estado_civil,nu_documento,no_apepat,no_apemat,no_nombres,
		fe_nacimiento,no_direccion,nu_contacto,email,fe_ingreso,fe_baja,il_visible,status
		FROM tb_empleado where id_empleado = ?";
	}

	public function actualizar(){
		return "UPDATE tb_empleado SET id_tipo_documento = ?,id_sexo = ?,id_estado_civil = ?,nu_documento = ?,
		no_apepat = ?,no_apemat = ?,no_nombres = ?,fe_nacimiento = ?,no_direccion = ?,nu_contacto = ?,email = ?,fe_ingreso = ?
		WHERE id_empleado = ?;";
	}

	public function eliminar(){
        return "DELETE FROM tb_empleado WHERE id_empleado = ?";
    }
}
?>