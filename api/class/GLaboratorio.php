<?php

class GLaboratorio{
	private $id;
	private $descripcion;
	private $activo;
	
	public function __construct(){
		$this->id=0;
		$this->descripcion='';
		$this->activo=TRUE;
	}
	
	public function getLaboratorio(){
		return "SELECT * FROM laboratorios WHERE idlaboratorio=?";
	}
	
	public function consultDocumentoCliente(){
		return "SELECT * FROM laboratorios WHERE idlaboratorio=?";
	}
	
	public function listarLaboratorios(){
		return "SELECT * FROM laboratorios ORDER BY desc_laboratorio";
	}

	public function actualizarLaboratorio(){
		return "UPDATE laboratorios SET desc_laboratorio=?, reg_sanitario=? WHERE idlaboratorio=?";
	}
	
	public function agregarLaboratorio(){
		return "INSERT INTO laboratorios (desc_laboratorio, reg_sanitario, idlaboratorio) VALUES (?, ?, ?)";
	}

	public function existeDescLaboratorio(){
		return "SELECT idlaboratorio FROM laboratorios WHERE desc_laboratorio=? AND idlaboratorio!=?";
	}	
}
?>