<?php
    try{
        require_once("../class/GLibfunciones.php");
        $OConex = new GConector();
        $init_stmt=$OConex->stmt_init();
        $sql_provincias="SELECT id_provincia, cod_provincia FROM provincias";
        if(!$init_stmt->prepare($sql_provincias))
            throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
        $init_stmt->execute();
        $result_provincia=$init_stmt->get_result();
        if($result_provincia->num_rows>0){
            $stmt_update=$OConex->stmt_init();
            $sql_distrito="UPDATE distritos SET id_provincia=? WHERE SUBSTR(cod_distrito,1,4)=?";
            if(!$stmt_update->prepare($sql_distrito))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            while($row_provincia=$result_provincia->fetch_object()){
                if(!$stmt_update->bind_param('is', $row_provincia->id_provincia, $row_provincia->cod_provincia))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
                $stmt_update->execute();
            }
        }    
    }catch(Exception $e){
        echo  $e->getMessage();
    }
?>