<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
$response_json	=array('success'=>false, 'num_rows'=>-1, 'data'=>array(), "messages"=>"Estas intentando algo inusual en el sistema");
require_once("./class/GLibfunciones.php");
$OConex = new GConector();
$init_stmt=$OConex->stmt_init();
$oper=(isset($_GET['oper']))?$_GET['oper']:'get';
switch($oper){
    case 'guardar':
        $data = json_decode(file_get_contents('php://input'));
        if(!isset($data->grupo) || !is_numeric($data->grupo))
            break;
        $a_parameters=array(0=>'');
        switch($data->grupo){
            case '0':
                if(!isset($data->abrev_impuesto,  $data->porc_impuesto,  $data->idmoneda,  $data->num_autorizacion))
                    break 2;
                $add_fields="nombre_impuesto=?, porc_impuesto=?, id_moneda=?, nautorizacionimpresa=?";
                $a_parameters[0].='sdis';
                $a_parameters[]=&$data->abrev_impuesto;
                $a_parameters[]=&$data->porc_impuesto;
                $a_parameters[]=&$data->idmoneda->idmoneda;
                $a_parameters[]=&$data->num_autorizacion;
            break;
            case '1':
                if(!isset($data->init_factura, $data->init_boleta, $data->autoidproducto, $data->inicia_codigo))
                    break 2;
                $add_fields="iniciar_factura=?, iniciar_boleta=?, autoidproducto=?, inicia_codigo=?";
                $autoincrement=($data->autoidproducto)?'1':'0';
                $a_parameters[0].='iisi';
                $a_parameters[]=&$data->init_factura;
                $a_parameters[]=&$data->init_boleta;
                $a_parameters[]=&$autoincrement;
                $a_parameters[]=&$data->inicia_codigo;
            break;
            default:
                $response_json['messages']="Estas intentando guardar una configuración errada";
            break 2;
        }
        $sql=sprintf("UPDATE configuracion SET %s", $add_fields);
        if(!$init_stmt->prepare($sql))
            throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
        if(!call_user_func_array(array($init_stmt, 'bind_param'), $a_parameters))
            throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
        $init_stmt->execute();
		$response_json['sql']=$sql;
        $response_json['affected_rows']=$init_stmt->affected_rows;
        $response_json['messages']=($init_stmt->affected_rows==1)?"Se actualizo con exito la información":"No se realizaron cambios en la configuración";
    break;
    case'get':
        $sql="SELECT c.*, m.simb_moneda, m.code_moneda FROM configuracion AS c INNER JOIN moneda AS m ON m.idmoneda=c.id_moneda LIMIT 0,1";
        if(!$init_stmt->prepare($sql))
            throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
        $init_stmt->execute();
        $result=$init_stmt->get_result();
        $response_json['success']=true;
        $response_json['num_rows']=$result->num_rows;
        if($response_json['num_rows']==1){
            $response_json['data']=array_combine( array('porc_impuesto', 'abrev_impuesto', 'init_factura', 'init_boleta', 'num_autorizacion', 'imprimir_venta', 'name_print', 'idmoneda', 'autoidproducto', 'inicia_codigo', 'simb_moneda', 'code_moneda'), array_values($result->fetch_assoc()));
            $response_json['data']['autoidproducto']=($response_json['data']['autoidproducto']=='1');
        }        
    break;
}
echo json_encode($response_json);
?>