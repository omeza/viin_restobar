<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
	$response_json	=array('success'=>false, 'num_rows'=>-1, 'rows'=>array(), "msg"=>"Estas intentando algo inusual en el sistema");
	require_once("./class/GLibfunciones.php");
	$OConex = new GConector();
	$init_stmt=$OConex->stmt_init();
	$OUbigeo = new GUbigeo();
	$data = json_decode(file_get_contents('php://input'));
	switch($_GET['oper']){
		case 'listarDepartamento':
			$response_json=array("totalItemCount"=>0, "numberOfPages"=>0, "rows"=>array(), "success"=>false);
			$name_method='listarDepartamento';
			$sql=call_user_func(array($OUbigeo, $name_method));
			$store_params=array(0=>'');
			$sql=sprintf($sql);
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			}
			$init_stmt->execute();			
			$result_rows=$init_stmt->get_result();
			$response_json['totalItemCount']=$result_rows->num_rows;
			if(isset($data->start, $data->number))
				$response_json['numberOfPages']=ceil($result_rows->num_rows/$data->number);
			else
				$response_json['numberOfPages']=1;
			$response_json['success']=true;
			if($result_rows->num_rows==0)
				break;
			$item=1;
			while($row=$result_rows->fetch_object()){						
				array_push($response_json['rows'], array("id"=>$row->id, "no_departamento"=>utf8_encode($row->no_departamento), "item"=>$item++ ) );
			}
		break;
		case 'listarProvincia':
			$response_json=array("totalItemCount"=>0, "numberOfPages"=>0, "rows"=>array(), "success"=>false);
			$name_method='listarProvincia';
			$sql=call_user_func(array($OUbigeo, $name_method));
			$store_params=array(0=>'');
			$sql=sprintf($sql);
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('s', $data->co_depart))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			
			$init_stmt->execute();
			$result_rows=$init_stmt->get_result();
			$response_json['totalItemCount']=$result_rows->num_rows;
			if(isset($data->start, $data->number))
				$response_json['numberOfPages']=ceil($result_rows->num_rows/$data->number);
			else
				$response_json['numberOfPages']=1;
			$response_json['success']=true;
			if($result_rows->num_rows==0)
				break;
			$item=1;
			while($row=$result_rows->fetch_object()){
				array_push($response_json['rows'], array("id"=>$row->id, "no_provincia"=>utf8_encode($row->no_provincia), "item"=>$item++ ) );
			}
		break;
		case 'listarDistrito':
			$response_json=array("totalItemCount"=>0, "numberOfPages"=>0, "rows"=>array(), "success"=>false);
			$name_method='listarDistrito';
			$sql=call_user_func(array($OUbigeo, $name_method));
			$store_params=array(0=>'');
			$sql=sprintf($sql);
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('ss', $data->co_depart,$data->co_provin))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			
			$init_stmt->execute();
			$result_rows=$init_stmt->get_result();
			$response_json['totalItemCount']=$result_rows->num_rows;
			if(isset($data->start, $data->number))
				$response_json['numberOfPages']=ceil($result_rows->num_rows/$data->number);
			else
				$response_json['numberOfPages']=1;
			$response_json['success']=true;
			if($result_rows->num_rows==0)
				break;
			$item=1;
			while($row=$result_rows->fetch_object()){
				array_push($response_json['rows'], array("id"=>$row->id, "no_distrito"=>utf8_encode($row->no_distrito), "item"=>$item++ ) );
			}
		break;		
	}
	echo json_encode($response_json);
}catch(Exception $e){
	$msg=$e->getOutMsg();
	switch($msg){
		case 'Expired token':
			$header=HTTPStatus(401);
			header($header);
		break;
		default:
			echo $msg;
	}
}
?>