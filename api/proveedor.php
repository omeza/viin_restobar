<?php
session_start();
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
	$response_json	=array('success'=>false, 'num_rows'=>-1, 'rows'=>array(), "messages"=>"Estas intentando algo inusual en el sistema");
	require_once("./class/GLibfunciones.php");
	$OConex = new GConector();
	$init_stmt=$OConex->stmt_init();
	$OProveedor = new GProveedor();
	$data = json_decode(file_get_contents('php://input'));
	switch($_GET['oper']){
		case 'listar':
            $store_params=array(0=>'');            
			if(isset($data->predicateObject)){
				foreach($data->predicateObject as $fields => $value){
					$store_params[0].='s';
					${$fields}=sprintf("%%%s%%",$value);
					$store_params[]=&${$fields};
					$OProveedor->addFilter($fields);
				}
			}
			$sql=$OProveedor->listar();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=true;
			$response_json['totalItemCount']=$result->num_rows;
			if($response_json['totalItemCount']==0)
				break;
			if(isset($data->start, $data->number)){
				$response_json['numberOfPages']=ceil($result->num_rows/$data->number);
				$Opagination=new GPagination();
				$Opagination->setInit($data->start);
				$Opagination->setLimit($data->number);
				$sql=$Opagination->prepareSQL($sql);
			}
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$i=(isset($data->start))?$data->start:0;
			while($rows=$result->fetch_assoc()){
				array_push($response_json['rows'], array_merge($rows, array("item"=>++$i)));
			}
		break;
		case 'guardar':
			if(!isset($data->id_tipo_documento, $data->no_proveedor, $data->nu_documento, $data->co_depart, $data->co_provin, $data->co_distri))
			break;		
			$OConex->setAutocommit(FALSE);
			$init_commit=$OConex->stmt_init();	
			$method=(empty($data->id_proveedor))?'agregar':'actualizar';
			$data->no_proveedor=strtoupper($data->no_proveedor);
            $data->id_ubigeo = $data->co_depart->value.$data->co_provin->value.$data->co_distri->value;
			$sql=call_user_func(array($OProveedor, $method));
			if(!$init_commit->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);			
			if(!$init_commit->bind_param('isssssssssssi',$data->id_tipo_documento, $data->id_ubigeo, $data->nu_documento,$data->no_proveedor, $data->no_direccion, $data->nu_telefono,$data->nu_celular,$data->no_contacto,$data->email,$data->nu_cuenta_uno,$data->nu_cuenta_dos,$data->no_observacion,$data->id_proveedor))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_commit->execute();						
			$response_json['success']=true;
			$response_json['affected_rows']=$init_commit->affected_rows;
			$response_json['error_mysql']=$init_commit->error;
			if($init_commit->affected_rows!=1){
				$response_json['messages']="No se realizaron cambios en el registro";
				break;
			}
			$response_json['messages']="El registro fue guardaron satisfactoriamente";
			$OConex->commit();
        break;
		case 'consultar':
			if(!isset($data->id_proveedor) || empty($data->id_proveedor))
				break;
			$sql=$OProveedor->consultar();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id_proveedor))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=TRUE;
			$response_json['num_rows']=$result->num_rows;
			if($response_json['num_rows']!=1)
				break;
			$response_json['rows']=$result->fetch_assoc();	
			$response_json['rows']['co_depart'] = substr($response_json['rows']['id_ubigeo'],-6, 2);
            $response_json['rows']['co_provin']= substr($response_json['rows']['id_ubigeo'],-4, 2);
            $response_json['rows']['co_distri']= substr($response_json['rows']['id_ubigeo'],-2, 2);		
			$response_json['messages']="Se encontro el registro con exito";
		break;
        case 'eliminar':
			if(!isset($data->id) || empty($data->id))
				break;
			$sql=$OProveedor->deleteProveedor();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$response_json['success']=TRUE;
			$response_json['affected_rows']=$init_stmt->affected_rows;
			$response_json['messages']=($init_stmt->affected_rows==1)?"Se elimino con éxito el registro":"";
			if($init_stmt->affected_rows!=1 && $init_stmt->errno!=0)
				$response_json['messages']=errorMySQL($init_stmt->errno);
		break;
		case 'listarProveedor':
			$response_json=array("totalItemCount"=>0, "numberOfPages"=>0, "rows"=>array(), "success"=>false);
			$name_method='listarProveedor';
			$sql=call_user_func(array($OProveedor, $name_method));
			$store_params=array(0=>'');
			$sql=sprintf($sql);
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			}
			$init_stmt->execute();
			$result_rows=$init_stmt->get_result();
			$response_json['totalItemCount']=$result_rows->num_rows;
			if(isset($data->start, $data->number))
				$response_json['numberOfPages']=ceil($result_rows->num_rows/$data->number);
			else
				$response_json['numberOfPages']=1;
			$response_json['success']=true;
			if($result_rows->num_rows==0)
				break;
			$item=1;
			while($row=$result_rows->fetch_object()){
				array_push($response_json['rows'], array("id"=>$row->id, "no_proveedor"=>$row->no_proveedor, "item"=>$item++ ) );
			}
		break;
		case 'status':
			if(!isset($data->id, $data->status) || empty($data->id))
				break;
			$sql=$OSucursal->consultar();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['num_rows']=$result->num_rows;
			if($result->num_rows==0){
				$response_json['messages']="Registro no se encuentra en el sistema";
				break;
			}
			$row=$result->fetch_object();
			if($row->status!=$data->status){
				$response_json['success']=TRUE;
				$response_json['messages']="Otro usuario realizo el cambio de status";
				break;
			}
			$status=($data->status=='0')?'1':'0';
			$sql=$OSucursal->changeStatus();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('si', $status, $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$response_json['affected_rows']=$init_stmt->affected_rows;
			$response_json['success']=TRUE;
			$response_json['messages']=($init_stmt->affected_rows==0)?"No se realizo cambio en el status": "Se realizo satisfactoriamente el cambio de status";
			if($init_stmt->affected_rows==1)
				$response_json['rows']['status']=$status;
		break;
	}
	echo json_encode($response_json);
}catch(Exception $e){
	echo $e->getOutMsg();
}
?>