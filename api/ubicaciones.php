<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
	$response_json	=array('success'=>false, 'num_rows'=>-1, 'rows'=>array(), "messages"=>"Estas intentando algo inusual en el sistema");
	require_once("./class/GLibfunciones.php");
	$OConex = new GConector();
	$init_stmt=$OConex->stmt_init();
	$data = json_decode(file_get_contents('php://input'));
	switch($_GET['oper']){
		case 'listar':
			$sql="SELECT * FROM ubicacion WHERE status='1'";
            if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['num_rows']=$result->num_rows;
			$response_json['success']=true;
			$i=0;
			$response_json['sql']=$sql;
			while($rows=$result->fetch_assoc()){
				array_push($response_json['rows'], array_merge($rows, array("id"=>++$i)));
			}
		break;
	}
	echo json_encode($response_json);
}catch(Exception $e){
	echo $e->getOutMsg();
}
?>