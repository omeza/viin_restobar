<?php
session_start();
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
	$response_json	=array('success'=>false, 'num_rows'=>-1, 'rows'=>array(), "messages"=>"Estas intentando algo inusual en el sistema");
	require_once("./class/GLibfunciones.php");
	$OConex = new GConector();
	$init_stmt=$OConex->stmt_init();
	$OEmpleado = new GEmpleado();
	$data = json_decode(file_get_contents('php://input'));
	switch($_GET['oper']){
		case 'listar':
            $store_params=array(0=>'');            
			if(isset($data->predicateObject)){
				foreach($data->predicateObject as $fields => $value){
					$store_params[0].='s';
					${$fields}=sprintf("%%%s%%",$value);
					$store_params[]=&${$fields};
					$OEmpleado->addFilter($fields);
				}
			}
			$sql=$OEmpleado->listar();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=true;
			$response_json['totalItemCount']=$result->num_rows;
			if($response_json['totalItemCount']==0)
				break;
			if(isset($data->start, $data->number)){
				$response_json['numberOfPages']=ceil($result->num_rows/$data->number);
				$Opagination=new GPagination();
				$Opagination->setInit($data->start);
				$Opagination->setLimit($data->number);
				$sql=$Opagination->prepareSQL($sql);
			}
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$i=(isset($data->start))?$data->start:0;
			while($rows=$result->fetch_assoc()){
				array_push($response_json['rows'], array_merge($rows, array("item"=>++$i)));
			}
		break;
		case 'guardar':
			if(!isset($data->id_tipo_documento, $data->id_sexo, $data->id_estado_civil, $data->nu_documento, $data->no_apepat, $data->no_nombres))
			break;		
			$OConex->setAutocommit(FALSE);
			$init_commit=$OConex->stmt_init();	
			
			$method=(empty($data->id_empleado))?'agregar':'actualizar';
			$data->no_nombres=strtoupper($data->no_nombres);
			$data->no_apepat=strtoupper($data->no_apepat);
			$data->no_apemat=strtoupper($data->no_apemat);
			$fe_ingreso=DateTime::createFromFormat("Y-m-d", $data->fe_ingreso);//Y-m-d\TH:i:s.uP
			$fe_nacimiento=DateTime::createFromFormat("Y-m-d", $data->fe_nacimiento);//Y-m-d\TH:i:s.uP
			$fe_ingreso=$fe_ingreso->format('Y-m-d');
			$fe_nacimiento=$fe_nacimiento->format('Y-m-d');       

			$sql=call_user_func(array($OEmpleado, $method));
			if(!$init_commit->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);			
			if(!$init_commit->bind_param('iiisssssssssi',$data->id_tipo_documento,$data->id_sexo,$data->id_estado_civil,$data->nu_documento,$data->no_apepat,$data->no_apemat,$data->no_nombres,$fe_nacimiento,
			$data->no_direccion,$data->nu_contacto,$data->email,$fe_ingreso,$data->id_empleado))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			
			
			$init_commit->execute();						
			$response_json['success']=true;
			$response_json['affected_rows']=$init_commit->affected_rows;
			$response_json['error_mysql']=$init_commit->error;
			if($init_commit->affected_rows!=1){
				$response_json['messages']="No se realizaron cambios en el registro";
				break;
			}
			$response_json['messages']="El registro fue guardaron satisfactoriamente";
			$OConex->commit();
        break;
		case 'consultar':
			if(!isset($data->id_empleado) || empty($data->id_empleado))
				break;
			$sql=$OEmpleado->getEmpleado();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id_empleado))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=TRUE;
			$response_json['num_rows']=$result->num_rows;
			if($response_json['num_rows']!=1)
				break;
			$response_json['rows']=$result->fetch_assoc();		
			$response_json['messages']="Se encontro el registro con exito";
		break;
		case 'eliminar':
			if(!isset($data->id) || empty($data->id))
				break;
			$sql=$OEmpleado->eliminar();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$response_json['success']=TRUE;
			$response_json['affected_rows']=$init_stmt->affected_rows;
			$response_json['messages']=($init_stmt->affected_rows==1)?"Se elimino con éxito el registro":"";
			if($init_stmt->affected_rows!=1 && $init_stmt->errno!=0)
				$response_json['messages']=errorMySQL($init_stmt->errno);
		break;
	}
	echo json_encode($response_json);
}catch(Exception $e){
	echo $e->getOutMsg();
}
?>