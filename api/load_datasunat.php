<?php
ini_set('max_execution_time', 30000000);
$estado=array();
$condicion=array();
require_once("./class/GLibfunciones.php");
$OConex = new GConector();
$init_stmt=$OConex->stmt_init();
$SQL="INSERT IGNORE INTO data_sunat (ruc, razon_social, status, ubigeo, direccion) VALUES (?, ?, ?, ?, ?)";
if(!$init_stmt->prepare($SQL))
    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $oConector->error, $oConector->errno);
for($i=1; $i<=1;$i++){
    $name_file=sprintf("./padron_reducido_ruc%d.txt", $i);
    $gestor=fopen($name_file, 'r') or die("Imposible leer el archivo ".$name_file);
    if($gestor){
        $x=0;
        while(($line=fgets($gestor))!==FALSE){
            $data=explode("|", $line);
            if(($i==1 && $x++==0) || $x<185937 || $data[2]!="ACTIVO")
                continue;
            $direccion=sprintf("%s %s %s %s %s", $data[5], $data[6], $data[7], $data[8], $data[9]);
            if(!$init_stmt->bind_param('sssss', $data[0], $data[1], $data[2], $data[4], $direccion))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
            $init_stmt->execute();
        }
    }
    fclose($gestor);
}
?>