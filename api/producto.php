<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
	$response_json	=array('success'=>false, 'num_rows'=>-1, 'rows'=>array(), "messages"=>"Estas intentando algo inusual en el sistema");
	require_once("./class/GLibfunciones.php");
	$OConex = new GConector();
	$OProducto = new GProducto();
	$init_stmt=$OConex->stmt_init();
	$data = json_decode(file_get_contents('php://input'));
	switch($_GET['oper']){
		case 'listar':
			$store_params=array(0=>'');
			if(isset($data->predicateObject)){
				foreach($data->predicateObject as $fields => $value){
					$store_params[0].='s';
					${$fields}=sprintf("%%%s%%",$value);
					$store_params[]=&${$fields};
					var_dump($fields);
					$OProducto->addFilter($fields);
				}
			}
			$sql=$OProducto->listarProducto();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
            $result=$init_stmt->get_result();
			$response_json['success']=true;
			$response_json['totalItemCount']=$result->num_rows;
			if($response_json['totalItemCount']==0)
				break;
			if(isset($data->start, $data->number)){
				$response_json['numberOfPages']=ceil($result->num_rows/$data->number);
				$Opagination=new GPagination();
				$Opagination->setInit($data->start);
				$Opagination->setLimit($data->number);
				$sql=$Opagination->prepareSQL($sql);
			}
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$i=(isset($data->start))?$data->start:0;
			while($rows=$result->fetch_assoc()){
				array_push($response_json['rows'], array_merge($rows, array("num"=>++$i)));
			}
			
		break;
		case 'listarInsumosProducto':
			$sql=$OProducto->getInsumosProducto();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=true;
			$response_json['totalItemCount']=$result->num_rows;
			if($response_json['totalItemCount']==0)
				break;
			if(isset($data->start, $data->number)){
				$response_json['numberOfPages']=ceil($result->num_rows/$data->number);
				$Opagination=new GPagination();
				$Opagination->setInit($data->start);
				$Opagination->setLimit($data->number);
				$sql=$Opagination->prepareSQL($sql);
			}
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$i=(isset($data->start))?$data->start:0;
			while($rows=$result->fetch_assoc()){
				array_push($response_json['rows'], array_merge($rows, array("item"=>++$i)));
			}
		break;
		case 'listarProductoxCategoria':
			$idSucursal = $_SESSION['idSucursal'];
			if ($data->id != 24) {
				$method=(empty($data->id))?'getProductosxCategoriaTotal':'getProductosxCategoriaUnidad';
			}else{
				$method='getProductosMenu';
			}
			

			$sql=call_user_func(array($OProducto, $method));
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			
			
			if(!$init_stmt->bind_param('ii',$idSucursal, $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=true;
			$response_json['totalItemCount']=$result->num_rows;
			if($response_json['totalItemCount']==0)
				break;
			if(isset($data->start, $data->number)){
				$response_json['numberOfPages']=ceil($result->num_rows/$data->number);
				$Opagination=new GPagination();
				$Opagination->setInit($data->start);
				$Opagination->setLimit($data->number);
				$sql=$Opagination->prepareSQL($sql);
			}
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
				if(!$init_stmt->bind_param('ii',$idSucursal, $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$i=(isset($data->start))?$data->start:0;
			while($rows=$result->fetch_assoc()){
				array_push($response_json['rows'], array_merge($rows, array("item"=>++$i)));
			}
		break;
		case 'comboProducto':
			$response_json=array("totalItemCount"=>0, "numberOfPages"=>0, "rows"=>array(), "success"=>false);
			$name_method='comboProducto';
			$sql=call_user_func(array($OProducto, $name_method));
			$store_params=array(0=>'');
			$sql=sprintf($sql);
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			}
			$init_stmt->execute();
			$result_rows=$init_stmt->get_result();
			$response_json['totalItemCount']=$result_rows->num_rows;
			if(isset($data->start, $data->number))
				$response_json['numberOfPages']=ceil($result_rows->num_rows/$data->number);
			else
				$response_json['numberOfPages']=1;
			$response_json['success']=true;
			if($result_rows->num_rows==0)
				break;
			$item=1;
			while($row=$result_rows->fetch_object()){
				array_push($response_json['rows'], array("id"=>$row->id, "no_producto"=>$row->no_producto, "item"=>$item++ ) );
			}
		break;
		
		case 'consultarV':
			if(!isset($data->id) || empty($data->id))
				break;
			$sql=$OProducto->getProducto();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=TRUE;
			$response_json['num_rows']=$result->num_rows;
			if($response_json['num_rows']!=1)
				break;
			$response_json['rows']= $result->fetch_assoc();
			$response_json['messages']="Se encontro el registro con exito";
		break;

		case 'sucursalProducto':
			if(!isset($data->id) || empty($data->id))
				break;
			$sql=$OProducto->getSucursalProducto();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=TRUE;
			$response_json['totalItemCount']=$result->num_rows;
			if($response_json['totalItemCount']==0)
				break;
			$i=0;
			while($rows=$result->fetch_assoc()){
				array_push($response_json['rows'], array_merge($rows, array("item"=>++$i)));
			}
			$response_json['messages']="Se encontro el registro con exito";
		break;
		
	}
	echo json_encode($response_json);
}catch(Exception $e){
	echo $e->getOutMsg();
}
?>