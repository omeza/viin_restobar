<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
$data = json_decode(file_get_contents('php://input'));
$response_json	=array('success'=>false, 'auth'=>false, 'admin'=>false, 'rows'=>array(), "messages"=>"Estas intentando algo inusual en el sistema");
if(isset($data->username, $data->clave) && (!empty($data->username) && !empty($data->clave) )){
	require_once("./class/GLibfunciones.php");
	$OConex = new GConector();
    $init_stmt=$OConex->stmt_init();    
    $data->clave=md5(sha1($data->clave));    
    $sql="SELECT u.id_usuario, e.id_tipo_documento, e.nu_documento, e.no_apepat, e.no_apemat, e.no_nombres,e.email, u.id_rol_sistema ,  u.id_sucursal , u.id_caja FROM tb_usuario AS u LEFT OUTER JOIN tb_empleado AS e ON e.id_empleado=u.id_empleado AND e.status='1' LEFT OUTER JOIN tc_tipo_documento AS td ON td.id_tipo_documento=e.id_tipo_documento LEFT OUTER JOIN rol_sistema AS rs ON rs.id_rol_sistema=u.id_rol_sistema WHERE u.status='1' AND u.login=? ";
    $sql="SELECT u.id_usuario, e.id_tipo_documento, e.nu_documento, TRIM(CONCAT_WS(' ', e.no_apepat, e.no_apemat)) AS apellidos, e.no_nombres, e.email, u.id_rol_sistema, rs.no_rol_sistema, ws.no_win_sistemas , u.id_sucursal , u.id_caja FROM tb_usuario AS u LEFT OUTER JOIN rol_sistema AS rs ON rs.id_rol_sistema=u.id_rol_sistema LEFT OUTER JOIN win_sistemas AS ws ON ws.id_win_sistemas=rs.state_default LEFT OUTER JOIN tb_empleado AS e ON e.id_empleado=u.id_empleado AND e.status='1'  WHERE u.status='1' AND u.login=? AND u.password=?";
    if(!$init_stmt->prepare($sql))
        throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
    if(!$init_stmt->bind_param('ss', $data->username, $data->clave))
        throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
    $init_stmt->execute();
    $result=$init_stmt->get_result(); 
    $response_json['success']=true;
    $response_json['auth']=($result->num_rows==1);   
    if($response_json['auth']){
        $response_json['rows']=$result->fetch_assoc();
        $response_json['rows']['states']=array();
        $sql_state="SELECT ws.no_win_sistemas FROM detalle_rol AS dr LEFT OUTER JOIN win_sistemas AS ws ON ws.id_win_sistemas=dr.id_win_sistemas WHERE dr.id_rol_sistema=?";
        if(!$init_stmt->prepare($sql_state))
            throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
        if(!$init_stmt->bind_param('i', $response_json['rows']['id_rol_sistema']))
            throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
        $init_stmt->execute();
        $result_states=$init_stmt->get_result();
        if($result_states->num_rows>0){
            while($row_rol=$result_states->fetch_assoc()){
                array_push($response_json['rows']['states'], $row_rol['no_win_sistemas']);
            }
        }
        $response_json['token']=GAuth::SignIn(['id'=>$response_json['rows']['id_usuario'],'username'=>$data->username,'idrol'=>$response_json['rows']['id_rol_sistema']]);
        
        $_SESSION['idUsuario']=$response_json['rows']['id_usuario'];
        $_SESSION['idSucursal']= $response_json['rows']['id_sucursal'];
        $_SESSION['idCaja']=$response_json['rows']['id_caja'];
        $_SESSION['token_Bearer']=$response_json['token'];
    }
  
    
    $response_json['messages']=(!$response_json['auth'])?"Los datos de acceso no coinciden en el sistema":"";
}
echo json_encode($response_json);
?> 