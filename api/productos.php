<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin:*');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE,OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

$a_events=array('guardar_producto');
if(isset($_REQUEST['oper']) && in_array($_REQUEST['oper'], $a_events)){
	try{	
    $response_json=array('success'=>false, 'num_rows'=>0, 'option'=>array(), "debug"=>false,'message'=>"Error Inesperado");
    require_once("./class/GLibfunciones.php");
    $OConex = new GConector();
    $OProducto = new GProducto();
    $init_stmt=$OConex->stmt_init();
    $data = json_decode(file_get_contents('php://input'));
		switch($_REQUEST['oper']){
      case 'guardar_producto':        
      
        if(!isset($_POST['orden']['no_producto'], $_POST['orden']['nu_precio'], $_POST['orden']['id_categoria_producto'], $_POST['orden']['id_destino']) )
        break;
        
      $OConex->setAutocommit(FALSE);
      $init_commit=$OConex->stmt_init();
      $method=(empty($_POST['orden']['id_producto']))?'guardarCabecera':'actualizarCabecera';
      $no_archivo=""; 
      if($_FILES == NULL){   
        $no_archivo=$_POST['file']; 
      } else{
        $no_archivo="imagenes/";
        $no_archivo.=$_FILES['file']['name']; 
      }  
     
      if($method== 'actualizarCabecera'){    
                
                $sql = $OProducto->geteliminarProductoInsumo();
                
                if (!$init_stmt->prepare($sql))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
                if (!$init_stmt->bind_param('i', $_POST['orden']['id_producto']))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
                $init_stmt->execute();
                $response_json['success'] = TRUE;
                $response_json['affected_rows'] = $init_stmt->affected_rows;
                $response_json['messages'] = ($init_stmt->affected_rows == 1) ? "Se elimino con éxito el registro" : "";
                if ($init_stmt->affected_rows != 1 && $init_stmt->errno != 0)
                    $response_json['messages'] = errorMySQL($init_stmt->errno);
                    
                $sql = $OProducto->geteliminarProductoSucursal();
                if (!$init_stmt->prepare($sql))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
                if (!$init_stmt->bind_param('i', $_POST['orden']['id_producto']))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
                $init_stmt->execute();
                $response_json['success'] = TRUE;
                $response_json['affected_rows'] = $init_stmt->affected_rows;
                $response_json['messages'] = ($init_stmt->affected_rows == 1) ? "Se elimino con éxito el registro" : "";
                if ($init_stmt->affected_rows != 1 && $init_stmt->errno != 0)
                    $response_json['messages'] = errorMySQL($init_stmt->errno);
      }  

   
      $sql=call_user_func(array($OProducto, $method));
      if(!$init_commit->prepare($sql))
        throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
       
        if(!$init_commit->bind_param('iiisdsi', $_POST['orden']['id_categoria_producto'],$_POST['orden']['id_destino'], $_POST['orden']['id_usuario'], $_POST['orden']['no_producto'], $_POST['orden']['nu_precio'], $no_archivo,$_POST['orden']['id_producto']))
        throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
      $init_commit->execute();
      if($init_commit->affected_rows!=1)
        break;
      if($method== 'actualizarCabecera'){    
        $id_producto=$_POST['orden']['id_producto'];
      }else{
        $id_producto=$init_commit->insert_id;
      }  
      if($_FILES != NULL){
            $file = $_FILES['file']['tmp_name'];
            $folder="./imagenes/";
            $name=$_FILES['file']['name'];
            if (!move_uploaded_file($file, $folder . $name)) {
              $response_json['message']='no se pudo guardar la imagen';
            }
      }
      
      
      $add_commit=$OConex->stmt_init();
      $methodd="agregarDetalle";
      $sql=call_user_func(array($OProducto, $methodd));
      if(!$add_commit->prepare($sql))
        throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
      foreach($_POST['detalle'] as $i => $item){
        if(!$add_commit->bind_param('iid',  $id_producto, $item['id_insumo'], $item['nu_cantidad']))
          throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
        $add_commit->execute();
        $response_json['messages']=sprintf("%s: %d", "ADD INVENTARIO", $add_commit->affected_rows);
        if($add_commit->affected_rows!=1){
          $response_json['messages']=sprintf("%s: %d", "error INVENTARIO", $id_producto);
          break 2;
        }
      }
      $methoddd="agregarSucursal";
      $sql=call_user_func(array($OProducto, $methoddd));
      if(!$add_commit->prepare($sql))
        throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
      foreach($_POST['detalleLocal'] as $i => $item){
        if(!$add_commit->bind_param('ii',  $id_producto, $item))
          throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
        $add_commit->execute();
        $response_json['messages']=sprintf("%s: %d", "ADD INVENTARIO", $add_commit->affected_rows);
        if($add_commit->affected_rows!=1){
          $response_json['messages']=sprintf("%s: %d", "error INVENTARIO", $id_producto);
          break 2;
        }
      }
  
      $response_json['messages']="Se registro la compra satisfactoriamente";
      $response_json['success']=TRUE;
      $response_json['affected_rows']=$init_commit->affected_rows;
      $OConex->commit();
      break;
      
		}
		echo json_encode($response_json);
	}catch(Exception $e){
		echo $e->getOutMsg();
	}
}