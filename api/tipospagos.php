<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
	$response_json	=array('success'=>false, 'num_rows'=>-1, 'data'=>array(), "msg"=>"Estas intentando algo inusual en el sistema");
	require_once("./class/GLibfunciones.php");
	$OConex = new GConector();
	$init_stmt=$OConex->stmt_init();
	$data = json_decode(file_get_contents('php://input'));
	switch($_GET['oper']){
		case 'listar':
            $sql="SELECT * FROM tipo_pago";
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $result=$init_stmt->get_result();
            $response_json['success']=true;
            $response_json['num_rows']=$result->num_rows;
            if($result->num_rows==0)
                break;
            while($row=$result->fetch_object()){
                array_push($response_json['data'], array("id"=>$row->id_tipo_pago, "descripcion"=>$row->desc_tipopago, "esp_banco"=>$row->especif_banco, "esp_serial"=>$row->especif_serial, "esp_request"=>$row->especif_request, "isdefault"=>$row->isdefault) );
            }
		break;
	}
	echo json_encode($response_json);
}catch(Exception $e){
	echo $e->getOutMsg();
}
?>