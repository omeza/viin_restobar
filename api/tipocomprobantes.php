<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
	$response_json	=array('success'=>false, 'num_rows'=>-1, 'data'=>array(), "msg"=>"Estas intentando algo inusual en el sistema");
	require_once("./class/GLibfunciones.php");
	$OConex = new GConector();
	$init_stmt=$OConex->stmt_init();
	$data = json_decode(file_get_contents('php://input'));
	switch($_GET['oper']){
		case 'listar':
		$response_json=array("totalItemCount"=>0, "numberOfPages"=>0, "rows"=>array(), "success"=>false);
		$sql="SELECT * FROM tc_tipo_comprobante WHERE co_tipo_comprobante IN ('01', '03', '10')";
		$store_params=array(0=>'');
		$sql=sprintf($sql);
		if(!$init_stmt->prepare($sql))
			throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
		if(count($store_params)>1){
			if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
		}
		$init_stmt->execute();
		$result_rows=$init_stmt->get_result();
		$response_json['totalItemCount']=$result_rows->num_rows;
		if(isset($data->start, $data->number))
			$response_json['numberOfPages']=ceil($result_rows->num_rows/$data->number);
		else
			$response_json['numberOfPages']=1;
		$response_json['success']=true;
		if($result_rows->num_rows==0)
			break;
		$item=1;
		while($row=$result_rows->fetch_object()){
			array_push($response_json['rows'], array("id"=>$row->id_tipo_comprobante, "codigo"=>$row->co_tipo_comprobante, "descripcion"=>$row->no_tipo_comprobante, "item"=>$item++ ) );
		}
	break;
	}
	echo json_encode($response_json);
}catch(Exception $e){
	echo $e->getOutMsg();
}
?>