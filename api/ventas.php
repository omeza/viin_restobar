<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin:*');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE,OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
$a_events=array('envioCorreo');
if(isset($_POST['oper']) && in_array($_POST['oper'], $a_events)){
    try{
      require_once("./class/GLibfunciones.php");
      require_once("./class/GConector.php");   
    $response_json=array('success'=>false, 'num_rows'=>0, 'option'=>array(), "debug"=>false);
		switch($_POST['oper']){ 
        case 'envioCorreo':
             $response_json=array("error"=>false, "message"=>'error encontrado', "success"=>false);
             $xml='';
             $pdf='';
             $cdr='';
             $moneda=array(8=>'USD', 7=>'PEN');
            $ruc_tlm='20605177612';
            $no_correo= $_POST['info']['email'];
            $cliente=$_POST['info']['no_cliente'];
            $ruc=$_POST['info']['nu_documento'];
            $id_tipocompro=$_POST['info']['id_tipo_comprobante'];
            $tipo=$_POST['info']['no_tipo_comprobante'];
            $serie=$_POST['info']['nu_serie'];
            $correlativo=$_POST['info']['nu_correlativo'];
            $numero= $serie.'-'.$correlativo;
            $monto=$_POST['info']['nu_total'];
            $fe_emision=$_POST['info']['fe_emision'];
             $codepdf=$ruc_tlm.'-0'.$id_tipocompro.'-'.$numero.'.pdf';
             $codexml=$ruc_tlm.'-0'.$id_tipocompro.'-'.$numero.'.xml';
             $codecdr='R-'.$ruc_tlm.'-0'.$id_tipocompro.'-'.$numero.'.zip';
             $path  = './api-viin/files/'; 
             $files = array_diff(scandir($path), array('.', '..')); 
             foreach($files as $file){
                if($codepdf == $file){
                   $pdf= $path.$file;
                    break;
                }          
            }
            foreach($files as $file){
                if($codexml == $file){
                    $xml= $path.$file;
                     break;
                 }
               
            }
            foreach($files as $file){
                if($codecdr== $file){
                    $cdr= $path.$file;
                     break;
                 }
               
            }
               //email variables
                $to = $no_correo;
                $from = 'omeza@solucionestecnologicasviin.com';
                $from_name = 'SOLUCIONES TECNOLOGIAS VIIN S.A.C.';
                $archivos = array($pdf,$xml,$cdr);
                $subject = 'Facturacion Electronica'; 
                      //email body content
                      $htmlContent =sprintf('<div>
                      <table border="0" width="600" cellspacing="0" align="center">
                      <tbody>
                      <tr>
                      <td>
                      <p style="text-align: left; font-size: 15px; font-family: Verdana,Geneva,sans-serif; color: #234c9f; margin: 0; line-height: 21px;">Estimado Cliente,</p>
                      <p style="text-align: left; font-size: 15px; font-family: Verdana,Geneva,sans-serif; color: #234c9f; margin: 0; line-height: 21px;">Sr(es):%s</p>
                      <p style="text-align: left; font-size: 15px; font-family: Verdana,Geneva,sans-serif; color: #234c9f; margin: 0; line-height: 21px;">RUC:%s</p>
                      </td>
                      </tr>
                      <tr>
                      <td>
                      <p style="text-align: left; font-size: 13px; font-family: Verdana,Geneva,sans-serif; color: #234c9f; margin: 0; line-height: 21px;">Le Informamos a usted que el Documento: %s, Ya se encuentra Disponible</p>
                      </td>
                      </tr>
                      <tr>
                      <td>
                      <div style="text-align: justify; font-size: 15px; padding: 5px; line-height: 18px; font-family: Verdana,Geneva,sans-serif; color: #7c7c7c;">
                      <div>
                      <p>Tipo: %s</p>
                      <p>Numero: %s</p>
                      <p>Monto: %s</p>
                      <p>Fecha Emisi&ograve;n: %s</p>
                      </div>
                      </div>
                      </td>
                      </tr>
                      <tr>
                      <td>
                      <p style="text-align: left; font-size: 13px; font-family: Verdana,Geneva,sans-serif; color: #234c9f; margin: 0; line-height: 21px;">Saludos Atentamente.</p>
                      </td>
                      </tr>
                      </tbody>
                      </table>
                      <table border="0" width="600" cellspacing="0" cellpadding="0" align="center">
                      <tbody>
                      <tr style="list-style: none; color: #234c9f; font-family: Verdana,Geneva,sans-serif; font-size: 14px; text-align: center; padding: 0px;">
                      <td colspan="5" align="center" height="40"><a target="_blank" rel="noopener"><img src="http://i1203.photobucket.com/albums/bb389/Brahman_PRO/sector-logo.jpeg" alt="TLM" border="0" /></a></td>
                      </tr>
                      <tr style="list-style: none; color: #234c9f; font-family: Verdana,Geneva,sans-serif; font-size: 14px; text-align: center; padding: 0px;">
                      <td colspan="5" align="center" height="40"><span style="font-family: Verdana,Geneva,sans-serif; text-align: center; font-size: 12px; letter-spacing: 12px; color: #9d9797;"><a href="http://solucionestecnologicasviin.com" target="_blank" rel="noopener">www.solucionestecnologicasviin.com</a></span></td>
                      </tr>
                      </tbody>
                      </table>
                      <hr />
                      <div><span style="color: #555555; font-family: Helvetica, sans-serif; font-size: xx-small;">Este correo fue enviado por <strong>VIIN</strong></span></div>
                      <div><span style="color: #555555; font-family: Helvetica, sans-serif; font-size: xx-small;">Ar&egrave;a de Sistemas.<br /> </span></div>
                      <img style="display: none!important; max-height: 0; width: 0; line-height: 0;" alt="" width="0" height="0" border="0" /></div>',  $cliente, $ruc, $numero,$tipo,$numero,$monto,$fe_emision);
                //call multi_attach_mail() function and pass the required arguments

                $send_email = multi_attach_mail($to,$subject,$htmlContent,$from,$from_name,$archivos);
                $send_email?true:false;
                if ($send_email==true) {
                    $response_json['message']="<h4>El correo se ha Enviado Exitosamente al Destinatario $no_correo</h4>";
                    $response_json['success']=true;
                }else{
                    $response_json['message']="<h4>El Envio del Correo ha Fallado Consulte con Soporte Tecnico.</h4>";
                    $response_json['success']=false;
                }
             break;
            }
            echo json_encode($response_json);
            //sqlsrv_close($conexion);
        }catch(Exception $e){
            echo $e->getOutMsg();
        }
}
?>