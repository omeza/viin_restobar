<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
	$response_json	=array('success'=>false, 'num_rows'=>-1, 'rows'=>array(), "messages"=>"Estas intentando algo inusual en el sistema");
	require_once("./class/GLibfunciones.php");
	$OConex = new GConector();
	$init_stmt=$OConex->stmt_init();
	$OTipdocumento = new GTipodocumentos();
	$data = json_decode(file_get_contents('php://input'));
	switch($_GET['oper']){
		case 'tiposalida':
			$response_json=array("totalItemCount"=>0, "numberOfPages"=>0, "rows"=>array(), "success"=>false);
			$name_method='ListarTipoSalida';
			$sql=call_user_func(array($OTipdocumento, $name_method));
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result_rows=$init_stmt->get_result();
			$response_json['totalItemCount']=$result_rows->num_rows;
			if(isset($data->start, $data->number))
				$response_json['numberOfPages']=ceil($result_rows->num_rows/$data->number);
			else
				$response_json['numberOfPages']=1;
			$response_json['success']=true;
			if($result_rows->num_rows==0)
				break;
			$item=1;
			while($row=$result_rows->fetch_object()){
				array_push($response_json['rows'], array("id"=>$row->id_tipo_salida, "desc_tiposalida"=>$row->desc_tiposalida) );
			}
		break;
		case 'eliminar':
			if(!isset($data->id) || empty($data->id))
				break;
			$sql=$OTipdocumento->borrarTDocumento();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$response_json['success']=TRUE;
			$response_json['affected_rows']=$init_stmt->affected_rows;
			$response_json['messages']=($init_stmt->affected_rows==1)?"Se elimino con éxito el registro":"";
			if($init_stmt->affected_rows!=1 && $init_stmt->errno!=0)
				$response_json['messages']=errorMySQL($init_stmt->errno);
		break;
		case 'status':
			if(!isset($data->id, $data->status) || empty($data->id))
				break;
			$sql=$OTipdocumento->getTipoDocumentos();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['num_rows']=$result->num_rows;
			if($result->num_rows==0){
				$response_json['messages']="Registro no se encuentra en el sistema";
				break;
			}
			$row=$result->fetch_object();
			if($row->status!=$data->status){
				$response_json['success']=TRUE;
				$response_json['messages']="Otro usuario realizo el cambio de status";
				break;
			}
			$status=($data->status=='0')?'1':'0';
			$sql=$OTipdocumento->updateStatusTDocumeno();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('si', $status, $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$response_json['affected_rows']=$init_stmt->affected_rows;
			$response_json['success']=TRUE;
			$response_json['messages']=($init_stmt->affected_rows==0)?"No se realizo cambio en el status": "Se realizo satisfactoriamente el cambio de status";
			if($init_stmt->affected_rows==1)
				$response_json['rows']['status']=$status;
		break;
		case 'listar':
			$response_json=array("totalItemCount"=>0, "numberOfPages"=>0, "rows"=>array(), "success"=>false);
			$name_method=(!isset($data->idcomprobantes))?'listarTipodocumentos':'getTipDocumento4comprobantes';
			$sql=call_user_func(array($OTipdocumento, $name_method));
			$add_filter="";
			$store_params=array(0=>'');
			$sql=sprintf($sql, $add_filter);
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			}
			$init_stmt->execute();
			$result_rows=$init_stmt->get_result();
			$response_json['totalItemCount']=$result_rows->num_rows;
			if(isset($data->start, $data->number))
				$response_json['numberOfPages']=ceil($result_rows->num_rows/$data->number);
			else
				$response_json['numberOfPages']=1;
			$response_json['success']=true;
			if($result_rows->num_rows==0)
				break;
			$item=1;
			while($row=$result_rows->fetch_object()){
				array_push($response_json['rows'], array("id"=>$row->id_tipo_documento, "nombre"=>$row->no_tipo_documento, "abreviatura"=>$row->no_abreviatura, "status"=>$row->status, "predeterminado"=>$row->il_predeterminado, "item"=>$item++ ) );			
			}
		break;
		case 'consultar':
			if(!isset($data->id) || empty($data->id))
				break;
			$sql=$OTipdocumento->consultDocumentoCliente();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result_stmt=$init_stmt->get_result();
			$response_json['num_rows']=$result_stmt->num_rows;
			$response_json['success']=true;
			if($result_stmt->num_rows!=1)
				break;
			$rows=$result_stmt->fetch_assoc();
			$response_json['rows']=array_combine(array("id_tipdoc", "desc_documento", "predeterminado", "status"), array_values($rows));
		break;
		case 'guardar':
			if(!isset($data->id_tipdoc, $data->desc_documento) || empty($data->desc_documento))
				break;
			$data->desc_documento=strtoupper($data->desc_documento);
			$predeterminado=(!isset($data->predeterminado))?'0':$data->predeterminado;
			if(empty($data->id_tipdoc))
				$sql=$OTipdocumento->agregarTipoDocumento();
			else
				$sql=$OTipdocumento->actualizarTipoDocumento();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('ssi', $data->desc_documento, $predeterminado, $data->id_tipdoc))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$response_json['success']=true;
			$response_json['affected_rows']=$init_stmt->affected_rows;
			if($init_stmt->affected_rows!=1){
				$response_json['messages']="No se realizaron cambios en el registro";
				break;
			}
			$response_json['messages']="Los datos se guardaron con éxito";
		break;
		case 'existPredet':
			$sql=$OTipdocumento->getTipDocpredterminado();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['num_rows']=$result->num_rows;
			$response_json['success']=true;
			if($response_json['num_rows']!=1){
				$response_json['messages']=($response_json['num_rows']>1)?"Existe un problema en la configuración":"No existe documentos predeterminado";
				break;
			}
			$response_json['data']=$result->fetch_object();
		break;
		case 'existTipDoc':
			$sql=$OTipdocumento->existeTipDocumento();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('si', $data->nombre, $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['num_rows']=$result->num_rows;
			$response_json['success']=true;
		break;
	}
	echo json_encode($response_json);
}catch(Exception $e){
	echo $e;
}
?>