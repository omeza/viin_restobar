-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 04-10-2018 a las 01:44:45
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.0.26

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `id6715088_db_sinven`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`id6715088_root`@`%` PROCEDURE `asign_num_comprobante` (IN `tip_comp` VARCHAR(1), IN `id_boleta` INT, INOUT `num_ticket` VARCHAR(11))  BEGIN
    DECLARE lastnum INT;
    SELECT IFNULL(MAX(CONVERT(idticket, UNSIGNED INT)), '0') INTO lastnum FROM boletas WHERE tipo_comprobante=tip_comp AND idboleta!=id_boleta;
    SET num_ticket=LPAD(lastnum+1, 11, '0');
    UPDATE boletas SET idticket=num_ticket WHERE idboleta=id_boleta;
END$$

CREATE DEFINER=`id6715088_root`@`%` PROCEDURE `autoincidproduct` (IN `idproducto` INT, OUT `codigo` CHAR(10))  BEGIN
	DECLARE lastid INT DEFAULT 0;
	SELECT inicia_codigo INTO lastid FROM configuracion;
    SET codigo = LPAD(TRIM(CONVERT(lastid+1, CHAR(10))) , 10, '0');
    UPDATE configuracion SET inicia_codigo=inicia_codigo+1;
    UPDATE productos SET cod_producto=codigo  WHERE idproductos=idproducto;
END$$

CREATE DEFINER=`id6715088_root`@`%` PROCEDURE `update_stock` (IN `idproducto` INT, IN `cantidad` INT)  NO SQL
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE lcstock INT DEFAULT 0;
	DECLARE stock INT DEFAULT 0;
	DECLARE exist_id INT;
    DECLARE stock_lote INT DEFAULT 0;
    DECLARE idlote INT DEFAULT 0;
    DECLARE fecha_lote DATE DEFAULT NULL;
	DECLARE curs1 CURSOR FOR SELECT stock FROM inventario WHERE idproductos=idproducto;
    DECLARE curs2 CURSOR FOR SELECT di.id_lotes, di.cantidad, STR_TO_DATE(di.fecha_vencimiento, '%Y-%m-%d') AS fec_vencimiento FROM detalle_inventario AS di WHERE di.idproductos=idproducto AND di.status='1' ORDER BY 2 ASC;    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	OPEN curs1;
	SELECT FOUND_ROWS() INTO exist_id ;
	IF exist_id = 1 THEN
    	FETCH NEXT FROM curs1 INTO lcstock;
   		UPDATE inventario SET stock=lcstock+cantidad WHERE idproductos=idproducto;
        OPEN curs2;
		 read_loop: LOOP
         	FETCH curs2 INTO idlote, stock_lote, fecha_lote;
			IF done THEN
      			LEAVE read_loop;
    		END IF;
            IF (stock_lote+cantidad)>=lcstock THEN
                UPDATE detalle_inventario SET status='0' WHERE id_lotes=idlote; 
            END IF;
            LEAVE read_loop;
         END LOOP;
	ELSE
   		INSERT INTO inventario VALUES (idproducto, cantidad);
	END IF;
	CLOSE curs1;
    CLOSE curs2;
END$$

--
-- Funciones
--
CREATE DEFINER=`id6715088_root`@`%` FUNCTION `calcular_neto_precio` (`monto` FLOAT(13,2), `impuesto` FLOAT(13,2), `excento` CHAR(1)) RETURNS FLOAT(13,2) NO SQL
BEGIN 
    DECLARE mto_neto FLOAT(13,2) DEFAULT 0.00;
    IF excento=0 THEN
        SET mto_neto=monto/(1+(impuesto/100));
    END IF;
    RETURN mto_neto;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `almacenes`
--

CREATE TABLE `almacenes` (
  `idalmacen` int(11) NOT NULL,
  `desc_almacen` varchar(30) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `almacenes`
--

INSERT INTO `almacenes` (`idalmacen`, `desc_almacen`, `status`) VALUES
(1, 'ALMACEN PRINCIPAL', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bancos`
--

CREATE TABLE `bancos` (
  `idbancos` int(11) NOT NULL,
  `sigla_banco` varchar(10) NOT NULL,
  `nombre_banco` varchar(45) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `bancos`
--

INSERT INTO `bancos` (`idbancos`, `sigla_banco`, `nombre_banco`, `status`) VALUES
(1, 'BBVA', 'BANCO CONTINENTAL BBVA', '1'),
(2, 'BCP', 'BANCO CREDITO DEL PERU', '1'),
(3, 'BNC', 'BANCO DE COMERCIO', '1'),
(4, 'BANBIF', 'BANCO INTERAMERICANO DE FINANZAS', '1'),
(5, 'BF', 'BANCO FINANCIERO', '1'),
(6, 'CITI', 'CITIBANK PERU', '1'),
(7, 'INTERBANK', 'INTERBANK', '1'),
(8, 'MB', 'MI BANCO', '1'),
(9, 'SCB', 'SCOTIABANK', '1'),
(10, 'BGP', 'BANCO GNB PERU', '1'),
(11, 'FALABELLA', 'BANCO FALABELLA', '1'),
(12, 'RIPLEY', 'BANCO RIPLEY', '1'),
(13, 'BSP', 'BANCO SANTANDER PERU', '1'),
(15, 'CENCOSUD1', 'BANCO CENCOSUD', '1'),
(16, 'ICBC', 'ICBC PERU BANK', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boletas`
--

CREATE TABLE `boletas` (
  `idboleta` int(11) NOT NULL,
  `idticket` varchar(11) DEFAULT NULL,
  `idempleado` int(11) NOT NULL,
  `idclientes` int(11) NOT NULL,
  `fecventa` date NOT NULL,
  `tipo_comprobante` enum('B','F','K') NOT NULL DEFAULT 'K',
  `factura_electronica` enum('0','1') NOT NULL DEFAULT '0',
  `idordencompra` int(11) DEFAULT NULL,
  `pagado` enum('0','1') NOT NULL DEFAULT '1',
  `total` double(13,2) NOT NULL,
  `subtotal` double(13,2) NOT NULL,
  `importe_impuesto` double(13,2) NOT NULL DEFAULT '0.00',
  `mto_impuesto` double(13,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `boletas`
--

INSERT INTO `boletas` (`idboleta`, `idticket`, `idempleado`, `idclientes`, `fecventa`, `tipo_comprobante`, `factura_electronica`, `idordencompra`, `pagado`, `total`, `subtotal`, `importe_impuesto`, `mto_impuesto`) VALUES
(1, '00000000001', 1, 1, '2018-09-15', 'K', '', NULL, '1', 25.00, 21.20, 3.80, 18.00),
(2, '00000000001', 1, 5, '2018-10-01', 'F', '', NULL, '1', 100.00, 84.75, 15.25, 18.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE `cargo` (
  `id_cargo` int(11) NOT NULL,
  `desc_cargo` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`id_cargo`, `desc_cargo`) VALUES
(1, 'ANALISTA Y DESARROLLADOR DE SISTEMA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `idclientes` int(11) NOT NULL,
  `idtip_documento` int(11) NOT NULL,
  `num_documento` varchar(15) NOT NULL,
  `apellidos_nombres` varchar(60) NOT NULL,
  `direccion` text,
  `tlf_hab` varchar(15) DEFAULT NULL,
  `tlf_movil` varchar(15) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`idclientes`, `idtip_documento`, `num_documento`, `apellidos_nombres`, `direccion`, `tlf_hab`, `tlf_movil`, `email`) VALUES
(1, 1, '0', 'CONTADO', 'SIN ESPECIFICAR', '----', '----', '---'),
(3, 1, '16894628', 'RAUL MIRANDA', 'los orumnos 28', '04246840547', '0424680547', 'raulpnfinformatica@gmail.com'),
(4, 5, '10200121975', 'BOTICA OPEN FARMA', 'Psj. 28 de enero # 128', '', '', ''),
(5, 5, '10406600292', 'BOTICA HINOSTROZA', 'av. ricardo palma # 405', '', '', ''),
(6, 5, '10210747520', 'BOTICA UNIVERSAL', 'av. pacheco # 341 - junin - tarma', '', '', ''),
(7, 5, '10205292794', 'BOTICA RAMIREZ FARMA', 'villa rica - pasco - oxapampa', '', '', ''),
(8, 5, '10422075424', 'BOTICA MARIA AUXILIADORA', 'jr. valentin cueva s/n - pasco - oxapampa', '', '', ''),
(9, 5, '10043412316', 'BOTICA NIñO JESUS', 'jr. grau # 108 - pasco - oxapampa', '', '', ''),
(10, 5, '10409158990', 'BOTICAS PERUANAS', 'jr. mullembruck # 463', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `idcompra` int(11) NOT NULL,
  `idord_compra` int(11) DEFAULT NULL,
  `numfactura` varchar(15) NOT NULL,
  `idproveedor` int(11) NOT NULL,
  `fecha_compra` date NOT NULL,
  `idcondicion_pago` int(11) NOT NULL,
  `dias_pago` int(11) NOT NULL DEFAULT '0',
  `fecha_pagar` date DEFAULT NULL,
  `porc_descuento` float(5,2) DEFAULT NULL,
  `subtotal` float(13,2) NOT NULL,
  `mto_impuesto` float(13,2) NOT NULL,
  `total` float(13,2) NOT NULL,
  `pagado` enum('0','1') NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`idcompra`, `idord_compra`, `numfactura`, `idproveedor`, `fecha_compra`, `idcondicion_pago`, `dias_pago`, `fecha_pagar`, `porc_descuento`, `subtotal`, `mto_impuesto`, `total`, `pagado`, `status`) VALUES
(3, 0, '777', 6, '2018-08-25', 1, 0, NULL, 0.00, 1.00, 0.00, 1.00, '1', '0'),
(4, 0, '0001-26020', 11, '2018-08-24', 2, 60, NULL, 0.00, 151.61, 18.19, 169.80, '0', '0'),
(6, 0, '092123', 2, '2018-08-28', 1, 0, NULL, 0.00, 285.71, 34.29, 320.00, '1', '0'),
(7, 0, '001-66654', 9, '2018-09-11', 2, 60, NULL, 0.00, 0.00, 0.00, 0.00, '0', '0'),
(9, 0, '878787', 6, '2018-09-12', 1, 0, NULL, 0.00, 21.00, 4.00, 25.00, '1', '0'),
(10, 0, '5122', 6, '2018-09-24', 2, 60, NULL, 0.00, 0.00, 0.00, 0.00, '0', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `condicion_pago`
--

CREATE TABLE `condicion_pago` (
  `idcondicion_pago` int(11) NOT NULL,
  `desc_condicion` varchar(45) NOT NULL,
  `predeterminado` enum('0','1') NOT NULL DEFAULT '0',
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `condicion_pago`
--

INSERT INTO `condicion_pago` (`idcondicion_pago`, `desc_condicion`, `predeterminado`, `status`) VALUES
(1, 'CONTADO', '1', '1'),
(2, 'CREDITO', '0', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE `configuracion` (
  `porc_impuesto` decimal(5,2) NOT NULL,
  `nombre_impuesto` varchar(8) NOT NULL,
  `iniciar_factura` int(11) DEFAULT NULL,
  `iniciar_boleta` int(11) DEFAULT NULL,
  `nautorizacionimpresa` varchar(12) NOT NULL,
  `imprimir_venta` enum('0','1') NOT NULL DEFAULT '0',
  `name_print` text,
  `id_moneda` int(11) NOT NULL,
  `autoidproducto` enum('0','1') NOT NULL DEFAULT '1',
  `inicia_codigo` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`porc_impuesto`, `nombre_impuesto`, `iniciar_factura`, `iniciar_boleta`, `nautorizacionimpresa`, `imprimir_venta`, `name_print`, `id_moneda`, `autoidproducto`, `inicia_codigo`) VALUES
(18.00, 'I.G.V.', NULL, NULL, '', '0', NULL, 1, '1', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentasbancarias`
--

CREATE TABLE `cuentasbancarias` (
  `idcuentasbancarias` int(11) NOT NULL,
  `idbancos` int(11) NOT NULL,
  `numctabancaria` varchar(20) NOT NULL,
  `idtipocta` int(11) NOT NULL,
  `idmoneda` int(11) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_orden_pedidos`
--

CREATE TABLE `detalles_orden_pedidos` (
  `iddetalles_opedido` int(11) NOT NULL,
  `idordenpedido` int(11) NOT NULL,
  `idproductos` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `mto_ult_compra` decimal(13,2) NOT NULL DEFAULT '0.00',
  `porc_impuesto` decimal(5,2) NOT NULL DEFAULT '0.00',
  `exento_impuesto` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_compras`
--

CREATE TABLE `detalle_compras` (
  `idcompra` int(11) NOT NULL,
  `idproducto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `mto_impuesto` float(5,2) NOT NULL DEFAULT '0.00',
  `excento_impuesto` enum('0','1') NOT NULL DEFAULT '0',
  `precio_unidad` float(13,2) NOT NULL DEFAULT '0.00',
  `total_importe` float(13,2) NOT NULL DEFAULT '0.00',
  `precio_neto` float(13,2) NOT NULL DEFAULT '0.00',
  `tot_impuesto` float(13,2) NOT NULL DEFAULT '0.00',
  `lote` char(10) NOT NULL,
  `fecha_vencimiento` char(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_compras`
--

INSERT INTO `detalle_compras` (`idcompra`, `idproducto`, `cantidad`, `mto_impuesto`, `excento_impuesto`, `precio_unidad`, `total_importe`, `precio_neto`, `tot_impuesto`, `lote`, `fecha_vencimiento`) VALUES
(3, 3, 50, 12.00, '0', 27.30, 1365.00, 24.38, 146.25, '', '2020-10'),
(4, 264, 60, 12.00, '0', 2.83, 169.80, 2.53, 18.19, '666666', '2020-09'),
(6, 262, 60, 12.00, '0', 2.00, 120.00, 1.79, 12.86, '12345', '2020-10'),
(6, 885, 20, 12.00, '0', 10.00, 200.00, 8.93, 21.43, '123456', '2019-09'),
(7, 271, 10, 18.00, '0', 0.00, 0.00, 0.00, 0.00, '1234568', '2020-08'),
(9, 271, 50, 18.00, '0', 0.50, 25.00, 0.42, 4.00, '77777', '2021-06'),
(10, 0, 60, 18.00, '0', 0.00, 0.00, 0.00, 0.00, '123352', '2018-12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_inventario`
--

CREATE TABLE `detalle_inventario` (
  `id_lotes` int(11) NOT NULL,
  `idproductos` int(11) NOT NULL,
  `lote` varchar(20) NOT NULL,
  `fecha_vencimiento` varchar(10) DEFAULT NULL,
  `idalmacen` int(11) DEFAULT NULL,
  `idubicacion` int(11) DEFAULT NULL,
  `pasillo` varchar(5) DEFAULT NULL,
  `stand` varchar(5) DEFAULT NULL,
  `fila` varchar(5) DEFAULT NULL,
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `fec_ingreso` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_inventario`
--

INSERT INTO `detalle_inventario` (`id_lotes`, `idproductos`, `lote`, `fecha_vencimiento`, `idalmacen`, `idubicacion`, `pasillo`, `stand`, `fila`, `cantidad`, `status`, `fec_ingreso`) VALUES
(1, 3, '', '2020-10-01', NULL, NULL, NULL, NULL, NULL, 50, '1', '2018-09-03 00:16:00'),
(2, 262, '12345', '2020-10-01', NULL, NULL, NULL, NULL, NULL, 60, '1', '2018-09-03 00:16:00'),
(3, 264, '666666', '2020-09-30', NULL, NULL, NULL, NULL, NULL, 60, '1', '2018-09-03 00:16:00'),
(4, 271, '1234568', '2020-08-31', NULL, NULL, NULL, NULL, NULL, 10, '1', '2018-09-12 17:22:27'),
(5, 885, '123456', '2019-09-01', NULL, NULL, NULL, NULL, NULL, 20, '1', '2018-09-03 00:16:00'),
(6, 901, '7777', '18-06-2020', 1, NULL, NULL, NULL, NULL, 50, '1', '2018-09-03 00:16:00'),
(7, 902, '87878787', '2020-07-08', 1, NULL, NULL, NULL, NULL, 50, '1', '2018-09-12 17:50:18'),
(8, 1379, '', '', 1, NULL, NULL, NULL, NULL, 15, '1', '2018-09-03 00:33:11'),
(9, 935, '77777', '2020-06-24', NULL, NULL, NULL, NULL, NULL, 50, '1', '2018-09-18 22:30:36'),
(10, 935, '78998989', '2019-01-01', NULL, NULL, NULL, NULL, NULL, 10, '1', '2018-09-18 22:30:36'),
(11, 274, '123546258', '2020-03-03', 1, NULL, NULL, NULL, NULL, 60, '1', '2018-09-19 22:51:46'),
(12, 0, '123352', '2018-12-01', NULL, NULL, NULL, NULL, NULL, 60, '1', '2018-09-25 18:00:25');

--
-- Disparadores `detalle_inventario`
--
DELIMITER $$
CREATE TRIGGER `addstockinicial` BEFORE INSERT ON `detalle_inventario` FOR EACH ROW BEGIN
	UPDATE inventario SET stock = stock + NEW.cantidad WHERE idproductos = NEW.idproductos;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_rol`
--

CREATE TABLE `detalle_rol` (
  `idrol_sistema` int(11) NOT NULL,
  `idwin_state` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_rol`
--

INSERT INTO `detalle_rol` (`idrol_sistema`, `idwin_state`) VALUES
(1, '1'),
(1, '10'),
(1, '11'),
(1, '12'),
(1, '13'),
(1, '14'),
(1, '15'),
(1, '16'),
(1, '17'),
(1, '18'),
(1, '19'),
(1, '20'),
(1, '21'),
(1, '24'),
(1, '25'),
(1, '26'),
(1, '27'),
(1, '28'),
(1, '29'),
(1, '30'),
(1, '31'),
(1, '32'),
(1, '33'),
(1, '34'),
(1, '35'),
(1, '36'),
(1, '37'),
(1, '38'),
(1, '39'),
(1, '40'),
(1, '41'),
(1, '42'),
(1, '43'),
(1, '44'),
(1, '45'),
(1, '46'),
(1, '47'),
(1, '48'),
(1, '49'),
(1, '5'),
(1, '51'),
(1, '52'),
(1, '53'),
(1, '54'),
(1, '55'),
(1, '56'),
(1, '57'),
(1, '6'),
(1, '7'),
(3, '1'),
(3, '10'),
(3, '12'),
(3, '13'),
(3, '14'),
(3, '15'),
(3, '18'),
(3, '19'),
(3, '21'),
(3, '24'),
(3, '25'),
(3, '26'),
(3, '46'),
(3, '48'),
(3, '49'),
(3, '51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_boleta`
--

CREATE TABLE `det_boleta` (
  `idboleta` int(11) NOT NULL,
  `idproductos` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `costo_unidad` double(13,2) NOT NULL,
  `importe_impuesto` float(13,2) NOT NULL,
  `exento_impuesto` enum('1','0') NOT NULL DEFAULT '0',
  `mto_impuesto` double(13,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `det_boleta`
--

INSERT INTO `det_boleta` (`idboleta`, `idproductos`, `cantidad`, `costo_unidad`, `importe_impuesto`, `exento_impuesto`, `mto_impuesto`) VALUES
(1, 274, 10, 2.50, 0.38, '0', 18.00),
(2, 901, 10, 10.00, 1.53, '0', 18.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_ordencompras`
--

CREATE TABLE `det_ordencompras` (
  `idord_compra` int(11) NOT NULL,
  `idproductos` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `mto_unidad` double(13,2) NOT NULL DEFAULT '0.00',
  `exento_impuesto` enum('0','1') NOT NULL DEFAULT '0',
  `mto_impuesto` double(13,2) NOT NULL DEFAULT '0.00',
  `mto_total` double(13,2) NOT NULL DEFAULT '0.00',
  `entregado` enum('0','1') NOT NULL DEFAULT '0',
  `fec_entregado` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_pagoboleta`
--

CREATE TABLE `det_pagoboleta` (
  `idboleta` int(11) NOT NULL,
  `id_tipo_pago` int(11) NOT NULL,
  `mto_pago` double(13,2) NOT NULL DEFAULT '0.00',
  `idbanco` int(11) DEFAULT NULL,
  `num_tarjeta` varchar(18) DEFAULT NULL,
  `num_referencia` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `det_pagoboleta`
--

INSERT INTO `det_pagoboleta` (`idboleta`, `id_tipo_pago`, `mto_pago`, `idbanco`, `num_tarjeta`, `num_referencia`) VALUES
(2, 1, 23.60, NULL, NULL, NULL),
(3, 1, 7.00, NULL, NULL, NULL),
(4, 1, 7.00, NULL, NULL, NULL),
(5, 1, 50.00, NULL, NULL, NULL),
(6, 1, 35.00, NULL, NULL, NULL),
(7, 1, 17.50, NULL, NULL, NULL),
(8, 1, 7.00, NULL, NULL, NULL),
(9, 1, 10.50, NULL, NULL, NULL),
(10, 1, 63.19, NULL, NULL, NULL),
(11, 1, 47.20, NULL, NULL, NULL),
(12, 1, 79.18, NULL, NULL, NULL),
(13, 1, 522.28, NULL, NULL, NULL),
(14, 1, 7.00, NULL, NULL, NULL),
(15, 1, 3.50, NULL, NULL, NULL),
(16, 1, 3.50, NULL, NULL, NULL),
(17, 1, 7.00, NULL, NULL, NULL),
(18, 1, 9.00, NULL, NULL, NULL),
(1, 1, 25.00, NULL, NULL, NULL),
(2, 1, 100.00, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `idempleado` int(11) NOT NULL,
  `idtip_documento` int(11) NOT NULL,
  `num_documento` varchar(15) NOT NULL,
  `apellido_paterno` varchar(45) NOT NULL,
  `apellido_materno` varchar(45) DEFAULT NULL,
  `nombres` varchar(45) NOT NULL,
  `sexo` enum('1','0') NOT NULL DEFAULT '0',
  `fecnacimiento` date NOT NULL,
  `edocivil` enum('C','S','D','V','O') NOT NULL DEFAULT 'S',
  `direccion_hab` text,
  `tlf_movil` varchar(15) DEFAULT NULL,
  `tlf_habitacion` varchar(15) DEFAULT NULL,
  `email` varchar(65) NOT NULL,
  `id_profesion` int(11) DEFAULT NULL,
  `id_cargo` int(11) DEFAULT NULL,
  `fec_ingreso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fec_inactivo` date DEFAULT NULL,
  `visible` enum('1','0') NOT NULL DEFAULT '1',
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`idempleado`, `idtip_documento`, `num_documento`, `apellido_paterno`, `apellido_materno`, `nombres`, `sexo`, `fecnacimiento`, `edocivil`, `direccion_hab`, `tlf_movil`, `tlf_habitacion`, `email`, `id_profesion`, `id_cargo`, `fec_ingreso`, `fec_inactivo`, `visible`, `status`) VALUES
(1, 4, '', 'SISTEMA', '', 'ADMINISTRADOR', '1', '1983-10-24', 'C', '', '', '', 'soporte@sisven-localhost.com', 1, 1, '2018-07-01 00:00:00', NULL, '0', '1'),
(2, 4, '159172202', 'AYBAR', 'DUNO', 'RONALD EDER', '1', '1983-10-24', 'C', 'Jr. Jose Santos Chocano 113', '931612632', NULL, 'read424@gmail.com', 1, 1, '2018-07-01 00:00:00', NULL, '1', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `idtip_documento` int(11) NOT NULL,
  `num_fiscal` varchar(12) NOT NULL,
  `nombre_empresa` varchar(60) NOT NULL,
  `nombre_comercial` varchar(75) DEFAULT NULL,
  `direccion` text NOT NULL,
  `direccion_local` text,
  `tlf_fiscal` varchar(15) DEFAULT NULL,
  `email` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`idtip_documento`, `num_fiscal`, `nombre_empresa`, `nombre_comercial`, `direccion`, `direccion_local`, `tlf_fiscal`, `email`) VALUES
(0, '20600783441', 'PHARMAGROUP', 'PHARMAGROUP S.A.C.', 'JR. ALHELI 345', NULL, NULL, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados_pais`
--

CREATE TABLE `estados_pais` (
  `idestados_pais` int(11) NOT NULL,
  `idpais` int(11) NOT NULL,
  `desc_estados` varchar(45) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estados_pais`
--

INSERT INTO `estados_pais` (`idestados_pais`, `idpais`, `desc_estados`, `status`) VALUES
(1, 104, 'AMAZONAS', '1'),
(2, 104, 'Ã¡NCASH', '1'),
(3, 104, 'APURIMAC', '1'),
(4, 104, 'AREQUIPA', '1'),
(5, 104, 'AYACUCHO', '1'),
(6, 104, 'CAJAMARCA', '1'),
(7, 104, 'CALLAO', '1'),
(8, 104, 'CUZCO', '1'),
(9, 104, 'HUANCAVELICA', '1'),
(10, 104, 'HUANUCO', '1'),
(11, 104, 'ICA', '1'),
(12, 104, 'JUNIN', '1'),
(13, 104, 'LA LIBERTAD', '1'),
(14, 104, 'LAMBAYEQUE', '1'),
(15, 104, 'LIMA', '1'),
(16, 104, 'LORETO', '1'),
(17, 104, 'MADRE DE DIOS', '1'),
(18, 104, 'MOQUEGUA', '1'),
(19, 104, 'PASCO', '1'),
(20, 104, 'PIURA', '1'),
(21, 104, 'PUNO', '1'),
(22, 104, 'SAN MARTIN', '1'),
(23, 104, 'TACNA', '1'),
(24, 104, 'TUMBES', '1'),
(25, 104, 'UCAYALI', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE `grupos` (
  `idgrupo` int(11) NOT NULL,
  `cod_grupo` varchar(8) NOT NULL,
  `desc_grupo` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`idgrupo`, `cod_grupo`, `desc_grupo`, `status`) VALUES
(1, '0001', 'INSUMOS', '1'),
(2, '0002', 'SERVICIOS', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `idproductos` int(11) NOT NULL,
  `stock` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inventario`
--

INSERT INTO `inventario` (`idproductos`, `stock`) VALUES
(0, 60),
(3, 35),
(262, 50),
(264, 28),
(271, 48),
(274, 50),
(885, 20),
(901, 34),
(902, 50),
(935, 60),
(1379, 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `laboratorios`
--

CREATE TABLE `laboratorios` (
  `idlaboratorio` int(11) NOT NULL,
  `desc_laboratorio` varchar(60) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `laboratorios`
--

INSERT INTO `laboratorios` (`idlaboratorio`, `desc_laboratorio`, `status`) VALUES
(1, 'ABBOTT LABORATORI', '1'),
(2, 'ABEEFE S.A', '1'),
(3, 'ABL', '1'),
(4, 'AC FARMA', '1'),
(5, 'AKORN', '1'),
(6, 'ALBONOVA S.A.', '1'),
(7, 'ALCON LABORATORIO', '1'),
(8, 'ASOFARMA S.A.', '1'),
(9, 'ATRAL S.A.', '1'),
(10, 'AVENTIS', '1'),
(11, 'B. BRAUN S.A.', '1'),
(12, 'BAGO LABORATORIOS', '1'),
(13, 'BAYER S.A.', '1'),
(14, 'BEBES', '1'),
(15, 'BEBIDA', '1'),
(16, 'BIOCODEX', '1'),
(17, 'BOEHRINGER INGELH', '1'),
(18, 'BRAUM MEDICAL', '1'),
(19, 'BRAUN', '1'),
(20, 'BYK GOLDEN  S.A.', '1'),
(21, 'CIFARMA S.A.', '1'),
(22, 'CIPA', '1'),
(23, 'CONSORCIO FARMACE', '1'),
(24, 'CORPORACION FARMA', '1'),
(25, 'D. A. CARRION S.A', '1'),
(27, 'DR. REDDY?S LABOR', '1'),
(28, 'ELI LILLY Y COMPA', '1'),
(29, 'ELIFARMA S.A.', '1'),
(30, 'FARMACEUTICA DEL', '1'),
(31, 'FARMACUTICA LATIN', '1'),
(32, 'FARMINDUSTRIA S.A', '1'),
(33, 'GARDEN HOUSE S.A.', '1'),
(34, 'GEDEON RICHTER S.', '1'),
(35, 'GENERAL FACTORY', '1'),
(36, 'GENFAR', '1'),
(37, 'GLAXO WELLCOME S.', '1'),
(38, 'GRACURE PHARMACEU', '1'),
(39, 'GROSSMAN S.A. DE', '1'),
(40, 'GRUNENTHAL GmbH.', '1'),
(42, 'INDUSTRIA ESPECIA', '1'),
(43, 'INDUSTRIALES GROV', '1'),
(44, 'INS. BIOLOGICO CO', '1'),
(45, 'INSTITUTO BIOLOGI', '1'),
(46, 'INSTITUTO BIOQUIM', '1'),
(47, 'INSTITUTO QUIMIOT', '1'),
(48, 'INSTITUTO SEROTER', '1'),
(49, 'INSTITUTO SUIZO D', '1'),
(50, 'ION', '1'),
(51, 'IQFARMA', '1'),
(52, 'IVAX', '1'),
(53, 'JANSSEN CILAG FAR', '1'),
(54, 'JUGUETE', '1'),
(55, 'KLONAL S.R.L.', '1'),
(56, 'LA SANTE S.A.', '1'),
(57, 'LA SANTE S.A. - o', '1'),
(58, 'LAB. DROGUERIA IN', '1'),
(59, 'LAB. FARMACEUTICO', '1'),
(60, 'LAB. PAVIL S.A.', '1'),
(61, 'LABOFAR', '1'),
(62, 'LABORATORIO FARMA', '1'),
(63, 'LABORATORIO FUJIS', '1'),
(64, 'LABORATORIOS BLIP', '1'),
(65, 'LABORATORIOS LANS', '1'),
(66, 'LABORATORIOS PORT', '1'),
(67, 'LABORATORIOS UNIDOS SA', '1'),
(68, 'LABOT', '1'),
(69, 'LCG', '1'),
(70, 'LECHES', '1'),
(71, 'LYKA LABORATORIOS', '1'),
(72, 'LUKOL', '1'),
(73, 'LUSA', '1'),
(74, 'MAGMA-SANITAS', '1'),
(75, 'MATERIAL MEDICO', '1'),
(76, 'MEDIFARMA', '1'),
(77, 'MERCK', '1'),
(78, 'MSD', '1'),
(79, 'MEDROCK', '1'),
(80, 'NATURGEN', '1'),
(81, 'NORTHIA S.A.C.I.F', '1'),
(82, 'NOVARTIS', '1'),
(83, 'OM', '1'),
(84, 'ORGANON DO BRASIL', '1'),
(85, 'P & G', '1'),
(86, 'PERFUMERIA', '1'),
(87, 'PERUGEN', '1'),
(88, 'PFIZER', '1'),
(89, 'PHARMALAB', '1'),
(91, 'ROCHE S', '1'),
(92, 'QUIMICA ZUISA', '1'),
(94, 'ROEMMERS', '1'),
(95, 'ROSTER S.A.', '1'),
(96, 'SANDERSON S.A.', '1'),
(97, 'SANOFI WINTHROP P', '1'),
(98, 'SAVAL S.A.', '1'),
(99, 'SCHERING AG', '1'),
(100, 'SCHERING PLOUGH', '1'),
(101, 'SHIJIAZHUANG PHAR', '1'),
(102, 'SMITHKLINE BEECHA', '1'),
(103, 'SYNTOFARMA S.A.', '1'),
(104, 'SANITAS', '1'),
(105, 'TERAPEUTICA BOLIV', '1'),
(107, 'TECNOFARMA', '1'),
(108, 'UNIMED', '1'),
(109, 'VARIOS', '1'),
(110, 'VIDASOL', '1'),
(111, 'VITALIS', '1'),
(112, 'WARNER LAMBERT', '1'),
(113, 'WEINER PHARMA Gmb', '1'),
(114, 'WUHAN PRIME PHARM', '1'),
(115, 'WUXI N? 7 PHARMAC', '1'),
(116, 'WYETH AYERTS CANA', '1'),
(117, 'WYETH S.A. DE C.V', '1'),
(118, 'ZAMBON GROUP S.p.', '1'),
(119, 'Z FARMA SRL', '1'),
(120, 'ACE FARMA S.A.', '1'),
(121, 'ADROFA', '1'),
(122, 'ALFA S.A', '1'),
(123, 'ALKOFARMA', '1'),
(124, 'APROPO', '1'),
(125, 'ATRAL DEL PERU (DIST PHARBAL)', '1'),
(126, 'B. BRAUN PERUANA S.A.', '1'),
(127, 'BAGO DEL PERU S.A LABORATORIOS', '1'),
(128, 'BAYER PERU S.A.', '1'),
(129, 'BENDI-C', '1'),
(130, 'BIOSYNTEC', '1'),
(131, 'BOEHRINGER INGELHEIM', '1'),
(132, 'CHANGZHOU MEDICAL', '1'),
(133, 'CIFARMA S.A', '1'),
(134, 'CIPA S.A.', '1'),
(135, 'CKF', '1'),
(136, 'COFANA', '1'),
(137, 'COLICHON S.A.', '1'),
(138, 'CONTINENTAL OPTICAL S.A.', '1'),
(139, 'CORINSER', '1'),
(140, 'DISTRIBUIDORA DANY', '1'),
(141, 'DOCTOR ANDREU Q.F. S.A', '1'),
(143, 'FARMA RECETAS', '1'),
(145, 'FARPASA', '1'),
(146, 'FARVET', '1'),
(147, 'GARDEN HOUSE S.A. LABORATORIOS', '1'),
(148, 'GENCOPHARMACEUTICAL SAC', '1'),
(149, 'GEN-FAR PERU S.A.', '1'),
(150, 'GENOMALAB', '1'),
(151, 'GLAXO SMITHKLINE', '1'),
(152, 'GLAXO WELLCOME S.A.', '1'),
(153, 'GRUNENTHAL PERUANA S.A.', '1'),
(154, 'HERSIL', '1'),
(155, 'HI-MED', '1'),
(156, 'HYNOSCHA MEDIC S.A.C', '1'),
(157, 'INDUQUIMICA S.A', '1'),
(158, 'INTIPHARMA S.A.C', '1'),
(159, 'INTRAVEN S.A.', '1'),
(160, 'IQ FARMA (INSTITUTO QUIMIOTERAPICO S.A.)', '1'),
(161, 'IQ MEDIC S.A.C', '1'),
(162, 'JOHNSON*&*JOHNSON DEL PERU', '1'),
(163, 'LAB. MEDROCK CORPORATION S.A.C', '1'),
(164, 'LABOFAR S.A.C', '1'),
(165, 'LABORATORIO MAVER LTDA', '1'),
(166, 'LABORATORIOS AC FARMA S.A', '1'),
(167, 'LABORATORIOS DELFARMA S.A.C.', '1'),
(168, 'LABORATORIOS INDUFAR', '1'),
(169, 'LABOT (LABORATORIOS AMERICANOS S.A)', '1'),
(170, 'LAFARMED', '1'),
(171, 'LAFRANCOL PERU S.A.', '1'),
(172, 'LANSIER LABORATORIO', '1'),
(173, 'LUKOL  S.A.C', '1'),
(174, 'LUSA S,A', '1'),
(175, 'MAGMA S.A.', '1'),
(176, 'MARFAN', '1'),
(177, 'MARKOS S.A.', '1'),
(178, 'MEDCO', '1'),
(179, 'MEDIFARMA S.A.', '1'),
(180, 'MERCK PERUANA S.A', '1'),
(181, 'MONT GROUP S.A.C', '1'),
(182, 'NESTLE', '1'),
(183, 'OQ PHARMA', '1'),
(184, 'PAK FARMA S.A', '1'),
(185, 'PAVIL PHARVET', '1'),
(186, 'PFIZER CORPORATION', '1'),
(187, 'PHARMAGEN S.A.C.', '1'),
(188, 'PHARMED CORPORATION S.A.C.', '1'),
(189, 'PORTUGAL SRL', '1'),
(190, 'QUILAB', '1'),
(191, 'ROEMMERS S.A.', '1'),
(192, 'ROXFARMA  S.A', '1'),
(193, 'RYMCO LABORATORIO', '1'),
(194, 'SANOFI AVENTIS  S.A', '1'),
(195, 'SCOP DEL PERU S.A.', '1'),
(196, 'SHERFARMA LAB.', '1'),
(197, 'SIEGFRIED S.A.', '1'),
(198, 'SIESALUD E.I.R.L', '1'),
(199, 'TERBOL PERU S.A.-', '1'),
(200, 'TEVA PHARMACEUTICAL INDUSTRIES', '1'),
(201, 'TRIFARMA S.A.', '1'),
(202, 'UNIMED DEL PERU S.A.', '1'),
(203, 'UNITED PHAMACEUTICAL DEL PERU', '1'),
(204, 'UQP DIVISION PERUFARMA', '1'),
(205, 'VICK', '1'),
(207, 'VITROFARMA S.A.', '1'),
(208, 'SIN ESPECIFICAR', '1'),
(210, 'BRISTOL-MYERS SQUIBB', '1'),
(211, 'LABORATORIO ROWEFEM', '1'),
(212, 'EUROFARMA', '1'),
(213, 'Laboratorios FERRER Grupo', '1'),
(214, 'Laboratorio Grupo Farmakonsuma', '1'),
(215, 'HEINZ', '1'),
(216, 'Laboratorios INTI', '1'),
(217, 'Laboratorio LAFARPE', '1'),
(218, 'Laboratorio NOVAX EIRL', '1'),
(219, 'Laboratorio PANALAB', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `linea`
--

CREATE TABLE `linea` (
  `idlinea` int(11) NOT NULL,
  `desc_linea` varchar(60) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `linea`
--

INSERT INTO `linea` (`idlinea`, `desc_linea`, `status`) VALUES
(1, 'BIENES DE CONSUMO', '1'),
(2, 'MEDICAMENTOS', '1'),
(3, 'PAPELERIAS', '1'),
(4, 'BISUTERIA', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `moneda`
--

CREATE TABLE `moneda` (
  `idmoneda` int(11) NOT NULL,
  `simb_moneda` varchar(4) NOT NULL,
  `nom_moneda` varchar(10) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `moneda`
--

INSERT INTO `moneda` (`idmoneda`, `simb_moneda`, `nom_moneda`, `status`) VALUES
(1, 'S/.', 'SOLES', '1'),
(3, '$.', 'DOLAR', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ordenes_compras`
--

CREATE TABLE `ordenes_compras` (
  `idord_compra` int(11) NOT NULL,
  `idproveedor` int(11) NOT NULL,
  `fecha_entrega` date NOT NULL,
  `id_tipo_pago` int(11) NOT NULL,
  `idempleado` int(11) NOT NULL,
  `fec_registro` datetime DEFAULT CURRENT_TIMESTAMP,
  `entregado` enum('0','1') NOT NULL DEFAULT '0',
  `observacion` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_pedidos`
--

CREATE TABLE `orden_pedidos` (
  `idordenpedido` int(11) NOT NULL,
  `idproveedor` int(11) NOT NULL,
  `fecha_orden` date NOT NULL,
  `observacion` text,
  `idusuario` int(11) NOT NULL,
  `fec_registro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_aprobada` date DEFAULT NULL,
  `fecha_anulada` date DEFAULT NULL,
  `status` enum('A','C','N','E') NOT NULL DEFAULT 'A' COMMENT 'A=>Aprobado\nC=>Cancelado\nN=>Anulado\nE=>Entregado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE `paises` (
  `idpais` int(11) NOT NULL,
  `desc_pais` varchar(45) NOT NULL,
  `prefijo` varchar(5) NOT NULL,
  `predeterminado` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`idpais`, `desc_pais`, `prefijo`, `predeterminado`) VALUES
(1, 'ALASKA', '1907', '0'),
(2, 'ALBANIA', '355', '0'),
(3, 'ALEMANIA', '49', '0'),
(4, 'ANDORRA', '376', '0'),
(5, 'ANGOLA', '244', '0'),
(6, 'ARABIA SAUDI', '966', '0'),
(7, 'ARGELIA', '213', '0'),
(8, 'ARGENTINA', '54', '0'),
(9, 'ARMENIA', '54', '0'),
(10, 'AUSTRALIA', '61', '0'),
(11, 'AUSTRIA', '43', '0'),
(12, 'BAHREIM', '973', '0'),
(13, 'BANGLADESH', '880', '0'),
(14, 'BELGICA', '32', '0'),
(15, 'BOLIVIA', '591', '0'),
(16, 'BOSNIA', '387', '0'),
(17, 'BRASIL', '55', '0'),
(18, 'BULGARIA', '359', '0'),
(19, 'CABO VERDE', '238', '0'),
(20, 'CAMBOYA', '855', '0'),
(21, 'CAMERUN', '237', '0'),
(22, 'CANADA', '1', '0'),
(23, 'CENTROAFRICANA, REP.', '236', '0'),
(24, 'REPUBLICA CHECA', '420', '0'),
(25, 'CHILE', '56', '0'),
(26, 'CHINA', '86', '0'),
(27, 'CHIPRE', '357', '0'),
(28, 'COLOMBIA', '57', '0'),
(29, 'REPUBLICA DEL CONGO', '242', '0'),
(30, 'REPUBLICA DEMOCATICA CONGO', '243', '0'),
(31, 'REPUBLICA DEMOCRATICA COREA', '850', '0'),
(32, 'REPUBLICA COREA', '82', '0'),
(33, 'COSTA DE MARFIL', '225', '0'),
(34, 'COSTA RICA', '506', '0'),
(35, 'CROACIA', '385', '0'),
(36, 'CUBA', '53', '0'),
(37, 'DINAMARCA', '45', '0'),
(38, 'REPUBLICA DOMINICANA', '1809', '0'),
(39, 'ECUADOR', '593', '0'),
(40, 'EGIPTO', '20', '0'),
(41, 'EL SALVADOR', '503', '0'),
(42, 'EMIRATOS ARABES UNIDOS', '971', '0'),
(43, 'REPUBLICA ESLOVACA', '421', '0'),
(44, 'ESLOVENIA', '386', '0'),
(45, 'ESPAÃ‘A', '34', '0'),
(46, 'ESTADOS UNIDOS', '1', '0'),
(47, 'ESTONIA', '372', '0'),
(48, 'ETIOPIA', '251', '0'),
(49, 'FILIPINAS', '63', '0'),
(50, 'FINLANDIA', '358', '0'),
(51, 'FRANCIA', '33', '0'),
(52, 'GIBRALTAR', '9567', '0'),
(53, 'GRECIA', '30', '0'),
(54, 'GROENLANDIA', '299', '0'),
(55, 'GUATEMALA', '502', '0'),
(56, 'GUINEA ECUATORIAL', '240', '0'),
(57, 'HAITI', '509', '0'),
(58, 'HAWAI', '1808', '0'),
(59, 'HONDURAS', '504', '0'),
(60, 'HONG KONG', '852', '0'),
(61, 'HUNGRIA', '36', '0'),
(62, 'INDIA', '91', '0'),
(63, 'INDONESIA', '62', '0'),
(64, 'IRAN', '98', '0'),
(65, 'IRAK', '964', '0'),
(66, 'IRLANDA', '353', '0'),
(67, 'ISLANDIA', '354', '0'),
(68, 'ISRAEL', '972', '0'),
(69, 'ITALIA', '39', '0'),
(70, 'JAMAICA', '1876', '0'),
(71, 'JAPON', '81', '0'),
(72, 'JORDANIA', '962', '0'),
(73, 'KENIA', '254', '0'),
(74, 'KUWAIT', '965', '0'),
(75, 'LAOS', '856', '0'),
(76, 'LETONIA', '371', '0'),
(77, 'LIBANO', '961', '0'),
(78, 'LIBERIA', '231', '0'),
(79, 'LIBIA', '218', '0'),
(80, 'LIECHTENSTEIN', '41', '0'),
(81, 'LITUANIA', '370', '0'),
(82, 'LUXEMBURGO', '352', '0'),
(83, 'MADAGASCAR', '261', '0'),
(84, 'MALASIA', '60', '0'),
(85, 'MALTA', '356', '0'),
(86, 'MARRUECOS', '212', '0'),
(87, 'MARTINICA', '596', '0'),
(88, 'MAURITANIA', '222', '0'),
(89, 'MEXICO', '52', '0'),
(90, 'MOLDAVIA', '373', '0'),
(91, 'MONACO', '377', '0'),
(92, 'MONGOLIA', '976', '0'),
(93, 'MOZAMBIQUE', '258', '0'),
(94, 'NAMIBIA', '264', '0'),
(95, 'NEPAL', '977', '0'),
(96, 'NICARAGUA', '505', '0'),
(97, 'NIGERIA', '234', '0'),
(98, 'NORUEGA', '47', '0'),
(99, 'NUEVA ZELANDA', '64', '0'),
(100, 'PAISES BAJOS', '31', '0'),
(101, 'PAKISTAN', '92', '0'),
(102, 'PANAMA', '507', '0'),
(103, 'PARAGUAY', '595', '0'),
(104, 'PERU', '51', '1'),
(105, 'POLONIA', '48', '0'),
(106, 'PORTUGAL', '351', '0'),
(107, 'PUERTO RICO', '1787', '0'),
(108, 'QATAR', '974', '0'),
(109, 'REINO UNIDO', '44', '0'),
(110, 'RUMANIA', '40', '0'),
(111, 'RUSIA', '7', '0'),
(112, 'SAN MARINO', '378', '0'),
(113, 'SENEGAL', '221', '0'),
(114, 'SINGAPUR', '65', '0'),
(115, 'SIRIA', '963', '0'),
(116, 'SOMALIA', '252', '0'),
(117, 'SRI-LANKA', '94', '0'),
(118, 'SUDAFRICA', '27', '0'),
(119, 'SUDAN', '249', '0'),
(120, 'SUECIA', '46', '0'),
(121, 'SUIZA', '41', '0'),
(122, 'TAILANDIA', '66', '0'),
(123, 'TAIWAN', '886', '0'),
(124, 'TANZANIA', '255', '0'),
(125, 'TUNEZ', '216', '0'),
(126, 'TURQUIA', '90', '0'),
(127, 'UCRANIA', '380', '0'),
(128, 'UGANDA', '256', '0'),
(129, 'URUGUAY', '598', '0'),
(130, 'VATICANO', '39', '0'),
(131, 'VENEZUELA', '58', '0'),
(132, 'VIETNAM', '84', '0'),
(133, 'YEMEN', '967', '0'),
(134, 'YUGOSLAVIA', '381', '0'),
(135, 'ZAMBIA', '260', '0'),
(136, 'ZIMBAWE', '263', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privilegios_rol`
--

CREATE TABLE `privilegios_rol` (
  `idpriv_rol` int(11) NOT NULL,
  `idrol_sistema` int(11) NOT NULL,
  `idwin_sistemas` int(11) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idproductos` int(11) NOT NULL,
  `cod_producto` varchar(10) DEFAULT NULL,
  `nom_producto` varchar(60) NOT NULL,
  `idlaboratorio` int(11) NOT NULL,
  `idgrupo` int(11) DEFAULT NULL,
  `idsubgrupo` int(11) DEFAULT NULL,
  `idlinea` int(11) DEFAULT NULL,
  `descripcion` text,
  `mto_compra` decimal(13,2) NOT NULL,
  `fec_ult_compra` date DEFAULT NULL,
  `exento_impuesto` enum('0','1') NOT NULL DEFAULT '0',
  `mto_impuesto` decimal(5,2) NOT NULL DEFAULT '0.00',
  `metod_venta` enum('1','2') NOT NULL DEFAULT '1',
  `mto_minimo` decimal(13,2) DEFAULT NULL,
  `mto_sugerido` decimal(13,2) DEFAULT NULL,
  `porc_ganancia` decimal(5,2) DEFAULT NULL,
  `facturarsinstock` enum('0','1') NOT NULL DEFAULT '0',
  `avisostockminimo` enum('0','1') NOT NULL,
  `stockminimo` int(11) NOT NULL DEFAULT '0',
  `medida` varchar(10) DEFAULT NULL,
  `peso` varchar(10) DEFAULT NULL,
  `idumedida` int(11) NOT NULL DEFAULT '13',
  `categoria` enum('1','2','3','4') NOT NULL DEFAULT '1',
  `usa_serial` enum('1','0') NOT NULL DEFAULT '1',
  `serial_1` varchar(15) DEFAULT NULL,
  `serial_2` varchar(15) DEFAULT NULL,
  `serial_3` varchar(15) DEFAULT NULL,
  `etiqueta_label` enum('0','1') NOT NULL DEFAULT '0',
  `etiqueta_precio` enum('0','1') NOT NULL DEFAULT '0',
  `etiqueta_codigo` enum('0','1') NOT NULL DEFAULT '0',
  `imagen` blob,
  `ocultar_venta` enum('0','1') NOT NULL DEFAULT '0',
  `fec_ingreso` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idproductos`, `cod_producto`, `nom_producto`, `idlaboratorio`, `idgrupo`, `idsubgrupo`, `idlinea`, `descripcion`, `mto_compra`, `fec_ult_compra`, `exento_impuesto`, `mto_impuesto`, `metod_venta`, `mto_minimo`, `mto_sugerido`, `porc_ganancia`, `facturarsinstock`, `avisostockminimo`, `stockminimo`, `medida`, `peso`, `idumedida`, `categoria`, `usa_serial`, `serial_1`, `serial_2`, `serial_3`, `etiqueta_label`, `etiqueta_precio`, `etiqueta_codigo`, `imagen`, `ocultar_venta`, `fec_ingreso`) VALUES
(1, 'OBS00004', 'Ensure Adv. Fresa x237ML', 1, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(2, 'OTC00035', 'PEDIASURE TRIPLE CHOCOL 237ML', 1, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(3, 'OTC00072', 'ENSURE ADV.CHOCOLATE X 237ML', 1, 1, NULL, 2, NULL, 27.30, '2018-08-25', '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(4, 'OTC00073', 'ENSURE ADV.VAINX237ML', 1, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(5, 'OTC00074', 'ENSURE ADVANCE VAINILLA X400G', 1, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(6, 'OTC00075', 'ENSURE ADVANCE VAINILLAX850G', 1, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(7, 'OTC00078', 'GLUCERNA CHOCOLATE TRIPL237ML', 1, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(8, 'OTC00079', 'GLUCERNA VAINILLA DE900GRAMOS', 1, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(9, 'OTC00080', 'GLUCERNA VAINILLA TRIPL237ML', 1, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(10, 'OTC00081', 'GLUCERNA VAINILLA X400GRAMOS', 1, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(11, 'OTC00087', 'PEDIASURE TRIPLE 237ML / VAIN', 1, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(12, 'MED00002', 'VELAMOX TABx500MGx100', 2, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(13, 'MED01021', 'VELAMOX 250MG X 10 CAP', 2, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(14, 'OTC00036', 'RED BULL ENERGY DRINKX250ML', 2, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(15, 'OTC00088', 'PRESER.DUREX CLASICO X 3UND', 2, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(16, 'OTC00089', 'PRESER.DUREX EXTR.SEGURO X3UN', 2, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(17, 'OTC00090', 'PRESER.DUREX MAXIMO PLACERX3U', 2, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(18, 'OTC00091', 'PRESER.DUREX PLACER PROLOX3UN', 2, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(19, 'OTC00092', 'PRESER.DUREX SENSI ULTR DELG', 2, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(20, 'AMP00156', 'CYCLOFEMINA AMPX1', 3, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(21, 'MED00003', 'FLAPEX E x 120 CAP ', 3, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(22, 'MED00054', 'FAMIDAL CREx60GR', 3, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(23, 'MED00055', 'HEDILAR FORTE 70MG/5ML X 100M', 3, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(24, 'MED00056', 'HEDILAR JBE x 100ML', 3, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(25, 'MED00058', 'HIPOGLOS UNG X 20 GR', 3, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(26, 'MED00063', 'RESPIBRON JBE ADUL X100ml', 3, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(27, 'MED00065', 'ANULETTE x21 COMP', 3, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(28, 'MED00067', 'DEFLAMAT 50MG.X 50 CAP ', 3, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(29, 'MED00068', 'FAMIDAL OVUX50 ', 3, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(30, 'AMP00001', 'CORTIFLEX 40MG/ML X 1 AMP', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(31, 'AMP00157', 'CORTIFLEX 50MG/5ML VIALx1AMP', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(32, 'MED00071', 'AZITROMICINA 500MGX30TAB', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:48'),
(33, 'MED00072', 'CEFACLOR 250ML X 75ML', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(34, 'MED00073', 'CEFACLOR 500MG X 20TAB', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(35, 'MED00074', 'CEFALEXINA 500MGX100TAB', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(36, 'MED00075', 'CLOZOL 1% CREMA TUBOX20G', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(37, 'MED00078', 'FENOTEC(FENOTEROL)X20ML GOTAS', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(38, 'MED00080', 'LOSARTAN 50MGx100TAB', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(39, 'MED00081', 'MUPIROX 2% UNG TUBOx15GR', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(40, 'MED00084', 'SUCRALEX 1GR/5ML X 200ML', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(41, 'MED00085', 'VAGISTEN 0.1% CREM VAGX15GR', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(42, 'MED00086', 'VENTIMAX 100MCG/250DOSIS INHA', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(43, 'MED00087', 'VAGISTEN X 10  OVULOS ', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(44, 'MED00088', 'TAMSULOSINA 0.4MG X50CAP ', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(45, 'MED00089', 'BIPERIDENO 2MG.x 100 TAB', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(46, 'MED00090', 'CLINDAMAX 300MG X 100CAP ', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(47, 'MED00091', 'DOXY 100MG.X100 TAB ', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(48, 'MED00092', 'VALSARTAN 160MG X100TAB', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(49, 'MED01078', 'VALSARTAN 80MG X100TAB ', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(50, 'MED01100', 'AMIODARONA 200 X 100TAB', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(51, 'MED01101', 'DOXICICLINA 100X100 TAB REC', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(52, 'MED01102', 'DUO VENTIMAX INH10ML/200DOSI', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(53, 'MED01103', 'FLACORT 30MG.x10 TAB', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(54, 'MED01205', 'MUPIROCINA 2%UNGUENTOX15G', 4, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(55, 'MED00007', 'MAGNESOL DISX33 LIMON', 21, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(56, 'MED00164', 'MAGNESOL DIS X 33  NARANJA', 21, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(57, 'MED00165', 'MAGNESOL DISPLAY X 33', 21, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(58, 'AMP00166', 'DECORTEN 4MG/2ML X AMP', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(59, 'MED00205', 'ASSIS 400 X 220  ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(60, 'MED00206', 'CLENBUVENT EXPECT FCOX120ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(61, 'MED00207', 'CLENBUVENT EXPECT.GTSX15ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(62, 'MED00208', 'DECORTEN ELIXIR 2MG/5X100ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(63, 'MED00209', 'ELIPRIM-BALSAMO SUSx100ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(64, 'MED00210', 'ELIPRIM-FORTE SUSx100ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(65, 'MED00211', 'ELIPRIM-PED SUSx200MGx60ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(66, 'MED00212', 'ELITON CIP AP JBE FCOX340ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(67, 'MED00213', 'ELITON FORTE JBE FCOX340ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(68, 'MED00214', 'ELITON NF JBEx340ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(69, 'MED00215', 'ELITON VIT GOT X 20 ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(70, 'MED00216', 'ELITON x 20ML GOTAS', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(71, 'MED00217', 'NASALER JBE X 60ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(72, 'MED00218', 'NONPIRON FRESA JBEx60ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(73, 'MED00219', 'NONPIRON GOTAS x 15ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(74, 'MED00220', 'NONPIRON PLATANO JBEx60ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(75, 'MED00222', 'SALBUVENT EXP. JBEX150ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:49'),
(76, 'MED00223', 'ZETALER  GTS X 15 ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(77, 'MED00224', 'ALBENTEL SUSP.X4FCOS X 20ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(78, 'MED00226', 'NISOPREX  X 120  ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(79, 'MED01082', 'ZETALER  X 60 ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(80, 'MED01212', 'VITAKID CRECIMIENTO SUSP. X180ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(81, 'PRE00004', 'ZETALER D X 60 ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(82, 'PRE00022', 'NASALER PLUS CLx15ML GTS', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(83, 'PRE00023', 'SILENAI CL JBEX120ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(84, 'PRE00024', 'SILENAI CL PED.JBEX120ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(85, 'PRE00025', 'SILENAI EXP.JBEX120ML', 29, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(86, 'AMP00042', 'AMIKACINA 500MG/2ML x10AMP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(87, 'AMP00043', 'BETAMETASONA 4MG/ML X1AMP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(88, 'AMP00044', 'DEXAMETASONA 4M/ML INY X10AMP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(89, 'AMP00045', 'DEXAMETASONA 8MG/2MLx1 AMP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(90, 'AMP00046', 'GENTAMICINA 160MGx2MLx1  AMP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(91, 'AMP00047', 'KETOROLACO 30MGx5 AMP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(92, 'AMP00048', 'DICLOFENACO 75MGx3MLx5 AMP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(93, 'AMP00049', 'KETOPROFENO 100MG/2MLx6 AMP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(94, 'AMP00171', 'MELOXICAM 15MG x 1AMP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(95, 'AMP00172', 'LINCOMICINA 600MG/2ML X 6 AMP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(96, 'AMP00173', 'FUROSEMIDA 20MGx2MLx10 AMP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(97, 'MED00013', 'SECNIDAZOL 500MGx4 TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(98, 'MED00014', 'LANZOPRAZOL 30MG X 7  CAP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(99, 'MED00261', 'ACICLOVIR 5% UNGX15GRX1', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(100, 'MED00262', 'ACIDO FUSIDICO 2%TUB.15G', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(101, 'MED00263', 'ALBENDAZOL 100MG/5MLx20ML SUSP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(102, 'MED00264', 'ALBENDAZOL 200MG x2 TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(103, 'MED00267', 'ATORVASTATINA 10MGX10TB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(104, 'MED00268', 'ATORVASTATINA 20MGX10TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(105, 'MED00269', 'AZITROMICINA 500 X 3TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(106, 'MED00270', 'BETAMETASONA 0.1%CREMX40GR', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(107, 'MED00271', 'CALCIO 600+ VIT D X 30 TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(108, 'MED00273', 'CEFALEXINA 250MG/5ML/60ML FCO', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(109, 'MED00274', 'CIPROFLOXACINO 500MGx10 TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(110, 'MED00275', 'CLOTRIMAZOL CRE/x6aplic.VAG.', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(111, 'MED00276', 'DEFLAZACOR 30MGX10 TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(112, 'MED00277', 'DEFLAZACORT 6MG X10', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(113, 'MED00278', 'DESLORATADINA 2.5X5ML X 60ML', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(114, 'MED00279', 'DICLOFENACO 1% GEL X 50 GRS', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(115, 'MED00280', 'DICLOFENACO 100MGx20 GRA', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(116, 'MED00281', 'DICLOFENACO 50MGx30TB RETARD', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(117, 'MED00283', 'DILTIAZEM 60MGx20 TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(118, 'MED00284', 'ERITROMICINA 250G/5ML/60ML', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:50'),
(119, 'MED00285', 'ESOMEPRAZOL 20MGX10TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(120, 'MED00286', 'ESOMEPRAZOL 40MGX10 TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(121, 'MED00287', 'FLUCONAZOL 150MG X1CAP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(122, 'MED00288', 'FLUCONAZOL 200 MG X 4 TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(123, 'MED00289', 'GEMFIBROZILO 600MGx20 TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(124, 'MED00290', 'HIDROCLOROTIAZIDA 50MGx30TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(125, 'MED00291', 'IBUPROFENO 100MGX 120ML SUSP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(126, 'MED00292', 'ISOCONAZOL 1% CREx20GR', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(127, 'MED00294', 'KETOPROFENO 100MGx30 TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(128, 'MED00295', 'KETOPROFENO 2.5% GELX60GR', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(129, 'MED00296', 'LEVOFLOXACINO 500MG TAB CJAX7', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(130, 'MED00297', 'MELOXICAN 15 MGx10 TABL', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(131, 'MED00298', 'METRONIDAZOL 250MG/5MLx120ML', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(132, 'MED00299', 'MINOCICLINA 100MG CAP CJAX10', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(133, 'MED00300', 'MOMETASONA FUROA 0.1% CREX15G', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(134, 'MED00301', 'NORFLOXACINO 400MGx14 TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(135, 'MED00302', 'OMEPRAZOL 20MG x 10 CAP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(136, 'MED00303', 'PARACETAMOL 500MG TAB CJAX100', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(137, 'MED00304', 'PARACETAMOL150MG/5ML JBEX60ML', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(138, 'MED00305', 'PIRANTEL 250MG/5/15ML SUSP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(139, 'MED00306', 'SILDENAFILO 100MG X 1 TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(140, 'MED00307', 'SILDENAFILO 50MG TAB CJAX4', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(141, 'MED00308', 'SILIMARINA 150MGX20CAP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(142, 'MED00309', 'SULFADIAZINA PLATA CREM X30GR', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(143, 'MED00310', 'TERBINAFINA 1% CREMA X20gr', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(144, 'MED00312', 'TERBINAFINA 250MG TAB CJAX14', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(145, 'MED00313', 'TINIDAZOL 500MG TAB CJAX8', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:51'),
(146, 'MED00314', 'VALSARTAN 160 MG X 14TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(147, 'MED00315', 'VALSARTAN RETARD 80MG X14TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(148, 'MED00316', 'VITAMINA C  500MG X 144 TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(149, 'MED00317', 'ALENDRONATO 70MGX4TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(150, 'MED00318', 'AMLODIPINO 10MG x10 TAB ', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(151, 'MED00319', 'AMLODIPINO 5MG x10 TAB ', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(152, 'MED00320', 'CEFALEXINA 500MGX10 CAP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(153, 'MED00321', 'CLARITROMICINA 500MGx10TAB ', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(154, 'MED00322', 'LOVASTATINA 20MGX10 TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(155, 'MED00323', 'CAPTOPRIL 50MG x30 TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(156, 'MED00325', 'LOSARTAN POTASICO 50MGx30 TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(157, 'MED00326', 'IBUPROFENO 800MGX50 TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(158, 'MED00327', 'MEBENDAZOL 100MG TAB CJAX60', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(159, 'MED00328', 'PIRANTEL 250MG TAB CJAX60', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(160, 'MED00329', 'FUROSEMIDA 40MGX100  TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(161, 'MED00330', 'IBUPROFENO 400MGx100 TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(162, 'MED00331', 'METRONIDAZOL 500MGx100 TAB', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(163, 'MED01091', 'YODOPOVIDONA SOLUCION 60ML', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(164, 'MED01208', 'ACIDO ACETILSALICILICO 100MG X 100TBS', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(165, 'MED01209', 'TADALAFILO 20 MG X 4 COMPRIMIDOS', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(166, 'MED01216', 'DICLOXACILINA 500MG X 50 CAPS', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(167, 'PSI00004', 'TRAMADOL 50 MG X 1 AMP', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(168, 'PSI00005', 'TRAMINOFENOL X 10TAB ', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(169, 'PSI00019', 'TRAMADOL 100MG.X 1 AMPX2ML', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(170, 'PSI00020', 'TRAMADOL 50 MG. X 10 CAP ', 36, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(171, 'INA00013', 'GLIBENCLAMIDA 5MG X 100TAB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(172, 'MED00047', 'SILDENAFILO 50MG X 50TABLETAS ', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(173, 'MED00439', 'AMOXICILINA 125MG/5ML X60ML', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(174, 'MED00440', 'AMOXICILINA 250MGX100TAB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(175, 'MED00441', 'AMOXICILINA FCOX250MGX60ML', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(176, 'MED00442', 'ATENOLOL 100MGx100 TAB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(177, 'MED00443', 'BIOBRONCOL JBE 250MGX75ML', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(178, 'MED00444', 'BRIMODIN 100MG/5ML JBE X 120M', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(179, 'MED00446', 'BRIMODIN EFERV 600 X20TAB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(180, 'MED00447', 'CEFACLOR 250MG/5ML SUSP X 75M', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(181, 'MED00448', 'CEFACLOR 500MG X 10CAPS', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(182, 'MED00449', 'CEFALEXINA 500MGX100CAP', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(183, 'MED00450', 'DICLOFENACO 50 MG X 100 TAB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(184, 'MED00451', 'DICLOXACILINA250MGX60ML', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(185, 'MED00452', 'DOLFENEX SR(DICLOF)100MGX30TAB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(186, 'MED00453', 'DOLOL ANTIGRIPAL X100TAB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:52'),
(187, 'MED00454', 'DOLOL GOTAS FCO 15ML', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(188, 'MED00455', 'FUROSEMIDA 40MGx100 TAB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(189, 'MED00456', 'GINOTHYL X 6 OVULO VAGINAL', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(190, 'MED00457', 'INDOMETACINA 25MG X 100CAP', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(191, 'MED00458', 'LEVONELLE 500MG x 7TAB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(192, 'MED00459', 'LEVONELLE 750MG x 5 TAB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(193, 'MED00460', 'MEBENDAZOL SUSX30ML', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(194, 'MED00461', 'METOCLOPRAMIDA 10 MG X 100TAB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(195, 'MED00462', 'MUPIROCINA 2% UNG TUBOX15GR', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(196, 'MED00463', 'PROPRANOLOL 40 MG X 100', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(197, 'MED00464', 'SILDENAFILO 100 MG X 50TAB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(198, 'MED00465', 'SULFATO FERROSO FCOX180ML', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(199, 'MED00466', 'TOSALBRON ADULTO X 100ML JBE', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(200, 'MED00467', 'TOSALBRON INFANTIL X100 ML JB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(201, 'MED00468', 'TRIAMCINOLONA 0.025% x60ML', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(202, 'MED00469', 'UROFURIN 100MGX120TAB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(203, 'MED00470', 'GASTRORAL SUSP.X 200ML', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(204, 'MED00471', 'BIOBRONCOL 500MGx50 CAP ', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(205, 'MED00472', 'ACIDO FOLICO 0.5 MGx100TAB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(206, 'MED00473', 'ALBENDAZOL 200MG X 100TB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(207, 'MED00474', 'AMOXICILINA 500 MG X100TAB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(208, 'MED00475', 'DICLOXACILINA CAPX500MGX100', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(209, 'MED00476', 'FENAZOPIRIDINA TABx100MGx100', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(210, 'MED00477', 'FLATUZYN X100 CAP ', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(211, 'MED01095', 'PREDNISONA 5 X 100 TAB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(212, 'MED01114', 'PREDNISONA 20MG X 100 TAB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(213, 'PSI00008', 'AMITRIPTILINA 25MGX100 TAB', 47, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(214, 'AMP00189', 'VOLTAREN 75MG/3ML X 10AMP', 82, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(215, 'MED00728', 'LAMISIL 1% CREX15GR', 82, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(216, 'MED00729', 'LAMISIL SPRAY FCOx30ML', 82, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(217, 'MED00730', 'VOLTAREN AEROSOL x 85 ML', 82, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(218, 'MED00731', 'VOLTAREN EMULGEN 1 % X 30 GRS', 82, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(219, 'MED00732', 'LAMISIL DERMGEL X 15GR', 82, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(220, 'MED00733', 'VOLTAREN SR 100 X 30 GRAG ', 82, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(221, 'MED00734', 'VOLTAREN GRAx50MGx100 ', 82, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(222, 'AMP00190', 'DICYNONE AMPX250MGX2ML', 83, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(223, 'MED00745', 'AERO-OM GOTx15ML ANIS', 83, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53'),
(224, 'MED00746', 'AERO-OM GOTX15ML FRESA', 83, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:53');
INSERT INTO `productos` (`idproductos`, `cod_producto`, `nom_producto`, `idlaboratorio`, `idgrupo`, `idsubgrupo`, `idlinea`, `descripcion`, `mto_compra`, `fec_ult_compra`, `exento_impuesto`, `mto_impuesto`, `metod_venta`, `mto_minimo`, `mto_sugerido`, `porc_ganancia`, `facturarsinstock`, `avisostockminimo`, `stockminimo`, `medida`, `peso`, `idumedida`, `categoria`, `usa_serial`, `serial_1`, `serial_2`, `serial_3`, `etiqueta_label`, `etiqueta_precio`, `etiqueta_codigo`, `imagen`, `ocultar_venta`, `fec_ingreso`) VALUES
(225, 'MED00747', 'MALTOFER FOL x30 TAB MAST', 83, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(226, 'MED00748', 'MALTOFER GOTX30ML', 83, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(227, 'MED00749', 'MALTOFER JBEx150ML', 83, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(228, 'MED00750', 'DOXIPROCT POMADA/TUBx12GR', 83, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(229, 'MED00751', 'DOXIPROCT-PLUS TUBX12GR', 83, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(230, 'MED00752', 'AERO OM ANIS 80MG X 100', 83, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(231, 'MED00753', 'AERO-OM COMx100 VAINILLA ', 83, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(232, 'MED01127', 'DICYNONE CAPX500MGX100 - 20 BLIS', 83, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(233, 'MED01128', 'MALTOFER X30 TAB MASTICABLE', 83, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(234, 'MED00758', 'HIRUDOID FORTE GELX14GR', 87, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(235, 'MED00759', 'HIRUDOID FORTE POMX14GR', 87, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(236, 'MED00760', 'HIRUDOID POMX14GR', 87, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(237, 'MED00761', 'PURINATOR SUSP X 100ML', 87, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(238, 'MED00762', 'PURINOR SUSP/FCOX100ML', 87, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(239, 'MED00763', 'PURINATOR X 100 TAB ', 87, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(240, 'MED01061', 'PURINOR  X 100 TAB', 87, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(241, 'MED01129', 'COMBIZYM-COMPX100TAB - 10 BLIS', 87, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(242, 'MED00030', 'BACTRIM FORTE x100 COMP ', 91, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(243, 'MED00820', 'BACTRIM BALSAMICO JBEx100ML', 91, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(244, 'MED00821', 'BACTRIM COMPX400/80MGX20', 91, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(245, 'MED00822', 'BACTRIM FORTE SUSX100ML', 91, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(246, 'MED00823', 'BACTRIM SUSP 200/40MG X 100ML', 91, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(247, 'MED00035', 'DIAREN COM X 200 ', 98, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(248, 'MED00921', 'DIAREN SUSX100ML', 98, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(249, 'MED00924', 'OTICUM GOT OTICAS X5ml', 98, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(250, 'MED00925', 'MIGRAX X 100  COMP', 98, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(251, 'PRE00011', 'TRIOVAL SUSX100ML', 98, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(252, 'PRE00031', 'TRIOVAL GOTX15ML', 98, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(253, 'MED00929', 'BRONCOFLAM BALSAM.X 60 ML', 99, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(254, 'MED00930', 'CITROCALCIO X 30 TAB', 99, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(255, 'MED00931', 'GINOFEN X 6 OVULOS', 99, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(256, 'MED00933', 'MEDIPIEL-B  CREMA X 20 GR', 99, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(257, 'MED00934', 'MEDITOS JBE X120 ML', 99, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:54'),
(258, 'MED00935', 'ORABIOT BALSAMICO X 60 ML NO', 99, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(259, 'MED00936', 'VARIXINA  GEL  X 40GRS', 99, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(260, 'MED00937', 'METROFEN  X10 OVUL', 99, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(261, 'MED00938', 'PROSTANATUR 320 X 30 CAPS', 99, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(262, 'MED00939', 'HIGANATUR 300MGX60CAP ', 99, 1, NULL, 2, NULL, 2.00, '2018-08-28', '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(263, 'MED00940', 'HIGANATUR B150 FORTEx60CAP ', 99, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(264, 'MED00941', 'HIGANATUR MAX FORTEX60CAP ', 0, 1, 0, 2, NULL, 2.83, '2018-08-24', '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(265, 'MED00942', 'BRONCOFLAM BALSAMICO X100 TAB ', 99, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(266, 'MED00943', 'CETIRIZINA TDN ALLERGY X 100 ', 99, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(267, 'MED00944', 'HIGANATUR B150MGX100 CAP ', 99, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(268, 'MED00945', 'MAXIFLAN FORTE 550x100TAB ', 99, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(269, 'MED00946', 'PANAFLAM FORTE X 100 TAB ', 99, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(270, 'MED00947', 'TAXIGEN 100MG.x 100 TAB ', 99, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(271, 'MED00948', 'COMPLEJO B FORTEX200 CAP', 99, 0, 0, 2, NULL, 0.50, '2018-09-12', '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 14, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(272, 'MED01139', 'HIGANATUR 500MG X30CAPS - 3 BLIS', 99, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(273, 'MED01140', 'MAXIDOL COMPUESTO X100 - 10 BLIS', 99, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(274, 'MED01149', 'HIGAPROTECTOR 400X60CAPX6BLISTER', 99, 1, NULL, 2, NULL, 1.20, NULL, '0', 18.00, '1', 2.50, 2.50, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(275, 'AMP00122', 'DIPROSPAN FAST X 2ML  AMP', 100, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(276, 'AMP00202', 'DIPROSPAN SP AMPX1ML', 100, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(277, 'MED00927', 'AFRIN 0.25% GOTx15ML  NI?O', 100, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(278, 'MED00928', 'QUADRIDERM EXTRA SP CREX10GR', 100, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(279, 'MED01073', 'TRIDERM CRE/TUBx15GR', 100, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(280, 'OTC00069', 'EMULSION SCOTT NARANJAX200ML', 102, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(281, 'OTC00390', 'EMULSION SCOTT CEREZAX200ML', 102, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(282, 'OTC00391', 'EMULSION SCOTT CEREZAX400ML', 102, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(283, 'OTC00394', 'EMULSION SCOTT NARANJ.X400ML', 102, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(284, 'AMP00203', 'FLODIN AMPx15MGx3', 107, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(285, 'MED00036', 'ZOLTUM COMP. 40MGx14', 107, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(286, 'MED00037', 'SUPRACAL 1 GR  X 100 COMP ', 107, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(287, 'MED00973', 'BLADURIL 200MGx20 COMP', 107, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(288, 'MED00974', 'CLINFOL DUO X 7 OVULOS', 107, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(289, 'MED00975', 'GESLUTIN-PNM 200MG X 10 TAB', 107, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(290, 'MED00976', 'NOVACILINA 750MG X 5  COMP', 107, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(291, 'MED00978', 'NOVACILINA 500 X 7 TAB', 107, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(292, 'MED00980', 'FLODIN 15MG X 100 COMP ', 107, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(293, 'MED01151', 'REFLUCIL 5MGX30COMPX3BLISTER', 107, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(294, 'OTC00001', 'IRRIGOR PLUS COMPx30 ', 107, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(295, 'PSI00011', 'SUPRACALM DUO  x 60 COMP', 107, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(296, 'AMP00207', 'VAXIGEL X 100 OVULOS ', 108, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(297, 'MED00040', 'FLEXURE MSM X 30 SOBRES', 108, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:55'),
(298, 'MED01009', 'ALBISEC X 12 CAP', 108, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(299, 'MED01010', 'BIANOS 1GR.x 2 TABLETAS', 108, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(300, 'MED01011', 'GESTAFER X30 CAP', 108, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(301, 'MED01013', 'UNICAL D x30CAP BLANDAS', 108, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(302, 'MED01014', 'VITAMINA E-400 UI X 50TAB', 108, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(303, 'MED01015', 'MUVETT S X 21 TAB', 108, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(304, 'MED01016', 'ALERCET 10MG X 100TB ', 108, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(305, 'MED01017', 'DAYFLU NF  X 100  CAP ', 108, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(306, 'MED01018', 'DOLONET FORTE 400MGX100TB ', 108, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(307, 'MED01089', 'MELOXX 15MGX100 CAP', 108, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(308, 'MED01144', 'BETADUO 1ML XAMP(BETAMETASONA', 108, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(309, 'PRE00017', 'ALERCET D X 100 CAP', 108, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(310, 'AMP00129', 'FLUCONAZOL 200MG/100MLX1AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(311, 'AMP00130', 'GENTAMICINA AMP 160 mg x10 amp', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(312, 'AMP00132', 'RANITIDINA 50MGx10 AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(313, 'AMP00133', 'AMIKACINA 500MG/2MLx 10 AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(314, 'AMP00134', 'AMPICILINA 1GR.AMPOLLA x10', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(315, 'AMP00135', 'BENCILP*BENZATINIA*1200x10AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(316, 'AMP00136', 'BENCILP*BENZATINICA*2400X10 AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(317, 'AMP00137', 'BENCILPE SODICA1000x10 AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(318, 'AMP00138', 'CEFALOTINA 1Gx10 AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(319, 'AMP00139', 'CEFAZOLINA 1G X10 AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(320, 'AMP00140', 'CEFRADINA 1GR X 10 AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(321, 'AMP00141', 'CEFTRIAXONA 1GR X 10 AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(322, 'AMP00142', 'CLINDAMICINA 600/4ML X 10 AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(323, 'AMP00143', 'DICLOFENACO 75MGx10 AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(324, 'AMP00144', 'HIDROCORTISONA 100MG X 10AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(325, 'AMP00145', 'HIOSCINA N-BUTIL 20MGX10 AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(326, 'AMP00146', 'LINCOMICINA 600 MG X 10 AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(327, 'AMP00147', 'OMEPRAZOL 40MG X10AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(328, 'AMP00148', 'OXACILINA 1GR X 10 AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(329, 'EST00001', 'VANCOMICINA 500MGX10AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(330, 'PSI00012', 'TRAMADOL 50 MG X 10  AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(331, 'PSI00023', 'TRAMADOL 100 MG X 10 AMP', 111, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(332, 'AMP00149', 'POSTINOR  X 1 COMP', 117, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(333, 'AMP00208', 'POSTINOR X 2 CAPSULAS', 117, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(334, 'MED01019', 'RIGEVIDON 21+7X28COMP', 117, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(335, 'OTC00002', 'SOLUCION BUROW SOL FCOX55ML', 121, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(336, 'OTC00096', 'BUK CITOSVITAMINA C X50 SOB', 121, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(337, 'OTC00097', 'BUK FRESA BOLSA X20 CARAMELOS', 121, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(338, 'OTC00098', 'BUK LIMON BOLSA X20 CARAMELOS', 121, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(339, 'OTC00099', 'BUK MENTA BOLSA X20 CARAMELOS', 121, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(340, 'OTC00100', 'BUK NARANJA BOLSAX20CARAMELOS', 121, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(341, 'AMP00158', 'FERANIN AMPX5 BEBIBLES', 122, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:56'),
(342, 'MED00093', 'FERANIN FCO/GOTx25MGx20ML', 122, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(343, 'MED00094', 'FERANIN FORT GOTX50MGX20ML', 122, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(344, 'MED00095', 'FERANIN JBEX100ML', 122, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(345, 'MED01023', 'FERANIN FOL X 150  GRA ', 122, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(346, 'OTC00039', 'PRESER.PIEL/SENSITIVX24(MORA) - 24 UND', 124, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(347, 'OTC00104', 'PRESER.PIEL FANTASI NARANJX24 - 24UND', 124, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(348, 'OTC00105', 'PRESER.PIEL FANTASIA FRESAx24 - 24 UND', 124, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(349, 'OTC00106', 'PRESER.PIEL FANTASIA MENTAx24 - 24 UND', 124, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(350, 'OTC00107', 'PRESER.PIEL RETARDANT CJAX24 - 24 UND', 124, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(351, 'OTC00108', 'PRESER.PIEL SUPER SEGURO X24 - 24 UND', 124, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(352, 'OTC00109', 'PRESER.PIEL/CLASICOx24(ROJO) - 24 UND', 124, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(353, 'OTC00110', 'PRESER.PIEL/CON AROSX24(NARA) - 24 UND', 124, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(354, 'OTC00111', 'PRESER.PIEL/CON ESPULX24(AZUL) - 24 UND', 124, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(355, 'OTC00469', 'PRESER.PIEL TRIPLE SENSACION X 24 UNID', 124, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(356, 'AMP00159', 'BETASPORINA INY IM 1G', 125, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(357, 'MED00106', 'VARIMINE SOL ORAL X 200 ML', 125, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(358, 'MED00108', 'VARIMINE-STRESS TABx20', 125, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(359, 'MED00109', 'MAXICEF(CEFIXIMA)400MGX8 COMP', 125, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(360, 'MED00110', 'ZINASEN TABx10MGx20', 125, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(361, 'OTC00114', 'FRUTTIFLEX PED FRESA X 500ML', 126, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(362, 'OTC00115', 'FRUTTIFLEX-50 ANISx1000ML', 126, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(363, 'OTC00116', 'FRUTTIFLEX-50 FRESAX1000ML', 126, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(364, 'OTC00466', 'FRUTTIFLEX FCO 1000ML TUTTI FRESA', 126, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(365, 'AMP00160', 'DIOXAFLEX PLUS X 3 AMP', 127, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(366, 'MED00118', 'NIFURAT SUSX100ML', 127, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(367, 'MED00121', 'NICOVEL CAP.VAG X 100 ', 127, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(368, 'MED00122', 'PANTOMICINA TABx500MGx100 ', 127, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(369, 'MED00123', 'DIOXAFLEX  PLUS X 120 CAP ', 127, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(370, 'MED00125', 'HISALER 10 MG  X120 COMP ', 127, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(371, 'MED00126', 'ANAFLEX MUJER X 150 CAP ', 127, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(372, 'MED00127', 'NASTIZOL COMP FORTE X150', 127, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(373, 'MED00128', 'NASTIZOL COMP JRx150 ', 127, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(374, 'MED01104', 'TALFLAN 15MG X 100 COMP - 10 BLIS', 127, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(375, 'PRE00002', 'NASTIZOL JR X 100 ML', 127, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(376, 'AMP00002', 'GYNODIAN-DEPOT AMPx1ML', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(377, 'AMP00161', 'MESIGYNA AMPx1ML', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(378, 'MED00129', 'APRONAX SUSx125MGx60ML', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(379, 'MED00131', 'ASPIRINA 100 MG X 140 TAB', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(380, 'MED00133', 'ASPIRINA TABX500MGX100', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:57'),
(381, 'MED00134', 'BEPANTHENE CREx30GR', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(382, 'MED00135', 'BEPANTHENE UNGx30GR', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(383, 'MED00136', 'BEROCCA PLUS COMPx30', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(384, 'MED00137', 'BEROCCA PLUS EFERV. X10COMP', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(385, 'MED00138', 'CANESTEN 1% CREM TUBX20GR', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(386, 'MED00139', 'CARDIOASPIRINA TABx100MGx20', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(387, 'MED00140', 'DIANE-35 GRAx21', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(388, 'MED00144', 'MICROGYNON  X21 GRAG', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(389, 'MED00145', 'NATELE  X  28  CAP', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(390, 'MED00146', 'SUPRADYN CAP CJAX30', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(391, 'MED00147', 'SUPRADYN GRAGEASx30', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(392, 'MED00149', 'YASMIN X 21 GRAG', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(393, 'MED00151', 'APRONAX 100MG.x 10 COMP ', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(394, 'MED00152', 'PANKREOFLAT-N X 60 TAB ', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(395, 'MED00153', 'APRONAX COMP 550MGx120 ', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(396, 'MED01025', 'YASMINIQ   X  28  COMP', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(397, 'MED01026', 'APRONAX COMX275MGX200 ', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(398, 'OTC00040', 'REDOXON DOBLE ACCION X 3TB', 128, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(399, 'MED00006', 'ACTIVA28X 28 GRAGEAS', 130, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(400, 'AMP00003', 'BUSCAPINA AMPx1', 131, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(401, 'AMP00004', 'BUSCAPINA COMPOSITUMX1AMP', 131, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(402, 'MED00154', 'BISOLVON ADULTOX120ML', 131, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(403, 'MED00155', 'BISOLVON NI?OS X120ML', 131, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(404, 'MED00156', 'BUSCAPINA GRAX20', 131, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(405, 'MED00157', 'MUCOSOLVAN 15MG/5ML PEDX120ML', 131, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(406, 'MED00158', 'MUCOSOLVAN JBEx30/5MLx120ML', 131, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(407, 'MED00159', 'MUCOSOLVAN-COMPS ADUL X 120ML', 131, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(408, 'MED00160', 'MUCOSOLVAN-COMPS PED JBE120ML', 131, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(409, 'MED00161', 'PHARMATON X30 CAP', 131, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(410, 'MED00162', 'BUSCAPINA-COMPU.TABx100 ', 131, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(411, 'MED00163', 'DULCOLAX 5MG  X100 GRAG ', 131, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(412, 'MED01027', 'MOBIC 15 MG X 100 TAB ', 131, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(413, 'AMP00008', 'ANEURIN 10.000X1AMPX3MLC/JE', 134, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(414, 'AMP00009', 'ANEURIN 25.000X3ML C/JER', 134, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(415, 'AMP00010', 'ANEURIN AMPx1000MGx3ML x1', 134, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(416, 'AMP00011', 'DOLO ANEURIN AMPOLLA+JERIN', 134, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(417, 'AMP00163', 'VITACOSE SOL INY AMPX2ML+GER', 134, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(418, 'AMP00232', 'ANEURIN 5000 SOL INY X 1AMP', 134, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:58'),
(419, 'EST00007', 'CEDEINA EXPECT.FCO X 60ML', 134, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(420, 'MED00166', 'SEDOTROPINA FLAT SUS FCOX15ML', 134, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(421, 'MED00167', 'SEDOTROPINA GOTx15ML', 134, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(422, 'MED00168', 'ANEURIN FORTE X100 GRA ', 134, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(423, 'MED01080', 'DOLO ANEURIN CJA.x10TABLET ', 134, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(424, 'MED01081', 'VITACOSE x 100 GRAGEAS ', 134, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(425, 'MED01105', 'SIM-12 CAPx30', 134, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(426, 'PRE00003', 'ERGOTRATE TABX0.2MGX30', 134, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(427, 'PRE00014', 'DIGRAVIN X100 TAB ', 134, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(428, 'OTC00134', 'ALGODON  X 500 GR', 135, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(429, 'OTC00136', 'ALGODON CKF 10GR PQTEx12', 135, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(430, 'OTC00137', 'ALGODON CKF 25GR PQTEx12', 135, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(431, 'AMP00210', 'VACUNA GEL SUSP.X6 FRASCX5ML', 137, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(432, 'AMP00211', 'BIO LACTOL AMPX25 - 25 UND', 137, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(433, 'OTC00004', 'VACUNA-GEL-NBSUSx200ML', 137, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(434, 'OTC00139', 'LACTO CEREAL KIDS JBE X 50ML', 137, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(435, 'OTC00140', 'VACUNA GEL 100 ML', 137, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(436, 'OTC00045', 'SINACHIS X 120  TAB ', 138, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(437, 'OTC00151', 'JERINGA 5ML/CC X 100 UNID', 138, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(438, 'OTC00152', 'JERINGA INSULINA 1ML X 100', 138, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(439, 'OTC00154', 'TECOTEST TEST DE EMBARAZO', 138, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(440, 'OTC00156', 'PRESER.VLADI/CLASICO(rojo)x24 - 24 UND', 138, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(441, 'OTC00158', 'PRESER.VLADI/NATURE(verde)x24 - 24 UND', 138, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(442, 'MED00174', 'NOXZOLIN X 60 ML SUSP', 139, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(443, 'OTC00005', 'CUAJO MARSHALL L-75x50TAB', 140, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(444, 'AMP00013', 'AMIKACINA 1G/4ML AMP.X1AMP', 140, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(445, 'AMP00014', 'CEFTRIAXONA 1GR IM/IV X 25AM', 140, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(446, 'AMP00015', 'FENITOINA 100MG/2ML I.V.X25AM', 140, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(447, 'AMP00017', 'KETOROLACO 60MG/2MLX25AMP', 140, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59');
INSERT INTO `productos` (`idproductos`, `cod_producto`, `nom_producto`, `idlaboratorio`, `idgrupo`, `idsubgrupo`, `idlinea`, `descripcion`, `mto_compra`, `fec_ult_compra`, `exento_impuesto`, `mto_impuesto`, `metod_venta`, `mto_minimo`, `mto_sugerido`, `porc_ganancia`, `facturarsinstock`, `avisostockminimo`, `stockminimo`, `medida`, `peso`, `idumedida`, `categoria`, `usa_serial`, `serial_1`, `serial_2`, `serial_3`, `etiqueta_label`, `etiqueta_precio`, `etiqueta_codigo`, `imagen`, `ocultar_venta`, `fec_ingreso`) VALUES
(448, 'AMP00018', 'PIRIDOXINA 300MG/3MLX25AMP', 140, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(449, 'AMP00019', 'AMIKACINA 500MG/2ML X25', 140, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(450, 'AMP00020', 'BENCIL.PENIC.BENZA1200 AMP X2', 140, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(451, 'AMP00021', 'BENCILPENI PROCAINICA x25 AMP', 140, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(452, 'AMP00022', 'BETAMETASONA 4MG/1ML AMPX25', 140, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(453, 'AMP00023', 'CLINDAMICINA 600MG/4MLX25AMP', 140, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(454, 'AMP00024', 'DIMENHIDRINATO 50MG X 25  AMP', 140, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(455, 'AMP00025', 'GLUCONATO CALCIO10% X10MLx25 UND', 140, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(456, 'AMP00026', 'ORFENADRINA 60MG/2MLX 25 AMP', 140, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(457, 'AMP00164', 'OXACILINA 1GR X 25 VIALES', 140, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(458, 'AMP00217', 'BENCIL PENIC.SODI.1000.25VIA', 140, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(459, 'MED00096', 'EXAZOL BALSAMI SUSP FCOx100ml', 141, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(460, 'MED00097', 'EXAZOL FORTE JBEX100ML', 141, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(461, 'MED00099', 'CIPROXAN 500MGX100TAB ', 141, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:31:59'),
(462, 'MED00100', 'DOPROX 550MGX100 COMP ', 141, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(463, 'MED00102', 'EXAZOL FORTE x 100 COMP ', 141, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(464, 'MED00103', 'MAVOL 50MGX100 COMP ', 141, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(465, 'MED00104', 'PALAGRIP-P CAPx100 ', 141, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(466, 'MED00105', 'URODIF 100MG X 100TAB ', 141, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(467, 'MED01024', 'URODIF FORTE x 100 TAB', 141, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(468, 'MED00249', 'CIRUELAX LIQUIDO  X 120 ML', 147, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(469, 'MED00250', 'CIRUELAX MERMELADA X150gr', 147, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(470, 'MED00251', 'CIRUELAX TE LAXANTE X10BL', 147, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(471, 'MED00252', 'CIRUELAX TE LAXANTEx30 BOLS', 147, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(472, 'MED00255', 'FINARTRIT ADVAN PVO X 30SOB', 147, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(473, 'MED00257', 'CIRUELAX FORTE X 60 COMP ', 147, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(474, 'MED00258', 'CONTRAVARIS x 60CAP-6 BLIS', 147, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(475, 'MED00259', 'CIRUELAX FORTE X 100COMP ', 147, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(476, 'MED00260', 'CIRUELAX x 100 COMP', 147, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(477, 'MED01036', 'FINARTRIT X 100 DISP ', 147, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(478, 'MED01111', 'CIRUELAX FORTE X24 - 2 BLIS', 147, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(479, 'MED01224', 'COLAGENO (HIDROLAGENO) X 30 SOBRES', 147, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(480, 'OTC00008', 'SUPOSIT.GLIC.NI?OS x100 UND', 148, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(481, 'OTC00050', 'NITRATO DE PLATA X 12 UNID', 148, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(482, 'OTC00212', 'GENSARNA(BENZ.BENC)LOCX 60ML', 148, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(483, 'OTC00214', 'SUPOSIT.GLIC.ADULTOX100 UNID', 148, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(484, 'OTC00215', 'VITAGEN JBE X 345ML', 148, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(485, 'OTC00216', 'YODOX ESPUMA X 60ML', 148, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(486, 'OTC00217', 'YODOX SOLUCION X 60ML', 148, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(487, 'MED01038', 'TUKOL-D JBE X 120 ML', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(488, 'OTC00219', 'ASEPXIA GEL EMERGENCIA COLOR PIEL.28GR', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(489, 'OTC00220', 'ASEPXIA JABON  AZUFRE', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(490, 'OTC00221', 'ASEPXIA JABON  EXFOLIANTE', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(491, 'OTC00223', 'ASEPXIA JABON  NEUTRO', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(492, 'OTC00224', 'ASEPXIA JABON FORTE', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:00'),
(493, 'OTC00226', 'ASEPXIA POLVO BEIGE MATE', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(494, 'OTC00228', 'ASEPXIA POLVO CLARO MATE', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(495, 'OTC00229', 'ASEPXIA POLVO NATURAL MATE', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(496, 'OTC00231', 'ASEPXIA TOALLITASx10UNID', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(497, 'OTC00232', 'CICATRICURE 4PASOS(KIT', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(498, 'OTC00233', 'CICATRICURE ANTIARRUGAS X 60', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(499, 'OTC00234', 'CICATRICURE DIA BEATY CAREX40G', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(500, 'OTC00235', 'CICATRICURE GEL 60 GR', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(501, 'OTC00236', 'CICATRICURE GELX30GR', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(502, 'OTC00237', 'CHAO X 100 TABL', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(503, 'OTC00238', 'MEDICASP SHAMPOOx130ML', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(504, 'OTC00239', 'SILKA MEDIC GEL 1% X 15GR', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(505, 'OTC00240', 'SILUET 40 GEL TERNICOx200ML', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(506, 'OTC00241', 'TIO NACHO ANT.CAID.CANASx415', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(507, 'OTC00242', 'NIKZON  x 90 TAB ', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(508, 'OTC00243', 'BIO ELECTRO X100TBS ', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(509, 'OTC00427', 'ASEPXIA JABON SOFT', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(510, 'OTC00429', 'ASEPXIA MAQ.CREMA BEIGE MATE', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(511, 'OTC00443', 'ASEPXIA GEL EMERGENCIA TRANSPARENTE X 28GR', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(512, 'OTC00455', 'CICATRICURE ANTIARRUGAS CREMA X30GR', 150, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(513, 'MED00015', 'ZENTEL TABX200MGX100 ', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(514, 'MED00332', 'AMOXIL SUSX250MGX60ML', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(515, 'MED00333', 'COREGA ULTRA MENTA X 40 GRS', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(516, 'MED00334', 'COREGA ULTRA SIN SABOR X 40GR', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(517, 'MED00335', 'COREGA ULTRA SIN SABOR X20GR', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(518, 'MED00336', 'DERMOVATE CREX30GR', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(519, 'MED00337', 'DERMOVATE UNGx40GR', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(520, 'MED00338', 'POSIPEN SUSx250MGx60ML', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(521, 'MED00339', 'RESPIRA MEJORx10 TIRAS NASAL', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:01'),
(522, 'MED00340', 'ZENTEL SUSX400MGX10MLX5FC', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(523, 'MED00342', 'AMOXIL 500MG  X100 CAP', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(524, 'MED00343', 'POSIPEN CAPx500MGx100 ', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(525, 'MED01039', 'VENTOLIN INHx200DOSIS', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(526, 'MED01190', 'ZINNAT 500 MG X 10 TBS', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(527, 'OTC00244', 'PANADOL 500MG  X100 TB', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(528, 'OTC00245', 'PANADOL ALLERGY TAB CJAX50', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(529, 'OTC00246', 'PANADOL ANTIGRIPAL NF TABX100', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(530, 'OTC00247', 'PANADOL EFER X 24SOB', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(531, 'OTC00248', 'PANADOL FORT/MIGRA?A  X48 TB', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(532, 'OTC00249', 'PANADOL GOTx15ML', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(533, 'OTC00250', 'PANADOL JBEx60ML', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(534, 'OTC00251', 'PANADOL X100 TB MASTICABLE', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(535, 'OTC00252', 'SAL DE FRUTA ENO X48SOB', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(536, 'OTC00253', 'SENSODYNE BLANQUEADOR X50 GR', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(537, 'OTC00470', 'SENSODYNE LIMPIEZA PROFUNDA DE 90 GR', 152, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(538, 'AMP00051', 'MEGACILINAAMP UNMILLON SIN/JE', 153, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(539, 'EST00002', 'CODIPRONT EXPEC JBEX60ML', 153, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(540, 'EST00008', 'CODIPRONT EXPECTORAN X 10 CAP', 153, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(541, 'MED00349', 'AFLAMAX SUS/x125MGX5ML/60ML', 153, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(542, 'MED00351', 'FEBRAX SUSP FCOX60ML', 153, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(543, 'MED00353', 'AFLAMAX 550MG TAB CJAX90', 153, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(544, 'MED01041', 'MEGACILINA-ORAL COMx120 ', 153, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(545, 'MED01093', 'TOPSYM-POLIVALENTE CREx5GR', 153, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(546, 'MED01094', 'FEBRAX X60 TAB ', 153, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(547, 'MED01148', 'MICROSER 8MGX50TABX5BLISTER', 153, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(548, 'PSI00001', 'TRAMAL AMPX50MGX1ML', 153, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(549, 'PSI00006', 'TRAMAL LONG COMX100MGX10 ', 153, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(550, 'PSI00007', 'ZALDIAR   X60 TAB ', 153, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(551, 'PSI00021', 'TRAMAL AMPX100MGX2ML X1', 153, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(552, 'PSI00022', 'TRAMAL CAPX50MGX10 ', 153, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(553, 'AMP00052', 'GENTASIL AMPx160MG2ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(554, 'AMP00175', 'HANALGEZE AMPx60MGx1', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(555, 'MED00354', 'ALERGICAL NEO SOL FCOX 15ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(556, 'MED00355', 'BETACREM CREx20GR', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(557, 'MED00356', 'BRONCOPULMIN NF X120ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(558, 'MED00357', 'CIPROLIN TABx500MGx10', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:02'),
(559, 'MED00358', 'CHARCOT UNGUEN. POTE X 100 GR', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(560, 'MED00359', 'DOLORAL GOTx40MGx15ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(561, 'MED00360', 'DOLORAL SUSX60ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(562, 'MED00361', 'ESCALDEX CREMA X 20MG', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(563, 'MED00362', 'FERRONICUM SOLUC.ORALx345ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(564, 'MED00363', 'FERROSIL(HIERRO)50MG/1MLX20ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(565, 'MED00365', 'GASEOPLUS GOT 80MGX15ML FRESA', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(566, 'MED00366', 'GASEOPLUS GOT 80MGx15ML NATUR', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(567, 'MED00367', 'HIEDRATOS JBE X 100ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(568, 'MED00368', 'INFECTRIM 200/40MGX60ML SUS', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(569, 'MED00369', 'INFECTRIM BALS.SUSPX50ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(570, 'MED00370', 'INFECTRIM BALSAM SUSX100ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(571, 'MED00371', 'INFECTRIM FORTE SUSPx100ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(572, 'MED00372', 'ISORBIDE ORAL TABx10MGx20', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(573, 'MED00373', 'LIBBERA 5MG/1MLGTSx15ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(574, 'MED00374', 'MAGAL II SUSx150ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(575, 'MED00375', 'MAGAL-D SUSP. X200ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(576, 'MED00376', 'MAXIMUS  PLUS  X 32 TAB', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(577, 'MED00377', 'MERTHIOLATE PLUS-TINTURAx60ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(578, 'MED00378', 'MUCOVIT CREX30GR', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(579, 'MED00379', 'MUCOVIT HRS JBX110ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(580, 'MED00380', 'MUCOVIT NF GOTX15ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(581, 'MED00381', 'MUCOVIT-B FCO/GOTx15ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(582, 'MED00382', 'NOTIL CREx10GR', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(583, 'MED00383', 'OSTEOVIT JR PLATAN.SUSPx200ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(584, 'MED00384', 'OSTEOVIT JR.FRESA SUSP x200ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(585, 'MED00385', 'TABRON NFx345ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(586, 'MED00386', 'TERNESIL CREMA X 20 GRS', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(587, 'MED00387', 'TONIKON JBE x 345ml', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(588, 'MED00388', 'TOPICREM CREX10GR', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(589, 'MED00389', 'ISORBIDE SUBLINGUAL X 25 TAB ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(590, 'MED00390', 'ACIDO FOLICO x0.5MG x30 TAB', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(591, 'MED00391', 'FLUIXX 600MG. X 30 SOBRES', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(592, 'MED00392', 'MADDRE DSS X 30 TABS', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(593, 'MED00393', 'PROSTASIL 0.5MG X 30 CAP ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(594, 'MED00394', 'PROSTASIL PLUS X 30 CAP ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:03'),
(595, 'MED00395', 'GASEOPLUS CAPx40 BLANDAS', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(596, 'MED00396', 'HANALGEZE 10MG  X50 TAB ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(597, 'MED00397', 'MERTHIOLATE PLUS-INCOLOx60ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(598, 'MED00398', 'UROCEFASABAL 400MG TAB CJAX60 ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(599, 'MED00399', 'VERTE X  60  CAP ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(600, 'MED00400', 'WARMI x 90 CAP ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(601, 'MED00401', 'AZO-CEFASABAL x 100 TAB ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(602, 'MED00402', 'AZOFLOX 400MG+50MGX100TAB ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(603, 'MED00403', 'CEFASABAL TABx100 ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(604, 'MED00404', 'DOLORAL TABX200MGX100 ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(605, 'MED00405', 'DOLORAL TABX400MGX100 ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(606, 'MED00406', 'GERO MUCOVIT  X 100 CAP ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(607, 'MED00407', 'GERO MUCOVIT PLUSx100CAP ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(608, 'MED00409', 'LIBBERA 5MG X 100TB ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(609, 'MED00410', 'MUCOVIT NF CAPx100 ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(610, 'MED00411', 'OSTEOVIT D 400MG X 100 ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(611, 'MED00412', 'OSTEOVIT D3 x 100 TAB', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(612, 'MED00413', 'PYRIDIUM COMPLEX X 100', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(613, 'MED00414', 'PYRIDIUM X100 GRAG ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(614, 'MED01043', 'UROPOL FORTE  X100', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(615, 'MED01044', 'PALDOLOR EXT FORTX200 TAB ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(616, 'MED01112', 'MADDRE CREMA DE 100 GR', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(617, 'MED01113', 'OSTEOVIT MAX FRESA  200ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(618, 'MED01228', 'PYRIDIUM COMPLEX X 100 TAB', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(619, 'MNAT00002', 'U?A DE GATO X 100 +MACA X 30', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(620, 'OTC00054', 'MACA SCHULER X 130 CAP', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(621, 'OTC00265', 'MADDRE POLVO CHOCOLATE X360', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(622, 'OTC00266', 'MADDRE POLVO DHA X 400G.VAINIL', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(623, 'OTC00267', 'MADDRE POLVO DHAX400 CHOCOLTE', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(624, 'OTC00268', 'MADDRE VAINILX360GR', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(625, 'OTC00269', 'OSTEOVIT POLVO X 330GR', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(626, 'OTC00270', 'P-V-M x 460GR CHOCOLATE POL', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(627, 'OTC00271', 'P-V-M X 460GR FRESA POL', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(628, 'OTC00272', 'P-V-M x 460GR VANILLA POL', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(629, 'OTC00273', 'P-V-Mx360 FRESA JUNIOR', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(630, 'OTC00274', 'P-V-Mx360 VANILLA JUNIOR', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(631, 'OTC00275', 'P-V-Mx360GR CHOCOLATE JUNIOR', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(632, 'OTC00277', 'U?A-DE GAT SCHULERX100', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(633, 'OTC00419', 'WARMI POLVO x 330GR', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(634, 'OTC00420', 'PROSTABIEN X 100 TAB ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:04'),
(635, 'OTC00477', 'MADDRE DHA CAPS BLANDA X 30', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(636, 'PRE00005', 'LIBBERA-D GOTAS  X 15 ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(637, 'PRE00027', 'ALERGICAL-SF JBEX60ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(638, 'PRE00028', 'LIBBERA-D  X  60 ML', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(639, 'PRE00029', 'ALERGICAL-SF X100 TAB ', 154, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(640, 'INA00004', 'METFORMINA 850MGX100', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(641, 'MED00043', 'TERAZOSINA 5MGx100 TAB', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(642, 'MED00415', 'ACICLOVIR 200MG X 100 TAB', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(643, 'MED00416', 'ACIDO ALENDRONICO 70MGx4TAB', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(644, 'MED00417', 'CAPTOPRIL 25 X100TBS', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(645, 'MED00418', 'CELECOXIB 200MG X 100 TAB', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(646, 'MED00419', 'CETIRIZINA 10MG X 100TAB', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(647, 'MED00420', 'CLORFENAMINA 4MGx100 TAB', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(648, 'MED00421', 'DEXAMETASONA 0.5MGx100 TAB', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(649, 'MED00422', 'DEXAMETASONA 1MGx100TAB', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(650, 'MED00423', 'DEXAMETASONA 4MGx 100TAB', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(651, 'MED00424', 'DIMENHIDRINATO 50MG X 100TB', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(652, 'MED00425', 'FLUCONAZOL 150MG X 2 CAP', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(653, 'MED00426', 'FURAZOLIDONA 100MGx100 TAB', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(654, 'MED00428', 'IBUPROFENO TABX400MGX100', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(655, 'MED00429', 'KETOPROFENO 100MG x100TAB', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(656, 'MED00430', 'LOPERAMIDA X 100 TAB', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(657, 'MED00431', 'PIROXICAM 20 MGX100TAB', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(658, 'MED00432', 'PREDNISONA 5MG/5ML X 120ML', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(659, 'MED00433', 'SALBUTAMOL 4MG X100 TAB', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(660, 'MED00435', 'TERBINAFINA 250MG X 30TAB', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(661, 'MED00436', 'CLOTRIMAZOL 500MGX50TAB VAG', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(662, 'MED00437', 'ACICLOVIR 400MG X 100TAB', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(663, 'MED00438', 'SINALERG 5MG X 100 TAB ', 157, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(664, 'MED00489', 'HEMORSAN UNG X 30MG', 216, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(665, 'MED00490', 'MUXATIL 100MG X 10 SOBRES', 216, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(666, 'MED00493', 'NOXOM   200MG x 6 TAB', 216, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(667, 'MED00494', 'NOXOM 500MGx6TAB', 216, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(668, 'MED00495', 'NOXOM X  60 ML  JBE', 216, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(669, 'MED01045', 'QUEMACURAN-L TUBO X 15GR', 216, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(670, 'OTC00055', 'TONICO INTI S/CAFEINA X200ML', 216, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(671, 'OTC00278', 'TONICO INTI C/CAFEINA X200 ML', 216, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05');
INSERT INTO `productos` (`idproductos`, `cod_producto`, `nom_producto`, `idlaboratorio`, `idgrupo`, `idsubgrupo`, `idlinea`, `descripcion`, `mto_compra`, `fec_ult_compra`, `exento_impuesto`, `mto_impuesto`, `metod_venta`, `mto_minimo`, `mto_sugerido`, `porc_ganancia`, `facturarsinstock`, `avisostockminimo`, `stockminimo`, `medida`, `peso`, `idumedida`, `categoria`, `usa_serial`, `serial_1`, `serial_2`, `serial_3`, `etiqueta_label`, `etiqueta_precio`, `etiqueta_codigo`, `imagen`, `ocultar_venta`, `fec_ingreso`) VALUES
(672, 'MED00496', 'CALOREX FORTE CREM X 20 GR', 159, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:05'),
(673, 'MED00498', 'EXCALDENE  CREMA X 40 GRS', 159, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(674, 'MED00499', 'HONGOFIN CREMA 20  GRS', 159, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(675, 'MED00500', 'HONGOTIL COMPUESTO X 15 GRS', 159, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(676, 'MED00503', 'SULFACREM UNG 20GR', 159, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(677, 'MED00504', 'SULFACREM UNG 5GR X 24 LATAS', 159, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(678, 'OTC00010', 'TALCO ORIGINAL JOHNSONx200GR', 162, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(679, 'OTC00279', 'ACEITE JOHNSONx50ML', 162, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(680, 'OTC00281', 'ESTUCHE PEQUE?O JOHNSONx2', 162, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(681, 'OTC00284', 'HILO DENTAL MENTA 24x50m', 162, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(682, 'OTC00286', 'LISTERINE ANTISARRO 180ML', 162, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(683, 'OTC00287', 'LISTERINE ANTISARRO x500ML', 162, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(684, 'OTC00289', 'LISTERINE CUID.TOTAL X 500ML', 162, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(685, 'OTC00290', 'PA?ITOS HU.ALOEVERA JHON 70+7', 162, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(686, 'OTC00291', 'PA?ITOS HUM.REP JOHNSON 70+70', 162, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(687, 'OTC00292', 'SHAMPOO ORIGIN JOHNSONx200ML', 162, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(688, 'OTC00293', 'TALCO JOHNSON ORIGIN 100GR', 162, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(689, 'OTC00432', 'SHAMPOO MANZ JOHNSONx200ML', 162, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(690, 'OTC00468', 'LISTERINE COOLMINT 500 ML', 162, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(691, 'MED00508', 'ACETILCIST600MGMUCOASMAT)X30S', 164, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(692, 'MED00509', 'CELEXOCIB(ARTICOX)X100CAP', 164, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(693, 'MED00510', 'DOLITO(IBUPROFENO) SUSX60ML', 164, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(694, 'MED00512', 'DONALAB CJA X100CAP', 164, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(695, 'MED00515', 'LABOPAN X100 TAB ', 164, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(696, 'MED01098', 'URONOLAB FORTE X100TAB - 10 BLIS', 164, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(697, 'MED01158', 'URONOLAB PLUS X100TAB-10BLIS', 164, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(698, 'MED01214', 'FENAZOPIRINA 100MG(FENPIRINOX)', 164, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(699, 'MED00185', 'AZITROMAC 200MG/5ML x 30ML', 167, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(700, 'MED00186', 'AZITROMAC 500 MG X 30 COMP', 167, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(701, 'MED00187', 'ENGRIP FORTEX100TAB', 167, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(702, 'MED00188', 'MIGRA DEL X100TAB REC', 167, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(703, 'MED00189', 'PROSTADEL LP X30CAPS', 167, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(704, 'MED00190', 'SILDEN UP 100MG 50TAB', 167, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(705, 'MED00191', 'CLENXOL F X 100TAB ', 167, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(706, 'MED00192', 'DEXALOR x 100 TAB ', 167, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(707, 'MED00193', 'ESPASMODEL.COMP. X100', 167, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(708, 'MED00194', 'LEVO-DEL x 100 TAB ', 167, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(709, 'MED01030', 'MIODEL RELAX x 100 TAB', 167, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(710, 'MED01031', 'TOP-DEL FORTE X120TAB', 167, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(711, 'MED01225', 'BRONCO-CLARIMAC CAJA X 30 TAB', 167, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(712, 'OTC00046', 'CLINDA C 300MG X 100 CAP', 167, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(713, 'AMP00055', 'BENCILP BENZATIN 1200X10AMP', 169, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(714, 'AMP00056', 'METAMIZOL 1GR/2ML X 50AMP', 169, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(715, 'AMP00177', 'DEXAMETASONA 4MG/2X100AMP', 169, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:06'),
(716, 'MED00018', 'SALBUTAMOL AEROSOL 100MCG', 169, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(717, 'MED00518', 'BECLOMETASONA 250MG/ AEROSOL', 169, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(718, 'MED00520', 'BROMURO DE IPRATROPIO 20MC/AE', 169, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(719, 'MED00521', 'BUDESONIDA 200 MCG/AEROSOL', 169, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(720, 'MED00522', 'GABAPENTINA 300MGX100TAB', 169, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(721, 'AMP00057', 'AGUA ESTERIL 5MLx100 IM/IV', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(722, 'AMP00058', 'DEXAMETASONA 4MG/1ML X 100AMP', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(723, 'FPL00171', 'AZITROMEDIC 500MG CAJA x3TAB', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(724, 'FPL00172', 'BETAFLAM 0.50% x 20GR', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(725, 'FPL00173', 'BRONCO EXPECT JBE X 60ML', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(726, 'FPL00174', 'BRONCOFLEMIN JBEX120ML', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(727, 'FPL00175', 'BRONCOMEDICAL 250MG X 60ML', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(728, 'FPL00176', 'CLORHYSTAMIN 4MG X 20TAB', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(729, 'FPL00178', 'DOLFAMIL 400MG CAJA X100TABLE', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(730, 'FPL00179', 'FARMISIL1% CRE TUBX20GR', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(731, 'FPL00180', 'FUZMEDIC 50MG/05ML SUSP', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(732, 'FPL00181', 'GASEOMEDIC GOT 15MLx ANIS', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(733, 'FPL00182', 'GASEOMEDIC GOTx15ML FRESA', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(734, 'FPL00183', 'GYVAXOL 500MG x 1TAB VAG', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(735, 'FPL00184', 'JUNIDOL SUSP.x 60ML', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(736, 'FPL00186', 'KLOFNIL 250MG/5ML X60ML', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(737, 'FPL00187', 'LEVOBIOTIC 500MG X10TB', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(738, 'FPL00188', 'MACROMICID 500MG CJA X50 TB', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(739, 'FPL00190', 'MEDIBRONCOL COMPS ADUL x120ML', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(740, 'FPL00191', 'MEDIBRONCOL COMPS PED X 120ML', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(741, 'FPL00193', 'MEDIMOXILIN 250MG X 60ML', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(742, 'FPL00194', 'MEDIMOXILIN CL SUSP 60ML', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(743, 'FPL00195', 'MEDITIL CREMA x 20GR', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(744, 'FPL00197', 'MEDITRIM FORTE SUSP X 100ML', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(745, 'FPL00198', 'MEDITRIM SIMPLE  X 60 ML', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(746, 'FPL00199', 'NOALERGYM 5MG/5ML X60ml', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(747, 'FPL00200', 'NYZMEDICAL 5MG/5ML FCOX 120ML', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(748, 'FPL00201', 'PARASITROL 20ML X2FCOS', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(749, 'FPL00202', 'PLUSZOL 150MG X 1 CAPS', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(750, 'FPL00203', 'RINALERG 5MG/5ML JBE FCOX60ML', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(751, 'FPL00204', 'SINFIDOL 100MG X 1TAB', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(752, 'FPL00205', 'APROMEDIX TABL x 100', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(753, 'FPL00206', 'BRONCOMEDICAL 500MGX100', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:07'),
(754, 'FPL00207', 'CLINDASTED 300MX100 CAP', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(755, 'FPL00208', 'DONAMEDICAL(LOPERAM)2MGX100TAB ', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(756, 'FPL00209', 'GASEOMEDIC 40MG X100TAB', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(757, 'FPL00210', 'GRAFLOMED 50MG X 100TAB', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(758, 'FPL00211', 'GRIPAMEDIC C TABL. X 100', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(759, 'FPL00212', 'HIGAMEDIC B150MGX100CAP', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(760, 'FPL00213', 'MEDIFLOXIN CJAX100 - 25 BLI(4)', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(761, 'FPL00214', 'MEDILGINA 500MG TAB CJAX100', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(762, 'FPL00215', 'MEDITRIM BALSAMICOx100TAB', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(763, 'FPL00218', 'NYZMEDICAL(PREDNIS20MGX100TAB', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(764, 'FPL00219', 'PLIDAMEDIC COMPT.x100TAB', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(765, 'FPL00220', 'REUMAFLAN 200MG X 100 CAPSULA', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(766, 'FPL00221', 'MEDICIRIN F x 120 TAB', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(767, 'FPL00222', 'MEDIRELAX X 120TAB ', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(768, 'FPL00223', 'NOALERGYM 10MG X 120 TAB ', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(769, 'FPL00320', 'VEMOXIL 500MG X 100CAP', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(770, 'FPL00323', 'SINFIDOL 50MG TAB CJAX1', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(771, 'FPL00324', 'DOLO CALM E/F.X 200 TAB', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(772, 'FPL00327', 'MEDIMOXILIN CL X 10 TAB', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(773, 'FPL00342', 'MEDIXAN 15MG CAJA X 100 TAB', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(774, 'FPL00351', 'DICLOGESIC GEL1%TUBO X30GR', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(775, 'FPL00400', 'CYFLOMEDIX 500MG TAB CAJA X 100', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(776, 'FPL00401', 'LYTORMED 20MG CAJA X 30 TAB REC', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(777, 'FPL00402', 'MEDILGINA 500MG/MLX10ML GOTAS', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(778, 'MED00044', 'QUENALMED 10MG CJAX50TAB', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(779, 'MED00045', 'UROMEDIC X 108 CAP ', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(780, 'OTC00029', 'PRESER.SENSACION NAT X 24', 170, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(781, 'MED00523', 'FLORIL GOT/OFTX15ML', 172, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(782, 'MED00524', 'FLORIL GOT/OFTX8ML', 172, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(783, 'MED00525', 'FRAMIDEX 2.5ML GOTS', 172, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(784, 'MED00527', 'LAGRIMAS-ISOTON SOLx15ML', 172, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(785, 'MED00528', 'LANCIPROX DX OFT.X 5ML', 172, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(786, 'MED00529', 'LANCIPROX SOL.OFTAL X 5ML', 172, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(787, 'MED00530', 'OTIDOL GOTAS X 5 ML', 172, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(788, 'MED00531', 'TERRAMISOL-A UNG/OFTx6GR', 172, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(789, 'MED00532', 'TETRALAN UNG.OFT.x 6 GR', 172, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:08'),
(790, 'AMP00178', 'FLUIMUCIL AMPx3ML x5', 173, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(791, 'MED00019', 'FLUIMUCIL ORAL SOBX200MGX30', 173, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(792, 'MED00539', 'FLUIMUCIL 600 X 20 COMP', 173, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(793, 'MED00540', 'FLUIMUCIL FCOX100MGX120ML', 173, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(794, 'MED00541', 'FLUTOX 17.7MG/5ML X120ML', 173, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(795, 'MED00542', 'OTOZAMBON FCO/GOTX10ML', 173, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(796, 'MED00543', 'TROPIVAG  X 14 CAP VAGINALES', 173, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(797, 'MED00544', 'FLUIMUCIL ORAL SOBx100MGx30', 173, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(798, 'MED01050', 'VITERNUM VITAMINA FCOx150ML', 173, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(799, 'MED01202', 'ISOPRINSINE 500X24 CAP.', 173, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(800, 'OTC00011', 'TRAUMAPLANT CREx25GR', 173, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(801, 'OTC00298', 'LACTAMOUSSE HIG INTIMA X 125M', 173, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(802, 'AMP00061', 'BENALGIN 10 000-C/JERX1AMP', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(803, 'AMP00062', 'BENALGIN 1000 X 1 ML AMP', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(804, 'AMP00063', 'CICLOSTERONA FUERTE AMPx1', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(805, 'AMP00064', 'DEXA-BENALGIN  AMP', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(806, 'AMP00065', 'DOLO-BENALGIN AMPX2ML+JERI', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(807, 'AMP00066', 'GENTAGRAM AMPX160MGX2ML', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(808, 'AMP00067', 'CLORFENAMINA MALEATO X 25 AMP', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(809, 'AMP00068', 'DEXAMETASONA 4MG X 25 AMP', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(810, 'AMP00069', 'DEXTRO-LUSA AMPx20MLx25', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(811, 'AMP00070', 'GENTAMICINA 160MG/2MG X25 AMP', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(812, 'AMP00071', 'KALIUM AMPx10MLx25', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(813, 'AMP00072', 'LACTATO-RINGER AMPx10MLx25', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(814, 'AMP00179', 'SEXSEG 1AMP.x 1ML+JERINGA', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(815, 'AMP00180', 'LIDOCAINA-2% S/E 20ML X 25AMP', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(816, 'MED00020', 'RHINO-DAZOL ADUGOT/FCOx15ML', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(817, 'MED00546', 'DOLO-BENALGIN FORTE X 20TAB', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(818, 'MED00547', 'GENTAMICINA 0.3% X 5ML GOTAS', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(819, 'MED00548', 'IBUPROFENO 100MG/5ML X 60ML', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(820, 'MED00549', 'LIDOCAINA-UNGUEN-5% X10GR', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:09'),
(821, 'MED00550', 'LUSAPRIM SUSP.X 60ML', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(822, 'MED00551', 'MISALUD TABx30', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(823, 'MED00552', 'NISONA 5MG/5MLX100ML SUSP', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(824, 'MED00555', 'WARFARINA 5MG X 100 TAB', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(825, 'MED00556', 'NISONA TAB 20MG x120', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(826, 'MED01117', 'PREDNISONA 20MGx100 TAB', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(827, 'MED01178', 'LACTULOSA 3.3G/5ML*120ML', 174, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(828, 'AMP00073', 'CEFTREX AMP X 1G', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(829, 'AMP00183', 'MIOFEDROL RELAX AMP. 1ML I.M', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(830, 'MED00574', 'BRONCODEX COMP x120ML', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(831, 'MED00575', 'BRONCODEX COMPUT.GTS X20ML', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(832, 'MED00576', 'CIPROMAX 500MGX100COMP', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(833, 'MED00577', 'CORTAFAN NF SUSPX100ML', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(834, 'MED00578', 'DERMAZOL CREx10GR', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(835, 'MED00579', 'DEXABRON NF JBEX120CC', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(836, 'MED00580', 'ESPASMOSEDIL COMPS.X100COMP', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(837, 'MED00581', 'HEMOCYTON JARX340ML', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(838, 'MED00582', 'KALLOZIL LIQUIDO FCOx8ML', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(839, 'MED00583', 'LINDIOL 1.5MG X 1 TABLETA', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(840, 'MED00584', 'NOVOTRIM BALSAMICOX100ML', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(841, 'MED00585', 'PARASITEL 400MGX10MLX100ML', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(842, 'MED00586', 'YODIL-COMPUES POTx13GRx12GR', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(843, 'MED00587', 'CORTAFAN X 100 COMP ', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(844, 'MED00588', 'DEXABRON NF X 100CAP', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(845, 'MED00589', 'DEXABRON PLUS TAB X100', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(846, 'MED00590', 'DOLOAPROXOL FORTE X 100TB', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(847, 'MED00591', 'HEPAVIT B COMPLEXx100CAP ', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(848, 'MED00592', 'MIOFEDROL RELAXPLUSx100CAP ', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(849, 'MED00593', 'NEOENZIMAX CAPX100 ', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(850, 'MED00594', 'NOVOTRIN BALSAMCOFORTx100 ', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(851, 'MED00595', 'PROSTAL  CAJA X 100 CAP', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:10'),
(852, 'MED00596', 'AMIGDAZOL NF X120TAB', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:11'),
(853, 'MED00597', 'UROBAC FORTE  X200 COMP', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:11'),
(854, 'MED01052', 'VITAVERAN B12 CIP JBE X 340ML', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:11'),
(855, 'MED01083', 'UROTAN-D X 100 CAP ', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:11'),
(856, 'MED01118', 'PROXIDOL 550MG x 100TAB - 25 BLIS', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:11'),
(857, 'OTC00299', 'LIMONADA COMPTA FCOX200ML', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:11'),
(858, 'PRE00001', 'PENETRO CARAMELO X 100SOBR', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:11'),
(859, 'PRE00006', 'MALDEX COMP JBEX120ML', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:11'),
(860, 'PRE00007', 'GRIPALERT PLUS NFX100TAB ', 177, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:11'),
(861, 'INA00008', 'METFORMINA 850 MG X 100 TAB', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:11'),
(862, 'MED00023', 'OMEPRAZOL TAB 20MGX100', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:11'),
(863, 'MED00598', 'ACICLOVIR 400MG. X 100TB', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:11'),
(864, 'MED00599', 'ATOS 15MG/5MLJBEX120ML', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:11'),
(865, 'MED00600', 'ATOS EXPECTX120ML', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:11'),
(866, 'MED00601', 'CLORANFENICOL CAPX500MGX100', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:11'),
(867, 'MED00602', 'DIMENHIDRINATO 50 MG X 100TAB', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:11'),
(868, 'MED00604', 'FLUCONAZOL 150MGx100 CAP', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:11'),
(869, 'MED00605', 'HIOSCINA BUTIL 10MG X100TAB', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:11'),
(870, 'MED00606', 'IBUPROFENO 100MG/5MLX60ML', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(871, 'MED00607', 'KETOROLACO 10MGx100 TAB', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(872, 'MED00608', 'MIGRALIVIAX100TAB', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(873, 'MED00609', 'MYCTRIM BALSAMICOSUSPX60ML', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(874, 'MED00610', 'MYCTRIM BALSAMICOX100TAB', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(875, 'MED00612', 'MYCTRIN FORTE X100TAB', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(876, 'MED00614', 'PROPANOLOL 40MG TABX100', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(877, 'MED00615', 'SALBUTAMOL 4MG.CAJAx100TAB', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(878, 'MED00617', 'CETIRIZINA 10MG X 100 TAB', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(879, 'MED00618', 'CIPROFLOXACINO 500MG.X100TAB', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(880, 'MED00619', 'CLORFENAMINA 4MGx100TAB', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(881, 'MED00620', 'DOXICICLINA 100MGX100CAP', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(882, 'MED00621', 'GABAPENTINA 300MG X100CAP', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(883, 'MED00622', 'KETOCONAZOL 200MG.X100TAB', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(884, 'MED00623', 'LORATADINA 10MG.x100 TAB', 178, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(885, 'AMP00074', 'AB-BRONCOL 1200MG  AMP', 179, 1, NULL, 2, NULL, 10.00, '2018-08-28', '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(886, 'AMP00075', 'AB-BRONCOL AMPX300MG', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(887, 'AMP00078', 'CEFACROL  500MG X 1  AMP.', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(888, 'AMP00079', 'CEFACROL IM 1000MG AMP', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(889, 'AMP00080', 'GRAVOL PED 30MGX1AMP', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(890, 'AMP00081', 'HIDROXOCOBALAMINA1MG/MLX50AMP', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(891, 'AMP00082', 'MEDICILINA AMP 1000+SOLV', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(892, 'AMP00083', 'MEDICORT 4MG/2ML X 1 AMP', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(893, 'AMP00084', 'RAQUIFEROL-D3 BEB AMPX10ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:12'),
(894, 'AMP00085', 'SOLOUNA 5 X1AMP', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:13');
INSERT INTO `productos` (`idproductos`, `cod_producto`, `nom_producto`, `idlaboratorio`, `idgrupo`, `idsubgrupo`, `idlinea`, `descripcion`, `mto_compra`, `fec_ult_compra`, `exento_impuesto`, `mto_impuesto`, `metod_venta`, `mto_minimo`, `mto_sugerido`, `porc_ganancia`, `facturarsinstock`, `avisostockminimo`, `stockminimo`, `medida`, `peso`, `idumedida`, `categoria`, `usa_serial`, `serial_1`, `serial_2`, `serial_3`, `etiqueta_label`, `etiqueta_precio`, `etiqueta_codigo`, `imagen`, `ocultar_venta`, `fec_ingreso`) VALUES
(895, 'AMP00086', 'SOLUNA AMPx2MLx1', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:13'),
(896, 'AMP00087', 'GRAVOL 50MG/1MG AMP X 3', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:13'),
(897, 'AMP00186', 'SOLUTRES 150MG/ML  AMPOLLA', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:13'),
(898, 'AMP00214', 'LINCOPLUS AMPX600MGX2ML X5', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:13'),
(899, 'AMP00226', 'MEDIVEL X10 OVULO - 2 BLIS', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:13'),
(900, 'MED00026', 'MEDICILINA  ORALX50TAB', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:13'),
(901, 'MED00648', 'AB MOKS  SUSP  X 105 ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:13'),
(902, 'MED00649', 'AB MOKS FORTE SUSP 70ML', 179, 1, NULL, 2, NULL, 1.25, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:13'),
(903, 'MED00650', 'ACARIL CREx42GR', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:13'),
(904, 'MED00651', 'ACARIL-S LOCION/FCOx60ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:13'),
(905, 'MED00652', 'AMOXIDIN 7 250 SUSP. 105 ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:13'),
(906, 'MED00653', 'BRONCO-AMOXIDIN 125MGx105ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:13'),
(907, 'MED00654', 'BRONCO-AMOXIDIN 250MGx105ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:13'),
(908, 'MED00655', 'BRONCOMEDIMOX 125MG/5MLX60', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:13'),
(909, 'MED00656', 'BRONCOMEDIMOX 250GX60ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:13'),
(910, 'MED00657', 'BRONCOMEDIMOX 250MG X105ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:13'),
(911, 'MED00658', 'CEFABRONCOL DUO JBE X 75ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:13'),
(912, 'MED00660', 'CLARIMED 250MG/5ML.SUSPx100ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:13'),
(913, 'MED00662', 'CLINDESS DUO X 7 OVULOS', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(914, 'MED00663', 'DOLOFARMALAN FORTTEx20TAB', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(915, 'MED00665', 'FLUIBRONCOL ORAL 600MGx20SOB', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(916, 'MED00666', 'FURACIN POMADAx35GR', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(917, 'MED00667', 'FUROXONA FORTE SUSPX120ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(918, 'MED00668', 'FUROXONA SUSPENSIONx120ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(919, 'MED00669', 'FUROXONA X 20ML GOTS', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(920, 'MED00671', 'GASEOVET 80MG(ANIS)GOTS X15ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(921, 'MED00672', 'GASEOVET 80MG(FRES)GOTS X15ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(922, 'MED00674', 'GRAVOL GOTAS X 10ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(923, 'MED00675', 'GRAVOL JBEx60ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(924, 'MED00676', 'GRAVOL SUPOS X 10 NI?OS', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(925, 'MED00677', 'HIDRAX FRESA FCO.x60ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(926, 'MED00678', 'HIDRAX MANZANA FCO.x60ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(927, 'MED00680', 'ISODINE SOLx60ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(928, 'MED00681', 'LOROPHYN X 3 OVULOS', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(929, 'MED00683', 'MEDICORT 2MG FCO X100ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(930, 'MED00685', 'PLASYODINE SOLx70ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(931, 'MED00686', 'RYNAT D GOTASx15ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(932, 'MED00687', 'SITIDERM NF CREMAX10GR', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(933, 'MED00688', 'SUCRAXOL SUSPX200ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(934, 'MED00689', 'SULFAMED L UNGUENTO x 15GR', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:14'),
(935, 'MED00690', 'AB MOKS FORTE X 16TAB ', 179, 1, 0, 2, NULL, 12.50, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(936, 'MED00691', 'DEQUAZOL-ORAL TABx500MGx20', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(937, 'MED00692', 'DOLOMAXXTABX400MGX20', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(938, 'MED00693', 'AB MOKS FORTE X 60 TAB - 15 BLI', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(939, 'MED00694', 'CEFABRONCOL DUO X 60 TAB', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(940, 'MED00695', 'CEFABRONCOL X 60 CAP ', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(941, 'MED00696', 'DEQUAZOL ORAL 500MG X 60TAB ', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(942, 'MED00697', 'DEQUAZOL-R COM/VAGX60', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(943, 'MED00698', 'DOLOMAX SUSP.100MG/5MLX60ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(944, 'MED00699', 'FURACIN SOL/TOPx60ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(945, 'MED00700', 'BRONCO-AMOXIDIN COMx100 ', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(946, 'MED00701', 'DONAMED  F X 100 TAB', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(947, 'MED00702', 'EQUALAX 5MG X 100 TAB ', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(948, 'MED00703', 'FUROXONA 100MGX100 TAB ', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(949, 'MED00705', 'GRAVOL 50MG x100 TAB', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(950, 'MED00706', 'GRIPA C MP X 100TAB - 10 BLI', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(951, 'MED00707', 'GRIPA CJUNIOR X 100TB - 25 BLI', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(952, 'MED00708', 'RYNATAN TABX100 - 25 BLI', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(953, 'MED00709', 'DIGESTAL X 120 CAP - 12 BLI', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(954, 'MED00711', 'GASEOVET 80MGX120 TAB - 12 BLI', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(955, 'MED00712', 'GASEOVET 40MG  X150 TAB - 15 BLI', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(956, 'MED01055', 'FUROXONA 100MGX20 TAB', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(957, 'MED01056', 'DOLOFARMALAN FORTEx250 TAB  - 25 BLI', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(958, 'MED01120', 'BRONCOMEDIMOX 125MG/5ML.105ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(959, 'MED01122', 'HORTRIN A SUSP X 105 ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(960, 'MED01123', 'HORTRIN-AX100TBS ANTIDIARREICO', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(961, 'MED01191', 'DOLOMAX 100MG/5ML FCO 60MLX2FRASCOS', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(962, 'OTC00013', 'ZAIDMAN CREMAx50GRS', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:15'),
(963, 'OTC00059', 'MENTHOLATUM POTEx30GRx12 - 12 UND', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(964, 'OTC00060', 'SALES HIDRAT x 25 SOBRES - 25 BLI', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(965, 'OTC00305', 'DENTIGEL FCO X 8 ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(966, 'OTC00306', 'LIMONADA PURGANTE EVAKUAx200M', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(967, 'OTC00308', 'MENTHOLATUM PTEx85GR', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(968, 'OTC00309', 'MENTHOLATUM UNGx18GRx12LAT', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(969, 'OTC00312', 'ZAIDMAN CREMAx30GR', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(970, 'OTC00446', 'ANDREWS ANTIACIDO MENTA SUSP FRASCO X 200 ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(971, 'OTC00447', 'ANDREWS ANTIACIDO CEREZA SUSP FRASCO X 200 ML', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(972, 'OTC00458', 'CARAMELO MENTHOLATUM X 60 SOBRES', 179, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(973, 'AMP00089', 'DEXA-NEUROBION X 3 AMP.JERINGA', 180, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(974, 'AMP00090', 'HEPABIONTA 2ML  X 3  AMP', 180, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(975, 'AMP00091', 'NEUROBION 1000 MG X 3 AMP', 180, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(976, 'AMP00219', 'DOLO-NEUROBION FORTE.X 3AMP JE', 180, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(977, 'AMP00227', 'NEUROBION HYPAK 10000 X 3AMPOL', 180, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(978, 'INA00003', 'GLUCOVANCE 500MG/2.5MGX30COM', 180, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(979, 'INA00010', 'GLUCOPHAGE COMx500MGx50 - 5 BLI', 180, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(980, 'MED00049', 'ARTREN CAPX100MGX15 - 1 BLI', 180, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(981, 'MED00713', 'EUTIROX 25MG.x 50 TAB', 180, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(982, 'MED00714', 'EUTIROX TABx125MGx50', 180, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(983, 'MED00716', 'ULCOGANT SUSx200ML', 180, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(984, 'MED00717', 'EUTIROX TABx100MGx50 - 5 BLI', 180, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(985, 'MED00718', 'EUTIROX TABx50MGx50', 180, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(986, 'MED00719', 'EUTIROX TABx75MGx50', 180, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(987, 'MED00720', 'DOLO-NEUROBION FORTEx100TB - 20 BLI', 180, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(988, 'MED00721', 'DOLO-NEUROBION-N x100 CAP - 25 BLI', 180, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(989, 'MED00724', 'DOLO-NEUROBION FORTE X200 - 40 BLI', 180, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(990, 'MED00725', 'HEPABIONTA X200 GRA - 50 BLI', 180, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(991, 'MED01125', 'EUTIROX TABX150MGX50', 180, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(992, 'OTC00093', 'BABY TEST CASSETE', 181, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(993, 'OTC00095', 'PRECISA TEST X1', 181, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(994, 'OTC00015', 'NIDO CRECIMIENTO+3 X 1600GR', 182, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:16'),
(995, 'OTC00313', 'CERELAC 5 CEREALES X 400 GR', 182, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(996, 'OTC00314', 'NAN 1 POLVO 900GR', 182, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(997, 'OTC00315', 'NAN I LATx400GR', 182, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(998, 'OTC00316', 'NAN II LATX400GR', 182, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(999, 'OTC00317', 'NAN II X 900 GRAMOS', 182, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1000, 'OTC00319', 'NIDO CRECIMIENTO +1 X 1600 GR', 182, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1001, 'OTC00435', 'NAN 3 LATA 800GR', 182, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1002, 'AMP00094', 'DROKSE 150MG/MLSUSPENCION INY', 184, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1003, 'AMP00095', 'EMKIT DS 1.5MG X 1 TABLETA', 184, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1004, 'AMP00191', 'NORIFAM 1 AMP.X 1ML', 184, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1005, 'MED01059', 'FAMILA 28F CJAX28TAB', 184, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1006, 'AMP00192', 'EXUNA AMPX2MLX1', 185, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1007, 'MED00755', 'VENOCAP CAPX16 - 1 BLI', 185, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1008, 'MED00757', 'DEXAVET 4MGX150 TAB - 15 BLI', 185, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1009, 'MED01060', 'BRONCOPHAR JBEX120ML', 185, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1010, 'OTC00061', 'REXALER DIEPHAR X 60', 185, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1011, 'OTC00330', 'BRONCOPHAR JBE.DIEPHARX60', 185, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1012, 'OTC00332', 'CORTIDIEPHAR X 60 UNITOMAS', 185, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1013, 'OTC00333', 'DEXTROMETORFANO DIEPHARX60', 185, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1014, 'OTC00334', 'DIMENS DIEPHAR X 60', 185, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1015, 'OTC00335', 'ENTEROPHAR FORTE SUSPX60', 185, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1016, 'OTC00336', 'IBUPIROL DIEPHAR X 60 UNITO', 185, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1017, 'OTC00337', 'MAFIDOL JBE X60DIEPHAR', 185, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1018, 'AMP00097', 'LINCOCIN AMPx300MGx1ML', 186, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1019, 'AMP00193', 'LINCOCIN AMPx600MGx2ML', 186, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1020, 'MED00764', 'EPAMIN  100 MG X 50 CAP', 186, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1021, 'MED00768', 'PROVERA TABx5MGx30', 186, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:17'),
(1022, 'OTC00341', 'PONSTAN RD X 100 TAB - 10 BLI', 186, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1023, 'AMP00101', 'MERGYNEX DUO 0.75MG X 2TAB', 187, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1024, 'AMP00102', 'MERGYNEX PLUS 1.5MG X 1TAB', 187, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1025, 'AMP00105', 'METAMIZOL 1GR/2ML x 50 AMP', 187, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1026, 'AMP00106', 'AGUA ESTERIL 5MLX 100AMP', 187, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1027, 'AMP00107', 'DEXAMETASONA 4MG/2ML X 50 AMP', 187, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1028, 'AMP00109', 'METAMIZOL SODI 1GR/2MLX100 AMP', 187, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1029, 'AMP00221', 'AMIKACINA 500MG/5ML X50AMP - 50 UND', 187, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1030, 'MED00770', 'AMOXI+ACID.CLAV500MG TAB CX10', 187, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1031, 'MED00771', 'BROMURO DE IPRATROPIO 20MC/AE', 187, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1032, 'MED01064', 'SALBUTAMOL AEROSX200 DOSIS', 187, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1033, 'OTC00021', 'PRESER.CLICK(CLASICO) X 48UND', 187, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1034, 'OTC00345', 'CEFUROXIMA 500MG X 10 TAB', 187, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1035, 'MED00769', 'ATORVASTATINA(ALKAST)40X30TAB', 188, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1036, 'MED01063', 'CEFUROXIMA(ALKOXIME)500X10TAB', 188, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1037, 'MED01171', 'BONCHECK 100MGX50TAB-5BLIS', 188, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1038, 'MED01172', 'ALERCHECK PLUS10MG CAPSX30-3BLIS', 188, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1039, 'MED01173', 'MISOCHECK200MG CAPSX10-1BLIS', 188, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1040, 'MED01174', 'DOLOCHECK PLUS400MGX100CAPS-10BLIST', 188, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1041, 'OTC00019', 'TAMSULOSINA 0.4 MG X 30 CAP', 188, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1042, 'OTC00343', 'ESOMEPRAZOL 20MGx30TAB', 188, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1043, 'OTC00344', 'ESOMEPRAZOL 40 MG X 30 TAB &&*', 188, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1044, 'MED00175', 'AMOXI+AC.CLAVUL(CLAVULIP)X10', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1045, 'MED00176', 'ATORVAST. 20MG(STALIP)X 100TA', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1046, 'MED00177', 'ATORVAST. 40MG(STALIP)X 30 TA', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1047, 'MED00178', 'CEFUROXIMA(CEFSTAL)500MGX10TA', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:18'),
(1048, 'MED00179', 'CELECOXIB 200( CELESTAL)', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1049, 'MED00180', 'ESOPRAZOLE 20MG X 30TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1050, 'MED00181', 'ESOPRAZOLE 40MG X 30 TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1051, 'MED00182', 'LEVOFLOXI(LEVOSTAL750MGX 10 TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1052, 'MED00183', 'OSTEO SANOS D3 X 30TAB (10', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1053, 'MED00184', 'PANTOPRAZOL 40MG X 30(PENSTAL', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1054, 'MED01221', 'CEFALEXINA (CFLIP) 500 X 100PAPS', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1055, 'FPL00148', 'CLARITROMICINA 500 MGx100 - 10 BLI', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1056, 'FPL00225', 'ACICLOVIR 200MG X 100TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1057, 'FPL00226', 'ACIDO FOLICO 0.5MGx100 TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1058, 'FPL00227', 'ALBENDAZOL 100MG/5MLx20ML SUP', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1059, 'FPL00228', 'AMBROXOL 15MG/5ML X 120 ML', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1060, 'FPL00229', 'AMBROXOL 30MG/5ML FCOX120ML', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1061, 'FPL00231', 'AMOXI 250MG+BROMHEXINA X60ML', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1062, 'FPL00232', 'AMOXI+AC CLAVULAMICO JBEx60ML', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1063, 'FPL00233', 'AMOXI+ACID.CLAV.500MG X 10TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1064, 'FPL00234', 'AMOXICI+AMBROXOL 500MGx100TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1065, 'FPL00235', 'AMOXICILINA 250MGX60ML', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1066, 'FPL00236', 'AMOXICILINA 500MG X 100 CAP', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1067, 'FPL00238', 'ATORVASTATINA 20MG X 100 TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1068, 'FPL00239', 'AZITROMICINA SUSP X 15ML', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1069, 'FPL00240', 'BETAPLUSS CREMA 0.05% x20G', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1070, 'FPL00241', 'CAPTOPRIL 25MG X 100TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1071, 'FPL00242', 'CELECOXIB 200MG X 100TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1072, 'FPL00243', 'CETIRIZINA 10MG X 100TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1073, 'FPL00244', 'CETIRIZINA X 60ML  JBE', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1074, 'FPL00245', 'CLARITROMICINA 250MG/5ML SUS', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1075, 'FPL00246', 'CLOPIDOGREL 75MG X 30TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1076, 'FPL00247', 'CLORANFENICOL 250MG/5MLx60ML', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:19'),
(1077, 'FPL00248', 'CLORANFENICOL 500MG X 100 CAP', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1078, 'FPL00249', 'CLORFENAMINA 2MG/5X60ML FCO', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1079, 'FPL00250', 'CLORFENAMINA MALEATO 4MGX100T', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1080, 'FPL00251', 'CLORFENAMINA X 120 JBE', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1081, 'FPL00252', 'CLOTRIMAZOL 1 SOLUCION X20ML', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1082, 'FPL00253', 'CLOTRIMAZOL 1% CREMA X 20GR', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1083, 'FPL00255', 'COMPLEJO B X 120ML  JB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1084, 'FPL00256', 'COMPLEJO B x 180ML JBE', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1085, 'FPL00258', 'DICLOFENACO 1%GEL x 20GR', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1086, 'FPL00260', 'DICLOXACILINA 250MGX60ML .', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1087, 'FPL00263', 'DOLODRAN EXTRA/FORTEX100TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1088, 'FPL00264', 'ENALAPRIL 10MG X 100 TBS', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1089, 'FPL00265', 'ENALAPRIL 20MG.X 100 TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1090, 'FPL00266', 'ERITROMICINA 250MG/5MLX60ML', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1091, 'FPL00267', 'ERITROMICINA 500 MG X 100TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1092, 'FPL00268', 'FLUCONAZOL 150MG X 100CAP', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1093, 'FPL00269', 'FURAZOLIDONA  X 120 ML  FCO', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1094, 'FPL00270', 'GEMFIBROZILO 600MG X 100TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1095, 'FPL00271', 'GLUCOFERVET MSM X 30 SOBRES', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1096, 'FPL00272', 'GUAIFENESINA X 120ML  JBE', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1097, 'FPL00273', 'HIDROXIDO AL+MAGNESIOX120ML', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1098, 'FPL00274', 'IBUPROFENO 400MG TAB CJAX100', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1099, 'FPL00277', 'KETOCONAZOL 2% CREMA X20', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1100, 'FPL00278', 'KETOCONAZOL 200MG X 100 TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1101, 'FPL00279', 'KETOROLACO X 100 TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1102, 'FPL00280', 'LACTULOSA 3.3MG/5ml x120ml', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1103, 'FPL00281', 'LEVOCETIRIZINA(ALERLIV)5MGX30', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1104, 'FPL00282', 'LEVOCTRIM 500X 10TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1105, 'FPL00283', 'LOPERAMIDA 2MG X100 TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1106, 'FPL00284', 'LORATADINA  X 60ML JBE', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1107, 'FPL00285', 'LORATADINA 10mg X 100TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1108, 'FPL00286', 'MELOXICAN 15 MGx100 TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1109, 'FPL00287', 'METAMIZOL 10ML GOTAS', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1110, 'FPL00289', 'METRONIDAZOL 250MG SUSPx60ML', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1111, 'FPL00290', 'METRONIDAZOL 500MG X10 OVULOS', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1112, 'FPL00291', 'NAPROXENO 275MG X 100TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:20'),
(1113, 'FPL00292', 'NAPROXENO TABx550MGx100', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1114, 'FPL00293', 'NORFLOXACINO+FENAZOPIRIDx100', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1115, 'FPL00295', 'OMEPRAZOLX100CAP', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1116, 'FPL00296', 'PARACETAMOL 120ML/5ML X 120ML', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21');
INSERT INTO `productos` (`idproductos`, `cod_producto`, `nom_producto`, `idlaboratorio`, `idgrupo`, `idsubgrupo`, `idlinea`, `descripcion`, `mto_compra`, `fec_ult_compra`, `exento_impuesto`, `mto_impuesto`, `metod_venta`, `mto_minimo`, `mto_sugerido`, `porc_ganancia`, `facturarsinstock`, `avisostockminimo`, `stockminimo`, `medida`, `peso`, `idumedida`, `categoria`, `usa_serial`, `serial_1`, `serial_2`, `serial_3`, `etiqueta_label`, `etiqueta_precio`, `etiqueta_codigo`, `imagen`, `ocultar_venta`, `fec_ingreso`) VALUES
(1117, 'FPL00297', 'PARACETAMOL 500MG X 100TB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1118, 'FPL00298', 'PARACETAMOL 60 ML JB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1119, 'FPL00299', 'PARACETAMOL X 10ML  GTS', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1120, 'FPL00300', 'PORTIL CREMA X20G', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1121, 'FPL00301', 'PREDNISONA 20MGX100TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1122, 'FPL00302', 'PREDNISONA 50MG X 100TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1123, 'FPL00303', 'PREDNISONA 5MG X 100TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1124, 'FPL00304', 'PREDNISONA 5MG/5ML X 60ML', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1125, 'FPL00305', 'SALBUTAMOL x 120ML  FCO', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1126, 'FPL00307', 'SULFA+TRI 400/80/5ML X 60ML', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1127, 'FPL00309', 'SULFA+TRIME 200-40 FCOx60ML', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1128, 'FPL00312', 'SULFATO FERROSO JBE X 180 ML', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1129, 'FPL00313', 'TETRACICLINA 500 MGX100MG', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1130, 'FPL00314', 'GLUCOFERVET X 30 SOBRES', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1131, 'FPL00315', 'CLINDAMICINA 300MGX100CAP', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1132, 'FPL00316', 'DOXICICLINA 100MGx100CAP - 6 BLI', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1133, 'FPL00317', 'SULFA+TRIME 800/160MG X100 TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1134, 'FPL00318', 'TERBINAFINA 250MG X 100TAB - 10 BLI', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1135, 'FPL00321', 'WELLPORT X 345ML JBE', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1136, 'FPL00325', 'TIAMINA 100MGx100 TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1137, 'FPL00329', 'CIPROFLOXACINO 500MGX100TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1138, 'FPL00330', 'COMPLEJO B X 300 CAP', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1139, 'FPL00332', 'FURAZOLIDONA 100MGx100 TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1140, 'INA00011', 'METFORMINA 850MG X 100 TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1141, 'MED00311', 'TERBINAFINA 1% CREMA X20GR', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1142, 'MED00613', 'PARACETAMOL 120MG X60ML', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1143, 'MED00787', 'AMOXI+ACID.CLAVU500X7TABL', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1144, 'MED00788', 'AMOXI+BROMHEXINA500/80MGX100 TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:21'),
(1145, 'MED01132', 'ATORVASTATINA 10MGX100TAB', 189, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1146, 'MED00791', 'ACICLOVIR 800MG X10 TABL', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1147, 'MED00792', 'ACIDO ALENDRONICO 70MGx4 TAB', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1148, 'MED00794', 'COLUQUIM 500MG X6 TAB', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1149, 'MED00795', 'COLUQUIM SUSPX60ML', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1150, 'MED00796', 'COLUQUIM x30ML SUSPENSI?N', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1151, 'MED00797', 'COMPLEJO B x 300COMP', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1152, 'MED00798', 'DIMENHIDRINATO 50MGX100COM', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1153, 'MED00799', 'DOXICICLINA 100MGX100CAP', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1154, 'MED00800', 'FLUCONAZOL 150MG X100 TAB', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1155, 'MED00801', 'FLUCOXIN 150MG CAP CJAX2', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1156, 'MED00802', 'HONGOCID POLx50GR', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1157, 'MED00803', 'HONGOCID U?AS LACA X 12 ML', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1158, 'MED00804', 'HONGOCID UNG X 15  GR', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1159, 'MED00805', 'LORATADINA  10MG X 100  TAB', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1160, 'MED00806', 'NODIAL CREMA x 10 GR', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1161, 'MED00807', 'PARACETAMOL 500MG X 100 TAB', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1162, 'MED00808', 'PREDNISONA 50MGx100TAB', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1163, 'MED00809', 'PREDNISONA 5MGx100TAB', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1164, 'MED00810', 'REPRIMAN 250MG/5ML JBEX50ML', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1165, 'MED00811', 'REPRIMAN 400MG/ML SOL FCX10ML', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1166, 'MED00812', 'REPRIMAN 500MGX100TAB', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1167, 'MED00813', 'DOXIPLUS 100MG X 50 CAP - 5 BLI', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1168, 'MED00814', 'QUINOBIOTIC 500MGx50TAB - 5 BLI', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1169, 'MED00815', 'QUIMIZOL  x 60 OVULO - 12 BLI', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1170, 'MED00816', 'ANTIGRIPINA PLUS X 100 TAB', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1171, 'MED00817', 'CETIRIZINA 10MGX100TAB', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1172, 'MED01133', 'DOXIPLUS 100 MG X 100 CAPS', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1173, 'MED01134', 'PREDNISONA 20MG X 100 TAB', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1174, 'MED01170', 'UROQUILAB 100 COMP- 10 BLIST', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1175, 'MED01203', 'ALCLIMAX 100MG X10 COMP', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1176, 'MED01204', 'ALCLIMAX 50 MG X10 COMP', 190, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1177, 'AMP00196', 'PLIDAN NF COMPUESTO X1 AMP', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1178, 'MED00031', 'PLIDAN GOTX5MGX20ML', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1179, 'MED00824', 'ABRILAR JBEX100ML', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1180, 'MED00825', 'ACI-TIP SUSX200ML', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:22'),
(1181, 'MED00826', 'AMOXIDAL DUO 500MG/5MLx60ML', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1182, 'MED00827', 'AMOXIDAL DUO 750MG/5M.X70ML', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1183, 'MED00828', 'AMOXIDAL DUO RESPIRA.SUPX70ML', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1184, 'MED00829', 'CIRIAX OTIC GOTAS', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1185, 'MED00830', 'COLUFASE GRAx500MGx6', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1186, 'MED00831', 'COLUFASE SUS/FCOx60ML', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1187, 'MED00832', 'COLUFASE SUSP. X 30ML', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1188, 'MED00833', 'CHELTIN JBE X 120 ML', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1189, 'MED00834', 'CHELTIN X 30ML  GTS', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1190, 'MED00838', 'PLIDAN FORTE X 20COMP', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1191, 'MED00839', 'DORIXINA FAST X 10 CAP', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1192, 'MED00840', 'ENDIAL 1MG X 20 COMP', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1193, 'MED00841', 'MICOLIS CREx30GR', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1194, 'MED00842', 'AMOXIDAL  DUO 250 MG X 90 ML', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1195, 'MED00843', 'AMOXIDAL DUO 875MGX98COMP ', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1196, 'MED00844', 'AMOXIDAL DUO RESPIR.X98TAB', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1197, 'MED00846', 'ATURAL x300MGx100 TAB', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1198, 'MED00847', 'DORIXINA B1.B6.B12x100TAB', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1199, 'MED00848', 'DORIXINA x125MGx100 COMP', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1200, 'MED00849', 'ACI -TIP CAJAX140 TAB', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1201, 'MED01086', 'PLIDAN NF x 100COMP ', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1202, 'MED01135', 'CIRIAX 500MG X 60 COMP - 6 BLIS', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1203, 'MED01136', 'TELAREN NF.15MGX100COMP - 10 BLIS', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1204, 'PRE00010', 'MIGRA DORIXINA X100TAB', 191, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1205, 'MED00032', 'XENILER 5MG/5ML JBEX60ML', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1206, 'MED00033', 'CRECIMAX PLUS  X 120  ML', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1207, 'MED00857', 'AEROX ANIS 80MG GOTX15', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1208, 'MED00858', 'AEROX FRESA GTS', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1209, 'MED00859', 'AEROX PLUS GTS X15ML', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1210, 'MED00861', 'BENZOATO DE BENZILO X60ML', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:23'),
(1211, 'MED00862', 'BISMOSAN  SUSP  150ML', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1212, 'MED00863', 'CIPROCALMEX 500X100COMP', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1213, 'MED00864', 'CIPROCALMEX FORTE X100TAB', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1214, 'MED00866', 'CRECIMAX PLUS SUSP.FCOX180mL', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1215, 'MED00867', 'DEXAFLAM 0.5MG/5ML FCOx100ML', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1216, 'MED00868', 'DEXAFLAM NF 2MG/5ML FCOX100ML', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1217, 'MED00869', 'FLAMICOX 20GR', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1218, 'MED00870', 'LAXULOSA 3.3G/5ml SOL.ORAL', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1219, 'MED00871', 'MAGNEZIN(MAGNESIO+ZINC) X33SO', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1220, 'MED00872', 'MUCOCETIL 100MG/5ML JBE X120M', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1221, 'MED00873', 'MUCOTRIM DILAT GOT X 15ML', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1222, 'MED00874', 'MUCOTRIM DILAT JBE X 120ML', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1223, 'MED00875', 'MUCOTRIM GTS.X 20 ML', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1224, 'MED00876', 'MUCOTRIM PLUS X 120 ML FCO', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1225, 'MED00878', 'NISOFLAM 5mg/5ml SUSP.ORAL', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1226, 'MED00880', 'PULMOL COMP.ANTIT.JBE X120ML', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1227, 'MED00882', 'ROXTRIM FORTE  SUSP X 60ML', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1228, 'MED00883', 'ROXTRIM PEDIATRICO FCO X60ML', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1229, 'MED00884', 'SUCRAGANT 1G/5ML FCO 200ML', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1230, 'MED00885', 'TOPIMICYN UNG ANTIBAC.14GR', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1231, 'MED00886', 'TRIMICOT CREMA TOP.TUBO 10GR', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1232, 'MED00887', 'XENILER 10mg/ml Sol.GOTAS', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1233, 'MED00888', 'ROXTRIM BALSAMICO PED X 60ML', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1234, 'MED00889', 'DOXICICLI(VICLORAX)X100CAP', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1235, 'MED00890', 'NISTAFEN  CAJX100 OVULOS ', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1236, 'MED00891', 'ROXTRIM BALSAMICO X 100TAB', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1237, 'MED00892', 'ROXTRIM FORTE X 100TAB', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1238, 'MED01138', 'ROXTIL B CREM TUBX10G', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1239, 'MED01206', 'POSULEN 9MGX6 OVULOS VAG', 192, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1240, 'AMP00120', 'LASIX AMPx20MGx2MLx5', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1241, 'AMP00121', 'PROFENID IM 100MG X 6 AMP', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1242, 'AMP00200', 'ENTEROGERMINA X 10 VIALES', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1243, 'AMP00201', 'PROFENID IV AMPX100MGX6', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1244, 'MED00908', 'BI PROFENID 150MGx10COMP', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1245, 'MED00909', 'FLAGYL 4% SUSx125MGx120ML', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1246, 'MED00910', 'FLAGYL 8% SUSx250MGx120ML', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:24'),
(1247, 'MED00911', 'LASIX 40 MG X 20 COMP', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1248, 'MED00912', 'NOVALGINA 500X50TAB', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1249, 'MED00913', 'NOVALGINA JBE X 100ML', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1250, 'MED00914', 'PROFENID GELx30GR', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1251, 'MED00915', 'RIFOCINA SPRAY FCOx20ML', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1252, 'MED00916', 'FLAGYL OVUx500MGx10', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1253, 'MED00917', 'FLAGYSTATINE OVUx500MGx10 ', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1254, 'MED00918', 'FLAGYL COMX500MGX20', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1255, 'MED00920', 'PRIMPERAN COMx10MGx100', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1256, 'MED01072', 'HIPERLIPEN 100MGx30 CAP ', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1257, 'MED01165', 'ALLEGRA PED 30MG/5MLSUSP ORAL150ML', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1258, 'MED01196', 'GELICART X30 SOBRES', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1259, 'OTC00022', 'SELSUN-AMARILLO 2.5%X180ML', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1260, 'OTC00067', 'SELSUN AMARILLO 2.5% - 40 SACHET', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1261, 'OTC00384', 'ICY-HOT BALSAMO POT/UNGX100', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1262, 'OTC00385', 'ICY-HOT CREMA TUBX35GR', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1263, 'OTC00386', 'LACTACYD  DELICATA 200ML', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1264, 'OTC00387', 'LACTACYD BREEZE PROBIO 200ML', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1265, 'OTC00388', 'LACTACYD FEMI.PROBIO.200ML', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1266, 'OTC00389', 'LACTACYD INFANTIL X 200 ML', 194, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1267, 'MED00949', 'AMOXICLIN 250/5ML X 60 ML', 197, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1268, 'MED00951', 'AMOXICLIN CL 250MG x 60ML', 197, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1269, 'MED00952', 'AMOXICLIN DUO 875MG X50TAB', 197, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1270, 'MED00953', 'AMOXICLIN DUO RESP.875x50TAB', 197, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1271, 'MED00955', 'AMOXICLIN JBE 125/5ML X 60 ML', 197, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1272, 'MED00956', 'BRONCO AMOXICLIN 125MFC/60ML', 197, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1273, 'MED00963', 'MUCOBIOTIC COMP.JBEx120ML', 197, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1274, 'MED00964', 'NOTIZOL CREMA X10GR', 197, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1275, 'MED00965', 'QUANOX GOTASX5ML', 197, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1276, 'MED00967', 'BRONCO AMOXICL 500MGX100TAB ', 197, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1277, 'MED01074', 'UROBIOTIC FORTE X100TAB', 197, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:25'),
(1278, 'MED01142', 'ANALGESIUM 10MG X 100TB', 197, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1279, 'AMP00123', 'CEFTRIAXON  1GRX25 AMP', 199, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1280, 'AMP00204', 'DEXAMETAS.4MG(TERBOMET)*25AMP', 199, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1281, 'AMP00205', 'TERBOTRIPLE 5000 AMPx25 UND', 199, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1282, 'MED00039', 'TERBOMOX 500 X100CAP', 199, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1283, 'MED00983', 'AMBROXOL 30MG/5MLX100 ML', 199, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1284, 'MED00987', 'TERBOMOX 250MG SUSPx60ML', 199, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1285, 'MED00988', 'FALEXIM 500MG.x100 CAP', 199, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1286, 'MED01185', 'FULL SPECTRUM NFX30COMP-3BLIS', 199, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1287, 'AMP00127', 'AMIKABIOT 1G/4ML AMP.', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1288, 'AMP00128', 'AMIKABIOT 500MG.x INYECTABLE', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1289, 'AMP00206', 'BETACORT  DEPOT AMPOLLAX1ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1290, 'AMP00229', 'GRAVAMIN 50MG/5ML CJAx3AMP', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1291, 'MED00991', 'BRONCO-TRIFAM SUSx125MGx60ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1292, 'MED00992', 'BRONCO-TRIFAMOX SUSX250X60ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1293, 'MED00993', 'BRONCOXAN DILAT GTSX15ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1294, 'MED00995', 'DOLOFENAC flex x20 tab', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1295, 'MED00996', 'ENDOVIT-5% FCOx1000ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1296, 'MED00998', 'EVACUOL GRANDE FCOx250ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1297, 'MED00999', 'EVACUOL PEDIATRICO FCOx65ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1298, 'MED01000', 'EVACUOL REGULAR FCOx130ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1299, 'MED01001', 'GRAVAMIN JBEx60ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1300, 'MED01002', 'SORBAMIN 30 FCOx1000ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1301, 'MED01003', 'BRONCO-TRIFAMOX 500MGx100', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1302, 'MED01075', 'XILONEST 5% POMx10GR', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1303, 'OTC00070', 'ELECTRORAL-PEDIA FRES X1000ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1304, 'OTC00395', 'DEXTROSA 5% FCO X 1000ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1305, 'OTC00396', 'ELECTROLIGHIT GRANADR X 475ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1306, 'OTC00399', 'ELECTROLIGHT GRANADIL X 800ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1307, 'OTC00401', 'ELECTROLIGHT MANDARINAX800ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1308, 'OTC00402', 'ELECTROLIGHT MARACUY X 475 ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1309, 'OTC00403', 'ELECTROLIGHT MARACUYA X 800ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:26'),
(1310, 'OTC00404', 'ELECTROLIGHT NARANJA X 475 ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1311, 'OTC00406', 'ELECTROLIGHT PI?A  X 475 ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1312, 'OTC00407', 'ELECTROLIGHT PI?A X 800 ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1313, 'OTC00451', 'ELECTRORAL- NF FRESA 500ML', 201, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1314, 'MED01153', 'QUEEN CETINA FTE500X100CAP', 207, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1315, 'MED01220', 'TOBAFAN-FORTE 2MG X 100 TAB', 207, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1316, 'OTC00416', 'GENTAMICINA 0.03% GOTAS', 207, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1317, 'AMP00005', 'AMIKIN 500MG/2ML X 1AMP', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1318, 'AMP00006', 'AMIKIN AMPx1GR', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1319, 'AMP00007', 'KENACORT IM AMPx40MGx1MLx1', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1320, 'OTC00118', 'ENFAGROW  PREMIUMN X 850GR', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1321, 'OTC00119', 'ENFAGROW PREMIUM VAINILX375GR', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1322, 'OTC00120', 'ENFAGROW PREMIUN VAINAX1100KG', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1323, 'OTC00121', 'ENFAGROW PREMIUN VAINIL 850GR', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1324, 'OTC00122', 'ENFAGROW PREMIUNX550GR', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1325, 'OTC00123', 'ENFAMIL 1 CON HIERROX 375 GR', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1326, 'OTC00124', 'ENFAMIL 1 PREMIUN X 375GR', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1327, 'OTC00125', 'ENFAMIL 2 PREMIUM X 375 GR', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1328, 'OTC00127', 'ENFAMIL 2/C HIERRO X 800GRS', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1329, 'OTC00128', 'SUSTAGEN CHOCOLATEX400GR', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1330, 'OTC00129', 'SUSTAGEN PRO VAINILLA X400GR', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1331, 'OTC00131', 'SUSTAGEN VAINILLA X400gr', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1332, 'OTC00132', 'ENFAMIL 2 C/HIERROX400GR', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1333, 'OTC00133', 'ENFAMIL 1 PREMIUN 850GR', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1334, 'OTC00421', 'ENFAGROW PREMIUM.NATURAL375 GR', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1335, 'OTC00422', 'ENFAGROW VAINI.X 1K.650GR+POR', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1336, 'OTC00423', 'ENFAMIL PREMIUM 1 X 1100GR', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1337, 'OTC00424', 'ENFAMIL PREMIUM 2 X 1100GR', 210, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1338, 'MED00041', 'DICLO K 50MG COMP.X100', 211, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27');
INSERT INTO `productos` (`idproductos`, `cod_producto`, `nom_producto`, `idlaboratorio`, `idgrupo`, `idsubgrupo`, `idlinea`, `descripcion`, `mto_compra`, `fec_ult_compra`, `exento_impuesto`, `mto_impuesto`, `metod_venta`, `mto_minimo`, `mto_sugerido`, `porc_ganancia`, `facturarsinstock`, `avisostockminimo`, `stockminimo`, `medida`, `peso`, `idumedida`, `categoria`, `usa_serial`, `serial_1`, `serial_2`, `serial_3`, `etiqueta_label`, `etiqueta_precio`, `etiqueta_codigo`, `imagen`, `ocultar_venta`, `fec_ingreso`) VALUES
(1339, 'MED00837', 'OVUDATE X 12 CAP VAG', 211, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1340, 'MED00850', 'BROSOL COMPUESTO SOL. X 120ML', 211, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1341, 'MED00851', 'DICLO K GTS. X 15ML', 211, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1342, 'MED00852', 'DICLO K SUSP. X60ML', 211, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1343, 'MED00854', 'TUSILEXIL GTS. X 25ML', 211, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1344, 'MED00855', 'TUSILEXIL JBE. X 120ML', 211, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1345, 'MED00856', 'DICLO K 100MG X 100COMP', 211, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1346, 'MED01137', 'ADAZOL 200 MG  X50 TAB - 25 BLIS', 211, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1347, 'AMP00027', 'CLINOMIN 1 AMPOLLA', 168, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1348, 'AMP00028', 'DICLOFENAC(VINIL75MG/3MLX6AMP', 168, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:27'),
(1349, 'AMP00165', 'METAM 1G/2ML X 5AMP(APIRON)', 168, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1350, 'MED00009', 'ODONTOL BIOTIC X 100 TAB', 168, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1351, 'MED00196', 'BANES 100MG/5MLSUSP.FCOX100ML', 168, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1352, 'MED00198', 'CALCIOFAR B12 SUSP.FCO X120ML', 168, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1353, 'MED00199', 'DERMAFAR CREMA TUBO X35GR', 168, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1354, 'MED00200', 'GRIFANTIL JBE X 60ML', 168, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1355, 'MED00201', 'MEMOVITAL B12 JBE X 120ML', 168, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1356, 'MED00202', 'OTOMICIN SOL GTAS FCO 10ML', 168, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1357, 'MED00203', 'Z-CAL 1000 2% GEL X 35 GR', 168, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1358, 'MED00204', 'Z-MOL1G.BLISTER X 20COMP', 168, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1359, 'AMP00029', 'CEFALOGEN I.M. AMPX1GR', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1360, 'AMP00030', 'CEFALOGEN IV.AMPx1GR', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1361, 'AMP00031', 'TRANSAMIN 1GX10ML I.V AMP', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1362, 'AMP00167', 'TRANSAMIN 250MG/2.5MLX1AMP', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1363, 'MED00011', 'TOBAN x 300 CAP', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1364, 'MED00227', 'BISMUCAR CEREZA 340ML', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1365, 'MED00228', 'BISMUCAR CEREZA X150 ML', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1366, 'MED00229', 'BISMUCAR FRESA 340ML', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1367, 'MED00230', 'BISMUCAR SUSX150ML   FRESA', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1368, 'MED00231', 'FLUMIL 150mg X 1cap', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1369, 'MED00232', 'MUCOCAR 100MG/5ML X 120ML', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1370, 'MED00234', 'PHYTO SOYA GEL VAG.X8DOSIS', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1371, 'MED00236', 'PHYTO SOYA 17.5MG X 60CAP', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1372, 'MED00237', 'BISMUCAR TABX100', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1373, 'MED00238', 'ORFENADRINA 100MGX100TAB', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1374, 'MED01033', 'TRANSAMIN CAPX12', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1375, 'MED01090', 'TOBAN F TAB 2MG x100', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1376, 'MED01106', 'ESPIRONE 25MG X 20 GRAS', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1377, 'MED01107', 'FOSMIN TAB. 70MG x 4', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1378, 'MED01222', 'PHYTO SOYA 35MG FT X 60 CAP', 212, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1379, 'AMP00033', 'AB-AMBROMOX 300MG INY', 32, 1, NULL, 2, NULL, 7.25, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:28'),
(1380, 'AMP00035', 'CLINDAMICINA 600MG X25  AMP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1381, 'AMP00036', 'DOLO-QUIMAGESICO 75MG/3MLX1AM', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1382, 'AMP00037', 'DOLO-QUIMAGESICO FLEX X2AMP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1383, 'AMP00038', 'NORFLEX AMP 60MG/2ML(C/CAJA)', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1384, 'AMP00039', 'AMIKACINA 500MG X 25 AMP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1385, 'AMP00040', 'DICLOFENACO 75MG/3ML X 25 AMP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1386, 'AMP00041', 'DEXAMETASONA 4MG INY X 100', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1387, 'AMP00152', 'TIOCTAN INYx5ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1388, 'AMP00153', 'RANITIDINA 50 MG/2ML x100 AM', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1389, 'AMP00168', 'GENTAMICINA 160MG/2MLx 25AMP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1390, 'FPL00002', 'DIXI-35 X 21  COMP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1391, 'FPL00003', 'UROCYCLAR X 90CAP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1392, 'FPL00005', 'ACICLOVIR 0.05%-CREx5GR', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1393, 'FPL00006', 'ACICLOVIR 200MGX100TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1394, 'FPL00007', 'ACICLOVIR 800MG X 10TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1395, 'FPL00008', 'ALBENDAZOL 100M/5MLx2FCOx20ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1396, 'FPL00009', 'ALOPURINOL 100MG X 30 TABS', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1397, 'FPL00010', 'ALOPURINOL 300MG TABX30', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1398, 'FPL00011', 'AMBROMOX SUSP. ORALX60ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1399, 'FPL00012', 'AMBROXOL 30MG/5ML JBEX120ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1400, 'FPL00013', 'AMLODIPINO 5MG X 100 TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1401, 'FPL00014', 'AMOXICILINA 250MG  x60 ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1402, 'FPL00015', 'AMOXICILINA 500MGX100TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1403, 'FPL00016', 'AQUASOL-A-C/PANTH TUBX29GR', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1404, 'FPL00017', 'ATORVASTATINA 10MG X 30 TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1405, 'FPL00018', 'ATORVASTATINA 20MG X 30 TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1406, 'FPL00019', 'AZITROMICINA 200MG/5ML X 15ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1407, 'FPL00020', 'AZITROMICINA 500MG X 3TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1408, 'FPL00021', 'BACTEROL BALSAMICO SUSx50ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1409, 'FPL00022', 'BACTEROL FORTE SUSP. 50ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:29'),
(1410, 'FPL00023', 'BACTEROL-INFANTIL FCOx60ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1411, 'FPL00024', 'BETAMETASONA 0.05%CREM.20GR', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1412, 'FPL00026', 'CADITAR 400MG X 10TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1413, 'FPL00027', 'CECET SUSPx60ML(DIENTRIN', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1414, 'FPL00028', 'CEFACLOR  250 MG X 75 ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1415, 'FPL00029', 'CEFACLOR 500MG X10 TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1416, 'FPL00030', 'CEFADROXILO 250MG/5MLX100ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1417, 'FPL00031', 'CEFALEXINA SUSX250MG/5MLX60ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1418, 'FPL00032', 'CEFRADINA SUSx250MGx60ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1419, 'FPL00033', 'CEREGEN FORTE FCOX180ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1420, 'FPL00035', 'CETIRIZINA 10MGX100TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1421, 'FPL00036', 'CETIRIZINA 5MG/5ML X 60ML JBE', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1422, 'FPL00037', 'CINARIZINA 75MG x100 CAP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1423, 'FPL00038', 'CIPROFLOXACINO 500MGX100TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1424, 'FPL00039', 'CLARITROMICINA 250MG/5ML SUSP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1425, 'FPL00040', 'CLOBETASOL 0.05% CREMA 25GR', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1426, 'FPL00041', 'CLOBETASOL 0.05% UNGUENTOx25G', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1427, 'FPL00042', 'CLOTRIMAZOL 1% CREMA X 40GR', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1428, 'FPL00043', 'CLUVAX CAP BLAN. VAG X 3', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1429, 'FPL00045', 'COMPLEJO B x120 ML JARABE', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1430, 'FPL00047', 'DEFLAZACORT 6MG X 10TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1431, 'FPL00052', 'DESTOLIT 5% LOCION X 100ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1432, 'FPL00053', 'DESTOLIT CREMA X 30GR', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1433, 'FPL00054', 'DEXAMETASONA 1M x40 TABL', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1434, 'FPL00055', 'DEXAMETASONA 4MG X 100TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1435, 'FPL00057', 'DICLOFENACO 1% GEL X 50  GRS', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1436, 'FPL00058', 'DILTIAZEN 60MGx20 COMP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1437, 'FPL00059', 'DOLO-QUIMAGESI AEROSOL1%X85ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1438, 'FPL00060', 'DOLO-QUIMAGESICO 1% GELx20GR', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1439, 'FPL00061', 'ENALAPRIL 5MG  X 20TABLETAS', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1440, 'FPL00062', 'ENALAPRIL TAB 10MGx140', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1441, 'FPL00063', 'ENALAPRIL TABx20MGx100', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1442, 'FPL00064', 'ERITROMICINA 250MG/5MLX60ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:30'),
(1443, 'FPL00065', 'ERITROMICINA 500MG X 100TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1444, 'FPL00066', 'FENAZOPIRIDINA 100MGX100TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1445, 'FPL00068', 'FLUCONAZOL 150MG X 2  CAP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1446, 'FPL00069', 'FLUIDASA 100MG SUSP. X 120ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1447, 'FPL00072', 'FUROSEMIDA 40MGX100TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1448, 'FPL00073', 'GEMFIBROZILO 600MGx30 TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1449, 'FPL00074', 'HEMORRODIL COMP UNGx28GR', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1450, 'FPL00075', 'HEMORRODIL UNGX20GR', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1451, 'FPL00076', 'IBUPROFENO 100MG/5MLX60ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1452, 'FPL00077', 'IBUPROFENO 600MG X 100TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1453, 'FPL00078', 'ISOCONAZOL 1% CREMA x20 GRS', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1454, 'FPL00079', 'ITRACONAZOL 100MG X 16CAPS', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1455, 'FPL00080', 'KETOCONAZOL 2% CREx10GR', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1456, 'FPL00081', 'KETOPROFENO 100MG X 30TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1457, 'FPL00084', 'LACTULOSA 3.33G/5ML SOL.100ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1458, 'FPL00085', 'LACTULOSA 3.33G/5ML x 200 ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1459, 'FPL00086', 'LAFITIL polv X56gr', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1460, 'FPL00088', 'LAFITIL UNG 15gr', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1461, 'FPL00089', 'LAFITIL UNG 28gr', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1462, 'FPL00090', 'LINCOMICINA 500MGx20 TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1463, 'FPL00091', 'LORATADINA 5MGX60ML JBE', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1464, 'FPL00092', 'LOSARTAN 50MGx60COMP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1465, 'FPL00093', 'MAGALDRAX SUSP X 200ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1466, 'FPL00095', 'MULTIDERM CREMAX10GR', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1467, 'FPL00096', 'MULTIMYCIN UNGX14GR', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1468, 'FPL00097', 'NAPROXENO SODI 550MG X100COMP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1469, 'FPL00099', 'NORFLEX PLUSX 10TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1470, 'FPL00100', 'OMEPRAZOL 20MG X 100 CAP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1471, 'FPL00101', 'OSCILLOCOCCINUM 200K GRAN SUB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:31'),
(1472, 'FPL00102', 'PANTOPRAZOL 40MGx14 TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1473, 'FPL00103', 'PARACETAMOL 500MGx100TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1474, 'FPL00104', 'PREDNISONA 20MG x 100 TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1475, 'FPL00107', 'PREDNISONA 5MG x 100TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1476, 'FPL00109', 'RANITIDINA 150 MG X 100 TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1477, 'FPL00110', 'SECNIDAZOL 500MG TAB X4und', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1478, 'FPL00112', 'SIMETICONA 40MGx30 TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1479, 'FPL00113', 'SIMETICONA 80MG G.FRESAx15ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1480, 'FPL00114', 'SIMETICONA 80MGx30 TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1481, 'FPL00115', 'SULFA+TRIM 800/160MG x100TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1482, 'FPL00118', 'TIBEX 1.5MG X 1TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1483, 'FPL00119', 'TRIMETABOL JBEx100ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1484, 'FPL00120', 'VI-SYNERAL PRONATAL X 30 TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1485, 'FPL00123', 'ADONA-AC-17 TABx30MGx10', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1486, 'FPL00124', 'HEMORRODIL COMP SUPX10', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1487, 'FPL00126', 'AMIODARONA 200MG x 30 TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1488, 'FPL00127', 'AMLODIPINO 10MG X 30 TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1489, 'FPL00129', 'DEXAMETASONA 0.5MG x30 TABL ', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1490, 'FPL00133', 'CARAMELO MULT.MENTA X 50 SOBR', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1491, 'FPL00134', 'CARAMELO MULT-NARJx50SOB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1492, 'FPL00135', 'CECET 200MGx50 TABLETAS', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1493, 'FPL00137', 'CADITAR 200MG X 60 CAP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1494, 'FPL00138', 'CADITAR VIT x 60 CAP - 10 blis', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1495, 'FPL00139', 'MAXUCAL D 800 X 60 COMP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1496, 'FPL00140', 'LIPEBIN JBE.X 90', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1497, 'FPL00141', 'ALBENDAZOL TABX200MGX100', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1498, 'FPL00142', 'BACTEROL-FORTE TABX100', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1499, 'FPL00143', 'CAPTOPRIL 25mg x 100TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1500, 'FPL00145', 'CEFADROXILO 500MGX100TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1501, 'FPL00146', 'CEFALEXINA 500MG X100 CAP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1502, 'FPL00147', 'CLARITROMICINA 500 MGX100', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1503, 'FPL00149', 'DESINFLAM 550X100TB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1504, 'FPL00150', 'DIBROLAX 5MG  X100 TB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1505, 'FPL00151', 'DIGOXINA 0.25MGx100 TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1506, 'FPL00152', 'DOLO-QUIMAGESI FLEXx100CA ', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1507, 'FPL00153', 'DOLO-QUIMAGESICO VITX100', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:32'),
(1508, 'FPL00154', 'ESPIRONOLACTONA 25MG x 100 TB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1509, 'FPL00155', 'IBUPROFENO 400MGx100TABLETAS', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1510, 'FPL00156', 'IBUPROFENO 800MG X 100TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1511, 'FPL00158', 'LANZOPRAZOL 30MG X 100CAP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1512, 'FPL00159', 'LORATADINA 10MGX100TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1513, 'FPL00160', 'MELOXICAN 15 MGx100 TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1514, 'FPL00161', 'METOCLOPRAMIDA 10MG X 100TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1515, 'FPL00162', 'ORFENADRINA X 100  TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1516, 'FPL00164', 'RANITIDINA 300MGX100 COMP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1517, 'FPL00165', 'TIOCTAN PLUS  X100 TB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1518, 'FPL00166', 'TIOCTAN-FUERTE X100 GRAG ', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1519, 'FPL00167', 'AMBROXOL 15MG/5ML JBEx120ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1520, 'FPL00168', 'CEFRADINA CAPx500MGx120', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1521, 'FPL00169', 'CLINDAMICINA 300MG X 120CAP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1522, 'FPL00170', 'DOLO-QUIMAGESICO 50MGX120', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1523, 'FPL00319', 'VITATHON X100 CAPSULAS', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1524, 'FPL00322', 'VITATHON X 35 CAPS', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1525, 'FPL00326', 'DUO-CVP-K CAPX40', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1526, 'FPL00328', 'ASSA-81 X 100COMP  - 10 BLIS', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1527, 'FPL00349', 'FRUTENZIMA X 120 - 30BLISTER', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1528, 'MED00042', 'ORFENADRINA 60MG/2MLX25AMP   -', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1529, 'MED00050', 'DAYAMINERAL JBE x120 ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1530, 'MED00239', 'CEREGEN FORTE 300ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:33'),
(1531, 'MED01034', 'DEFLAZACORT 30MGX10TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1532, 'MED01109', 'REPELETE', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1533, 'MED01116', 'CEFRADINA 250 X 60 ML SUSP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1534, 'MED01150', 'NORFLEX X 100TABX10BLISTER', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1535, 'MED01184', 'ASSA- 81X30TAB-3BLIS', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1536, 'MED01207', 'CINAFLOX-F 500MGX60TAB-10BLIS', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1537, 'OTC00025', 'JABON KAUFMAN X 3 UNID', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1538, 'OTC00026', 'REPELENTE PREM - 40 SACHET', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1539, 'OTC00182', 'NOPUCID 2 EN 1 SHANPU X 60ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1540, 'OTC00184', 'REPELENTE LOCx100ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1541, 'OTC00185', 'SUGAFOR  X 440 TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1542, 'OTC00186', 'THIMOLINA LEONARD 100ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1543, 'OTC00187', 'NOPUCID 10 CREX12GRX40', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1544, 'OTC00418', 'THIMOLINA-LEO.x200ML', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1545, 'OTC00463', 'CARAMELOS MULTI-BIOTICOS MIEL 25SOBX4-25BLIS', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1546, 'PSI00013', 'SERTRALINA 50MGX100 TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1547, 'PSI00014', 'AMITRIPTILINA 25MG X 100TB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1548, 'PSI00016', 'FLUOXETINA 20MG X 100CAP', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1549, 'PSI00024', 'ALPAZ 0.25MGX20TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1550, 'PSI00026', 'ZATRIX 0.5 X 100 TAB - 10 BLIS', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1551, 'PSI00027', 'ZATRIX 2MG X 100TAB - 10 BLIS', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1552, 'PSI00032', 'CLONAZEPAN 0.5MGX100TAB', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1553, 'PSI00033', 'DIAZEPAM 10MG X200TABLETAS', 32, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1554, 'OTC00188', 'ACNOMEL CREMA 30 GR', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:34'),
(1555, 'OTC00190', 'BEN-GAY GELX28GR', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1556, 'OTC00191', 'COLIRIO EYEMO 0.05% SOLX12ML', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1557, 'OTC00193', 'ETIQUET/DESOD/CREMAx20SACHETS', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1558, 'OTC00194', 'ETIQUET\\DESOD\\CREM 75GR', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1559, 'OTC00195', 'ETIQUET\\DESOD\\CREM36GR UNIS', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1560, 'OTC00196', 'LECHE MAGNESIA 360MLx1 FRASCO', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1561, 'OTC00197', 'MEJORAL X200 TB', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35');
INSERT INTO `productos` (`idproductos`, `cod_producto`, `nom_producto`, `idlaboratorio`, `idgrupo`, `idsubgrupo`, `idlinea`, `descripcion`, `mto_compra`, `fec_ult_compra`, `exento_impuesto`, `mto_impuesto`, `metod_venta`, `mto_minimo`, `mto_sugerido`, `porc_ganancia`, `facturarsinstock`, `avisostockminimo`, `stockminimo`, `medida`, `peso`, `idumedida`, `categoria`, `usa_serial`, `serial_1`, `serial_2`, `serial_3`, `etiqueta_label`, `etiqueta_precio`, `etiqueta_codigo`, `imagen`, `ocultar_venta`, `fec_ingreso`) VALUES
(1562, 'OTC00199', 'SAL DE ANDREWS TRIP. ACCIX30', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1563, 'OTC00200', 'SAL DE ANDREWS x 100 sob', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1564, 'OTC00203', 'LECHE MAGNESIA NATURAL 120ML X 12', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1565, 'OTC00206', 'ACNOMEL GRAN.LIMP X 40', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1566, 'OTC00207', 'QUITOSO PLUS X 40 SACHET', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1567, 'OTC00208', 'PADRAX SOBX60 ', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1568, 'OTC00209', 'EFER C X 100', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1569, 'OTC00459', 'LECHE MAGNESIA CIRUELA X 120 ML X 1FRASCO', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1570, 'OTC00460', 'LECHE MAGNESIA NATURAL X120ML X1FRASCO', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1571, 'OTC00461', 'LECHE MAGNESIA CEREZA X 120ML X1FRASCO', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1572, 'OTC00462', 'LECHE MAGNESIA KIDS X120ML X1FRASCO', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1573, 'OTC00474', 'ACNOMEL GEL TUB 30G', 145, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1574, 'AMP00169', 'NUCLEO-CMP-FORTE AMPX3', 213, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1575, 'INA00002', 'GLIDIABET COMX5MGX100 ', 213, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1576, 'MED00012', 'HADENSA SUPX10', 213, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1577, 'MED00046', 'GAMALATE NF X60 TAB ', 213, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1578, 'MED00241', 'DISLEP GOT FCOX20ML', 213, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1579, 'MED00242', 'GAMALATE-B6 SOLX80ML', 213, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1580, 'MED00243', 'HADENSA POMX15GR', 213, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1581, 'MED00244', 'ANGINOVAG AEROSOLx10ML', 213, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1582, 'MED00245', 'TIORFAN 10MG LACTANTEx18 SACHET', 213, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1583, 'MED00246', 'TIORFAN 30MG NI?OSx18 SACHET', 213, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1584, 'MED00247', 'DISLEP DE 25MG X 20 COMP', 213, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1585, 'MED00248', 'NUCLEO-CMP-FORTE CAPX20', 213, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:35'),
(1586, 'AMP00050', 'BEDOYECTA AMPOLLA X 2ML', 214, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1587, 'AMP00174', 'DOLOFAST PRE SOL.INYECT+JER', 214, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1588, 'MED00344', 'CALCIBON NATAL FORTE X 30 TAB', 214, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1589, 'MED00345', 'CALCIBONE D   X30 COMP', 214, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1590, 'MED00346', 'CALCIBONE D X 60TB', 214, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1591, 'MED00348', 'KEFLEX  500MG X 24 TAB  ', 214, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1592, 'MED01040', 'UROCIT-K x 14 TAB', 214, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1593, 'OTC00053', 'KIDCAL TUTTI FRUT SUSPX180ML', 214, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1594, 'OTC00254', 'KIDCAL  FRESA SUSPX180 ML', 214, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1595, 'OTC00256', 'COLADOS BANANAx113GR', 215, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1596, 'OTC00257', 'COLADOS CIRUELA/PASASx113GR', 215, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1597, 'OTC00258', 'COLADOS COCKTAIL FRUTASx113GR', 215, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1598, 'OTC00259', 'COLADOS FRUTAS MIXTASx113GR', 215, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1599, 'OTC00260', 'COLADOS FRUTAS TROPIC.x113GR', 215, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1600, 'OTC00261', 'COLADOS MANZANAx113GR', 215, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1601, 'OTC00262', 'COLADOS MELOCOTONx113GR', 215, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1602, 'OTC00263', 'COLADOS PERAx113GR', 215, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1603, 'OTC00264', 'COLADOS POSTRE/FRUTASx113GR', 215, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1604, 'MED00478', 'ALERGIZINA 10MGX100TAB', 158, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1605, 'MED00479', 'ARTRIMIDA POLV X 30SOBRES', 158, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1606, 'MED00481', 'CLAVUNIL  500MG+125MG X 10TAB', 158, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1607, 'MED00482', 'CLAVUNIL 250MG+62.5MG/5ML SUSP', 158, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1608, 'MED00485', 'PHARMAPRED 5MG/5ML X 100ML', 158, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1609, 'MED00486', 'PLAKIT 1.5MG X 1 TABLETA', 158, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1610, 'MED00487', 'PHARMAPRED 20MG X100 TAB', 158, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1611, 'MED01159', 'ARTRIM', 158, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1612, 'MED01047', 'FISIODOL FORTE 2% GELx50GR', 217, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1613, 'MED00735', 'MIGRADIN FORTE X 160 TAB ', 218, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1614, 'MED00736', 'ALERFORT 5MG TAB CJAX100', 218, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1615, 'MED00737', 'DOLO RAPID X 100TAB ', 218, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1616, 'MED00738', 'DOLOPLUS RELAX X100TAB ', 218, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1617, 'MED00739', 'FEBRADOL EXT. FORTEX100TAB', 218, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:36'),
(1618, 'MED00740', 'LEVOCETIRIZINA 5MG X100TAB', 218, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:37'),
(1619, 'MED00741', 'MELOXIFLAM FORTE X 100TAB', 218, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:37'),
(1620, 'MED00742', 'NOVIDOL DUO TAB CJAX100', 218, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:37'),
(1621, 'MED00743', 'QUINOFLOX FORTE X 100TAB ', 218, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:37'),
(1622, 'MED00744', 'EMIN FORTE X 200 TAB', 218, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:37'),
(1623, 'MED01058', 'NOVAGRIP X 200 TAB ', 218, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:37'),
(1624, 'MED01145', 'NOVANOR 1(1.5)x1blister', 218, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:37'),
(1625, 'MED01167', 'PONARIS 500X7COMP-10BLIS', 219, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:37'),
(1626, 'MED01183', 'CETRILER 5MGX100COMP', 219, 1, NULL, 2, NULL, 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, NULL, NULL, 13, '1', '0', NULL, NULL, NULL, '0', '0', '0', NULL, '0', '2018-08-22 19:32:37'),
(1627, '0000000006', '', 0, 0, 0, 0, '', 0.00, NULL, '0', 18.00, '1', 0.00, 0.00, NULL, '0', '0', 0, '0', '', 0, '1', '0', '', NULL, '', '0', '0', '0', NULL, '0', '2018-08-28 17:11:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesion`
--

CREATE TABLE `profesion` (
  `id_profesion` int(11) NOT NULL,
  `desc_profesion` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `profesion`
--

INSERT INTO `profesion` (`id_profesion`, `desc_profesion`) VALUES
(1, 'BACHILLER EN CIENCIAS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `idproveedor` int(11) NOT NULL,
  `idtip_documento` int(11) NOT NULL,
  `num_documento` varchar(15) NOT NULL,
  `razon_social` varchar(60) NOT NULL,
  `direccion` text NOT NULL,
  `idestados_pais` int(11) NOT NULL,
  `repr_legal` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `tlf_hab` varchar(15) DEFAULT NULL,
  `tlf_movil` varchar(15) DEFAULT NULL,
  `fec_ingreso` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`idproveedor`, `idtip_documento`, `num_documento`, `razon_social`, `direccion`, `idestados_pais`, `repr_legal`, `email`, `tlf_hab`, `tlf_movil`, `fec_ingreso`, `status`) VALUES
(1, 5, '20100061474', 'REPRESENTACIONES DECO S.A.C.', 'JR ALFRED ROSENBLAT N?145', 104, '', '', '', '', '0000-00-00 00:00:00', '1'),
(2, 5, '20517636534', 'LAFARMED', '', 104, '', '', '', '', '0000-00-00 00:00:00', '1'),
(3, 5, '20501932257', 'SCOP DEL PERU S.A.', 'AV. NICOLAS AYLLON N? 2931-INT. C-2 EL AGUSTINO - LIMA', 104, '', '', '01-3627709', '', '0000-00-00 00:00:00', '1'),
(4, 5, '20553438960', 'SERVICIOS INTEGRALES ESPECIALIZADOS EN SALUD E.I.R.L', 'AV. GRAL GARZON 689 - JESUS MARIA', 104, '', '', '', '', '0000-00-00 00:00:00', '1'),
(5, 5, '20478203676', 'LIDER PHARMA S.A', 'AV. PROCERES DE LA INDEPENDENCIA MZ A LT 4 LIMA', 104, '', '', '', '', '0000-00-00 00:00:00', '1'),
(6, 5, '20384891943', 'BOTICAS Y SALUD S.A.C', 'JR. ALFREDO ROSENBLAT NRO. 145 SANTIAGO DE SURCO', 104, '', '', '', '', '0000-00-00 00:00:00', '1'),
(7, 5, '20428837780', 'INTIPHARMA SAC', 'CALLE BOLIVAR 270 - MIRAFLORES', 104, '', '', '', '', '0000-00-00 00:00:00', '1'),
(8, 5, '20541377701', 'DROGUERIA FARMAFAST E.I.R.L.', 'JR. TACNA NRO. 1093 - HUANCAYO', 104, '', '', '', '', '0000-00-00 00:00:00', '1'),
(9, 5, '20568469055', 'DISTRIBUIDORA E IMPORTACIONES NUEVA LUZ S.A.C', 'JR PIURA NRO. 301 - HUANCAYO', 104, '', '', '', '', '0000-00-00 00:00:00', '1'),
(10, 5, '20100085225', 'QUIMICA SUIZA', 'AV. REPUBLICA DE PANAMA 2577 URB. SANTA CALTALINA', 104, '', '', '', '', '0000-00-00 00:00:00', '1'),
(11, 5, '20565269781', 'HYNOSCHA MEDIC S.A.C', 'JR. CALLAO NRO. 613 LIMA', 104, '', '', '', '', '0000-00-00 00:00:00', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_sistema`
--

CREATE TABLE `rol_sistema` (
  `idrol_sistema` int(11) NOT NULL,
  `nom_rol` varchar(30) NOT NULL,
  `state_default` int(11) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `hidden` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rol_sistema`
--

INSERT INTO `rol_sistema` (`idrol_sistema`, `nom_rol`, `state_default`, `status`, `hidden`) VALUES
(1, 'SUPERUSUARIO', 47, '1', '1'),
(2, 'ADMINISTRADOR', 0, '0', '0'),
(3, 'CAJERO', 49, '1', '0'),
(4, 'CLIENTE', 0, '1', '0'),
(5, 'SUPERVISOR', 0, '1', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subgrupos`
--

CREATE TABLE `subgrupos` (
  `idsubgrupo` int(11) NOT NULL,
  `idgrupo` int(11) NOT NULL,
  `desc_subgrupo` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `subgrupos`
--

INSERT INTO `subgrupos` (`idsubgrupo`, `idgrupo`, `desc_subgrupo`, `status`) VALUES
(1, 1, 'VARIOS', '1'),
(2, 1, 'NUEVO SUBGRUPO', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoctasbancarias`
--

CREATE TABLE `tipoctasbancarias` (
  `idtipocta` int(11) NOT NULL,
  `desctipocta` varchar(45) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipoctasbancarias`
--

INSERT INTO `tipoctasbancarias` (`idtipocta`, `desctipocta`, `status`) VALUES
(1, 'AHORRO', '1'),
(2, 'CORRIENTE', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_pago`
--

CREATE TABLE `tipo_pago` (
  `id_tipo_pago` int(11) NOT NULL,
  `desc_tipopago` varchar(25) NOT NULL,
  `especif_banco` enum('0','1') NOT NULL DEFAULT '0',
  `especif_serial` enum('0','1') NOT NULL DEFAULT '0',
  `especif_request` enum('0','1') NOT NULL DEFAULT '0',
  `isdefault` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_pago`
--

INSERT INTO `tipo_pago` (`id_tipo_pago`, `desc_tipopago`, `especif_banco`, `especif_serial`, `especif_request`, `isdefault`) VALUES
(1, 'EFECTIVO', '0', '0', '0', '1'),
(2, 'TARJETA DE DEBITO', '1', '1', '1', '0'),
(3, 'TARJETA DE CREDITO', '1', '1', '1', '0'),
(4, 'CHEQUE', '1', '1', '1', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tip_documento`
--

CREATE TABLE `tip_documento` (
  `idtip_documento` int(11) NOT NULL,
  `desc_documento` varchar(45) NOT NULL,
  `predeterminado` enum('1','0') NOT NULL DEFAULT '0',
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tip_documento`
--

INSERT INTO `tip_documento` (`idtip_documento`, `desc_documento`, `predeterminado`, `status`) VALUES
(1, 'D.N.I.', '1', '1'),
(2, 'PASAPORTE', '0', '1'),
(3, 'CARNET EXTRANJERIA', '0', '1'),
(4, 'P.T.P.', '0', '1'),
(5, 'R.U.C.', '0', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ubicacion`
--

CREATE TABLE `ubicacion` (
  `idubicacion` int(11) NOT NULL,
  `desc_ubicacion` varchar(20) NOT NULL,
  `disp_ventas` enum('0','1') NOT NULL DEFAULT '0',
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidadmedidad`
--

CREATE TABLE `unidadmedidad` (
  `idumedida` int(11) NOT NULL,
  `abrev_umedida` varchar(8) NOT NULL,
  `desc_umedida` varchar(55) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `unidadmedidad`
--

INSERT INTO `unidadmedidad` (`idumedida`, `abrev_umedida`, `desc_umedida`, `status`) VALUES
(1, 'ALMOH', 'ALMOHADILLA', '1'),
(2, 'BOL', ' BOLSA', '1'),
(3, 'CAJ', ' CAJA', '1'),
(4, 'ENV', ' ENVASE', '1'),
(5, 'FRASC', ' FRASCO', '1'),
(6, 'GRAG', ' GRAGEA', '1'),
(7, 'JABON', ' JABON', '1'),
(8, 'POTE', ' POTE', '1'),
(9, 'SACHET', ' SACHET', '1'),
(10, 'SOB', ' SOBRE', '1'),
(11, 'TUB', ' TUBO', '1'),
(12, 'UND', ' UNIDAD', '1'),
(13, 'S/N', 'SIN ESPECIFICAR', '1'),
(14, 'CAP.BLA', 'CAP. BLAND', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuario` int(11) NOT NULL,
  `idempleado` int(11) NOT NULL,
  `idrol_sistema` int(11) NOT NULL,
  `fec_ingreso` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fec_inactivo` date DEFAULT NULL,
  `login` varchar(10) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `visible` enum('0','1') NOT NULL DEFAULT '1',
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuario`, `idempleado`, `idrol_sistema`, `fec_ingreso`, `fec_inactivo`, `login`, `password`, `visible`, `status`) VALUES
(1, 1, 1, '2018-07-01 00:00:00', NULL, 'root', 'f64ba71a5d1ccd651a3d742b1e9c5c77', '0', '1'),
(3, 2, 3, '2018-07-01 00:00:00', NULL, 'read424', 'fb3c95c8e50d2aa225d8dfd6a7bf113b', '1', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas_temporal`
--

CREATE TABLE `ventas_temporal` (
  `idproductos` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `idusuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ventas_temporal`
--

INSERT INTO `ventas_temporal` (`idproductos`, `cantidad`, `idusuario`) VALUES
(901, 30, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `win_sistemas`
--

CREATE TABLE `win_sistemas` (
  `idwin_state` int(11) NOT NULL,
  `name_state` varchar(60) NOT NULL,
  `state_parent` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `win_sistemas`
--

INSERT INTO `win_sistemas` (`idwin_state`, `name_state`, `state_parent`) VALUES
(1, 'dashboard.catalogo', '0'),
(2, 'dashboard.catalogo.bancos', '0'),
(3, 'dashboard.catalogo.bancos-details', '0'),
(4, 'dashboard.catalogo.ctasbancarias', '0'),
(5, 'dashboard.catalogo.ctasbancarias-details', '0'),
(6, 'dashboard.catalogo.monedas', '0'),
(7, 'dashboard.catalogo.monedas-details', '0'),
(8, 'dashboard.catalogo.tipoctabancarias', '0'),
(9, 'dashboard.catalogo.tipoctabancarias-details', '0'),
(10, 'dashboard.catalogo.clientes', '0'),
(11, 'dashboard.catalogo.cliente-details', '0'),
(12, 'dashboard.catalogo.proveedores', '0'),
(13, 'dashboard.catalogo.proveedor-details', '0'),
(14, 'dashboard.catalogo.laboratorio', '0'),
(15, 'dashboard.catalogo.laboratorio-details', '0'),
(16, 'dashboard.catalogo.lineas', '0'),
(17, 'dashboard.catalogo.lineas-details', '0'),
(18, 'dashboard.catalogo.productos', '0'),
(19, 'dashboard.catalogo.producto-details', '0'),
(20, 'dashboard.catalogo.grupos', '0'),
(21, 'dashboard.catalogo.grupo-details', '0'),
(22, 'dashboard.catalogo.subgrupos', '0'),
(23, 'dashboard.catalogo.subgrupo-details', '0'),
(24, 'dashboard.catalogo.unidadmedida', '0'),
(25, 'dashboard.catalogo.unidadmedida-details', '0'),
(26, 'dashboard.compras', '0'),
(27, 'dashboard.compras.orden-pedido', '0'),
(28, 'dashboard.compras.orden-details', '0'),
(29, 'dashboard.compras.orden-compra', '0'),
(30, 'dashboard.compras.orden-compra.details', '0'),
(31, 'dashboard.configuracion', '0'),
(32, 'dashboard.configuracion.paises', '0'),
(33, 'dashboard.configuracion.departamentospais', '0'),
(34, 'dashboard.configuracion.tipodocumento-details', '0'),
(35, 'dashboard.inventario', '0'),
(36, 'dashboard.inventario.inicial', '0'),
(37, 'dashboard.reportes', '0'),
(38, 'dashboard.reportes.ventas', '0'),
(39, 'dashboard.rrhh', '0'),
(40, 'dashboard.rrhh.empleados', '0'),
(41, 'dashboard.rrhh.empleado-details', '0'),
(42, 'dashboard.seguridad', '0'),
(43, 'dashboard.seguridad.usuarios', '0'),
(44, 'dashboard.seguridad.roles', '0'),
(45, 'dashboard.seguridad.rol-details', '0'),
(46, 'dashboard', '0'),
(47, 'dashboard.home', '0'),
(48, 'dashboard.ventas', '0'),
(49, 'dashboard.ventas.cajas', '0'),
(51, 'dashboard.compras.details-factura', '0'),
(52, 'dashboard.compras.facturas-compras', '0'),
(53, 'dashboard.configuracion.empresa', '0'),
(54, 'dashboard.configuracion.fiscal', '0'),
(55, 'dashboard.configuracion.tiposdocumentos', '0'),
(56, 'dashboard.configuracion.pais-details', '0'),
(57, 'dashboard.reportes.productos', '0');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `almacenes`
--
ALTER TABLE `almacenes`
  ADD PRIMARY KEY (`idalmacen`),
  ADD UNIQUE KEY `desc_almacen_UNIQUE` (`desc_almacen`);

--
-- Indices de la tabla `bancos`
--
ALTER TABLE `bancos`
  ADD PRIMARY KEY (`idbancos`),
  ADD UNIQUE KEY `idbancos_UNIQUE` (`idbancos`),
  ADD UNIQUE KEY `sigla_banco_UNIQUE` (`sigla_banco`);

--
-- Indices de la tabla `boletas`
--
ALTER TABLE `boletas`
  ADD PRIMARY KEY (`idboleta`),
  ADD UNIQUE KEY `idboletas_UNIQUE` (`idboleta`),
  ADD KEY `num_ticket` (`idticket`,`tipo_comprobante`,`factura_electronica`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id_cargo`),
  ADD UNIQUE KEY `id_cargo_UNIQUE` (`id_cargo`),
  ADD UNIQUE KEY `desc_cargo_UNIQUE` (`desc_cargo`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`idclientes`),
  ADD UNIQUE KEY `idclientes_UNIQUE` (`idclientes`),
  ADD UNIQUE KEY `doc_identidad` (`idtip_documento`,`num_documento`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`idcompra`),
  ADD UNIQUE KEY `idcompra_UNIQUE` (`idcompra`),
  ADD UNIQUE KEY `fact_prov` (`numfactura`,`idproveedor`);

--
-- Indices de la tabla `condicion_pago`
--
ALTER TABLE `condicion_pago`
  ADD PRIMARY KEY (`idcondicion_pago`),
  ADD UNIQUE KEY `idcondicion_pago_UNIQUE` (`idcondicion_pago`),
  ADD UNIQUE KEY `desc_condicion_UNIQUE` (`desc_condicion`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD KEY `id_moneda_fk_idx` (`id_moneda`);

--
-- Indices de la tabla `cuentasbancarias`
--
ALTER TABLE `cuentasbancarias`
  ADD PRIMARY KEY (`idcuentasbancarias`),
  ADD UNIQUE KEY `idcuentasbancarias_UNIQUE` (`idcuentasbancarias`),
  ADD UNIQUE KEY `numctabancaria_UNIQUE` (`numctabancaria`),
  ADD KEY `idbancos_fk_idx` (`idbancos`),
  ADD KEY `idmoneda_fk_idx` (`idmoneda`),
  ADD KEY `idtipocta_fk_idx` (`idtipocta`);

--
-- Indices de la tabla `detalles_orden_pedidos`
--
ALTER TABLE `detalles_orden_pedidos`
  ADD PRIMARY KEY (`iddetalles_opedido`),
  ADD UNIQUE KEY `iddetalles_opedido_UNIQUE` (`iddetalles_opedido`),
  ADD KEY `idordenpedido_idx` (`idordenpedido`),
  ADD KEY `idproducto_fk_idx` (`idproductos`);

--
-- Indices de la tabla `detalle_inventario`
--
ALTER TABLE `detalle_inventario`
  ADD PRIMARY KEY (`id_lotes`);

--
-- Indices de la tabla `detalle_rol`
--
ALTER TABLE `detalle_rol`
  ADD KEY `rol_state` (`idrol_sistema`,`idwin_state`);

--
-- Indices de la tabla `det_boleta`
--
ALTER TABLE `det_boleta`
  ADD UNIQUE KEY `bolet_articulo` (`idboleta`,`idproductos`);

--
-- Indices de la tabla `det_ordencompras`
--
ALTER TABLE `det_ordencompras`
  ADD KEY `idorden_compra_fk_idx` (`idord_compra`),
  ADD KEY `idproductos_idx` (`idproductos`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`idempleado`),
  ADD UNIQUE KEY `idempleado_UNIQUE` (`idempleado`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD UNIQUE KEY `doc_identidad` (`idtip_documento`,`num_documento`),
  ADD KEY `id_profesion_fk_idx` (`id_profesion`),
  ADD KEY `id_cargo_fk_idx` (`id_cargo`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`num_fiscal`),
  ADD UNIQUE KEY `num_fiscal_UNIQUE` (`num_fiscal`);

--
-- Indices de la tabla `estados_pais`
--
ALTER TABLE `estados_pais`
  ADD PRIMARY KEY (`idestados_pais`),
  ADD UNIQUE KEY `idestados_pais_UNIQUE` (`idestados_pais`),
  ADD UNIQUE KEY `desc_estados_UNIQUE` (`desc_estados`);

--
-- Indices de la tabla `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`idgrupo`),
  ADD UNIQUE KEY `idgrupo_UNIQUE` (`idgrupo`),
  ADD UNIQUE KEY `descgrupo_UNIQUE` (`desc_grupo`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`idproductos`),
  ADD UNIQUE KEY `idproductos_UNIQUE` (`idproductos`),
  ADD KEY `idproductos_fk_idx` (`idproductos`);

--
-- Indices de la tabla `laboratorios`
--
ALTER TABLE `laboratorios`
  ADD PRIMARY KEY (`idlaboratorio`),
  ADD UNIQUE KEY `idlaboratorio_UNIQUE` (`idlaboratorio`),
  ADD UNIQUE KEY `desc_laboratorio_UNIQUE` (`desc_laboratorio`);

--
-- Indices de la tabla `linea`
--
ALTER TABLE `linea`
  ADD PRIMARY KEY (`idlinea`),
  ADD UNIQUE KEY `idlinea_UNIQUE` (`idlinea`),
  ADD UNIQUE KEY `desc_linea_UNIQUE` (`desc_linea`);

--
-- Indices de la tabla `moneda`
--
ALTER TABLE `moneda`
  ADD PRIMARY KEY (`idmoneda`),
  ADD UNIQUE KEY `idmoneda_UNIQUE` (`idmoneda`),
  ADD UNIQUE KEY `simb_moneda_UNIQUE` (`simb_moneda`),
  ADD UNIQUE KEY `nom_moneda_UNIQUE` (`nom_moneda`);

--
-- Indices de la tabla `ordenes_compras`
--
ALTER TABLE `ordenes_compras`
  ADD PRIMARY KEY (`idord_compra`),
  ADD UNIQUE KEY `idord_compra_UNIQUE` (`idord_compra`),
  ADD KEY `idproveedor_fk_idx` (`idproveedor`),
  ADD KEY `idtipo_pago_fk_idx` (`id_tipo_pago`),
  ADD KEY `idempleado_fk_idx` (`idempleado`);

--
-- Indices de la tabla `orden_pedidos`
--
ALTER TABLE `orden_pedidos`
  ADD PRIMARY KEY (`idordenpedido`),
  ADD UNIQUE KEY `idordenpedido_UNIQUE` (`idordenpedido`),
  ADD KEY `idproveedor_fk_idx` (`idproveedor`);

--
-- Indices de la tabla `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`idpais`),
  ADD UNIQUE KEY `idpais_UNIQUE` (`idpais`);

--
-- Indices de la tabla `privilegios_rol`
--
ALTER TABLE `privilegios_rol`
  ADD PRIMARY KEY (`idpriv_rol`),
  ADD UNIQUE KEY `idpriv_rol_UNIQUE` (`idpriv_rol`),
  ADD UNIQUE KEY `rol_privilegio` (`idrol_sistema`,`idwin_sistemas`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idproductos`),
  ADD UNIQUE KEY `idproductos_UNIQUE` (`idproductos`),
  ADD UNIQUE KEY `nom_producto_UNIQUE` (`nom_producto`,`idlaboratorio`) USING BTREE,
  ADD KEY `idlaboratorio_fk_idx` (`idlaboratorio`),
  ADD KEY `idlinea_fk_idx` (`idlinea`),
  ADD KEY `idumedida_fk_idx` (`idumedida`);

--
-- Indices de la tabla `profesion`
--
ALTER TABLE `profesion`
  ADD PRIMARY KEY (`id_profesion`),
  ADD UNIQUE KEY `id_profesion_UNIQUE` (`id_profesion`),
  ADD UNIQUE KEY `desc_profesion_UNIQUE` (`desc_profesion`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`idproveedor`),
  ADD UNIQUE KEY `idproveedor_UNIQUE` (`idproveedor`);

--
-- Indices de la tabla `rol_sistema`
--
ALTER TABLE `rol_sistema`
  ADD PRIMARY KEY (`idrol_sistema`),
  ADD UNIQUE KEY `idrol_sistema_UNIQUE` (`idrol_sistema`),
  ADD UNIQUE KEY `nom_rol_UNIQUE` (`nom_rol`);

--
-- Indices de la tabla `subgrupos`
--
ALTER TABLE `subgrupos`
  ADD PRIMARY KEY (`idsubgrupo`),
  ADD UNIQUE KEY `idsubgrupo_UNIQUE` (`idsubgrupo`);

--
-- Indices de la tabla `tipoctasbancarias`
--
ALTER TABLE `tipoctasbancarias`
  ADD PRIMARY KEY (`idtipocta`),
  ADD UNIQUE KEY `idtipoctasbancarias_UNIQUE` (`idtipocta`),
  ADD UNIQUE KEY `desctipocta_UNIQUE` (`desctipocta`);

--
-- Indices de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  ADD PRIMARY KEY (`id_tipo_pago`),
  ADD UNIQUE KEY `id_tipo_pago_UNIQUE` (`id_tipo_pago`);

--
-- Indices de la tabla `tip_documento`
--
ALTER TABLE `tip_documento`
  ADD PRIMARY KEY (`idtip_documento`),
  ADD UNIQUE KEY `idtip_documento_UNIQUE` (`idtip_documento`),
  ADD UNIQUE KEY `desc_documento_UNIQUE` (`desc_documento`);

--
-- Indices de la tabla `ubicacion`
--
ALTER TABLE `ubicacion`
  ADD PRIMARY KEY (`idubicacion`),
  ADD UNIQUE KEY `desc_ubicacion_UNIQUE` (`desc_ubicacion`);

--
-- Indices de la tabla `unidadmedidad`
--
ALTER TABLE `unidadmedidad`
  ADD PRIMARY KEY (`idumedida`),
  ADD UNIQUE KEY `idumedida_UNIQUE` (`idumedida`),
  ADD UNIQUE KEY `desc_umedida_UNIQUE` (`desc_umedida`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuario`),
  ADD UNIQUE KEY `idusuarios_UNIQUE` (`idusuario`),
  ADD UNIQUE KEY `idempleado_UNIQUE` (`idempleado`),
  ADD UNIQUE KEY `login_UNIQUE` (`login`),
  ADD KEY `id_rolsistema_fk_idx` (`idrol_sistema`);

--
-- Indices de la tabla `ventas_temporal`
--
ALTER TABLE `ventas_temporal`
  ADD UNIQUE KEY `venta_unika` (`idproductos`,`idusuario`),
  ADD KEY `idproductos` (`idproductos`);

--
-- Indices de la tabla `win_sistemas`
--
ALTER TABLE `win_sistemas`
  ADD PRIMARY KEY (`idwin_state`),
  ADD UNIQUE KEY `idwin_sistemas_UNIQUE` (`idwin_state`),
  ADD UNIQUE KEY `name_state_UNIQUE` (`name_state`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `almacenes`
--
ALTER TABLE `almacenes`
  MODIFY `idalmacen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `bancos`
--
ALTER TABLE `bancos`
  MODIFY `idbancos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `boletas`
--
ALTER TABLE `boletas`
  MODIFY `idboleta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id_cargo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `idclientes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `idcompra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `condicion_pago`
--
ALTER TABLE `condicion_pago`
  MODIFY `idcondicion_pago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `cuentasbancarias`
--
ALTER TABLE `cuentasbancarias`
  MODIFY `idcuentasbancarias` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `detalles_orden_pedidos`
--
ALTER TABLE `detalles_orden_pedidos`
  MODIFY `iddetalles_opedido` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalle_inventario`
--
ALTER TABLE `detalle_inventario`
  MODIFY `id_lotes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `idempleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `estados_pais`
--
ALTER TABLE `estados_pais`
  MODIFY `idestados_pais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `grupos`
--
ALTER TABLE `grupos`
  MODIFY `idgrupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `laboratorios`
--
ALTER TABLE `laboratorios`
  MODIFY `idlaboratorio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=220;

--
-- AUTO_INCREMENT de la tabla `linea`
--
ALTER TABLE `linea`
  MODIFY `idlinea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `moneda`
--
ALTER TABLE `moneda`
  MODIFY `idmoneda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ordenes_compras`
--
ALTER TABLE `ordenes_compras`
  MODIFY `idord_compra` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `orden_pedidos`
--
ALTER TABLE `orden_pedidos`
  MODIFY `idordenpedido` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `paises`
--
ALTER TABLE `paises`
  MODIFY `idpais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT de la tabla `privilegios_rol`
--
ALTER TABLE `privilegios_rol`
  MODIFY `idpriv_rol` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `idproductos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1628;

--
-- AUTO_INCREMENT de la tabla `profesion`
--
ALTER TABLE `profesion`
  MODIFY `id_profesion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `idproveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `rol_sistema`
--
ALTER TABLE `rol_sistema`
  MODIFY `idrol_sistema` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `subgrupos`
--
ALTER TABLE `subgrupos`
  MODIFY `idsubgrupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipoctasbancarias`
--
ALTER TABLE `tipoctasbancarias`
  MODIFY `idtipocta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  MODIFY `id_tipo_pago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tip_documento`
--
ALTER TABLE `tip_documento`
  MODIFY `idtip_documento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `ubicacion`
--
ALTER TABLE `ubicacion`
  MODIFY `idubicacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `unidadmedidad`
--
ALTER TABLE `unidadmedidad`
  MODIFY `idumedida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `win_sistemas`
--
ALTER TABLE `win_sistemas`
  MODIFY `idwin_state` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD CONSTRAINT `id_moneda_fk` FOREIGN KEY (`id_moneda`) REFERENCES `moneda` (`idmoneda`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cuentasbancarias`
--
ALTER TABLE `cuentasbancarias`
  ADD CONSTRAINT `idbancos_fk` FOREIGN KEY (`idbancos`) REFERENCES `bancos` (`idbancos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idmoneda_fk` FOREIGN KEY (`idmoneda`) REFERENCES `moneda` (`idmoneda`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idtipocta_fk` FOREIGN KEY (`idtipocta`) REFERENCES `tipoctasbancarias` (`idtipocta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalles_orden_pedidos`
--
ALTER TABLE `detalles_orden_pedidos`
  ADD CONSTRAINT `idordenpedido_fk` FOREIGN KEY (`idordenpedido`) REFERENCES `orden_pedidos` (`idordenpedido`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `idproducto_fk` FOREIGN KEY (`idproductos`) REFERENCES `productos` (`idproductos`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `det_boleta`
--
ALTER TABLE `det_boleta`
  ADD CONSTRAINT `idboletas_fk` FOREIGN KEY (`idboleta`) REFERENCES `boletas` (`idboleta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `det_ordencompras`
--
ALTER TABLE `det_ordencompras`
  ADD CONSTRAINT `idorden_compra_fk` FOREIGN KEY (`idord_compra`) REFERENCES `ordenes_compras` (`idord_compra`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idproductos` FOREIGN KEY (`idproductos`) REFERENCES `productos` (`idproductos`) ON DELETE NO ACTION ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
