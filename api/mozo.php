<?php
session_start();
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try {
    $response_json    = array('success' => false, 'num_rows' => -1, 'rows' => array(), "messages" => "Estas intentando algo inusual en el sistema");
    require_once("./class/GLibfunciones.php");
    $OConex = new GConector();
    $init_stmt = $OConex->stmt_init();
    $OMozo = new GMozo();
    $data = json_decode(file_get_contents('php://input'));
    switch ($_GET['oper']) {
        case 'listarDetalleProductos':
            $sql = $OMozo->listarDetalleProductos();
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
            if (!$init_stmt->bind_param('i', $data->id_pedido_cabecera))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);

            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $response_json['idPedido'] = $data->id_pedido_cabecera;
            $response_json['success'] = true;
            $response_json['totalItemCount'] = $result->num_rows;
            if ($response_json['totalItemCount'] == 0)
                break;
            if (isset($data->start, $data->number)) {
                $response_json['numberOfPages'] = ceil($result->num_rows / $data->number);
                $Opagination = new GPagination();
                $Opagination->setInit($data->start);
                $Opagination->setLimit($data->number);
                $sql = $Opagination->prepareSQL($sql);
            }
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if (!$init_stmt->bind_param('i', $data->id_pedido_cabecera))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);

            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $i = (isset($data->start)) ? $data->start : 0;
            while ($rows = $result->fetch_assoc()) {
                array_push($response_json['rows'], array_merge($rows, array("item" => ++$i)));
            }
            break;
        case 'listarAtendidas':
             $idUSuario = $_SESSION['idUsuario'];
            $sql = $OMozo->listarAtendidas();
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
            if (!$init_stmt->bind_param('i', $idUSuario))
                    throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
            
            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $response_json['success'] = true;
            $response_json['totalItemCount'] = $result->num_rows;
            if ($response_json['totalItemCount'] == 0)
                break;
            if (isset($data->start, $data->number)) {
                $response_json['numberOfPages'] = ceil($result->num_rows / $data->number);
                $Opagination = new GPagination();
                $Opagination->setInit($data->start);
                $Opagination->setLimit($data->number);
                $sql = $Opagination->prepareSQL($sql);
            }
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
                if (!$init_stmt->bind_param('i', $idUSuario))
                throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
        
            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $i = (isset($data->start)) ? $data->start : 0;
            while ($rows = $result->fetch_assoc()) {
                array_push($response_json['rows'], array_merge($rows, array("item" => ++$i)));
            }
            break;
        case 'listarAtendidasPago':

            $idSucursal = $_SESSION['idSucursal'];
            $sql = $OMozo->listarAtendidasPago();
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
            if(!$init_stmt->bind_param('i', $idSucursal))
				throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			
            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $response_json['success'] = true;
            $response_json['totalItemCount'] = $result->num_rows;
            if ($response_json['totalItemCount'] == 0)
                break;

            $i = 0;
            while ($rows = $result->fetch_assoc()) {
                array_push($response_json['rows'], array_merge($rows, array("item" => ++$i)));
            }
            break;
        case 'listar':
            $store_params = array(0 => '');
            if (isset($data->predicateObject)) {
                foreach ($data->predicateObject as $fields => $value) {
                    $store_params[0] .= 's';
                    ${$fields} = sprintf("%%%s%%", $value);
                    $store_params[] = &${$fields};
                    $OMozo->addFilter($fields);
                }
            }
            $sql = $OMozo->listar();
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
            if (count($store_params) > 1) {
                if (!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
                    throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
            }
            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $response_json['success'] = true;
            $response_json['totalItemCount'] = $result->num_rows;
            if ($response_json['totalItemCount'] == 0)
                break;
            if (isset($data->start, $data->number)) {
                $response_json['numberOfPages'] = ceil($result->num_rows / $data->number);
                $Opagination = new GPagination();
                $Opagination->setInit($data->start);
                $Opagination->setLimit($data->number);
                $sql = $Opagination->prepareSQL($sql);
            }
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if (count($store_params) > 1) {
                if (!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
                    throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
            }
            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $i = (isset($data->start)) ? $data->start : 0;
            while ($rows = $result->fetch_assoc()) {
                array_push($response_json['rows'], array_merge($rows, array("item" => ++$i)));
            }
            break;
        case 'guardar':
            if (intval($data->idPedido) > 0) {
                $id_pedido_cabecera = $data->idPedido;
            } else {
                $id_pedido_cabecera = 0;
            }
            if (intval($data->idPedido) > 0) {
                $sql = $OMozo->eliminarParaActualizar();
                if (!$init_stmt->prepare($sql))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
                if (!$init_stmt->bind_param('i', $data->idPedido))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
                $init_stmt->execute();
                $response_json['success'] = TRUE;
                $response_json['affected_rows'] = $init_stmt->affected_rows;
                $response_json['messages'] = ($init_stmt->affected_rows == 1) ? "Se elimino con éxito el registro" : "";
                if ($init_stmt->affected_rows != 1 && $init_stmt->errno != 0)
                    $response_json['messages'] = errorMySQL($init_stmt->errno);

                $sql = $OMozo->eliminarMesas();
                if (!$init_stmt->prepare($sql))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
                if (!$init_stmt->bind_param('i', $data->idPedido))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
                $init_stmt->execute();
                $response_json['success'] = TRUE;
                $response_json['affected_rows'] = $init_stmt->affected_rows;
                $response_json['messages'] = ($init_stmt->affected_rows == 1) ? "Se elimino con éxito el registro" : "";
                if ($init_stmt->affected_rows != 1 && $init_stmt->errno != 0)
                    $response_json['messages'] = errorMySQL($init_stmt->errno);
            }

            if (!isset($data->detalle))
                break;
            $id_usuario = $_SESSION['idUsuario'];
            $totalPedido = 0;
            foreach ($data->detalle as $i => $item) {
                $totalPedido = $item->nu_total + $totalPedido;
            }

            //-------------------------------CABECERA-----------------------------------------
            $OConex->setAutocommit(FALSE);
            $init_commit = $OConex->stmt_init();
            $method = (empty($id_pedido_cabecera)) ? 'agregar' : 'actualizarCabeceraPedido';
            $sql = call_user_func(array($OMozo, $method));
            if (!$init_commit->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if (!$init_commit->bind_param('idi', $id_usuario, $totalPedido, $id_pedido_cabecera))
                throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
            $init_commit->execute();

            if ($init_commit->affected_rows != 1)
                break;
            if (intval($data->idPedido) > 0) {
                $id_pedido_cabecera = $data->idPedido;
            } else {
                $id_pedido_cabecera = $init_commit->insert_id;
            }
            //------------------------------------------------------------------------
            $add_commit = $OConex->stmt_init();
            $methodd = "agregarDetalle";
            $sql = call_user_func(array($OMozo, $methodd));

            if (!$add_commit->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);

            foreach ($data->detalle as $i => $item) {

                for($i = 0 ; $i< $item->nu_cantidad ; $i++){
                    $cantidad = 1;
                    if (!$add_commit->bind_param('iiisi',  $id_pedido_cabecera, $item->id_producto, $cantidad, $item->no_observacion, $item->id_producto_menu))
                    throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
                    $add_commit->execute();
                }
            }

            $add_commit = $OConex->stmt_init();
            $methodd = "agregarMesas";
            $sql = call_user_func(array($OMozo, $methodd));
            if (!$add_commit->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            foreach ($data->mesa as $i => $item) {
                if (!$add_commit->bind_param('ii',  $id_pedido_cabecera, $item->id_mesa))
                    throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
                $add_commit->execute();
                $response_json['messages'] = sprintf("%s: %d", "ADD INVENTARIO", $add_commit->affected_rows);
                if ($add_commit->affected_rows != 1) {
                    $response_json['messages'] = sprintf("%s: %d", "error INVENTARIO", $item->id_mesa);
                    break;
                }
            }

            $response_json['messages'] = "Se registro la compra satisfactoriamente";
            $response_json['success'] = TRUE;
            $response_json['affected_rows'] = $init_commit->affected_rows;
            $OConex->commit();
            break;
        case 'consultar':
            if (!isset($data->id_pedido_cabecera) || empty($data->id_pedido_cabecera))
                break;

            $sql = $OMozo->listarMesaPedido();
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
            if (!$init_stmt->bind_param('i', $data->id_pedido_cabecera))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);

            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $response_json['success'] = true;
            $response_json['totalItemCount'] = $result->num_rows;
            if ($response_json['totalItemCount'] == 0)
                break;
            if (isset($data->start, $data->number)) {
                $response_json['numberOfPages'] = ceil($result->num_rows / $data->number);
                $Opagination = new GPagination();
                $Opagination->setInit($data->start);
                $Opagination->setLimit($data->number);
                $sql = $Opagination->prepareSQL($sql);
            }
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if (!$init_stmt->bind_param('i', $data->id_pedido_cabecera))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);

            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $i = (isset($data->start)) ? $data->start : 0;
            while ($rows = $result->fetch_assoc()) {
                array_push($response_json['rows'], array_merge($rows, array("item" => ++$i)));
            }
            break;

        case 'consultarDetalle':
            if (!isset($data->id_pedido_cabecera) || empty($data->id_pedido_cabecera))
                break;

            $sql = $OMozo->listarPedidos();
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
            if (!$init_stmt->bind_param('i', $data->id_pedido_cabecera))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);

            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $response_json['success'] = true;
            $response_json['totalItemCount'] = $result->num_rows;
            if ($response_json['totalItemCount'] == 0)
                break;
            if (isset($data->start, $data->number)) {
                $response_json['numberOfPages'] = ceil($result->num_rows / $data->number);
                $Opagination = new GPagination();
                $Opagination->setInit($data->start);
                $Opagination->setLimit($data->number);
                $sql = $Opagination->prepareSQL($sql);
            }
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if (!$init_stmt->bind_param('i', $data->id_pedido_cabecera))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);

            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $i = (isset($data->start)) ? $data->start : 0;
            while ($rows = $result->fetch_assoc()) {
                $rows['nu_total'] = $rows['nu_cantidad'] * $rows['nu_precio'];
                array_push($response_json['rows'], array_merge($rows, array("id_pedido" => ++$i)));
            }

            break;
        case 'consultarTotal':
            $sql = $OMozo->consultarTotales();
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $response_json['success'] = TRUE;
            $response_json['num_rows'] = $result->num_rows;
            if ($response_json['num_rows'] != 1)
                break;
            $response_json['rows'] = $result->fetch_assoc();
            $response_json['messages'] = "Se encontro el registro con exito";
            break;
        case 'eliminar':
            if (!isset($data->id) || empty($data->id))
                break;
            $sql = $OMozo->eliminar();
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if (!$init_stmt->bind_param('i', $data->id))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
            $init_stmt->execute();
            $response_json['success'] = TRUE;
            $response_json['affected_rows'] = $init_stmt->affected_rows;
            $response_json['messages'] = ($init_stmt->affected_rows == 1) ? "Se elimino con éxito el registro" : "";
            if ($init_stmt->affected_rows != 1 && $init_stmt->errno != 0)
                $response_json['messages'] = errorMySQL($init_stmt->errno);
            break;
        case 'listarMesasSucursal':
            $idSucursal = $_SESSION['idSucursal'];
            $response_json = array("totalItemCount" => 0, "numberOfPages" => 0, "rows" => array(), "success" => false);
            $name_method = 'listarMesasSucursal';
            $sql = call_user_func(array($OMozo, $name_method));
            $store_params = array(0 => '');
            $sql = sprintf($sql);
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if (!$init_stmt->bind_param('i', $idSucursal))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);

            $init_stmt->execute();
            $result_rows = $init_stmt->get_result();
            $response_json['totalItemCount'] = $result_rows->num_rows;
            if (isset($data->start, $data->number))
                $response_json['numberOfPages'] = ceil($result_rows->num_rows / $data->number);
            else
                $response_json['numberOfPages'] = 1;
            $response_json['success'] = true;
            if ($result_rows->num_rows == 0)
                break;
            $item = 1;
            while ($row = $result_rows->fetch_object()) {
                array_push($response_json['rows'], array("id" => $row->id, "nu_mesa" => $row->nu_mesa, "item" => $item++));
            }
            break;
    }
    echo json_encode($response_json);
} catch (Exception $e) {
    echo $e->getOutMsg();
}
