<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors',1);
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
	$response_json	=array('success'=>false, 'num_rows'=>-1, 'rows'=>array(), "msg"=>"Estas intentando algo inusual en el sistema");
	require_once("./class/GLibfunciones.php");
	$token=GAuth::getAuthorization();
	$data_token=GAuth::GetData($token);
	$response_json['token']=GAuth::SignIn($data_token);
	switch($_GET['oper']){
        case 'listar':
            $ruta_powershell = 'c:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe'; #Necesitamos el powershell
            $opciones_para_ejecutar_comando = "-c";#Ejecutamos el powershell y necesitamos el "-c" para decirle que ejecutaremos un comando
            $espacio = " "; #ayudante para concatenar
            $comillas = '"'; #ayudante para concatenar
            $comando = 'get-WmiObject -class Win32_printer |ft name';
            exec(
                $ruta_powershell
                . $espacio
                . $opciones_para_ejecutar_comando
                . $espacio
                . $comillas
                . $comando
                . $comillas,
                $resultado,
                $codigo_salida);
                if(is_array($resultado) && count($resultado)>3){
                    for($j=3; $j< count($resultado); $j++){
                        $impresora=trim($resultado[$j]);
                        if(!empty($impresora))
                            array_push($response_json['rows'], $impresora);
                    }
                }
		break;
	}
	echo json_encode($response_json);
}catch(Exception $e){
	$msg=$e->getOutMsg();
	switch($msg){
		case 'Expired token':
			$header=HTTPStatus(401);
			header($header);
		break;
		default:
			echo $msg;
	}
}
?>