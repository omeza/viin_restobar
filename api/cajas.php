<?php
session_start();
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
	$response_json	=array('success'=>false, 'num_rows'=>-1, 'rows'=>array(), "messages"=>"Estas intentando algo inusual en el sistema");
	require_once("./class/GLibfunciones.php");
	$OConex = new GConector();
	$init_stmt=$OConex->stmt_init();
	$OCajas = new GCajas();
	$data = json_decode(file_get_contents('php://input'));
	switch($_GET['oper']){
		case 'listar':
            $store_params=array(0=>'');            
			if(isset($data->predicateObject)){
				foreach($data->predicateObject as $fields => $value){
					$store_params[0].='s';
					${$fields}=sprintf("%%%s%%",$value);
					$store_params[]=&${$fields};
					$ONacionalidad->addFilter($fields);
				}
			}
			$sql=$OCajas->listar();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=true;
			$response_json['totalItemCount']=$result->num_rows;
			if($response_json['totalItemCount']==0)
				break;
			if(isset($data->start, $data->number)){
				$response_json['numberOfPages']=ceil($result->num_rows/$data->number);
				$Opagination=new GPagination();
				$Opagination->setInit($data->start);
				$Opagination->setLimit($data->number);
				$sql=$Opagination->prepareSQL($sql);
			}
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$i=(isset($data->start))?$data->start:0;
			while($rows=$result->fetch_assoc()){
				array_push($response_json['rows'], array_merge($rows, array("item"=>++$i)));
			}
		break;
		case 'listarCajaSucursal':
			$sql=$OCajas->listarCajaSucursal();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->idSucursal->id))
				throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=true;
			$response_json['totalItemCount']=$result->num_rows;
			if($response_json['totalItemCount']==0)
				break;
			
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->idSucursal->id))
				throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$i=(isset($data->start))?$data->start:0;
			while($rows=$result->fetch_assoc()){
				array_push($response_json['rows'], array_merge($rows, array("item"=>++$i)));
			}
		break;
		case 'guardarCaja':
			if(!is_array($data->detalle))
				break;		
			$sql=$OCajas->eliminarTotal();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			$init_stmt->execute();

			$OConex->setAutocommit(FALSE);
			$init_commit=$OConex->stmt_init();	
			$method=(empty($data->id))?'agregar':'actualizar';
			$sql=call_user_func(array($OCajas, $method));
			if(!$init_commit->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			foreach($data->detalle as $i => $item){
				if(!$init_commit->bind_param('iis',$item->id_caja, $item->id_sucursal, $item->no_caja))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
				$init_commit->execute();	
			}			
			$response_json['success']=true;
			$response_json['affected_rows']=$init_commit->affected_rows;
			$response_json['error_mysql']=$init_commit->error;
			if($init_commit->affected_rows!=1){
				$response_json['messages']="No se realizaron cambios en el registro";
				break;
			}
			$response_json['messages']="El registro fue guardaron satisfactoriamente";
			$OConex->commit();
        break;
		case 'guardar':
            if(!is_array($data->detalle))
				break;		
				$OConex->setAutocommit(FALSE);
				$init_commit=$OConex->stmt_init();
				$method=(empty($data->orden->id_venta))?'guardarCabecera':'actualizar';
				$data->orden->id_usuario = $_SESSION['idUsuario'];
				$sql=call_user_func(array($OCajas, $method));
				if(!$init_commit->prepare($sql))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
				if(!$init_commit->bind_param('iiissdddii', $data->orden->id_cliente,$data->orden->id_tipo_comprobante, $data->orden->id_usuario, $data->orden->nu_serie, $data->orden->nu_correlativo, $data->orden->nu_sub_total,$data->orden->nu_igv,$data->orden->nu_total,$data->orden->id_pedido_cabecera,$data->orden->id_venta))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
				$init_commit->execute();           
	
				if($init_commit->affected_rows!=1)
					break;
				$id_venta_cabecera=$init_commit->insert_id;	
				$methoddd="guardarParcial";
				$sql=call_user_func(array($OCajas, $methoddd));
				if(!$init_commit->prepare($sql))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
				if(!$init_commit->bind_param('iddddd', $id_venta_cabecera,$data->orden->nu_efectivo, $data->orden->nu_izipay, $data->orden->nu_trasnferencia, $data->orden->nu_credito, $data->orden->nu_vuelto))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
				$init_commit->execute();
				$add_commit=$OConex->stmt_init();
				$methodd="guardarDetalle";
				$sql=call_user_func(array($OCajas, $methodd));
				if(!$add_commit->prepare($sql))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
				foreach($data->detalle as $i => $item){
					if(!$add_commit->bind_param('iiddi',  $id_venta_cabecera, $item->id_producto, $item->cantidad,$item->nu_precio,$item->id_pedido_detalle))
						throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
					$add_commit->execute();
					$response_json['messages']=sprintf("%s: %d", "ADD INVENTARIO", $add_commit->affected_rows);
					if($add_commit->affected_rows!=1){
						$response_json['messages']=sprintf("%s: %d", "error INVENTARIO", $item->idproducto);
						break 2;
					}
				}
				$response_json['messages']="Se registro la compra satisfactoriamente";
				$response_json['success']=TRUE;
				$response_json['correlativo']=$data->orden->nu_correlativo;	
				$response_json['id_tipo_comprobante']=$data->orden->id_tipo_comprobante;				
				$response_json['affected_rows']=$init_commit->affected_rows;
				$OConex->commit();
		break;
		case 'actualizar':
				$OConex->setAutocommit(FALSE);
				$init_commit=$OConex->stmt_init();
				$correlativo= $data->correlativo + 1;
				$methoddd="actualizarCorrelativo";
				$sql=call_user_func(array($OCajas, $methoddd));
				if(!$init_commit->prepare($sql))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
				
				if(!$init_commit->bind_param('iii',$correlativo,$data->idComprobante,$data->orden->id_sucursal))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
				$init_commit->execute();
				  							     
				$response_json['messages']="Se registro satisfactoriamente";
				$response_json['success']=TRUE;
				$response_json['affected_rows']=$init_commit->affected_rows;
				$OConex->commit();
		break;
        case 'consultar':
			if(!isset($data->id) || empty($data->id))
				break;
			$sql=$OCajas->consultar();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=TRUE;
			$response_json['num_rows']=$result->num_rows;
			if($response_json['num_rows']!=1)
				break;
			$response_json['rows']=array_combine( array("id", "id_sucursal", "nombre"), array_values($result->fetch_assoc()));
			
			$response_json['messages']="Se encontro el registro con exito";
		break;
        case 'eliminar':
			if(!isset($data->id) || empty($data->id))
				break;
			$sql=$OCajas->eliminar();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$response_json['success']=TRUE;
			$response_json['affected_rows']=$init_stmt->affected_rows;
			$response_json['messages']=($init_stmt->affected_rows==1)?"Se elimino con éxito el registro":"";
			if($init_stmt->affected_rows!=1 && $init_stmt->errno!=0)
				$response_json['messages']=errorMySQL($init_stmt->errno);
		break;
		case 'listarCajas':
			$response_json=array("totalItemCount"=>0, "numberOfPages"=>0, "rows"=>array(), "success"=>false);
			$name_method='listarCajas';
			$sql=call_user_func(array($OCajas, $name_method));
			$store_params=array(0=>'');
			$sql=sprintf($sql);
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id_sucursal->value))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			
			$init_stmt->execute();
			$result_rows=$init_stmt->get_result();
			$response_json['totalItemCount']=$result_rows->num_rows;
			if(isset($data->start, $data->number))
				$response_json['numberOfPages']=ceil($result_rows->num_rows/$data->number);
			else
				$response_json['numberOfPages']=1;
			$response_json['success']=true;
			if($result_rows->num_rows==0)
				break;
			$item=1;
			while($row=$result_rows->fetch_object()){
				array_push($response_json['rows'], array("id"=>$row->id, "descripcion_caja"=>$row->descripcion_caja, "item"=>$item++ ) );
			}
		break;
		case 'totales':
			$sql=$OCajas->totales();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=TRUE;
			$response_json['num_rows']=$result->num_rows;
			if($response_json['num_rows']!=1)
				break;
			$response_json['rows']=$result->fetch_assoc();		
			$response_json['messages']="Se encontro el registro con exito";
		break;
		case 'totalesDia':
			$sql=$OCajas->totalesDia();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=TRUE;
			$response_json['num_rows']=$result->num_rows;
			if($response_json['num_rows']!=1)
				break;
			$response_json['rows']=$result->fetch_assoc();		
			$response_json['messages']="Se encontro el registro con exito";
		break;
		case 'listarVentas':
            $store_params=array(0=>'');            
			if(isset($data->predicateObject)){
				foreach($data->predicateObject as $fields => $value){
					$store_params[0].='s';
					${$fields}=sprintf("%%%s%%",$value);
					$store_params[]=&${$fields};
					$OCompras->addFilter($fields);
				}
			}
			$sql=$OCajas->listaVentas();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=true;
			$response_json['totalItemCount']=$result->num_rows;
			if($response_json['totalItemCount']==0)
				break;
			if(isset($data->start, $data->number)){
				$response_json['numberOfPages']=ceil($result->num_rows/$data->number);
				$Opagination=new GPagination();
				$Opagination->setInit($data->start);
				$Opagination->setLimit($data->number);
				$sql=$Opagination->prepareSQL($sql);
			}
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$i=(isset($data->start))?$data->start:0;
			while($rows=$result->fetch_assoc()){
				array_push($response_json['rows'], array_merge($rows, array("item"=>++$i)));
			}
		break;
		case 'listarVentasDia':

			$idUsuario = $_SESSION['idUsuario'];

			$sql=$OCajas->listaVentasDia();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $idUsuario))
				throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=true;
			$response_json['totalItemCount']=$result->num_rows;
			if($response_json['totalItemCount']==0)
				break;
			if(isset($data->start, $data->number)){
				$response_json['numberOfPages']=ceil($result->num_rows/$data->number);
				$Opagination=new GPagination();
				$Opagination->setInit($data->start);
				$Opagination->setLimit($data->number);
				$sql=$Opagination->prepareSQL($sql);
			}
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $idUsuario))
				throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$i=(isset($data->start))?$data->start:0;
			while($rows=$result->fetch_assoc()){
				array_push($response_json['rows'], array_merge($rows, array("item"=>++$i)));
			}
		break;
		case 'estadoCaja':
			$sql=$OCajas->cajaAbierta();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data))
				throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=true;
			$response_json['totalItemCount']=$result->num_rows;
			if($response_json['totalItemCount']==0)
				break;
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$i=0;
			while($rows=$result->fetch_assoc()){
				array_push($response_json['rows'], array_merge($rows, array("item"=>++$i)));
			}
		break;
		case 'listarCajasUsuario':
            $store_params=array(0=>'');            
			if(isset($data->predicateObject)){
				foreach($data->predicateObject as $fields => $value){
					$store_params[0].='s';
					${$fields}=sprintf("%%%s%%",$value);
					$store_params[]=&${$fields};
					$OCompras->addFilter($fields);
				}
			}
			$sql=$OCajas->listaCajaUsuario();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=true;
			$response_json['totalItemCount']=$result->num_rows;
			if($response_json['totalItemCount']==0)
				break;
			if(isset($data->start, $data->number)){
				$response_json['numberOfPages']=ceil($result->num_rows/$data->number);
				$Opagination=new GPagination();
				$Opagination->setInit($data->start);
				$Opagination->setLimit($data->number);
				$sql=$Opagination->prepareSQL($sql);
			}
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$i=(isset($data->start))?$data->start:0;
			while($rows=$result->fetch_assoc()){
				array_push($response_json['rows'], array_merge($rows, array("item"=>++$i)));
			}
		break;
		case 'guardarAtertura':		
				$OConex->setAutocommit(FALSE);
				$init_commit=$OConex->stmt_init();
				// $method=(empty($data->id_caja_accion))?'guardarAtertura':'actualizar';
				if(!$data->id_caja_accion){
					$method='guardarAtertura';
					$data->nu_monto_cierre = null;
				}else{
					$method='actualizar';
				}

				$sql=call_user_func(array($OCajas, $method));
				
				if(!$init_commit->prepare($sql))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
				if(!$init_commit->bind_param('iddi', $data->id_caja, $data->nu_monto_apertura, $data->nu_monto_cierre, $data->id_caja_accion))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
				$init_commit->execute();           
	
				$response_json['messages']="Se aperturo la caja";
				$response_json['success']=TRUE;				
				$response_json['affected_rows']=$init_commit->affected_rows;
				$OConex->commit();
		break;
	}
	echo json_encode($response_json);
}catch(Exception $e){
	echo $e->getOutMsg();
}
?>