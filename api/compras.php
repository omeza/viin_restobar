<?php
session_start();
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
	$response_json	=array('success'=>false, 'num_rows'=>-1, 'rows'=>array(), "messages"=>"Estas intentando algo inusual en el sistema");
	require_once("./class/GLibfunciones.php");
	$OConex = new GConector();
	$init_stmt=$OConex->stmt_init();
	$OCompras = new GCompras();
	$data = json_decode(file_get_contents('php://input'));
	switch($_GET['oper']){
		case 'listar':
            $store_params=array(0=>'');            
			if(isset($data->predicateObject)){
				foreach($data->predicateObject as $fields => $value){
					$store_params[0].='s';
					${$fields}=sprintf("%%%s%%",$value);
					$store_params[]=&${$fields};
					$OCompras->addFilter($fields);
				}
			}
			$sql=$OCompras->listar();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=true;
			$response_json['totalItemCount']=$result->num_rows;
			if($response_json['totalItemCount']==0)
				break;
			if(isset($data->start, $data->number)){
				$response_json['numberOfPages']=ceil($result->num_rows/$data->number);
				$Opagination=new GPagination();
				$Opagination->setInit($data->start);
				$Opagination->setLimit($data->number);
				$sql=$Opagination->prepareSQL($sql);
			}
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$i=(isset($data->start))?$data->start:0;
			while($rows=$result->fetch_assoc()){
				array_push($response_json['rows'], array_merge($rows, array("item"=>++$i)));
			}
		break;
        case 'guardar':
			if(!isset($data->orden->id_sucursal, $data->orden->id_proveedor, $data->orden->id_tipo_comprobante, $data->orden->nu_serie, $data->orden->nu_correlativo,$data->orden->nu_sub_total,$data->orden->nu_igv,$data->orden->nu_total) )
				break;
			$OConex->setAutocommit(FALSE);
			$init_commit=$OConex->stmt_init();
            $method=(empty($data->orden->id_compra_cabecera))?'agregar':'actualizar';
            $sql=call_user_func(array($OCompras, $method));
			if(!$init_commit->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if(!$init_commit->bind_param('iiissdddssii', $data->orden->id_proveedor,$data->orden->id_tipo_comprobante, $data->orden->id_usuario, $data->orden->nu_serie, $data->orden->nu_correlativo, $data->orden->nu_sub_total,$data->orden->nu_igv,$data->orden->nu_total,$data->orden->nu_serie_c,$data->orden->nu_correlativo_c,$data->orden->id_sucursal,$data->orden->id_compra_cabecera))
				throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
            $init_commit->execute();           

			if($init_commit->affected_rows!=1)
				break;
            $id_ordencompra=$init_commit->insert_id;
            
			$add_commit=$OConex->stmt_init();
            $methodd="agregarDetalle";
            $sql=call_user_func(array($OCompras, $methodd));
			if(!$add_commit->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			foreach($data->detalle as $i => $item){
                if(!$add_commit->bind_param('iiddd',  $id_ordencompra, $item->id_insumo, $item->nu_cantidad,$item->nu_precio, $item->no_total))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
				$add_commit->execute();
				$response_json['messages']=sprintf("%s: %d", "ADD INVENTARIO", $add_commit->affected_rows);
				if($add_commit->affected_rows!=1){
					$response_json['messages']=sprintf("%s: %d", "error INVENTARIO", $item->idproducto);
					break 2;
				}
			}
			$response_json['messages']="Se registro la compra satisfactoriamente";
			$response_json['success']=TRUE;
			$response_json['affected_rows']=$init_commit->affected_rows;
			$OConex->commit();
        break;
		case 'consultar':
			if(!isset($data->id_empleado) || empty($data->id_empleado))
				break;
			$sql=$OCompras->getEmpleado();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id_empleado))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=TRUE;
			$response_json['num_rows']=$result->num_rows;
			if($response_json['num_rows']!=1)
				break;
			$response_json['rows']=$result->fetch_assoc();		
			$response_json['messages']="Se encontro el registro con exito";
		break;
		case 'consultarTotal':
			$sql=$OCompras->consultarTotales();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=TRUE;
			$response_json['num_rows']=$result->num_rows;
			if($response_json['num_rows']!=1)
				break;
			$response_json['rows']=$result->fetch_assoc();		
			$response_json['messages']="Se encontro el registro con exito";
		break;
		case 'eliminar':
			if(!isset($data->id) || empty($data->id))
				break;
			$sql=$OCompras->eliminar();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(!$init_stmt->bind_param('i', $data->id))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			$init_stmt->execute();
			$response_json['success']=TRUE;
			$response_json['affected_rows']=$init_stmt->affected_rows;
			$response_json['messages']=($init_stmt->affected_rows==1)?"Se elimino con éxito el registro":"";
			if($init_stmt->affected_rows!=1 && $init_stmt->errno!=0)
				$response_json['messages']=errorMySQL($init_stmt->errno);
		break;
	}
	echo json_encode($response_json);
}catch(Exception $e){
	echo $e->getOutMsg();
}
?>