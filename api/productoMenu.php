<?php
session_start();
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
try{
	$response_json	=array('success'=>false, 'num_rows'=>-1, 'rows'=>array(), "messages"=>"Estas intentando algo inusual en el sistema");
	require_once("./class/GLibfunciones.php");
	$OConex = new GConector();
	$init_stmt=$OConex->stmt_init();
	$OProductoMenu = new GProductoMenu();
	$data = json_decode(file_get_contents('php://input'));
	switch($_GET['oper']){
		case 'listarCabecera':
            $store_params=array(0=>'');            
			if(isset($data->predicateObject)){
				foreach($data->predicateObject as $fields => $value){
					$store_params[0].='s';
					${$fields}=sprintf("%%%s%%",$value);
					$store_params[]=&${$fields};
					$ONacionalidad->addFilter($fields);
				}
			}
			$sql=$OProductoMenu->listarCabeceraMenu();
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$response_json['success']=true;
			$response_json['totalItemCount']=$result->num_rows;
			if($response_json['totalItemCount']==0)
				break;
			if(isset($data->start, $data->number)){
				$response_json['numberOfPages']=ceil($result->num_rows/$data->number);
				$Opagination=new GPagination();
				$Opagination->setInit($data->start);
				$Opagination->setLimit($data->number);
				$sql=$Opagination->prepareSQL($sql);
			}
			if(!$init_stmt->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			if(count($store_params)>1){
				if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
					throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
			}
			$init_stmt->execute();
			$result=$init_stmt->get_result();
			$i=(isset($data->start))?$data->start:0;
			while($rows=$result->fetch_assoc()){
				array_push($response_json['rows'], array_merge($rows, array("item"=>++$i)));
			}
        break;
        case 'listarDetalle':
            if (!isset($data->id_producto_menu))
                break;

            $sql = $OProductoMenu->listarDetalle();
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
            if (!$init_stmt->bind_param('i', $data->id_producto_menu))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);

            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $response_json['success'] = true;
            $response_json['no_producto'] = $data->no_producto;
            $response_json['nu_cantidad_platos'] = $data->nu_cantidad_platos;
            $response_json['totalItemCount'] = $result->num_rows;
            if ($response_json['totalItemCount'] == 0)
                break;
            if (isset($data->start, $data->number)) {
                $response_json['numberOfPages'] = ceil($result->num_rows / $data->number);
                $Opagination = new GPagination();
                $Opagination->setInit($data->start);
                $Opagination->setLimit($data->number);
                $sql = $Opagination->prepareSQL($sql);
            }
            if (!$init_stmt->prepare($sql))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
            if (!$init_stmt->bind_param('i', $data->id_producto_menu))
                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);

            $init_stmt->execute();
            $result = $init_stmt->get_result();
            $i = (isset($data->start)) ? $data->start : 0;
            while ($rows = $result->fetch_assoc()) {
                array_push($response_json['rows'], array_merge($rows, array("id_detalle" => ++$i)));
            }

            break;
        case 'guardar':	
			$OConex->setAutocommit(FALSE);
            $init_commit=$OConex->stmt_init();
			$method="guardar";
			$sql=$OProductoMenu->guardarProductoMenu();
			if(!$init_commit->prepare($sql))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
			
		    if(!$init_commit->bind_param('ii',$data->id_producto, $data->nu_cantidad_platos))
				throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
				$init_commit->execute();	
						
			$response_json['success']=true;
			$response_json['affected_rows']=$init_commit->affected_rows;
			$response_json['error_mysql']=$init_commit->error;
			if($init_commit->affected_rows!=1){
				$response_json['messages']="No se realizaron cambios en el registro";
				break;
			}
			$response_json['messages']="El registro fue guardaron satisfactoriamente";
			$OConex->commit();
        break;
        
	}
	echo json_encode($response_json);
}catch(Exception $e){
	echo $e->getOutMsg();
}
?>