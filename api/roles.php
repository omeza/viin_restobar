<?php
session_start();
//session_name("antares");
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
$a_events=array('consultar', 'guardar', 'existe', 'changestatus', 'listar', 'eliminar', 'menu_user');
if(isset($_GET['oper']) && in_array($_GET['oper'], $a_events)){
    try{
        require_once("./class/GLibfunciones.php");
        $oConector= new GConector();
        $oRol= new GRoles();
        $init_stmt=$oConector->stmt_init();
        $response_json =array('error'=>"","success"=>false, "data"=>array(), "num_rows"=>-1, "affected_rows"=>-1, "messages"=>"");
        $data = json_decode(file_get_contents('php://input'));
        switch($_GET['oper']){
            case 'menu_user':
                $response_json['rows']=array();
                if(empty($data->id)){
                    $response_json['rows']=$data->menu;
                    echo json_encode($data->menu);
                    die();
                }
                $sql="SELECT ws.no_win_sistemas as name_state FROM detalle_rol AS dr INNER JOIN win_sistemas AS ws ON ws.id_win_sistemas=dr.id_win_sistemas WHERE dr.id_rol_sistema=? AND ws.no_win_sistemas=?";
                if(!$init_stmt->prepare($sql))
                    throw new GException("Vaya! ocurrio un  problema en el sistema.<br/>Consulte con el Soporte Tecnico", $oConector->error, $oConector->errno);
                
               
                foreach($data->menu as $i => $item){
                    if(isset($item->children)){
                        $total_child=count($item->children);
                        foreach($item->children as $j => $subitem){
                            if(!$init_stmt->bind_param('is', $data->id, $subitem->id))
                                throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $oConector->error, $oConector->errno);
                            
                            $init_stmt->execute();                            
                            $result_stmt=$init_stmt->get_result();                        
                           
                            $data->menu[$i]->children[$j]->state->selected=($result_stmt->num_rows==1)?true:false;
                        }                    
                        
                    }

                    if(!$init_stmt->bind_param('is', $data->id, $item->id))
                        throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $oConector->error, $oConector->errno);
                    $init_stmt->execute();
                    $result_stmt=$init_stmt->get_result();

                    $data->menu[$i]->state->selected=($result_stmt->num_rows==1)?'false':'true';
                    if(isset($data->menu[$i]->state->opened)){
                        unset($data->menu[$i]->state->opened);
                    }
                }
                $response_json['rows']=$data->menu;
                echo json_encode($response_json['rows']);
                die();
            break;
            case 'eliminar':
                if(!isset($data->id) || empty($data->id))
                    break;
                $sql=$oRol->deleteRol();
                if(!$init_stmt->prepare($sql))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
                if(!$init_stmt->bind_param('i', $data->id))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
                $init_stmt->execute();
                $response_json['success']=TRUE;
                $response_json['affected_rows']=$init_stmt->affected_rows;
                $response_json['messages']=($init_stmt->affected_rows==1)?"Se elimino con éxito el registro":"";
                if($init_stmt->affected_rows!=1 && $init_stmt->errno!=0)
                    $response_json['messages']=errorMySQL($init_stmt->errno);
            break;
            case 'consultar':
                if(!isset($data->id) || empty($data->id))
                    break;
                $sql= $oRol->getRol();
                if(!$init_stmt->prepare($sql))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $oConector->error, $oConector->errno);
                if(!$init_stmt->bind_param('i',$data->id))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $oConector->error, $oConector->errno);
                $init_stmt->execute();
                $result_stmt=$init_stmt->get_result();
                $response_json['num_rows']=$result_stmt->num_rows;
                $response_json['success']=true;
                if($result_stmt->num_rows!=1)
                    break;
                $rows=$result_stmt->fetch_assoc();
                $response_json['rows']=array_combine(array("id_rol", "nombre_rol", "status"), array_values($rows));
                $sql="SELECT ws.no_win_sistemas as name_state, ws.no_parent as state_parent FROM detalle_rol AS dr INNER JOIN win_sistemas AS ws ON ws.id_win_sistemas=dr.id_win_sistemas WHERE dr.id_rol_sistema=?";
                if(!$init_stmt->prepare($sql))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $oConector->error, $oConector->errno);
                if(!$init_stmt->bind_param('i', $data->id))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $oConector->error, $oConector->errno);
                $init_stmt->execute();
                $result_stmt=$init_stmt->get_result();
                $response_json['rows']['items']=array();
                while($rows=$result_stmt->fetch_object()){
                    array_push($response_json['rows']['items'], $rows);
                }
            break;
            case 'changestatus':
                if(!isset($data->id) || empty($data->id))
                    break;
                $sql=$oRol->getRol();
                if(!$init_stmt->prepare($sql))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
                if(!$init_stmt->bind_param('i', $data->id))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
                $init_stmt->execute();
                $result=$init_stmt->get_result();
                $response_json['num_rows']=$result->num_rows;
                if($result->num_rows==0){
                    $response_json['messages']="Registro no se encuentra en el sistema";
                    break;
                }
                $row=$result->fetch_object();
                if($row->status!=$data->status){
                    $response_json['success']=TRUE;
                    $response_json['messages']="Otro usuario realizo el cambio de status";
                    break;
                }
                $status=($data->status=='0')?'1':'0';
                $sql=$oRol->changeStatus();
                if(!$init_stmt->prepare($sql))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
                if(!$init_stmt->bind_param('si', $status, $data->id))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
                $init_stmt->execute();
                $response_json['affected_rows']=$init_stmt->affected_rows;
                $response_json['success']=TRUE;
                $response_json['messages']=($init_stmt->affected_rows==0)?"No se realizo cambio en el status": "Se realizo satisfactoriamente el cambio de status";
                if($init_stmt->affected_rows==1)
                    $response_json['rows']['status']=$status;
                break;
            case 'existe':
                if($data->id===null)
                    $data->id=0;
                $sql=$oRol->existRol();
				if(!$init_stmt->prepare($sql))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $objConex->error, $objConex->errno);
                if(!$init_stmt->bind_param("si", $data->descripcion, $data->id))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $objConex->error, $objConex->errno);
                $init_stmt->execute();
                $store_result=$init_stmt->get_result();
                $response_json['num_rows']=$store_result->num_rows;
                $response_json['success']=TRUE;
            break;
            case 'listar':
                $response_json=array("totalItemCount"=>0, "numberOfPages"=>0, "rows"=>array(), "success"=>false);
                $store_params=array(0=>'');
                if(isset($data->predicateObject)){
                    foreach($data->predicateObject as $fields => $value){
                        $store_params[0].='s';
                        ${$fields}=sprintf("%%%s%%",$value);
                        $store_params[]=&${$fields};
                        $OProveedor->addFilter($fields);
                    }
                }
                $sql= $oRol->listar();
				if(!$init_stmt->prepare($sql))
					throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $objConex->error, $objConex->errno);
                if(count($store_params)>1){
                    if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
                        throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
                }
                $init_stmt->execute();
                $result=$init_stmt->get_result();
                $response_json['success']=true;
                $response_json['totalItemCount']=$result->num_rows;
                if($response_json['totalItemCount']==0)
                    break;
                if(isset($data->start, $data->number)){
                    $response_json['numberOfPages']=ceil($result->num_rows/$data->number);
                    $Opagination=new GPagination();
                    $Opagination->setInit($data->start);
                    $Opagination->setLimit($data->number);
                    $sql=$Opagination->prepareSQL($sql);
                }
                if(!$init_stmt->prepare($sql))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
                if(count($store_params)>1){
                    if(!call_user_func_array(array($init_stmt, 'bind_param'), $store_params))
                        throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
                }
                $init_stmt->execute();
                $result=$init_stmt->get_result();
                $i=(isset($data->start))?$data->start:0;
                while($rows=$result->fetch_assoc()){
                    array_push($response_json['rows'], array_merge($rows, array("item"=>++$i)));
                }
            break;
            case 'guardar':
                if(!isset($data->id_rol, $data->nombre_rol, $data->status, $data->items) || empty($data->nombre_rol) || !is_array($data->items) )
                    break;
				$data->nombre_rol=strtoupper($data->nombre_rol);
                if(empty($data->id_rol)){
                    $sql= $oRol->agregarRol();
                }else{
                    $sql= $oRol->editarRol();
                }
                $oConector->setAutocommit(FALSE);
                $init_commit=$oConector->stmt_init();
                if(!$init_commit->prepare($sql))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $oConector->error, $oConector->errno);
                if(!$init_commit->bind_param('si',$data->nombre_rol, $data->id_rol))
                    throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $oConector->error, $oConector->errno);
                $init_commit->execute();
                $response_json['affected_rows']=$init_commit->affected_rows;
                if($response_json['affected_rows']==-1){
                    $response_json['messages']="No se pudo guardar el registro";
                    break;
                }else{
                    $response_json['success']=true;
                    $id_rol=(empty($data->id_rol))?$init_commit->insert_id:$data->id_rol;
                    if(empty($id_rol)){
                        $response_json['messages']="Ocurrio un problema al guardar el registro";
                        break;
                    }
                    if(!empty($data->id_rol)){
                        $sql_delete="DELETE FROM detalle_rol WHERE id_rol_sistema=?";
                        if(!$init_commit->prepare($sql_delete))
                            throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $oConector->error, $oConector->errno);
                        if(!$init_commit->bind_param('i', $data->id_rol))
                            throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $oConector->error, $oConector->errno);
                        $init_commit->execute();
                        if($init_commit->error!=''){
                            $response_json['messages']="Ocurrio un problema al quitar la configuracion del rol";
                            break;
                        }
                    }
                    $sql_state="INSERT INTO detalle_rol SELECT ?, id_win_sistemas FROM win_sistemas WHERE no_win_sistemas=?";
                    if(!$init_commit->prepare($sql_state))
                        throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $oConector->error, $oConector->errno);
                    foreach($data->items as $i => $menu){
                        if(!$init_commit->bind_param('is', $id_rol, $menu ))
                            throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $oConector->error, $oConector->errno);
                        $init_commit->execute();
                        if($init_commit->affected_rows!=1 && $init_commit->error!=''){
                            $response_json['messages']="No se pudo registrar uno de los item al Rol";
                            break 2;
                        }
                    }
                }
                $response_json['success']=true;
                $response_json['affected_rows']=1;
                $response_json['messages']="Se guardó con éxito el registro";
                $oConector->commit();
            break;
        }
    echo json_encode($response_json);
    }catch(Exception $e){
        echo $e->getOutMsg();
    }
}
?>