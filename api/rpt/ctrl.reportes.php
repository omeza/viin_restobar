<?php
session_start();
error_reporting(0);
ini_set('display_errors',0);
header('Access-Control-Allow-Origin:');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE,OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
require '../api-viin/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\Style;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
$a_events=array('rpt_sucursal');
if(isset($_POST['oper']) && in_array($_POST['oper'], $a_events)){
    try{
        $response_json=array('success'=>false,'message'=>'','empleados'=>array(), 'num_rows'=>0, 'option'=>array(), "debug"=>false);
     
        switch($_POST['oper']){
          case 'rpt_sucursal':
          $datos=json_decode($_POST['data']); 
          // Create new Spreadsheet object
          $spreadsheet = new Spreadsheet(); 
          // Set document properties
          $spreadsheet->getProperties()->setCreator('VIIN')
              ->setLastModifiedBy('VIIN')
              ->setTitle('Office 2007 XLSX Test Document')
              ->setSubject('Office 2007 XLSX Test Document')
              ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
              ->setKeywords('office 2007 openxml php')
              ->setCategory('Test result file');
          // Add some data
          $spreadsheet->setActiveSheetIndex(0)
          ->setCellValue('B2', 'LISTA DE SUCURSALES')
          ->mergeCells('B2:E2');

          $spreadsheet->getActiveSheet()->getStyle('B2')->getAlignment()->applyFromArray(
                    [
                        'horizontal'   => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical'     => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                        'textRotation' => 0,
                        'wrapText'     => TRUE
                    ]
          );

          $spreadsheet->getActiveSheet()->getStyle('B2:E2')->getBorders()->applyFromArray(
                     [
                         'bottom' => [
                             'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                             'color' => [
                                 'rgb' => '808080'
                             ]
                         ],
                         'top' => [
                             'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                             'color' => [
                                 'rgb' => '808080'
                             ]
                         ],
                         'right' => [
                             'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                             'color' => [
                                 'rgb' => '808080'
                             ]
                         ],
                         'left' => [
                             'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                             'color' => [
                                 'rgb' => '808080'
                             ]
                         ]  
                     ]
             );



          $spreadsheet->setActiveSheetIndex(0)
          ->setCellValue('B4', '#')
          ->setCellValue('C4', 'SUCURSAL')
          ->setCellValue('D4', 'DIRECCION')
          ->setCellValue('E4', 'OBSERVACION');

          $spreadsheet->getActiveSheet()->getStyle('B4:E4')->getAlignment()->applyFromArray(
            [
                'horizontal'   => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical'     => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'textRotation' => 0,
                'wrapText'     => TRUE
            ]
            );

            $spreadsheet->getActiveSheet()->getStyle('B4:E4')->getBorders()->applyFromArray(
                        [
                            'bottom' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => [
                                    'rgb' => '808080'
                                ]
                            ],
                            'top' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => [
                                    'rgb' => '808080'
                                ]
                            ],
                            'right' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => [
                                    'rgb' => '808080'
                                ]
                            ],
                            'left' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => [
                                    'rgb' => '808080'
                                ]
                            ]
                        ]
                );
          $i=0;
          $f=5;  
         
         
          foreach($datos as $r){  
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue('B'.$f, $r->id_sucursal)
                ->setCellValue('C'.$f, $r->no_sucursal)
                ->setCellValue('D'.$f, $r->no_direccion)
                ->setCellValue('E'.$f, $r->no_observacion);
                $spreadsheet->getActiveSheet()->getStyle('B'.$f.':E'.$f)->getBorders()->applyFromArray(
                    [
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => [
                                'rgb' => '808080'
                            ]
                        ],
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => [
                                'rgb' => '808080'
                            ]
                        ],
                        'right' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => [
                                'rgb' => '808080'
                            ]
                        ],
                        'left' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => [
                                'rgb' => '808080'
                            ]
                        ]
                    ]
            );
                $i++;
                $f++;
          }             
          for($i ='B'; $i<='E'; $i++){
                    $spreadsheet->setActiveSheetIndex(0)->getColumnDimension($i)->setAutoSize(TRUE);
          }
                
          // Rename worksheet
          $spreadsheet->getActiveSheet()->setTitle('Sucursal');

         
          
          // Set active sheet index to the first sheet, so Excel opens this as the first sheet
          $spreadsheet->setActiveSheetIndex(0);
          // Redirect output to a client’s web browser (Xlsx)
          header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
          header('Content-Disposition: attachment;filename="Lista-Sucursal.xlsx"');
          header('Cache-Control: max-age=0');
          // If you're serving to IE 9, then the following may be needed
          header('Cache-Control: max-age=1');
          // If you're serving to IE over SSL, then the following may be needed
          header('Expires: Mon, 26 Jul 2020 05:00:00 GMT'); // Date in the past
          header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
          header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
          header('Pragma: public'); // HTTP/1.0
          $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
          ob_end_clean();
          $writer->save('php://output');
          exit;
          break;
      }
		echo json_encode($response_json);
	}catch(Exception $e){
		echo $e->getOutMsg();
	}
}
?>
