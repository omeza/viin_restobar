-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-09-2018 a las 01:23:24
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Base de datos: `db_sinven`
--

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `asign_num_comprobante`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `asign_num_comprobante` (IN `tip_comp` VARCHAR(1), IN `id_boleta` INT, INOUT `num_ticket` VARCHAR(11))  BEGIN
    DECLARE lastnum INT;
    SELECT IFNULL(MAX(CONVERT(idticket, UNSIGNED INT)), '0') INTO lastnum FROM boletas WHERE tipo_comprobante=tip_comp AND idboleta!=id_boleta;
    SET num_ticket=LPAD(lastnum+1, 11, '0');
    UPDATE boletas SET idticket=num_ticket WHERE idboleta=id_boleta;
END$$

DROP PROCEDURE IF EXISTS `autoincidproduct`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `autoincidproduct` (IN `idproducto` INT, OUT `codigo` CHAR(10))  BEGIN
	DECLARE lastid INT DEFAULT 0;
	SELECT inicia_codigo INTO lastid FROM configuracion;
    SET codigo = LPAD(TRIM(CONVERT(lastid+1, CHAR(10))) , 10, '0');
    UPDATE configuracion SET inicia_codigo=inicia_codigo+1;
    UPDATE productos SET cod_producto=codigo  WHERE idproductos=idproducto;
END$$

DROP PROCEDURE IF EXISTS `update_stock`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_stock` (IN `idproducto` INT, IN `cantidad` INT)  NO SQL
BEGIN
	DECLARE lcstock INT DEFAULT 0;
	DECLARE stock INT DEFAULT 0;
	DECLARE exist_id INT;
	DECLARE curs1 CURSOR FOR SELECT stock FROM inventario WHERE idproductos=idproducto;
	OPEN curs1;
	SELECT FOUND_ROWS() INTO exist_id ;
	IF exist_id = 1 THEN
    	FETCH NEXT FROM curs1 INTO lcstock;
   		UPDATE inventario SET stock=lcstock+cantidad WHERE idproductos=idproducto;
	ELSE
   		INSERT INTO inventario VALUES (idproducto, cantidad);
	END IF;
	CLOSE curs1;
END$$

--
-- Funciones
--
DROP FUNCTION IF EXISTS `calcular_neto_precio`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `calcular_neto_precio` (`monto` FLOAT(13,2), `impuesto` FLOAT(13,2), `excento` CHAR(1)) RETURNS FLOAT(13,2) NO SQL
BEGIN 
    DECLARE mto_neto FLOAT(13,2) DEFAULT 0.00;
    IF excento=0 THEN
        SET mto_neto=monto/(1+(impuesto/100));
    END IF;
    RETURN mto_neto;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `almacenes`
--

DROP TABLE IF EXISTS `almacenes`;
CREATE TABLE `almacenes` (
  `idalmacen` int(11) NOT NULL,
  `desc_almacen` varchar(30) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bancos`
--

DROP TABLE IF EXISTS `bancos`;
CREATE TABLE `bancos` (
  `idbancos` int(11) NOT NULL,
  `sigla_banco` varchar(10) NOT NULL,
  `nombre_banco` varchar(45) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boletas`
--

DROP TABLE IF EXISTS `boletas`;
CREATE TABLE `boletas` (
  `idboleta` int(11) NOT NULL,
  `idticket` varchar(11) DEFAULT NULL,
  `idempleado` int(11) NOT NULL,
  `idclientes` int(11) NOT NULL,
  `fecventa` date NOT NULL,
  `tipo_comprobante` enum('B','F','K') NOT NULL DEFAULT 'K',
  `factura_electronica` enum('0','1') NOT NULL DEFAULT '0',
  `idordencompra` int(11) DEFAULT NULL,
  `pagado` enum('0','1') NOT NULL DEFAULT '1',
  `total` double(13,2) NOT NULL,
  `subtotal` double(13,2) NOT NULL,
  `mto_impuesto` double(13,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

DROP TABLE IF EXISTS `cargo`;
CREATE TABLE `cargo` (
  `id_cargo` int(11) NOT NULL,
  `desc_cargo` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `idclientes` int(11) NOT NULL,
  `idtip_documento` int(11) NOT NULL,
  `num_documento` varchar(15) NOT NULL,
  `apellidos_nombres` varchar(60) NOT NULL,
  `direccion` text,
  `tlf_hab` varchar(15) DEFAULT NULL,
  `tlf_movil` varchar(15) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

DROP TABLE IF EXISTS `compras`;
CREATE TABLE `compras` (
  `idcompra` int(11) NOT NULL,
  `idord_compra` int(11) DEFAULT NULL,
  `numfactura` varchar(15) NOT NULL,
  `idproveedor` int(11) NOT NULL,
  `fecha_compra` date NOT NULL,
  `idcondicion_pago` int(11) NOT NULL,
  `dias_pago` int(11) NOT NULL DEFAULT '0',
  `fecha_pagar` date DEFAULT NULL,
  `porc_descuento` float(5,2) DEFAULT NULL,
  `subtotal` float(13,2) NOT NULL,
  `mto_impuesto` float(13,2) NOT NULL,
  `total` float(13,2) NOT NULL,
  `pagado` enum('0','1') NOT NULL,
  `status` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `condicion_pago`
--

DROP TABLE IF EXISTS `condicion_pago`;
CREATE TABLE `condicion_pago` (
  `idcondicion_pago` int(11) NOT NULL,
  `desc_condicion` varchar(45) NOT NULL,
  `predeterminado` enum('0','1') NOT NULL DEFAULT '0',
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

DROP TABLE IF EXISTS `configuracion`;
CREATE TABLE `configuracion` (
  `porc_impuesto` decimal(5,2) NOT NULL,
  `nombre_impuesto` varchar(8) NOT NULL,
  `iniciar_factura` int(11) DEFAULT NULL,
  `iniciar_boleta` int(11) DEFAULT NULL,
  `nautorizacionimpresa` varchar(12) NOT NULL,
  `id_moneda` int(11) NOT NULL,
  `autoidproducto` enum('0','1') NOT NULL DEFAULT '1',
  `inicia_codigo` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentasbancarias`
--

DROP TABLE IF EXISTS `cuentasbancarias`;
CREATE TABLE `cuentasbancarias` (
  `idcuentasbancarias` int(11) NOT NULL,
  `idbancos` int(11) NOT NULL,
  `numctabancaria` varchar(20) NOT NULL,
  `idtipocta` int(11) NOT NULL,
  `idmoneda` int(11) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `data_sunat`
--

DROP TABLE IF EXISTS `data_sunat`;
CREATE TABLE `data_sunat` (
  `ruc` varchar(15) NOT NULL,
  `razon_social` varchar(90) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `ubigeo` varchar(10) DEFAULT NULL,
  `direccion` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_orden_pedidos`
--

DROP TABLE IF EXISTS `detalles_orden_pedidos`;
CREATE TABLE `detalles_orden_pedidos` (
  `iddetalles_opedido` int(11) NOT NULL,
  `idordenpedido` int(11) NOT NULL,
  `idproductos` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `mto_ult_compra` decimal(13,2) NOT NULL DEFAULT '0.00',
  `porc_impuesto` decimal(5,2) NOT NULL DEFAULT '0.00',
  `exento_impuesto` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_compras`
--

DROP TABLE IF EXISTS `detalle_compras`;
CREATE TABLE `detalle_compras` (
  `idcompra` int(11) NOT NULL,
  `idproducto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `mto_impuesto` float(5,2) NOT NULL DEFAULT '0.00',
  `excento_impuesto` enum('0','1') NOT NULL DEFAULT '0',
  `precio_unidad` float(13,2) NOT NULL DEFAULT '0.00',
  `total_importe` float(13,2) NOT NULL DEFAULT '0.00',
  `precio_neto` float(13,2) NOT NULL DEFAULT '0.00',
  `tot_impuesto` float(13,2) NOT NULL DEFAULT '0.00',
  `lote` char(10) NOT NULL,
  `fecha_vencimiento` char(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_inventario`
--

DROP TABLE IF EXISTS `detalle_inventario`;
CREATE TABLE `detalle_inventario` (
  `idproductos` int(11) NOT NULL,
  `lote` varchar(20) NOT NULL,
  `fecha_vencimiento` varchar(10) DEFAULT NULL,
  `idalmacen` int(11) DEFAULT NULL,
  `idubicacion` int(11) DEFAULT NULL,
  `pasillo` varchar(5) DEFAULT NULL,
  `stand` varchar(5) DEFAULT NULL,
  `fila` varchar(5) DEFAULT NULL,
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Disparadores `detalle_inventario`
--
DROP TRIGGER IF EXISTS `addstockinicial`;
DELIMITER $$
CREATE TRIGGER `addstockinicial` BEFORE INSERT ON `detalle_inventario` FOR EACH ROW BEGIN
	UPDATE inventario SET stock = stock + NEW.cantidad WHERE idproductos = NEW.idproductos;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_rol`
--

DROP TABLE IF EXISTS `detalle_rol`;
CREATE TABLE `detalle_rol` (
  `idrol_sistema` int(11) NOT NULL,
  `idwin_state` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_boleta`
--

DROP TABLE IF EXISTS `det_boleta`;
CREATE TABLE `det_boleta` (
  `idboleta` int(11) NOT NULL,
  `idproductos` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `costo_unidad` double(13,2) NOT NULL,
  `exento_impuesto` enum('1','0') NOT NULL DEFAULT '0',
  `mto_impuesto` double(13,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_ordencompras`
--

DROP TABLE IF EXISTS `det_ordencompras`;
CREATE TABLE `det_ordencompras` (
  `idord_compra` int(11) NOT NULL,
  `idproductos` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `mto_unidad` double(13,2) NOT NULL DEFAULT '0.00',
  `exento_impuesto` enum('0','1') NOT NULL DEFAULT '0',
  `mto_impuesto` double(13,2) NOT NULL DEFAULT '0.00',
  `mto_total` double(13,2) NOT NULL DEFAULT '0.00',
  `entregado` enum('0','1') NOT NULL DEFAULT '0',
  `fec_entregado` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_pagoboleta`
--

DROP TABLE IF EXISTS `det_pagoboleta`;
CREATE TABLE `det_pagoboleta` (
  `idboleta` int(11) NOT NULL,
  `id_tipo_pago` int(11) NOT NULL,
  `mto_pago` double(13,2) NOT NULL DEFAULT '0.00',
  `idbanco` int(11) DEFAULT NULL,
  `num_tarjeta` varchar(18) DEFAULT NULL,
  `num_referencia` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

DROP TABLE IF EXISTS `empleado`;
CREATE TABLE `empleado` (
  `idempleado` int(11) NOT NULL,
  `idtip_documento` int(11) NOT NULL,
  `num_documento` varchar(15) NOT NULL,
  `apellido_paterno` varchar(45) NOT NULL,
  `apellido_materno` varchar(45) DEFAULT NULL,
  `nombres` varchar(45) NOT NULL,
  `sexo` enum('1','0') NOT NULL DEFAULT '0',
  `fecnacimiento` date NOT NULL,
  `edocivil` enum('C','S','D','V','O') NOT NULL DEFAULT 'S',
  `direccion_hab` text,
  `tlf_movil` varchar(15) DEFAULT NULL,
  `tlf_habitacion` varchar(15) DEFAULT NULL,
  `email` varchar(65) NOT NULL,
  `id_profesion` int(11) NOT NULL,
  `id_cargo` int(11) NOT NULL,
  `fec_ingreso` date NOT NULL,
  `fec_inactivo` date DEFAULT NULL,
  `visible` enum('1','0') NOT NULL DEFAULT '1',
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

DROP TABLE IF EXISTS `empresa`;
CREATE TABLE `empresa` (
  `idtip_documento` int(11) NOT NULL,
  `num_fiscal` varchar(12) NOT NULL,
  `nombre_empresa` varchar(60) NOT NULL,
  `nombre_comercial` varchar(75) DEFAULT NULL,
  `direccion` text NOT NULL,
  `direccion_local` text,
  `tlf_fiscal` varchar(15) DEFAULT NULL,
  `email` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados_pais`
--

DROP TABLE IF EXISTS `estados_pais`;
CREATE TABLE `estados_pais` (
  `idestados_pais` int(11) NOT NULL,
  `idpais` int(11) NOT NULL,
  `desc_estados` varchar(45) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas_electronicas`
--

DROP TABLE IF EXISTS `facturas_electronicas`;
CREATE TABLE `facturas_electronicas` (
  `idfacturaelectronica` int(11) NOT NULL,
  `anulado` enum('0','1') NOT NULL DEFAULT '0',
  `fec_anulado` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

DROP TABLE IF EXISTS `grupos`;
CREATE TABLE `grupos` (
  `idgrupo` int(11) NOT NULL,
  `cod_grupo` varchar(8) NOT NULL,
  `desc_grupo` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

DROP TABLE IF EXISTS `inventario`;
CREATE TABLE `inventario` (
  `idproductos` int(11) NOT NULL,
  `stock` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `laboratorios`
--

DROP TABLE IF EXISTS `laboratorios`;
CREATE TABLE `laboratorios` (
  `idlaboratorio` int(11) NOT NULL,
  `desc_laboratorio` varchar(60) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `linea`
--

DROP TABLE IF EXISTS `linea`;
CREATE TABLE `linea` (
  `idlinea` int(11) NOT NULL,
  `desc_linea` varchar(60) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `moneda`
--

DROP TABLE IF EXISTS `moneda`;
CREATE TABLE `moneda` (
  `idmoneda` int(11) NOT NULL,
  `simb_moneda` varchar(4) NOT NULL,
  `nom_moneda` varchar(10) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ordenes_compras`
--

DROP TABLE IF EXISTS `ordenes_compras`;
CREATE TABLE `ordenes_compras` (
  `idord_compra` int(11) NOT NULL,
  `idproveedor` int(11) NOT NULL,
  `fecha_entrega` date NOT NULL,
  `id_tipo_pago` int(11) NOT NULL,
  `idempleado` int(11) NOT NULL,
  `fec_registro` datetime DEFAULT CURRENT_TIMESTAMP,
  `entregado` enum('0','1') NOT NULL DEFAULT '0',
  `observacion` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_pedidos`
--

DROP TABLE IF EXISTS `orden_pedidos`;
CREATE TABLE `orden_pedidos` (
  `idordenpedido` int(11) NOT NULL,
  `idproveedor` int(11) NOT NULL,
  `fecha_orden` date NOT NULL,
  `observacion` text,
  `idusuario` int(11) NOT NULL,
  `fec_registro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_aprobada` date DEFAULT NULL,
  `fecha_anulada` date DEFAULT NULL,
  `status` enum('A','C','N','E') NOT NULL DEFAULT 'A' COMMENT 'A=>Aprobado\nC=>Cancelado\nN=>Anulado\nE=>Entregado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos_compras`
--

DROP TABLE IF EXISTS `pagos_compras`;
CREATE TABLE `pagos_compras` (
  `idcompra` int(11) NOT NULL,
  `fecha_pago` date NOT NULL,
  `mto_pago` double(13,2) NOT NULL,
  `id_tipo_pago` int(11) DEFAULT NULL,
  `ref_pago` varchar(15) DEFAULT NULL,
  `monto_pagado` double(13,2) DEFAULT NULL,
  `fecha_pagado` date DEFAULT NULL,
  `fecha_reprogramada` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

DROP TABLE IF EXISTS `paises`;
CREATE TABLE `paises` (
  `idpais` int(11) NOT NULL,
  `desc_pais` varchar(45) NOT NULL,
  `prefijo` varchar(5) NOT NULL,
  `predeterminado` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privilegios_rol`
--

DROP TABLE IF EXISTS `privilegios_rol`;
CREATE TABLE `privilegios_rol` (
  `idpriv_rol` int(11) NOT NULL,
  `idrol_sistema` int(11) NOT NULL,
  `idwin_sistemas` int(11) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos` (
  `idproductos` int(11) NOT NULL,
  `cod_producto` varchar(10) DEFAULT NULL,
  `nom_producto` varchar(60) NOT NULL,
  `idlaboratorio` int(11) NOT NULL,
  `idgrupo` int(11) DEFAULT NULL,
  `idsubgrupo` int(11) DEFAULT NULL,
  `idlinea` int(11) DEFAULT NULL,
  `descripcion` text,
  `mto_compra` decimal(13,2) NOT NULL,
  `fec_ult_compra` date DEFAULT NULL,
  `exento_impuesto` enum('0','1') NOT NULL DEFAULT '0',
  `mto_impuesto` decimal(5,2) NOT NULL DEFAULT '0.00',
  `metod_venta` enum('1','2') NOT NULL DEFAULT '1',
  `mto_minimo` decimal(13,2) DEFAULT NULL,
  `mto_sugerido` decimal(13,2) DEFAULT NULL,
  `porc_ganancia` decimal(5,2) DEFAULT NULL,
  `facturarsinstock` enum('0','1') NOT NULL DEFAULT '0',
  `avisostockminimo` enum('0','1') NOT NULL,
  `stockminimo` int(11) NOT NULL DEFAULT '0',
  `medida` varchar(10) DEFAULT NULL,
  `peso` varchar(10) DEFAULT NULL,
  `idumedida` int(11) NOT NULL DEFAULT '13',
  `categoria` enum('1','2','3','4') NOT NULL DEFAULT '1',
  `usa_serial` enum('1','0') NOT NULL DEFAULT '1',
  `serial_1` varchar(15) DEFAULT NULL,
  `serial_2` varchar(15) DEFAULT NULL,
  `serial_3` varchar(15) DEFAULT NULL,
  `etiqueta_label` enum('0','1') NOT NULL DEFAULT '0',
  `etiqueta_precio` enum('0','1') NOT NULL DEFAULT '0',
  `etiqueta_codigo` enum('0','1') NOT NULL DEFAULT '0',
  `imagen` blob,
  `ocultar_venta` enum('0','1') NOT NULL DEFAULT '0',
  `fec_ingreso` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesion`
--

DROP TABLE IF EXISTS `profesion`;
CREATE TABLE `profesion` (
  `id_profesion` int(11) NOT NULL,
  `desc_profesion` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
CREATE TABLE `proveedores` (
  `idproveedor` int(11) NOT NULL,
  `idtip_documento` int(11) NOT NULL,
  `num_documento` varchar(15) NOT NULL,
  `razon_social` varchar(60) NOT NULL,
  `direccion` text NOT NULL,
  `idestados_pais` int(11) NOT NULL,
  `repr_legal` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `tlf_hab` varchar(15) DEFAULT NULL,
  `tlf_movil` varchar(15) DEFAULT NULL,
  `fec_ingreso` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_sistema`
--

DROP TABLE IF EXISTS `rol_sistema`;
CREATE TABLE `rol_sistema` (
  `idrol_sistema` int(11) NOT NULL,
  `nom_rol` varchar(30) NOT NULL,
  `state_default` int(11) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `hidden` enum('1','0') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subgrupos`
--

DROP TABLE IF EXISTS `subgrupos`;
CREATE TABLE `subgrupos` (
  `idsubgrupo` int(11) NOT NULL,
  `idgrupo` int(11) NOT NULL,
  `desc_subgrupo` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoctasbancarias`
--

DROP TABLE IF EXISTS `tipoctasbancarias`;
CREATE TABLE `tipoctasbancarias` (
  `idtipocta` int(11) NOT NULL,
  `desctipocta` varchar(45) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_pago`
--

DROP TABLE IF EXISTS `tipo_pago`;
CREATE TABLE `tipo_pago` (
  `id_tipo_pago` int(11) NOT NULL,
  `desc_tipopago` varchar(25) NOT NULL,
  `especif_banco` enum('0','1') NOT NULL DEFAULT '0',
  `especif_serial` enum('0','1') NOT NULL DEFAULT '0',
  `especif_request` enum('0','1') NOT NULL DEFAULT '0',
  `isdefault` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tip_documento`
--

DROP TABLE IF EXISTS `tip_documento`;
CREATE TABLE `tip_documento` (
  `idtip_documento` int(11) NOT NULL,
  `desc_documento` varchar(45) NOT NULL,
  `predeterminado` enum('1','0') NOT NULL DEFAULT '0',
  `status` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ubicacion`
--

DROP TABLE IF EXISTS `ubicacion`;
CREATE TABLE `ubicacion` (
  `idubicacion` int(11) NOT NULL,
  `desc_ubicacion` varchar(20) NOT NULL,
  `disp_ventas` enum('0','1') NOT NULL DEFAULT '0',
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidadmedidad`
--

DROP TABLE IF EXISTS `unidadmedidad`;
CREATE TABLE `unidadmedidad` (
  `idumedida` int(11) NOT NULL,
  `abrev_umedida` varchar(8) NOT NULL,
  `desc_umedida` varchar(55) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `idusuario` int(11) NOT NULL,
  `idempleado` int(11) NOT NULL,
  `idrol_sistema` int(11) NOT NULL,
  `fec_ingreso` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fec_inactivo` date DEFAULT NULL,
  `login` varchar(10) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `visible` enum('0','1') NOT NULL DEFAULT '1',
  `status` enum('0','1') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas_temporal`
--

DROP TABLE IF EXISTS `ventas_temporal`;
CREATE TABLE `ventas_temporal` (
  `idproductos` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `idusuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `win_sistemas`
--

DROP TABLE IF EXISTS `win_sistemas`;
CREATE TABLE `win_sistemas` (
  `idwin_state` int(11) NOT NULL,
  `name_state` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `almacenes`
--
ALTER TABLE `almacenes`
  ADD PRIMARY KEY (`idalmacen`),
  ADD UNIQUE KEY `desc_almacen_UNIQUE` (`desc_almacen`);

--
-- Indices de la tabla `bancos`
--
ALTER TABLE `bancos`
  ADD PRIMARY KEY (`idbancos`),
  ADD UNIQUE KEY `idbancos_UNIQUE` (`idbancos`),
  ADD UNIQUE KEY `sigla_banco_UNIQUE` (`sigla_banco`);

--
-- Indices de la tabla `boletas`
--
ALTER TABLE `boletas`
  ADD PRIMARY KEY (`idboleta`),
  ADD UNIQUE KEY `idboletas_UNIQUE` (`idboleta`),
  ADD KEY `num_ticket` (`idticket`,`tipo_comprobante`,`factura_electronica`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id_cargo`),
  ADD UNIQUE KEY `id_cargo_UNIQUE` (`id_cargo`),
  ADD UNIQUE KEY `desc_cargo_UNIQUE` (`desc_cargo`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`idclientes`),
  ADD UNIQUE KEY `idclientes_UNIQUE` (`idclientes`),
  ADD UNIQUE KEY `doc_identidad` (`idtip_documento`,`num_documento`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`idcompra`),
  ADD UNIQUE KEY `idcompra_UNIQUE` (`idcompra`),
  ADD UNIQUE KEY `fact_prov` (`numfactura`,`idproveedor`);

--
-- Indices de la tabla `condicion_pago`
--
ALTER TABLE `condicion_pago`
  ADD PRIMARY KEY (`idcondicion_pago`),
  ADD UNIQUE KEY `idcondicion_pago_UNIQUE` (`idcondicion_pago`),
  ADD UNIQUE KEY `desc_condicion_UNIQUE` (`desc_condicion`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD KEY `id_moneda_fk_idx` (`id_moneda`);

--
-- Indices de la tabla `cuentasbancarias`
--
ALTER TABLE `cuentasbancarias`
  ADD PRIMARY KEY (`idcuentasbancarias`),
  ADD UNIQUE KEY `idcuentasbancarias_UNIQUE` (`idcuentasbancarias`),
  ADD UNIQUE KEY `numctabancaria_UNIQUE` (`numctabancaria`),
  ADD KEY `idbancos_fk_idx` (`idbancos`),
  ADD KEY `idmoneda_fk_idx` (`idmoneda`),
  ADD KEY `idtipocta_fk_idx` (`idtipocta`);

--
-- Indices de la tabla `data_sunat`
--
ALTER TABLE `data_sunat`
  ADD PRIMARY KEY (`ruc`),
  ADD UNIQUE KEY `ruc_UNIQUE` (`ruc`),
  ADD KEY `ruc_index` (`ruc`);

--
-- Indices de la tabla `detalles_orden_pedidos`
--
ALTER TABLE `detalles_orden_pedidos`
  ADD PRIMARY KEY (`iddetalles_opedido`),
  ADD UNIQUE KEY `iddetalles_opedido_UNIQUE` (`iddetalles_opedido`),
  ADD KEY `idordenpedido_idx` (`idordenpedido`),
  ADD KEY `idproducto_fk_idx` (`idproductos`);

--
-- Indices de la tabla `detalle_inventario`
--
ALTER TABLE `detalle_inventario`
  ADD PRIMARY KEY (`idproductos`);

--
-- Indices de la tabla `detalle_rol`
--
ALTER TABLE `detalle_rol`
  ADD KEY `rol_state` (`idrol_sistema`,`idwin_state`);

--
-- Indices de la tabla `det_boleta`
--
ALTER TABLE `det_boleta`
  ADD UNIQUE KEY `bolet_articulo` (`idboleta`,`idproductos`);

--
-- Indices de la tabla `det_ordencompras`
--
ALTER TABLE `det_ordencompras`
  ADD KEY `idorden_compra_fk_idx` (`idord_compra`),
  ADD KEY `idproductos_idx` (`idproductos`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`idempleado`),
  ADD UNIQUE KEY `idempleado_UNIQUE` (`idempleado`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD UNIQUE KEY `doc_identidad` (`idtip_documento`,`num_documento`),
  ADD KEY `id_profesion_fk_idx` (`id_profesion`),
  ADD KEY `id_cargo_fk_idx` (`id_cargo`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`num_fiscal`),
  ADD UNIQUE KEY `num_fiscal_UNIQUE` (`num_fiscal`);

--
-- Indices de la tabla `estados_pais`
--
ALTER TABLE `estados_pais`
  ADD PRIMARY KEY (`idestados_pais`),
  ADD UNIQUE KEY `idestados_pais_UNIQUE` (`idestados_pais`),
  ADD UNIQUE KEY `desc_estados_UNIQUE` (`desc_estados`);

--
-- Indices de la tabla `facturas_electronicas`
--
ALTER TABLE `facturas_electronicas`
  ADD PRIMARY KEY (`idfacturaelectronica`);

--
-- Indices de la tabla `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`idgrupo`),
  ADD UNIQUE KEY `idgrupo_UNIQUE` (`idgrupo`),
  ADD UNIQUE KEY `descgrupo_UNIQUE` (`desc_grupo`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`idproductos`),
  ADD UNIQUE KEY `idproductos_UNIQUE` (`idproductos`),
  ADD KEY `idproductos_fk_idx` (`idproductos`);

--
-- Indices de la tabla `laboratorios`
--
ALTER TABLE `laboratorios`
  ADD PRIMARY KEY (`idlaboratorio`),
  ADD UNIQUE KEY `idlaboratorio_UNIQUE` (`idlaboratorio`),
  ADD UNIQUE KEY `desc_laboratorio_UNIQUE` (`desc_laboratorio`);

--
-- Indices de la tabla `linea`
--
ALTER TABLE `linea`
  ADD PRIMARY KEY (`idlinea`),
  ADD UNIQUE KEY `idlinea_UNIQUE` (`idlinea`),
  ADD UNIQUE KEY `desc_linea_UNIQUE` (`desc_linea`);

--
-- Indices de la tabla `moneda`
--
ALTER TABLE `moneda`
  ADD PRIMARY KEY (`idmoneda`),
  ADD UNIQUE KEY `idmoneda_UNIQUE` (`idmoneda`),
  ADD UNIQUE KEY `simb_moneda_UNIQUE` (`simb_moneda`),
  ADD UNIQUE KEY `nom_moneda_UNIQUE` (`nom_moneda`);

--
-- Indices de la tabla `ordenes_compras`
--
ALTER TABLE `ordenes_compras`
  ADD PRIMARY KEY (`idord_compra`),
  ADD UNIQUE KEY `idord_compra_UNIQUE` (`idord_compra`),
  ADD KEY `idproveedor_fk_idx` (`idproveedor`),
  ADD KEY `idtipo_pago_fk_idx` (`id_tipo_pago`),
  ADD KEY `idempleado_fk_idx` (`idempleado`);

--
-- Indices de la tabla `orden_pedidos`
--
ALTER TABLE `orden_pedidos`
  ADD PRIMARY KEY (`idordenpedido`),
  ADD UNIQUE KEY `idordenpedido_UNIQUE` (`idordenpedido`),
  ADD KEY `idproveedor_fk_idx` (`idproveedor`);

--
-- Indices de la tabla `pagos_compras`
--
ALTER TABLE `pagos_compras`
  ADD UNIQUE KEY `idcompr_fecha` (`idcompra`,`fecha_pago`);

--
-- Indices de la tabla `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`idpais`),
  ADD UNIQUE KEY `idpais_UNIQUE` (`idpais`);

--
-- Indices de la tabla `privilegios_rol`
--
ALTER TABLE `privilegios_rol`
  ADD PRIMARY KEY (`idpriv_rol`),
  ADD UNIQUE KEY `idpriv_rol_UNIQUE` (`idpriv_rol`),
  ADD UNIQUE KEY `rol_privilegio` (`idrol_sistema`,`idwin_sistemas`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idproductos`),
  ADD UNIQUE KEY `idproductos_UNIQUE` (`idproductos`),
  ADD UNIQUE KEY `nom_producto_UNIQUE` (`nom_producto`,`idlaboratorio`) USING BTREE,
  ADD KEY `idlaboratorio_fk_idx` (`idlaboratorio`),
  ADD KEY `idlinea_fk_idx` (`idlinea`),
  ADD KEY `idumedida_fk_idx` (`idumedida`);

--
-- Indices de la tabla `profesion`
--
ALTER TABLE `profesion`
  ADD PRIMARY KEY (`id_profesion`),
  ADD UNIQUE KEY `id_profesion_UNIQUE` (`id_profesion`),
  ADD UNIQUE KEY `desc_profesion_UNIQUE` (`desc_profesion`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`idproveedor`),
  ADD UNIQUE KEY `idproveedor_UNIQUE` (`idproveedor`);

--
-- Indices de la tabla `rol_sistema`
--
ALTER TABLE `rol_sistema`
  ADD PRIMARY KEY (`idrol_sistema`),
  ADD UNIQUE KEY `idrol_sistema_UNIQUE` (`idrol_sistema`),
  ADD UNIQUE KEY `nom_rol_UNIQUE` (`nom_rol`);

--
-- Indices de la tabla `subgrupos`
--
ALTER TABLE `subgrupos`
  ADD PRIMARY KEY (`idsubgrupo`),
  ADD UNIQUE KEY `idsubgrupo_UNIQUE` (`idsubgrupo`);

--
-- Indices de la tabla `tipoctasbancarias`
--
ALTER TABLE `tipoctasbancarias`
  ADD PRIMARY KEY (`idtipocta`),
  ADD UNIQUE KEY `idtipoctasbancarias_UNIQUE` (`idtipocta`),
  ADD UNIQUE KEY `desctipocta_UNIQUE` (`desctipocta`);

--
-- Indices de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  ADD PRIMARY KEY (`id_tipo_pago`),
  ADD UNIQUE KEY `id_tipo_pago_UNIQUE` (`id_tipo_pago`);

--
-- Indices de la tabla `tip_documento`
--
ALTER TABLE `tip_documento`
  ADD PRIMARY KEY (`idtip_documento`),
  ADD UNIQUE KEY `idtip_documento_UNIQUE` (`idtip_documento`),
  ADD UNIQUE KEY `desc_documento_UNIQUE` (`desc_documento`);

--
-- Indices de la tabla `ubicacion`
--
ALTER TABLE `ubicacion`
  ADD PRIMARY KEY (`idubicacion`),
  ADD UNIQUE KEY `desc_ubicacion_UNIQUE` (`desc_ubicacion`);

--
-- Indices de la tabla `unidadmedidad`
--
ALTER TABLE `unidadmedidad`
  ADD PRIMARY KEY (`idumedida`),
  ADD UNIQUE KEY `idumedida_UNIQUE` (`idumedida`),
  ADD UNIQUE KEY `desc_umedida_UNIQUE` (`desc_umedida`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuario`),
  ADD UNIQUE KEY `idusuarios_UNIQUE` (`idusuario`),
  ADD UNIQUE KEY `idempleado_UNIQUE` (`idempleado`),
  ADD UNIQUE KEY `login_UNIQUE` (`login`),
  ADD KEY `id_rolsistema_fk_idx` (`idrol_sistema`);

--
-- Indices de la tabla `ventas_temporal`
--
ALTER TABLE `ventas_temporal`
  ADD PRIMARY KEY (`idproductos`),
  ADD UNIQUE KEY `venta_unika` (`idproductos`,`idusuario`);

--
-- Indices de la tabla `win_sistemas`
--
ALTER TABLE `win_sistemas`
  ADD PRIMARY KEY (`idwin_state`),
  ADD UNIQUE KEY `idwin_sistemas_UNIQUE` (`idwin_state`),
  ADD UNIQUE KEY `name_state_UNIQUE` (`name_state`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `almacenes`
--
ALTER TABLE `almacenes`
  MODIFY `idalmacen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `bancos`
--
ALTER TABLE `bancos`
  MODIFY `idbancos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `boletas`
--
ALTER TABLE `boletas`
  MODIFY `idboleta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id_cargo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `idclientes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `idcompra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `condicion_pago`
--
ALTER TABLE `condicion_pago`
  MODIFY `idcondicion_pago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `cuentasbancarias`
--
ALTER TABLE `cuentasbancarias`
  MODIFY `idcuentasbancarias` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `detalles_orden_pedidos`
--
ALTER TABLE `detalles_orden_pedidos`
  MODIFY `iddetalles_opedido` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `idempleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `estados_pais`
--
ALTER TABLE `estados_pais`
  MODIFY `idestados_pais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `facturas_electronicas`
--
ALTER TABLE `facturas_electronicas`
  MODIFY `idfacturaelectronica` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `grupos`
--
ALTER TABLE `grupos`
  MODIFY `idgrupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `laboratorios`
--
ALTER TABLE `laboratorios`
  MODIFY `idlaboratorio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=220;

--
-- AUTO_INCREMENT de la tabla `linea`
--
ALTER TABLE `linea`
  MODIFY `idlinea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `moneda`
--
ALTER TABLE `moneda`
  MODIFY `idmoneda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ordenes_compras`
--
ALTER TABLE `ordenes_compras`
  MODIFY `idord_compra` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `orden_pedidos`
--
ALTER TABLE `orden_pedidos`
  MODIFY `idordenpedido` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `paises`
--
ALTER TABLE `paises`
  MODIFY `idpais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT de la tabla `privilegios_rol`
--
ALTER TABLE `privilegios_rol`
  MODIFY `idpriv_rol` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `idproductos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1627;

--
-- AUTO_INCREMENT de la tabla `profesion`
--
ALTER TABLE `profesion`
  MODIFY `id_profesion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `idproveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `rol_sistema`
--
ALTER TABLE `rol_sistema`
  MODIFY `idrol_sistema` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `subgrupos`
--
ALTER TABLE `subgrupos`
  MODIFY `idsubgrupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipoctasbancarias`
--
ALTER TABLE `tipoctasbancarias`
  MODIFY `idtipocta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tipo_pago`
--
ALTER TABLE `tipo_pago`
  MODIFY `id_tipo_pago` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tip_documento`
--
ALTER TABLE `tip_documento`
  MODIFY `idtip_documento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `ubicacion`
--
ALTER TABLE `ubicacion`
  MODIFY `idubicacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `unidadmedidad`
--
ALTER TABLE `unidadmedidad`
  MODIFY `idumedida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `win_sistemas`
--
ALTER TABLE `win_sistemas`
  MODIFY `idwin_state` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD CONSTRAINT `id_moneda_fk` FOREIGN KEY (`id_moneda`) REFERENCES `moneda` (`idmoneda`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cuentasbancarias`
--
ALTER TABLE `cuentasbancarias`
  ADD CONSTRAINT `idbancos_fk` FOREIGN KEY (`idbancos`) REFERENCES `bancos` (`idbancos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idmoneda_fk` FOREIGN KEY (`idmoneda`) REFERENCES `moneda` (`idmoneda`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idtipocta_fk` FOREIGN KEY (`idtipocta`) REFERENCES `tipoctasbancarias` (`idtipocta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `detalles_orden_pedidos`
--
ALTER TABLE `detalles_orden_pedidos`
  ADD CONSTRAINT `idordenpedido_fk` FOREIGN KEY (`idordenpedido`) REFERENCES `orden_pedidos` (`idordenpedido`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `idproducto_fk` FOREIGN KEY (`idproductos`) REFERENCES `productos` (`idproductos`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `det_boleta`
--
ALTER TABLE `det_boleta`
  ADD CONSTRAINT `idboletas_fk` FOREIGN KEY (`idboleta`) REFERENCES `boletas` (`idboleta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `det_ordencompras`
--
ALTER TABLE `det_ordencompras`
  ADD CONSTRAINT `idorden_compra_fk` FOREIGN KEY (`idord_compra`) REFERENCES `ordenes_compras` (`idord_compra`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idproductos` FOREIGN KEY (`idproductos`) REFERENCES `productos` (`idproductos`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD CONSTRAINT `id_cargo_fk` FOREIGN KEY (`id_cargo`) REFERENCES `cargo` (`id_cargo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `id_profesion_fk` FOREIGN KEY (`id_profesion`) REFERENCES `profesion` (`id_profesion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD CONSTRAINT `idproductos_fk` FOREIGN KEY (`idproductos`) REFERENCES `productos` (`idproductos`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ordenes_compras`
--
ALTER TABLE `ordenes_compras`
  ADD CONSTRAINT `idempleado_fk` FOREIGN KEY (`idempleado`) REFERENCES `empleado` (`idempleado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idproveedor_fk` FOREIGN KEY (`idproveedor`) REFERENCES `proveedores` (`idproveedor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idtipo_pago_fk` FOREIGN KEY (`id_tipo_pago`) REFERENCES `tipo_pago` (`id_tipo_pago`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pagos_compras`
--
ALTER TABLE `pagos_compras`
  ADD CONSTRAINT `idcompra_fk` FOREIGN KEY (`idcompra`) REFERENCES `compras` (`idcompra`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `idlaboratorio_fk` FOREIGN KEY (`idlaboratorio`) REFERENCES `laboratorios` (`idlaboratorio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idlinea_fk` FOREIGN KEY (`idlinea`) REFERENCES `linea` (`idlinea`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idumedida_fk` FOREIGN KEY (`idumedida`) REFERENCES `unidadmedidad` (`idumedida`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `id_empleado_fk` FOREIGN KEY (`idempleado`) REFERENCES `empleado` (`idempleado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `idrol_sistema_fk` FOREIGN KEY (`idrol_sistema`) REFERENCES `rol_sistema` (`idrol_sistema`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `rol_sistema` ADD `is_editable` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `nom_rol`;
COMMIT;
