<?php

error_reporting(E_ALL);
ini_set('display_errors',1);
if(isset($_POST['id_categoria'], $_POST['tipo_reporte']) && !empty($_POST['tipo_reporte'])){
    $a_parameters=array(0=>'');
    if(!empty($_POST['id_categoria'])){
        $a_parameters[0].='i';
        $a_parameters[]=&$_POST['id_categoria'];
        $addfilter="WHERE p.id_categoria_producto=?";
    }
    switch($_POST['tipo_reporte']){
        case 'precios':
            
            $sql=sprintf("SELECT p.id_producto, p.no_producto, p.id_categoria_producto, c.no_categoria_producto, d.id_destino, d.no_destino, p.nu_precio 
            FROM tb_producto AS p
            INNER JOIN tb_categoria_producto AS c ON p.id_categoria_producto = c.id_categoria_producto
            INNER JOIN tb_destino AS d ON p.id_destino = d.id_destino %s
            ORDER BY  c.no_categoria_producto, p.no_producto", (isset($addfilter))?$addfilter:'');
            $titulo="Lista de Precios";
            $is_laboratorio=true;
            $a_header=array( 
                0=>array( 'title'=>'CODIGO', 'border'=>'TBL', 'width'=>20, 'endl'=>0, 'text-align'=>'C' ),
                1=>array( 'title'=>'DESCRIPCION', 'border'=>'TB', 'width'=>90, 'endl'=>0, 'text-align'=>'C' ),
                2=>array( 'title'=>'CATEGORIA.', 'border'=>'TB', 'width'=>15, 'endl'=>0, 'text-align'=>'C' ),
                3=>array( 'title'=>'DESTINO', 'border'=>'TB', 'width'=>13, 'endl'=>0, 'text-align'=>'C' ),
                4=>array( 'title'=>'PRECIO', 'border'=>'TBR', 'width'=>0, 'endl'=>1, 'text-align'=>'C'),
            );

            $a_body=array( 
                0=>array( 'text-align'=>'C', 'name_field'=>'id_producto', 'border'=>'' ),
                1=>array( 'text-align'=>'L', 'name_field'=>'no_producto', 'border'=>'' ),
                2=>array( 'text-align'=>'C', 'name_field'=>'no_categoria_producto', 'border'=>'' ),
                3=>array( 'text-align'=>'R', 'name_field'=>'no_destino', 'border'=>'' ),
                4=>array( 'text-align'=>'R', 'name_field'=>'nu_precio', 'border'=>'' ),
            );
        break;
       
        case 'precios_existencia':
            $sql=sprintf("SELECT p.idproductos, p.cod_producto, p.nom_producto, p.id_categoria, l.desc_laboratorio, um.abrev_umedida, um.desc_umedida, IFNULL(i.stock,'-') AS stock, p.mto_compra, p.mto_sugerido FROM productos AS p LEFT OUTER JOIN unidadmedidad AS um ON um.idumedida=p.idumedida LEFT OUTER JOIN inventario AS i ON i.idproductos=p.idproductos LEFT OUTER JOIN laboratorios AS l ON l.id_categoria=p.id_categoria %s ORDER BY  l.desc_laboratorio, p.nom_producto", (isset($addfilter))?$addfilter:'');
            $titulo="Lista de Precios Laboratorio vs Existencia";
            $islote=true;
            $is_laboratorio=true;
            $a_header=array( 
                0=>array( 'title'=>'CODIGO', 'border'=>'TBL', 'width'=>20, 'endl'=>0, 'text-align'=>'C' ),
                1=>array( 'title'=>'DESCRIPCION', 'border'=>'TB', 'width'=>70, 'endl'=>0, 'text-align'=>'C' ),
                2=>array( 'title'=>'Lote/Vence', 'border'=>'TB', 'width'=>25, 'endl'=>0, 'text-align'=>'C' ),
                3=>array( 'title'=>'UND.', 'border'=>'TB', 'width'=>15, 'endl'=>0, 'text-align'=>'C' ),
                4=>array( 'title'=>'STOCK', 'border'=>'TB', 'width'=>20, 'endl'=>0, 'text-align'=>'C' ),
                5=>array( 'title'=>'PRECIO VENTA', 'border'=>'TBR', 'width'=>0, 'endl'=>1, 'text-align'=>'C'),
            );
            $a_body=array( 
                0=>array( 'text-align'=>'C', 'name_field'=>'cod_producto', 'border'=>'' ),
                1=>array( 'text-align'=>'L', 'name_field'=>'nom_producto', 'border'=>'' ),
                2=>array( 'text-align'=>'C', 'name_field'=>'', 'border'=>'' ),
                3=>array( 'text-align'=>'C', 'name_field'=>'abrev_umedida', 'border'=>'' ),
                4=>array( 'text-align'=>'C', 'name_field'=>'stock', 'border'=>'' ),
                5=>array( 'text-align'=>'C', 'name_field'=>'mto_sugerido', 'border'=>'' ),
            );
            $a_lote=array(
                0=>array( 'width'=>90, 'text-align'=>'C', 'border'=>'',  'endl'=>0 ),
                1=>array( 'width'=>25, 'text-align'=>'C', 'border'=>'', 'name_field'=>'lote', 'endl'=>0 ),
                2=>array( 'width'=>15, 'text-align'=>'R', 'border'=>'', 'constante'=>'fecha_vencimiento', 'endl'=>0 ),
                3=>array( 'width'=>0, 'text-align'=>'R', 'border'=>'', 'endl'=>1 )
            );
        break;
        case 'precio_costo':
            $sql=sprintf("SELECT p.idproductos, p.cod_producto, p.nom_producto, p.id_categoria, l.desc_laboratorio, um.abrev_umedida, um.desc_umedida, IFNULL(i.stock,'-') AS stock, p.mto_compra, p.mto_sugerido, p.porc_ganancia, p.metod_venta FROM productos AS p LEFT OUTER JOIN unidadmedidad AS um ON um.idumedida=p.idumedida LEFT OUTER JOIN inventario AS i ON i.idproductos=p.idproductos LEFT OUTER JOIN laboratorios AS l ON l.id_categoria=p.id_categoria %s ORDER BY  l.desc_laboratorio, p.nom_producto", (isset($addfilter))?$addfilter:'');
            $titulo="Lista de Precio Costo";
            $is_laboratorio=true;
            $a_header=array( 
                0=>array( 'title'=>'CODIGO', 'border'=>'TBL', 'width'=>20, 'endl'=>0, 'text-align'=>'C' ),
                1=>array( 'title'=>'DESCRIPCION', 'border'=>'TB', 'width'=>70, 'endl'=>0, 'text-align'=>'C' ),
                2=>array( 'title'=>'UND.', 'border'=>'TB', 'width'=>10, 'endl'=>0, 'text-align'=>'C' ),
                3=>array( 'title'=>'STOCK', 'border'=>'TB', 'width'=>15, 'endl'=>0, 'text-align'=>'C' ),
                4=>array( 'title'=>'PRECIO VENTA', 'border'=>'TB', 'width'=>20, 'endl'=>0, 'text-align'=>'R'),
                5=>array( 'title'=>'Utilid.%', 'border'=>'TB', 'width'=>20, 'endl'=>0, 'text-align'=>'C' ),
                6=>array( 'title'=>'Ganancia', 'border'=>'TBR', 'width'=>20, 'endl'=>1, 'text-align'=>'C' ),
            );
            $a_body=array( 
                0=>array( 'text-align'=>'C', 'name_field'=>'cod_producto', 'border'=>'' ),
                1=>array( 'text-align'=>'L', 'name_field'=>'nom_producto', 'border'=>'' ),
                2=>array( 'text-align'=>'C', 'name_field'=>'abrev_umedida', 'border'=>'' ),
                3=>array( 'text-align'=>'R', 'name_field'=>'stock', 'border'=>'' ),
                4=>array( 'text-align'=>'R', 'name_field'=>'mto_sugerido', 'border'=>'' ),
                5=>array( 'text-align'=>'R', 'constante'=>'utilidad', 'border'=>'' ),
                6=>array( 'text-align'=>'R', 'constante'=>'ganancia', 'border'=>'' )
            );
        break;
        case 'existencia_lab':
            $sql=sprintf("SELECT p.idproductos, p.cod_producto, p.nom_producto, p.id_categoria, l.desc_laboratorio, um.abrev_umedida, um.desc_umedida, IFNULL(i.stock,'-') AS stock, p.mto_compra, p.mto_sugerido FROM productos AS p LEFT OUTER JOIN unidadmedidad AS um ON um.idumedida=p.idumedida LEFT OUTER JOIN inventario AS i ON i.idproductos=p.idproductos LEFT OUTER JOIN laboratorios AS l ON l.id_categoria=p.id_categoria %s ORDER BY l.desc_laboratorio, p.nom_producto", (isset($addfilter))?$addfilter:'');
            $titulo="Existencia Laboratorio";
            $islote=true;
            $is_laboratorio=true;
            $a_header=array( 
                0=>array( 'title'=>'CODIGO', 'border'=>'TBL', 'width'=>20, 'endl'=>0, 'text-align'=>'C' ),
                1=>array( 'title'=>'DESCRIPCION', 'border'=>'TB', 'width'=>90, 'endl'=>0, 'text-align'=>'C' ),
                2=>array( 'title'=>'UND.', 'border'=>'TB', 'width'=>10, 'endl'=>0, 'text-align'=>'C' ),
                3=>array( 'title'=>'STOCK', 'border'=>'TB', 'width'=>15, 'endl'=>0, 'text-align'=>'C' ),
                4=>array( 'title'=>'LOTE', 'border'=>'TB', 'width'=>20, 'endl'=>0, 'text-align'=>'R'),
                5=>array( 'title'=>'VENCE', 'border'=>'TB', 'width'=>20, 'endl'=>1, 'text-align'=>'C' ),
            );
            $a_body=array( 
                0=>array( 'text-align'=>'C', 'name_field'=>'cod_producto', 'border'=>'' ),
                1=>array( 'text-align'=>'L', 'name_field'=>'nom_producto', 'border'=>'' ),
                2=>array( 'text-align'=>'C', 'name_field'=>'abrev_umedida', 'border'=>'' ),
                3=>array( 'text-align'=>'R', 'name_field'=>'stock', 'border'=>'' ),
                4=>array( 'text-align'=>'R', 'name_field'=>'', 'border'=>'' ),
                5=>array( 'text-align'=>'R', 'name_field'=>'', 'border'=>'' ),
            );
            $a_lote=array(
                0=>array( 'width'=>135, 'text-align'=>'C', 'border'=>'', 'endl'=>0, 'name_field'=>'' ),
                1=>array( 'width'=>20, 'text-align'=>'R', 'border'=>'', 'endl'=>0, 'name_field'=>'lote' ),
                2=>array( 'width'=>20, 'text-align'=>'R', 'border'=>'', 'endl'=>1, 'constante'=>'fecha_vencimiento' )
            );
        break;
        case 'precio_linea':
        break;
        case 'productos':
            $sql=sprintf("SELECT p.idproductos, p.cod_producto, p.nom_producto, p.id_categoria, l.desc_laboratorio, um.abrev_umedida, um.desc_umedida, IFNULL(i.stock,'-') AS stock, p.mto_compra, p.mto_sugerido, p.porc_ganancia, p.metod_venta FROM productos AS p LEFT OUTER JOIN unidadmedidad AS um ON um.idumedida=p.idumedida LEFT OUTER JOIN inventario AS i ON i.idproductos=p.idproductos LEFT OUTER JOIN laboratorios AS l ON l.id_categoria=p.id_categoria %s ORDER BY p.nom_producto", (isset($addfilter))?$addfilter:'');
            $titulo="Lista de Productos";
            $is_laboratorio=false;
            $a_header=array( 
                0=>array( 'title'=>'CODIGO', 'border'=>'TBL', 'width'=>18, 'endl'=>0, 'text-align'=>'C' ),
                1=>array( 'title'=>'DESCRIPCION', 'border'=>'TB', 'width'=>55, 'endl'=>0, 'text-align'=>'C' ),
                2=>array( 'title'=>'LABORATORIO', 'border'=>'TB', 'width'=>50, 'endl'=>0, 'text-align'=>'C' ),
                3=>array( 'title'=>'UND.', 'border'=>'TB', 'width'=>8, 'endl'=>0, 'text-align'=>'C' ),
                4=>array( 'title'=>'STOCK', 'border'=>'TB', 'width'=>10, 'endl'=>0, 'text-align'=>'C' ),
                5=>array( 'title'=>'P. COSTO', 'border'=>'TB', 'width'=>20, 'endl'=>0, 'text-align'=>'R'),
                6=>array( 'title'=>'P. VENTA', 'border'=>'TB', 'width'=>0, 'endl'=>1, 'text-align'=>'C' ),
            );
            $a_body=array( 
                0=>array( 'text-align'=>'C', 'name_field'=>'cod_producto', 'border'=>'' ),
                1=>array( 'text-align'=>'L', 'name_field'=>'nom_producto', 'border'=>'' ),
                2=>array( 'text-align'=>'L', 'name_field'=>'desc_laboratorio', 'border'=>'' ),
                3=>array( 'text-align'=>'C', 'name_field'=>'abrev_umedida', 'border'=>'' ),
                4=>array( 'text-align'=>'R', 'name_field'=>'stock', 'border'=>'' ),
                5=>array( 'text-align'=>'R', 'name_field'=>'mto_compra', 'border'=>'' ),
                6=>array( 'text-align'=>'R', 'name_field'=>'mto_sugerido', 'border'=>'' )
            );
        break;
    }
    if(isset($sql)){

        require_once("../api/class/GConector.php");
        $OConex = new GConector();
        $init_stmt=$OConex->stmt_init();   
        if(!$init_stmt->prepare($sql))
            throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
    
        if(count($a_parameters)>1){
            if(!call_user_func_array(array($init_stmt, 'bind_param'), $a_parameters))
                throw new GException("Vaya! ocurrio un problema serio en el sistema.<br/>Consulte con el Soporte Tecnico", $conex->error, $conex->errno);
        }
        /*$lote_stmt=$OConex->stmt_init();
        $sql_stock="SELECT di.cantidad, di.fecha_vencimiento, di.lote FROM detalle_inventario AS di WHERE di.idproductos=? AND !ISNULL(di.fecha_vencimiento) AND di.status='1' ORDER BY di.fecha_vencimiento ASC";
        if(!$lote_stmt->prepare($sql_stock))
            throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
        
        if(!$init_stmt->bind_param('ss', $_POST['fec_ini'], $_POST['fec_hasta']))
            throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
        */
        $init_stmt->execute();
        $result_stmt=$init_stmt->get_result();
        
        if($result_stmt->num_rows>0){
            $data=array();
            while($row=$result_stmt->fetch_assoc()){
                $data[$row['idproductos']]= $row;
                if(isset($islote) && (is_numeric($row['stock']) && $row['stock']>0)){
                    if(!$lote_stmt->bind_param('i', $row['idproductos']))
                        throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
                    $lote_stmt->execute();
                    $result_lote=$lote_stmt->get_result();
                    if($result_lote->num_rows==0)
                        continue;
                    $data[$row['idproductos']]['lote']=array();
                    while($row_lote=$result_lote->fetch_assoc()){
                        array_push($data[$row['idproductos']]['lote'], $row_lote);
                    }
                }
            }
        }
        require_once("../api/class/GReports.php");
        $pdf = new GReports(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetMargins(15, 32, 20);
        $pdf->addHeader($a_header);
        $pdf->setTitulo($titulo);
        $pdf->setFooterData(array(0,64,0), array(0,64,128));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('dejavusans', '', 12, '', true);
        $pdf->AddPage();
        if(isset($data)){           
            foreach($data as $item => $rows){               
                if($is_laboratorio){
                    if(!isset($id_marca) || $id_marca!=$rows['id_categoria']){
                        $pdf->SetFont('', 'B', 7);
                        $pdf->Cell(8, 5, $rows['id_categoria'], '', 0, 'C');
                        $pdf->Cell(0, 5, $rows['desc_laboratorio'], '', 1, 'L');
                        $pdf->SetFont('', '');
                        $id_marca=$rows['id_categoria'];
                    }
                }
                $pdf->SetFont('', '', 7);
                foreach($a_body as $item_b => $row_body){
                    if(isset($row_body['name_field']))
                        $value=(isset($rows[$row_body['name_field']]))?$rows[$row_body['name_field']]:'';
                    if(isset($row_body['constante'])){
                        switch($row_body['constante']){
                            case 'utilidad':
                                if(isset($rows['metod_venta'])==1){
                                    if($rows['mto_compra']>0)
                                        $value=sprintf("%.2f", (float) (($rows['mto_sugerido']*100)/$rows['mto_compra']));
                                    else
                                        $value=sprintf("%.2f", (float) ($rows['mto_sugerido']*100));
                                }else
                                    $value=sprintf("%.2f", $rows['porc_ganancia']);
                            break;
                            case 'ganancia':
                                $value=sprintf("%.2f", $rows['mto_sugerido']-$rows['mto_compra']);
                            break;
                        }
                    }
                    $pdf->Cell($a_header[$item_b]['width'], 5, $value, $row_body['border'], $a_header[$item_b]['endl'], $row_body['text-align']);
                }
                if(isset($rows['lote'])){
                    foreach($rows['lote'] as $x => $lote){
                        foreach($a_lote as $x_row => $item_lote){
                            $value_lote="";
                            if(isset($item_lote['name_field']) && !empty($item_lote['name_field']))
                                $value_lote=(isset($rows[$item_lote['name_field']]))?$lote[$item_lote['name_field']]:'';
                            if(isset($item_lote['constante'])){
                                switch($item_lote['constante']){
                                    case 'fecha_vencimiento':
                                        $fecha_vencimiento= DateTime::createFromFormat('Y-m-d', $lote['fecha_vencimiento']);
                                        $value_lote=(!is_bool($fecha_vencimiento))?$fecha_vencimiento->format('m/Y'):'';
                                    break;
                                    default:
                                        $value_lote="";
                                }
                            }
                            $pdf->Cell($item_lote['width'], 5, $value_lote, '', $item_lote['endl'], $item_lote['text-align']);
                        }
                    }
                }
            }
        }
        
        $pdf->Output('reporte.pdf', 'I');
    }
}


?>