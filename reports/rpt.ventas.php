<?php

error_reporting(E_ALL);
ini_set('display_errors',1);
require_once("../api/class/GLibfunciones.php");

$conn=new GConector();
$init_stmt=$conn->stmt_init();
$sql_empresa="SELECT e.idtip_documento, td.desc_documento, e.num_fiscal, e.nombre_empresa, e.direccion, e.tlf_fiscal, e.email FROM empresa AS e INNER JOIN tip_documento AS td ON td.idtip_documento=e.idtip_documento";
if(!$init_stmt->prepare($sql_empresa))
	throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $conn->error, $conn->errno);
$init_stmt->execute();
$result_empresa=$init_stmt->get_result();
if($result_empresa->num_rows==1){
	$row_empresa=$result_empresa->fetch_assoc();
}
$sql_config="SELECT nombre_impuesto AS nombre, porc_impuesto AS mto_impuesto FROM configuracion LIMIT 0, 1";
if(!$init_stmt->prepare($sql_config))
	throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $conn->error, $conn->errno);
$init_stmt->execute();
$result_config=$init_stmt->get_result();
if($result_config->num_rows==1){
	$row_impuesto=$result_config->fetch_assoc();
}

$oProduct=new GProducto();
$sql=$oProduct->getVentas();//
if(!$init_stmt->prepare($sql))
	throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $conn->error, $conn->errno);
if(!$init_stmt->bind_param('ss', $_POST['fec_inicio'], $_POST['fec_hasta']))
	throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $conn->error, $conn->errno);
$init_stmt->execute();
$result=$init_stmt->get_result();
$a_ventas=array();
if($result->num_rows>0){
	if(isset($_POST['detail_venta'])){
		$stmt_details=$conn->stmt_init();
		$sql_details=$oProduct->detailsVenta();
		if(!$stmt_details->prepare($sql_details))
			throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);	
	}
	while($rows=$result->fetch_assoc()){
		if(!isset($stmt_details)){
			array_push($a_ventas, $rows);
			continue;
		}
		if(!$stmt_details->bind_param('i', $rows['idboleta']))
			throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
		$stmt_details->execute();
		$result_details=$init_stmt->get_result();
		if($result_details->num_rows>0){
			$rows['details']=array();
			while($row_details=$result_details->fetch_assoc()){
				array_push($rows['details'], $row_details);
			}
		}
		array_push($a_ventas, $rows);
	}
}

$twig = new GTemplate();
$header_html=$twig->render('header.html', array('empresa'=>$row_empresa, 'titulo'=>array('name'=>"Reporte de ventas diarias")));
$body_reports=$twig->render('template.ventasdiarias.html', array(
	'detalle_reports'=>array("especif_vendedor"=>FALSE, "details_ventas"=>TRUE, "groupvendedor"=>TRUE), 
	'ventas'=>$a_ventas,
	'impuesto'=>$row_impuesto
	)
);

$oReport= new GReports('P', 'mm', 'A4', true, 'UTF-8', false);
$oReport->SetMargins(5, 37, 5, TRUE);
$oReport->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$oReport->setHeaderHTML($header_html);
$oReport->SetAutoPageBreak(TRUE, 25);
$oReport->AddPage();
$oReport->WriteHTML($body_reports);
$oReport->Output();
/*
$mto_impuesto=(isset($row_config))?$row_config['nombre_impuesto']:'';
$oEmpresa=array_merge($row_empresa, $row_config);
*/
?>