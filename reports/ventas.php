<?php
error_reporting(E_ALL);
ini_set('display_errors',1);
$fec_inicio=$_POST['fec_inicio'];
$fec_hasta=$_POST['fec_hasta'];

if(isset($_POST['fec_inicio'], $_POST['fec_hasta']) && (!empty($_POST['fec_inicio']) && !empty($_POST['fec_hasta']))){
	$fec_inicio=DateTime::createFromFormat('Y-m-d', $_POST['fec_inicio'])->format('Y-m-d');
	$fec_hasta=DateTime::createFromFormat('Y-m-d', $_POST['fec_hasta'])->format('Y-m-d');
}else{
	$fec_inicio=DateTime::createFromFormat('Y-m-d', '01/01/2018')->format('Y-m-d');
	$fec_hasta=DateTime::createFromFormat('Y-m-d', '31/12/2018' )->format('Y-m-d');
}
require_once("../api/class/GConector.php");
$OConex = new GConector();
$init_stmt=$OConex->stmt_init();

$sql_config="SELECT nombre_impuesto, porc_impuesto FROM configuracion LIMIT 0,1";
if(!$init_stmt->prepare($sql_config))
	throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
$init_stmt->execute();
$result_config=$init_stmt->get_result();
if($result_config->num_rows==1){
	$row_config=$result_config->fetch_object();
}
$mto_impuesto=(isset($row_config))?$row_config->nombre_impuesto:'';
$sql="SELECT b.idticket, DATE_FORMAT(b.fecventa, '%Y-%m-%d') AS fecventa, b.idclientes, c.apellidos_nombres, b.total, b.subtotal, b.mto_impuesto, b.importe_impuesto FROM boletas AS b LEFT OUTER JOIN clientes AS c ON c.idclientes=b.idclientes WHERE b.fecventa BETWEEN ? AND ? ORDER BY b.idempleado, b.idticket";
if(!$init_stmt->prepare($sql))
	throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico<br/>", $OConex->error, $OConex->errno);
if(!$init_stmt->bind_param('ss', $fec_inicio, $fec_hasta))
	throw new GException("Vaya! ocurrio un problema en el sistema.<br/>Consulte con el Soporte Tecnico", $OConex->error, $OConex->errno);
$init_stmt->execute();

require_once("../api/class/GReports.php");
$pdf = new GReports(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetMargins(20, 20, 20);
$pdf->setTitulo('Reporte de Ventas Diarias');
$pdf->setFooterData(array(0,64,0), array(0,64,128));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
$pdf->SetHeaderMargin(7);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
//$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}
$pdf->setFontSubsetting(true);
$pdf->SetFont('dejavusans', '', 14, '', true);
$pdf->AddPage();
$pdf->Ln(8);
$pdf->SetFont('', 'B', 10);
$pdf->Cell(0,5,sprintf(" Desde : %s Hasta : %s ", $fec_inicio, $fec_hasta),0, 0,'C');
$pdf->Ln(11);
$pdf->SetFont('', '', 9);
$pdf->Cell(15, 5, '#Ticket', 'TB', 0, 'C');
$pdf->Cell(20,5,'Fecha', 'TB', 0,'C');
$pdf->Cell(68,5,'Cliente', 'TB', 0,'L');
$pdf->Cell(22,5,'Sub-Total', 'TB', 0,'C');
$pdf->Cell(25,5,'Importe '.$mto_impuesto , 'TB', 0,'C');
$pdf->Cell(0,5,'Total', 'TB', 1,'C');
$pdf->Ln(1);
$result=$init_stmt->get_result();
$sum_total=$sum_impuesto=$sum_subtotal=0.00;
if($result->num_rows>0){
	$pdf->SetFont('', '', 8);
	while($rows=$result->fetch_object()){
		$rows->fecventa=DateTime::createFromFormat('Y-m-d', $rows->fecventa)->format('d-m-Y');
		$pdf->Cell(20, 5, $rows->idticket, 'B', 0, 'C');
		$pdf->Cell(18, 5, $rows->fecventa, 'B', 0,'C');
		$pdf->Cell(68, 5, $rows->apellidos_nombres, 'B', 0,'L');
		$pdf->Cell(22, 5, $rows->subtotal, 'B', 0,'R');
		$pdf->Cell(25, 5, $rows->importe_impuesto, 'B', 0,'R');
		$pdf->Cell(0, 5, $rows->total, 'B', 1,'R');
		$sum_total+=sprintf("%.2f", $rows->total);
		$sum_impuesto+=sprintf("%.2f", $rows->importe_impuesto);
		$sum_subtotal+=sprintf("%.2f", $rows->subtotal);
	}
}
$pdf->Ln(1);
$pdf->Cell(103, 5, '', 'T', 0, 'C');
$pdf->Cell(22, 5, sprintf("%.2f", $sum_subtotal), 'T', 0,'R');
$pdf->Cell(25, 5, sprintf("%.2f",$sum_impuesto), 'T', 0,'R');
$pdf->Cell(0, 5, sprintf("%.2f",$sum_total), 'T', 1,'R');

$pdf->Output('example_001.pdf', 'I');

?>